Thank you for buying the RIM Highlighting System Asset. 
With this Asset you can easily highlight GameObjects with a RIM Glow effect. 
This Asset does not require Unity Pro.

How To:

Simply drag and drop the "kw_HighlightableObject" Script to the GameObject you want to highlight. 
You can use the Settings at the Ispector Menu to easily adjust the Highlighting Glow effect.

If you want to Highlight a Object when the mousecursor hovers it you have to drag and drop the "kw_HighlightWithMouseHover" 
Script to the Object. The Object needs to have a collider to work with this script.

You are allowed to use this asset in non-commercial and commercial games and programs, 
but you are not allowed to include this asset into an asset you want to publish to the UnityAssetStore or other asset resources sites.

Contact data:

Thomas Feichtinger
Segerwiesen 12
5203 Koestendorf Austria

contact@kawetofe.com
www.kawetofe.com