﻿Shader "Custom/RandomTilingMask2" {
    Properties {
        _Tex1 ("Texture", 2D) = "white" {}
        [Toggle] _UseRandMask ("Use Random Mask", Int) = 0
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
     
        CGPROGRAM
        #pragma surface surf Lambert
 
        sampler2D _Tex1;
	    float _UseRandMask;
					   
        struct Input {
            float2 uv_Tex1;
            float3 worldPos;
        };
 
        // generic pseudo-random function
        float rand2 ( float2 coords ){
            return frac(sin(dot(coords, float2(12.9898,78.233))) * 43758.5453);
        }
 
        void surf (Input IN, inout SurfaceOutput o) {
        
        	// declare the rotation matrixes
			const float2x2 rotMx[4] = {
				float2x2(1, 0, 0, 1),
				float2x2(0, 1, -1, 0),
				float2x2(-1, 0, 0, -1),
				float2x2(0, -1, 1, 0)
			};       

  			const half4 maskRGBA[4] = {
				half4(1, 0, 0, 0),
				half4(0, 1, 0, 0),
				half4(0, 0, 1, 0),
				half4(0, 0, 0, 1)
			};
			
        	// calculate rotation matrix parameters from the original UV data
			int x = (round(rand2(floor(IN.uv_Tex1))*3)) * _UseRandMask; // switch random mask on/off
	
			// rotate texture UVs based on the rotation matrix
			float2 uvTex1 = mul(IN.uv_Tex1 - 0.5, rotMx[x]);
			uvTex1.xy += 0.5;
			
			// use input texture with calculated UVs
			half4 tex1 = tex2D (_Tex1, uvTex1);

			o.Albedo = tex1.rgb;
            o.Alpha = tex1.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}