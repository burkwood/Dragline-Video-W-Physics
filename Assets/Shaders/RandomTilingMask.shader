﻿Shader "Custom/RandomTilingMask" {
    Properties {
        _Tex1 ("Texture", 2D) = "white" {}
        [Toggle] _UseRandMask ("Use Random Mask", Int) = 0
    }
    SubShader {
        Tags { "RenderType"="Opaque" }
        LOD 200
     
        CGPROGRAM
        #pragma surface surf Lambert
 
        sampler2D _Tex1;
	    float _UseRandMask;
					   
        struct Input {
            float2 uv_Tex1;
            float3 worldPos;
        };
 
        // generic pseudo-random function
        float rand2 ( float2 coords ){
            return frac(sin(dot(coords, float2(12.9898,78.233))) * 43758.5453);
        }
 
        void surf (Input IN, inout SurfaceOutput o) {
        
        	// declare the rotation matrixes
			const float2x2 rotMx[4] = {
				float2x2(1, 0, 0, 1),
				float2x2(0, 1, -1, 0),
				float2x2(-1, 0, 0, -1),
				float2x2(0, -1, 1, 0)
			};       

  			const half4 maskRGBA[4] = {
				half4(1, 0, 0, 0),
				half4(0, 1, 0, 0),
				half4(0, 0, 1, 0),
				half4(0, 0, 0, 1)
			};
			
        	// calculate rotation matrix parameters from the original UV data
			int x = (round(rand2(floor(IN.uv_Tex1))*3)) * _UseRandMask; // switch random mask on/off
	
			half4 mask = maskRGBA[x];
			
			// rotate texture UVs based on the rotation matrix 
			float2 uvTex1 = mul(IN.uv_Tex1 - 0.5, rotMx[0]);
			uvTex1.xy += 0.5;
			
			float2 uvTex2 = mul(IN.uv_Tex1 - 0.5, rotMx[1]);
			uvTex2.xy += 0.5;
			
			float2 uvTex3 = mul(IN.uv_Tex1 - 0.5, rotMx[2]);
			uvTex3.xy += 0.5;
			
			float2 uvTex4 = mul(IN.uv_Tex1 - 0.5, rotMx[3]);
			uvTex4.xy += 0.5;
			
			// use input texture with calculated UVs
			half4 tex1 = tex2D (_Tex1, uvTex1);
			half4 tex2 = tex2D (_Tex1, uvTex2);
			half4 tex3 = tex2D (_Tex1, uvTex3);
			half4 tex4 = tex2D (_Tex1, uvTex4);
			
			o.Albedo = float3(0,0,0);
			o.Albedo = lerp(o.Albedo, tex1.rgb, mask.r);
			o.Albedo = lerp(o.Albedo, tex2.rgb, mask.g);
			o.Albedo = lerp(o.Albedo, tex3.rgb, mask.b);
			o.Albedo = lerp(o.Albedo, tex4.rgb, mask.a);
			
            o.Alpha = tex1.a;
        }
        ENDCG
    }
    FallBack "Diffuse"
}