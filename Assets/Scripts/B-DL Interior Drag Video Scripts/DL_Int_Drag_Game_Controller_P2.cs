﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class DL_Int_Drag_Game_Controller_P2 : MonoBehaviour
{
    Interior_DL_SFX interiorDLSFX;
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    public float runTime;
    public int queue = 0;                                                                       //Index of SFX queues
    public AudioClip[] audioClip;                                                               //List of Audio Clips
    private AudioSource playAudio;
    public float startTime;
    public float delayTime = 2f;
    public float playTime;
    public Text captionText;
    private bool audioTriggered = false;
    private string[] captions = new string[]
    {
        "When the pay out command is issued, several things occur.",                                         //Cap [0]
        "As the bucket is paid out due to gravity, the generators remain idle.",                             //Cap [1]
        "The drag brakes are released allowing the drag drum to turn freely.",                               //Cap [2]
        "The drag drum turn the gears in the gearbox which turn the drag motor gears as shown.",             //Cap [3]
        "The drag motors generate power that is stored for later use.",                                      //Cap [4]
    };
    // Use this for initialization
    void Start ()
    {
        interiorDLSFX = GetComponent<Interior_DL_SFX>();                                        //Get reference to the special effects script
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                          //Get reference to the fade to black screen script
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();                //Get reference to the virtual joystick script
        playAudio = GetComponent<AudioSource>();                                              //Get reference to the audio player
        playAudio.clip = audioClip[0];
        captionText.text = "";                                                                   //Clear the caption text
    }
	
	// Update is called once per frame
	void Update ()
    {
        runTime = Time.time;
        UpdatePlayTime();
        //******************************************************************************************************************************
        //Scene Setup
        //Extend Rope, disable drag gen status, engage fade screen, display fade screen text
        interiorDLSFX.dragDispOn = true;                                                       //Turn on Drag Side Gen Status Display
        interiorDLSFX.hoistDispOn = false;                                                       //Turn off Hoist Side Gen Status Display
        if (queue == 0)
        {
            //Extend Hoist rope, fade screen and show fade text
            if (Time.time < 6)
            {
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.showFadeText = true;
            }
            //Unfade screen and hide fade text
            if (Time.time > 9) //Lights up and set to first queue
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
            }
        }
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 10)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //"When the pay out command is issued, several things occur." Cap [0]
        //Highlight left Joystick and move to Pay Out Position
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            //Show Captions
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[0];
            }
            //Highlight virtual joystick
            if (playAudio.isPlaying == true && playTime > 0.5 + delayTime)
            {
                virtualJoystickController.leftJoyHighlight = Status.Flash;
                virtualJoystickController.leftJoyVert = vJoyMove.Up;
            }
            //Clear captions after 2 secons and stop highlighting joystick
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                //virtualJoystickController.rightJoyVert = vJoyMove.None;
            }
        }
        //******************************************************************************************************************************
        //Start the second Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 1 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[1];
            playAudio.Play();
        }
        //Caption & SFX Manager during the second Queue
        //"As the bucket is paid out due to gravity, the generators remain idle."  Cap [1]
        //Highlight generators and show gen engaged text
        if (queue == 2)
        {
            //If audio is playing
            if (playAudio.isPlaying == true)
            {
                //Show the correct Caption
                captionText.text = captions[1];
                //Highlight generators
                if (playTime > 2.6)
                {
                    interiorDLSFX.genHighlightStatus = Status.Flash;
                }

            }
            //If audio clip ends, wait 2 secs then clear captions and stop highlighting
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                interiorDLSFX.genHighlightStatus = Status.Off;
                captionText.text = "";
            }

        }
        //******************************************************************************************************************************
        //Start the third Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 2 && Time.time > startTime + delayTime * 2 + playAudio.clip.length && captionText.text == "")
        {
            queue = 3;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[2];
            playAudio.Play();
        }
        //Caption and SFX Manager during the thirdh Queue
        //"The drag brakes are released allowing the drag drum to turn freely." Cap [2]
        //Extend hoist cable for 5s
        if (queue == 3)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[2];
                //Reel in cable
                if (playTime > 1)
                {
                    interiorDLSFX.dragMCtrl = MotorControl.Extend;
                    interiorDLSFX.dragDrumStatus = Status.Flash;
                }
            }
            //Stop hoist drum and clear captions
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                interiorDLSFX.dragMCtrl = MotorControl.None;
                interiorDLSFX.dragDrumStatus = Status.Off;
                captionText.text = "";
            }
        }
        //******************************************************************************************************************************
        //Start the forth Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 3 && Time.time > startTime + delayTime * 2 + playAudio.clip.length && captionText.text == "")
        {
            queue = 4;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[3];
            playAudio.Play();
        }
        //Caption and SFX Manager during the forthd Queue
        //"The drag drum turns the gears in the gearbox which turns the drag motor gears as shown here." Cap [3]
        //Show the hoist gear movie P2
        if (queue == 4)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[3];
                if (playTime > 0.3)
                {
                    interiorDLSFX.dragDrumStatus = Status.Flash;
                }
                if (playTime > 2)
                {
                    interiorDLSFX.dragGearboxStatus = Status.Flash;
                }
                if (playTime > 3.8)
                {
                    interiorDLSFX.dragMotorStatus = Status.Flash;
                }
            }
            if (playAudio.isPlaying == false)
            {
                //After audio clip but less then audio length + screen open time + video clip length
                if (Time.time > startTime + playAudio.clip.length && Time.time < startTime + playAudio.clip.length + interiorDLSFX.screenScaleTime + interiorDLSFX.dragClips[1].length)
                {
                    interiorDLSFX.allScreens.dragScreen.open = true;
                    interiorDLSFX.allScreens.dragScreen.secondClip = true;
                    interiorDLSFX.allScreens.dragScreen.play = true;
                    interiorDLSFX.dragDrumStatus = Status.Solid;
                    interiorDLSFX.dragGearboxStatus = Status.Solid;
                    interiorDLSFX.dragMotorStatus = Status.Solid;
                }
                //After movie finished close screen
                if (Time.time > startTime + playAudio.clip.length + interiorDLSFX.screenScaleTime + interiorDLSFX.dragClips[1].length - 0.5)
                {
                    interiorDLSFX.allScreens.dragScreen.play = false;
                    interiorDLSFX.allScreens.dragScreen.open = false;
                    interiorDLSFX.dragDrumStatus = Status.Off;
                    interiorDLSFX.dragGearboxStatus = Status.Off;
                    interiorDLSFX.dragMotorStatus = Status.Off;
                }
                //clear captions
                if (Time.time > startTime + playAudio.clip.length + interiorDLSFX.screenScaleTime * 2 + interiorDLSFX.dragClips[1].length - 0.5 + delayTime)
                {
                    captionText.text = "";

                }
            }
        }
        //******************************************************************************************************************************
        //Start the fifth Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 4 && Time.time > startTime + delayTime * 2 + playAudio.clip.length + interiorDLSFX.screenScaleTime * 2 + interiorDLSFX.dragClips[1].length - 0.5 && captionText.text == "")
        {
            queue = 5;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[4];
            playAudio.Play();
        }
        //Ca[tion and SFX Manager during the fifth Queue
        //"The drag motors generate power that is stored for later use." Cap [4]
        //Reset joystick at end
        if (queue == 5)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[4];
                //Reel in cable
                if (playTime > 0.3)
                {
                    interiorDLSFX.dragMotorStatus = Status.Flash;
                }
            }
            //Stop hoist drum and clear captions
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                interiorDLSFX.dragMotorStatus = Status.Off;
                virtualJoystickController.leftJoyVert = vJoyMove.None;
                virtualJoystickController.leftJoyHighlight = Status.Off;

                captionText.text = "";
            }
        }
        //******************************************************************************************************************************
        //Start the sixth Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 5 && Time.time > startTime + playAudio.clip.length + delayTime * 2 && captionText.text == "")
        {
            queue = 6;
            startTime = Time.time;
        }
        //Caption and SFX manager during the sixth queue
        if (queue == 6)
        {
            fadeScreenController.showFadeScreen = true;
        }
    }
    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
