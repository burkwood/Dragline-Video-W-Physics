﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class M_Boom_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClips;                                                                                                   //Audio Player
    Marion_DL_SFX DL_SFX;                                                                                                          //The script that controls all hiding and highlighting
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;

    private float[] delayTimes = new float[] { 3, 5, 5, 5, 3 };                                                                     //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {
        "The dragline boom is the most critical structural element of the machine.",											//Cap [0]

		"The boom provides both flexibility and support during the dig cycle.",													//Cap [1]

		"The boom is comprised of: main chords.",																				//Cap [2]

		"These are the largest members of the boom.",																			//Cap [3]

		"These members take the most load while digging.",																		//Cap [4]

		"The main chords are also supported by the lacings.",																	//Cap [5]

		"These are smaller diameter members that provide stability for the main chords.",										//Cap [6]

		"The boom is connected to the machine at the boom feet with a pinned connection to revolving frame lugs.",				//Cap [7]

		"Blanked [8]",																												//Cap [8]

		"The Boom is held in position by upper suspension ropes.",																//Cap [9]

		"These are attached via pinned connections at the boom point and the mast head.",										//Cap [10]

		"Blanked [11]",																											//Cap [11]

		"These ropes hold the boom at the specified angle, providing both strength and flexibility.",							//Cap [12]

		"Blanked [13]",																											//Cap [13]

		"The intermediate suspension ropes are individually tensioned.",														//Cap [14]

		"They keep the boom straight while providing mid boom support.",														//Cap [15]

		"This prevents the weight of the boom itself from sagging.",															//Cap [16]

		"The boom point contains swivelling sheaves to allow the the hoist ropes to travel."									//Cap [17]
	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = GameObject.FindWithTag("GameController").GetComponent<Marion_DL_SFX>();                                       //Get reference to the SFX script
        playAudio = GetComponent<AudioSource>();                                                                                //Get Reference to the Audio Source
        playAudio.clip = audioClips[0];                                                                                          //Load the first Audio Clip
        captionText.text = "";                                                                                                  //Clear the Captions
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();                                                                                                       //Publish a running update of time elapsed on the playing audio clip

     //******************************************************************************************************************************
        //Start the first Queue 
        //Camera moves from Home to Whole Boom Left
        if (queue == 0 && nextStage == false && Time.time > delayTime)                                                              //START Queue 1
        {
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 1;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the Camera Movement was triggered
        }

        //Caption & SFX Manager during the first Queue
        //"The draglineboom is the most critical structural element of the machine." Cap [0]
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0])                                      //Start Playing Audio after camera has stopped moving 
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
                startTime = Time.time;                                                                                              //Store when the clip started playing
            }

            if (playAudio.isPlaying == true) { captionText.text = captions[0]; }                                                        //If audio clip is playing, show captions
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) { captionText.text = ""; }           //Don't clear caption till 2 Seconds after Audio ends
        }

     //******************************************************************************************************************************
        // Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 2
        {
            playAudio.clip = audioClips[1];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 2;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the second Queue
        //"The boom provides both flexibility and support during the dig cycle." Cap [1]
        if (queue == 2)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[1]; }                                                    //If audio clip is playing, show captions
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) { captionText.text = ""; }           //Don't clear caption till 2 Seconds after Audio ends 

        }

     //******************************************************************************************************************************
        // Start the Third Queue
        //No Camera Movement
        if (queue == 2 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[2];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 3;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
        }

        //Caption  & SFX Manager during the third stage
        //"The Boom is comprised of: Main Chords." Cap [2]
        //Highlight Boom Chords
        if (queue == 3)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[2]; }                                                    //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 1.9)                                                                      //Delay highlighting the chords
            {
                DL_SFX.highlighting.boomChords.status = Status.Flash;                                                               //Highlight Chords
            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) { captionText.text = ""; }           //Don't clear caption till 2 Seconds after Audio ends
        }

     //******************************************************************************************************************************
        // Start the Fourth Queue
        //No Camera Movement
        if (queue == 3 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[3];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 4;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
        }

        //Caption  & SFX Manager during the fourth stage
        //"These are the largest members of the Boom." Cap [3]
        //"These members take the most load while digging." Cap [4]
        //Boom Chords Stay Highlighted till end
        if (queue == 4)
        {
            if (playAudio.isPlaying == true && playTime < 4.5) { captionText.text = captions[3]; }
            if (playAudio.isPlaying == true && playTime > 4.5) { captionText.text = captions[4]; }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.boomChords.status = Status.Off;                                                                 //Unhighlight chords
            }
        }

     //******************************************************************************************************************************
        // Start the Fifth Queue
        //Camera moves from Whole Boom Left to Top Rotated
        if (queue == 4 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            nextStage = true;                                                                                                       //trigger next camera movement
            playAudio.clip = audioClips[4];                                                                                              //change clip
            queue = 5;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the Camera Movement was triggered
        }

        //Caption  & SFX Manager during the fifth stage
        //"The Main Chords are also supported by the lacings." Cap [5]
        //Highlight Lacings
        if (queue == 5 && nextStage == false && Time.time > startTime + delayTimes[1])                                              //Start playing audio after camera has stopped moving
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }
            if (playAudio.isPlaying == true) { captionText.text = captions[5]; }                                                    //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 2.3)
            {
                DL_SFX.highlighting.boomLacings.status = Status.Flash;                                                              //Highlight Boom Lacings

            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1]) { captionText.text = ""; }           //Don't clear caption till 2 Seconds after Audio ends
        }

     //******************************************************************************************************************************
        // Start the Sixth Queue
        //No Camera Movement
        if (queue == 5 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1] + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[5];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 6;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store time when clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the sixth stage
        //"These are smaller diameter members that provide stability for the main chords." Cap [6]
        //Lacings highlighted till end
        if (queue == 6)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[6]; }                                                    //If audio clip is playing, show captions
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends																				
                DL_SFX.highlighting.boomLacings.status = Status.Off;                                                                //Unhighlight Lacings
            }
        }

     //******************************************************************************************************************************
        // Start the Seventh Stage
        //Move from Top rotated to Boom Feet
        if (queue == 6 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            nextStage = true;                                                                                                       //trigger next camera movement
            playAudio.clip = audioClips[6];                                                                                              //change clip
            queue = 7;                                                                                                              //Update the Queue number
            startTime = Time.time;                                                                                                  //Store when the camera started moving
        }

        //Caption  & SFX Manager during the seventh stage
        //"The Boom is connected to the machine at the Boom Feet" Cap [7]
        //"with a pinned connection to revolving frame lugs." Cap [8]
        //Highlight Boom Feet
        if (queue == 7 && nextStage == false && Time.time > startTime + delayTimes[2])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play Loaded Clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }
            if (playAudio.isPlaying == true) { captionText.text = captions[7]; }                                                    //If audio is playing, show captions
            if (playAudio.isPlaying == true && playTime > 2.1) { DL_SFX.highlighting.boomFeet.status = Status.Flash; }              //Highlight Boom Feet
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[2])
            {
                DL_SFX.highlighting.boomFeet.status = Status.Off;                                                                   //Unhighlight Boom Feet
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
            }
        }

     //******************************************************************************************************************************
        // Start the Eighth Stage
        //Move from Boom Feet to Whole Boom Left
        if (queue == 7 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            nextStage = true;                                                                                                       //trigger next camera movement
            playAudio.clip = audioClips[7];                                                                                              //change clip
            queue = 8;                                                                                                              //Update Queue Number
            startTime = Time.time;                                                                                                  //Store when camera started moving
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the eighth stage
        //"The Boom is held in position by Upper Suspension Ropes." Cap [9]
        //"These are attached via pinned connections at the boom point" Cap [10]
        //"and the Mast Head." Cap [10]
        //Highlight Upper Suspension ropes, boom point & mast head
        if (queue == 8 && nextStage == false && Time.time > startTime + delayTimes[3])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play Audio Clip
                audioTriggered = true;                                                                                              //Flag Audio Clip has been triggered
            }
            if (playAudio.isPlaying == true && playTime > 1.9) { DL_SFX.highlighting.upSusRopes.status = Status.Flash; }                //Highlight Upper Suspension Ropes
            if (playAudio.isPlaying == true && playTime < 5.6) { captionText.text = captions[9]; }
            if (playAudio.isPlaying == true && playTime > 5.6)
            {
                captionText.text = captions[10];
                DL_SFX.highlighting.upSusRopes.status = Status.Off;                                                                 //Un-highlight Upper Suspension Ropes
            }
            if (playAudio.isPlaying == true && playTime > 8.4) { DL_SFX.highlighting.boomTip.status = Status.Flash; }                   //Highlight Boom Tip
            if (playAudio.isPlaying == true && playTime > 9.2) { DL_SFX.highlighting.mastHead.status = Status.Flash; }              //Highlight Mast Head
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[3]) { captionText.text = ""; } //Don't clear caption till 2 Seconds after Audio ends
        }

     //******************************************************************************************************************************
        // Start the Ninth Stage 
        //No Camera Movement
        if (queue == 8 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[8];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 9;                                                                                                              //Update Queue Number
            startTime = Time.time;                                                                                                  //Store when audio clip was started
        }

        //Caption  & SFX Manager during the ninth stage
        //"These ropes hold the Boom at the Specified angle," Cap [12]
        //"providing both strength and flexibility." Cap [12]
        //Upper SUspension Ropes, Boom Point & Mast Head Stay Highlighted till end
        if (queue == 9)
        {
            if (playAudio.isPlaying == true)
            {
                DL_SFX.highlighting.upSusRopes.status = Status.Flash;                                                               //Highlight Upper Suspension Ropes
                DL_SFX.highlighting.boomTip.status = Status.Off;                                                                    //Un-highlight Boom Tip
                DL_SFX.highlighting.mastHead.status = Status.Off;                                                                   //Un-highlight Mast Head
                captionText.text = captions[12];                                                                                    //If Audio is playing, show captions
            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.upSusRopes.status = Status.Off;                                                                 //Un-highlight Upper Suspension Ropes
            }
        }

     //******************************************************************************************************************************
        // Start the Tenth Stage
        //No Camera Movement
        if (queue == 9 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[9];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 10;                                                                                                             //Update Queue Number
            startTime = Time.time;                                                                                                  //Store when Audio Clip was Started
        }

        //Caption  & SFX Manager during the tenth stage
        //"The Intermediate Suspension Ropes are individually tensioned." Cap [14]
        //Highlight Intermediate Suspension Ropes
        if (queue == 10)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[14]; }                                                   //If Audio Clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 0.2) { DL_SFX.highlighting.intSusRopes.status = Status.Flash; }               //Highlight Intermediate Suspension Ropes
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) { captionText.text = ""; }           //Don't clear caption till 2 Seconds after Audio ends
        }

     //******************************************************************************************************************************
		// Start the Eleventh Stage
		//NoCamera Movement
		if (queue==10 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audioClips [10];																							//change clip
			playAudio.Play ();																										//play clip
			queue = 11;																												//Updated Queue Number
			startTime = Time.time;																									//Store when Audio Clip was Started
		}

		//Caption  & SFX Manager during the eleventh stage
		//"They keep the Boom straight while providing mid Boom support." Cap [15]
		//"This prevents the weight of the Boom itself from sagging." Cap [16]
		//Highlight Intermediate Suspension Ropes till the end
		if (queue == 11) 
		{

			if (playAudio.isPlaying == true && playTime < 5.6) {captionText.text = captions [15];}									//If audio is playing, display captions
			if (playAudio.isPlaying == true && playTime > 5.6) {captionText.text = captions [16];}
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 
			{
				captionText.text = "";																								//Don't clear caption till 2 Seconds after Audio ends
				DL_SFX.highlighting.intSusRopes.status = Status.Off;																//Un-highlight Intermediate Suspension Ropes
			}

		}

     //******************************************************************************************************************************
		// Start the Twelfth Stage
		//No Camera Movement
		if (queue==11 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audioClips [11];																							//change clip
			playAudio.Play ();																										//play clip
			queue = 12;																												//Update The Queue Number
			startTime = Time.time;																									//Store when Audio Clip was started
		}

		//Caption  & SFX Manager during the twelfth stage
		//"The Boom Point contains swivelling sheaves to allow the the Hoist Ropes to travel," Cap [17]
		//Highlight Boom point and delfection towers till the end
		if (queue == 12) 
		{
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[17];
                if (playTime > 0.2)
                {
                    DL_SFX.fadeManager.boomTip.enabled = true;
                    DL_SFX.highlighting.boomSheave.status = Status.Flash;
                }
            }
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 
			{
				captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.boomSheave.status = Status.Off;                                                                 //Un-highlight Boom Sheave
                DL_SFX.fadeManager.boomTip.enabled = false;                                                                         //Unfade BOom Tip
            }
		}

        //******************************************************************************************************************************
        // Start the Thirteenth Stage
        //Camera moves from Whole Boom Left to Home Position
        if (queue == 12 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            nextStage = true;                                                                                                       //trigger next camera movement
            queue = 13;                                                                                                             //Update the Queue Number
            startTime = Time.time;                                                                                                  //Store when camera movement triggered
            captionText.text = "";                                                                                                  //Clear captions
        }
    }

    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
