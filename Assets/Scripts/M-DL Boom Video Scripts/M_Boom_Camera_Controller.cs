﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M_Boom_Camera_Controller : MonoBehaviour
{
    public GameObject[] cameras;                                                                    //List of all virtual cameras
    public string[] childNames;                                                                     //List of Names of Virtual Cameras
    public int stage;                                                                               //Stage indicator
    public bool nextStage;                                                                          //Start next camera move flag
    public float startTime;                                                                         //Camera movement start time
    public float delayTime = 5;                                                                     //Minimum time between camera movements	



    private string[] cameraOrder = new string[] { "First Move", "Second Move", "Third Move", "Fourth Move", "Fifth Move" };
    private GameObject mainCamera;

    // Use this for initialization
    void Start ()
    {
        cameras = GameObject.FindGameObjectsWithTag("VirtualCamera");                               //find list of all virtual cameras
        childNames = new string[cameras.Length];                                                    //
        for (int i = 0; i < cameras.Length; i++)                                                    //disable all virtual cameras and record names
        {
            cameras[i].SetActive(false);
            childNames[i] = cameras[i].name;
        }

        mainCamera = GameObject.FindWithTag("MainCamera");                                          //Find Main Camera
        mainCamera.transform.position = new Vector3(71.8f, 23.6f, 36.1f);                           //Set Main Camera starting transform
        mainCamera.transform.rotation = Quaternion.Euler(0f, 270f, 0f);                         //Set Main Camera starting rotation
        mainCamera.GetComponent<Camera>().fieldOfView = 60f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        GameObject gameControllerNextStage = GameObject.FindWithTag("GameController");
        nextStage = gameControllerNextStage.GetComponent<M_Boom_Game_Controller>().nextStage;

        if (stage == 0 && nextStage == true)                                                        //Move from Home to Whole Boom Left
        {
            MoveCamera(0);
            gameControllerNextStage.GetComponent<M_Boom_Game_Controller>().nextStage = false;
            startTime = Time.time;
            stage = 1;
        }

        if (stage == 1 && nextStage == true && Time.time > startTime + delayTime)                   //Whole Boom Left to Top Rotated
        {
            MoveCamera(1);
            gameControllerNextStage.GetComponent<M_Boom_Game_Controller>().nextStage = false;
            startTime = Time.time;
            stage = 2;
        }

        if (stage == 2 && nextStage == true && Time.time > startTime + delayTime)                   //Top Rotated to Boom Feet
        {
            MoveCamera(2);
            gameControllerNextStage.GetComponent<M_Boom_Game_Controller>().nextStage = false;
            startTime = Time.time;
            stage = 3;
        }

        if (stage == 3 && nextStage == true && Time.time > startTime + delayTime)                   //Boom Feet to Whole Boom Left
        {
            MoveCamera(3);
            gameControllerNextStage.GetComponent<M_Boom_Game_Controller>().nextStage = false;
            startTime = Time.time;
            stage = 4;
        }

        if (stage == 4 && nextStage == true && Time.time > startTime + delayTime)                   //Whole Boom Left to Home Position
        {
            MoveCamera(4);
            gameControllerNextStage.GetComponent<M_Boom_Game_Controller>().nextStage = false;
            startTime = Time.time;
            stage = 5;
        }
    }

    //Trigger camera movement
    void MoveCamera(int num)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            if (childNames[i] == cameraOrder[num])
            {
                cameras[i].SetActive(true);
            }
        }
    }
}
