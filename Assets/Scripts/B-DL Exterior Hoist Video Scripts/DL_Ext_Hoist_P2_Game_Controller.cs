﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class DL_Ext_Hoist_P2_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClip;                                                                                               //Audio Player
    Obi_Bucyrus_DL_SFX DL_SFX;                                                                                                  //The script that controls all hiding and highlighting
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    Obi_Bucyrus_Controller obiBucyrusController;
    Operator_Chair_Controller operatorChairController;
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private float[] delayTimes = new float[] { 7, 2, 4, 6, 8 };                                                                    //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "As the hoist ropes unspool, the bucket is lowered."									//Cap [0]

	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Obi_Bucyrus_DL_SFX>();                                                            //Get reference to the SFX script
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                                                      //Get reference to Fade Screen Controller
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();
        obiBucyrusController = GetComponent<Obi_Bucyrus_Controller>();
        operatorChairController = GetComponent<Operator_Chair_Controller>();
        playAudio = GetComponent<AudioSource>();                                                                           //Get reference to the Audio Source
        playAudio.clip = audioClip[0];                                                                                         //Load the first clip
        captionText.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();
        //******************************************************************************************************************************
        //Scene Setup
        //Extend Rope, disable drag gen status, engage fade screen, display fade screen text
        if (queue == 0)
        {
            //Fade Screen, show fade text, extend bucket
            if (Time.time < 2)
            {
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.fadeMessage = "Dragline Exterior";
                fadeScreenController.showFadeText = true;
            }
            if (Time.time > 2 && Time.time < 4)
            {
                obiBucyrusController.hoistMotor = MotorControl.Retract;
            }
            if (Time.time > 8)
            {
                obiBucyrusController.hoistMotor = MotorControl.None;
            }
            //Unfade Screen and Hide text
            if (Time.time > 9)
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
            }
        }
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 10)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //"As the hoist ropes unspool, the bucket is lowered." Cap [0]
        //Move right joy to bucket down position and lower bucket
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            //Show captions, 
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[0];
            }
            //Highlight virtual joystick move joystick and raise bucket
            if (playAudio.isPlaying == true && playTime > 0.5 + delayTime)
            {
                virtualJoystickController.rightJoyHighlight = Status.Flash;
                virtualJoystickController.rightJoyVert = vJoyMove.Up;
                obiBucyrusController.hoistMotor = MotorControl.Extend;
            }
            //Clear Captions, reset joystick and stop raising bucket after 2 seconds
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime * 3 + playAudio.clip.length)
            {
                captionText.text = "";
                obiBucyrusController.hoistMotor = MotorControl.None;
                virtualJoystickController.rightJoyVert = vJoyMove.None;
                fadeScreenController.showFadeScreen = true;
            }
        }
    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
