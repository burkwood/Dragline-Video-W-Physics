﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class M_Mast_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClips;                                                                                                   //Audio Player
    Marion_DL_SFX DL_SFX;                                                                                                          //The script that controls all hiding and highlighting
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;

    private float[] delayTimes = new float[] { 3, 5, 5, 5, 5, 3 };                                                                      //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {
        "Like the boom, the mast is comprised of main structural members called chords.",										//Cap [0]

		"The chords are joined by lacings.",																					//Cap [1]

		"The mast is held in position by link plates.",																	//Cap [2]

		"These are attached via pinned connections at the mast head and the gantry.",											//Cap [3]

		"Also similar to the boom structure, the lower end of the mast is called the foot.",									//Cap [4]

		"The foot is secured to the revolving frame by foot pins inserted into revolving frame lugs.",							//Cap [5]
	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = GameObject.FindWithTag("GameController").GetComponent<Marion_DL_SFX>();                                   //Get reference to the SFX script
        playAudio = GetComponent<AudioSource>();                                                                                //Get Reference to the Audio Source
        playAudio.clip = audioClips[0];                                                                                          //Load the first Audio Clip
        captionText.text = "";                                                                                                  //Clear the Captions
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();

//******************************************************************************************************************************
        //Start the first Queue 
        //Camera moves from Home to Whole Mast Left
        if (queue == 0 && nextStage == false && Time.time > delayTime)                                                              //START Queue 1 
        {
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 1;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the Camera Movement was triggered
        }

        //Caption & SFX Manager during the first Queue
        //"Like the boom, the mast is comprised of main structural members called chords." Cap [0]
        //Highlight Mast Chords till the end
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0])                                          //Start Playing Audio after camera has stopped moving
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
                startTime = Time.time;                                                                                              //Store when the clip started playing
            }

            if (playAudio.isPlaying == true) { captionText.text = captions[0]; }                                                        //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 4.0f) { DL_SFX.highlighting.mastChords.status = Status.Flash; }               //Highlight Mast Chords
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1)
            {
                DL_SFX.highlighting.mastChords.status = Status.Off;                                                                 //Un-highlight Mast Chords 
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
            }
        }
//******************************************************************************************************************************	
        //Start the second Queue
        //Camera moves from Whole Mast Left to Mast Rear
        if (queue == 1 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[1];                                                                                              //Change Audio Clip
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 2;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            audioTriggered = false;
        }

        //Caption & SFX Manager during the Second Queue
        //"The chords are joined by lacings." Cap [1]
        //Highlight Mast Lacings till the end
        if (queue == 2 && nextStage == false && Time.time > startTime + delayTimes[1])
        {
            //Fade Gantry, Link Plate and Hoist Ropes!!!!!!!!
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[1];                                                                                     //If audio clip is playing, show captions
                DL_SFX.fadeManager.gantry.enabled = true;
                DL_SFX.fadeManager.linkPlate.enabled = true;
                DL_SFX.fadeManager.hoistRopes.enabled = true;

            } 
            if (playAudio.isPlaying == true && playTime > 1.4) { DL_SFX.highlighting.mastLacings.status = Status.Flash; }               //Highlight Mast Lacings
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends		
                DL_SFX.highlighting.mastLacings.status = Status.Off;                                                                //Un-highlight Mast Lacings
                DL_SFX.fadeManager.gantry.enabled = false;
                DL_SFX.fadeManager.linkPlate.enabled = false;
                DL_SFX.fadeManager.hoistRopes.enabled = false;
            }
        }
//******************************************************************************************************************************	
        //Start the Third Queue
        //Camera moves from Mast Rear to Whole Mast Left
        if (queue == 2 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[2];                                                                                              //Change Audio Clip
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 3;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            audioTriggered = false;
        }

        //Caption & SFX Manager during the Third Queue
        //"The mast is held in position by link plates." Cap [2]
        //Highlight suspension ropes till the end
        if (queue == 3 && nextStage == false && Time.time > startTime + delayTimes[2])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }
            if (playAudio.isPlaying == true) { captionText.text = captions[2]; }                                                        //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 1.9) { DL_SFX.highlighting.loSusRopes.status = Status.Flash; }                //Highlight Lower Suspension Ropes
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[2] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends		
                DL_SFX.highlighting.loSusRopes.status = Status.Off;                                                                 //Un-highlight Lower Suspension Ropes
            }

        }
//******************************************************************************************************************************	
        //Start the Forth Queue
        //No camera movement
        if (queue == 3 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[3];                                                                                              //Change Audio Clip
            queue = 4;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            audioTriggered = false;
            playAudio.Play();                                                                                                       //Play loaded clip
        }

        //Caption & SFX Manager during the Forth Queue
        //"These are attached via pinned connections at the mast head and the gantry." Cap [3]
        //Highlight mast head and A-frame
        if (queue == 4)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[3]; }                                                        //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 2.5) { DL_SFX.highlighting.mastHead.status = Status.Flash; }              //Highlight Mast Head
            if (playAudio.isPlaying == true && playTime > 3.4) { DL_SFX.highlighting.gantry.status = Status.Flash; }          //Highlight Mast Head
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends		
                DL_SFX.highlighting.mastHead.status = Status.Off;                                                                   //Un-highlight Mast Head
                DL_SFX.highlighting.gantry.status = Status.Off;                                                               //Un-highlight A-frame
            }
        }
//******************************************************************************************************************************	
        //Start the Fifth Queue
        //Camera moves from Whole Mast Left to Mast Foot
        if (queue == 4 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[4];                                                                                              //Change Audio Clip
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 5;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            audioTriggered = false;                                                                                                 //Ensure audio triggered flag is false											

        }

        //Caption & SFX Manager during the Fifth Queue
        //"Also similar to the boom structure, the lower end of the mast is called the foot." Cap [4]
        //Highlight Mast Foot
        if (queue == 5 && nextStage == false && Time.time > startTime + delayTimes[3])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }
            if (playAudio.isPlaying == true) { captionText.text = captions[4]; }                                                        //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 3.9) { DL_SFX.highlighting.mastFeet.status = Status.Flash; }              //Highlight Lower Suspension Ropes
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[3] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends		
            }

        }
//******************************************************************************************************************************	
        //Start the Sixth Queue
        //No Camera Movement
        if (queue == 5 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            playAudio.clip = audioClips[5];                                                                                              //Change Audio Clip
            queue = 6;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            audioTriggered = false;
            playAudio.Play();                                                                                                       //Play loaded clip
        }

        //Caption & SFX Manager during the Sixth Queue
        //"The foot is secured to the revolving frame by foot pins inserted into revolving frame lugs." Cap [5]
        //Highlight Mast Foot till end
        if (queue == 6)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[5]; }                                                        //If audio clip is playing, show captions
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends		
                DL_SFX.highlighting.mastFeet.status = Status.Off;                                                                   //Un-highlight Mast Foot
            }
        }
//******************************************************************************************************************************	
        //Start the Seventh Queue
        //Camera Moves from Mast Feet to Whole Mast Left
        if (queue == 6 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            queue = 7;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            nextStage = true;                                                                                                       //Trigger camera movement
        }

        //No Captions or SFX

//******************************************************************************************************************************	
        //Start the Eighth Queue
        //Camera Moves from Whole Mast Left to Home Position
        if (queue == 7 && nextStage == false && Time.time > startTime + delayTimes[4] && captionText.text == "")
        {
            queue = 8;                                                                                                              //Update Queue number
            startTime = Time.time;                                                                                                  //Store when Camera Movement was triggered
            nextStage = true;                                                                                                       //Trigger camera movement
        }

        //No Captions or SFX
    }

    //******************************************************************************************************************************
    //Function Section

    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
