﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class CaptionBKGNDFader : MonoBehaviour
{
    public GameObject fadeScreen;
    private float fadedAlpha = 0f;
    private float solidAlpha = 255f;
    private Color fadeScreenColor;
    public bool showFadeScreen = false;
    public float fadeToBlackTime = 1f;
    // Use this for initialization
    void Start () {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        fadeScreenColor = fadeScreen.GetComponent<Image>().color;
        if (showFadeScreen)
        {
            //Debug.Log("Fade Screen called");
            fadeScreenColor.a = Mathf.Lerp(fadeScreenColor.a, solidAlpha, fadeToBlackTime * Time.deltaTime);
        }
        else
        {
            fadeScreenColor.a = Mathf.Lerp(fadeScreenColor.a, fadedAlpha, fadeToBlackTime * Time.deltaTime);
        }
        fadeScreen.GetComponent<Image>().color = fadeScreenColor;

    }
}
