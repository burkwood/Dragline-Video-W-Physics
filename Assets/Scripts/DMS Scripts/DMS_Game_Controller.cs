﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;
using System;
using IntPublicClass;
using UnityEngine.UI;

public class DMS_Game_Controller : MonoBehaviour {

    Fade_Screen_Controller fadeScreenController;
    CaptionBKGNDFader fadeToBlackController;
    public VideoPlayer videoPlayer;     //Reference to the video player
    public Text captionText;
    public GameObject queueTextParent;
    public Text queueText;
    public GameObject captionBackground;
    public Image fadeImage;
    private Color captionBackgroundColor;
    private Color captionTextColor;
    private float fadedAlpha = 0f;
    private float solidAlpha = 255f;
    private float solidTextAlpha = 1.5f;
    private Color whiteScreen = new Color(255f, 255f, 255f);
    private Color blackScreen = new Color(0f, 0f, 0f);
    public float fadeTime = 3f;
    private AudioSource playAudio;
    public AudioClip[] audioClips;
    public VideoClip[] videoClips;
    public Sprite[] screenShots;
    //public int videoIndex;
    //public bool loadVideo;
    //public bool playClip;
    public bool showQueueText;
    public bool hideCaptionBacking;
    public int queue = 0;
    public float runTime;
    public float startTime;
    public float delayTime = 2f;
    public float screenDelayTime = 5f;
    public float playTime;
    public float videoPreLoadTime = 2f;
    private string[] captions = new string[]
    {
        //Clip 001
        "This will discuss different types of alarms through DMS & digging styles that cause alarms.",                                  //Cap [0]
        "Boom Bounce",                                                                                                                  //Cap [1]
        "When a boom bounce occurs,",                                                                                                   //Cap [2]
        "the instantaneous compressive stress in the boom exceeds the threshold.",                                                      //Cap [3]
        "In other words, when the bucket rapidly bounces up and down,",                                                                 //Cap [4]
        "this causes the boom to bend down.",                                                                                           //Cap [5]
        "This results in high stresses in the boom and suspension ropes.",                                                              //Cap [6]
        //Clip 002
        "When the boom bends down, the upper chords are in tension",                                                                    //Cap [7]
        "and the lower chords are in compression.",                                                                                     //Cap [8]
        "When this happens, the HMI shows the instantaneous compressive stress.",                                                       //Cap [9]
        "This is shown on the HMI on the left boom compression indicator.",                                                             //Cap [10]
        "If the stress is high enough, it will exceed the threshold.",                                                                  //Cap [11]
        "This results in a Boom Bounce alarm",                                                                                          //Cap [12]
        "High stresses in the boom and ropes accelerate wear.",                                                                         //Cap [13]
        "This fatigue can lead to premature cracking and failure.",                                                                     //Cap [14]
        "Boom Bounce alarms can be avoided by reducing sudden vertical movements,",                                                     //Cap [15]
        "especially while engaged in the cut or while carrying large payloads.",                                                        //Cap [16]
        //Clip 003
        "Side Load",                                                                                                                    //Cap [17]
        "When a side load occurs,",                                                                                                     //Cap [18]
        "the instantaneous bending stress in the boom exceeds the threshold.",                                                          //Cap [19]
        "In other words, when the bucket rapidly moves left or right,",                                                                 //Cap [20]
        "this causes the boom to quickly bend.",                                                                                        //Cap [21]
        "This results in high stresses in the boom and suspension ropes.",                                                              //Cap [22]
        //Clip 004
        "When the boom bends to the left, the left chords are in compression",                                                          //Cap [23]
        "and the right chords are in tension.",                                                                                         //Cap [24]
        "The inverse is true when the boom bends to the right.",                                                                        //Cap [25]
        "When this happens, the HMI shows the instantaneous bending stress.",                                                           //Cap [26]
        "This is shown on the HMI on the lower boom bending indicator.",                                                                //Cap [27]
        "If the stress is high enough, it will exceed the threshold.",                                                                  //Cap [28]
        "This results in a Side Load alarm",                                                                                            //Cap [29]
        "High stresses in the boom and ropes accelerate wear.",                                                                         //Cap [30]
        "This fatigue can lead to premature cracking and failure.",                                                                     //Cap [31]
        "Side Load alarms can be avoided by keeping the bucket centered beneath the boom,",                                             //Cap [32]
        "and avoiding sudden starts or stops while swinging,",                                                                          //Cap [33]
        "especially while engaged in the cut or carrying large payloads.",                                                              //Cap [34]
        //Clip 005
        "Fatigue Alarms",                                                                                                               //Cap [35]
        "Fatigue alarms occur from the total stresses accrued during a single cycle.",                                                  //Cap [36]
        "This is when the total bending or compressive stresses exceed the threshold.",                                                 //Cap [37]
        "Fatigue is defined as the accumulation of small loads over time.",                                                             //Cap [38]
        "For example, imagine that the boom is a paperclip.",                                                                           //Cap [39]
        "If you bend the paperclip,",                                                                                                   //Cap [40]
        "fatigue events are the small back and forth bending motions.",                                                                 //Cap [41]
        "These don't break the paperclip but weaken the material.",                                                                     //Cap [42]
        "Rough Dig",                                                                                                                    //Cap [43]
        "When a rough dig cycle occurs,",                                                                                               //Cap [44]
        "the cumulative compressive stress in the boom exceeds the threshold.",                                                         //Cap [45]
        "In other words, when the bucket bounces up and down,",                                                                         //Cap [46]
        "this causes the boom to bend up and down.",                                                                                    //Cap [47]
        "This results in low, but repeated stress on the boom and suspension ropes.",                                                   //Cap [48]
        //Clip 006
        "When the boom repeatedly bends back and forth,",                                                                               //Cap [49]
        "total cycle stress increases.",                                                                                                //Cap [50]
        "When this happens, the HMI shows the total compressive stress.",                                                               //Cap [51]
        "This is shown on the HMI on the right boom compression indicator.",                                                            //Cap [52]
        "If the stress total is high enough, it will exceed the threshold.",                                                            //Cap [53]
        "This results in a Rough Dig alarm.",                                                                                           //Cap [54]
        "Rough Dig alarms can be avoided by filling the bucket in a single pass,",                                                      //Cap [55]
        "avoiding unnecessary vertical bucket movements,",                                                                              //Cap [56]
        "and reducing cycle times.",                                                                                                    //Cap [57]
        //Clip 007
        "Long Sideload",                                                                                                                //Cap [58]
        "When a long sideload cycle occurs,",                                                                                           //Cap [59]
        "the cumulative bending stress in the boom exceeds the threshold.",                                                             //Cap [60]
        "In other words, when the bucket swings left and right,",                                                                       //Cap [61]
        "this causes the boom to bend.",                                                                                                //Cap [62]
        "This results in low, but repeated stress on the boom and suspension ropes.",                                                   //Cap [63]
        //Clip 008
        "When the boom repeatedly bends back and forth,",                                                                               //Cap [64]
        "total cycle stress increases.",                                                                                                //Cap [65]
        "When this happens, the HMI shows the total bending stress.",                                                                   //Cap [66]
        "This is shown on the HMI on the top boom bending indicator.",                                                                  //Cap [67]
        "If the stress total is high enough, it will exceed the threshold.",                                                            //Cap [68]
        "This results in a Long Sideload alarm.",                                                                                       //Cap [69]
        "Long Sideload alarms can be avoided by reducing unnecessary swinging bucket movements,",                                       //Cap [70]
        "keeping the bucket centered beneath the boom,",                                                                                //Cap [71]
        "and reducing cycle times.",                                                                                                    //Cap [72]
        //Clip 009
        "Production Codes",                                                                                                             //Cap [73]
        "Production codes are used to categorize the type of digging being performed.",                                                 //Cap [74]
        "The operator selects the appropriate production code from the HMI.",                                                           //Cap [75]
        //Clip 010
        "Production code time begins to count once the bucket crosses the bucket counting threshold.",                                 //Cap [76]
        "Production codes are held through delays, unless changed by the operator.",                                                    //Cap [77]
        "This means that following the completion of a delay,",                                                                         //Cap [78]
        "the production code used before the delay code was entered will be automatically used again.",                                 //Cap [79]
        "unless changed by the operator.",                                                                                              //Cap [80]
        //Clip 011
        "Non-Productive Codes",                                                                                                         //Cap [81]
        "Most digging operations follow the standard dig-swing-dump-return cycle.",                                                     //Cap [82]*********************
        "For these operations, bucket counting and full alarming is desired.",                                                          //Cap [83]
        "However, nonstandard operations can also occur, such as Stirring the Well.",                                                   //Cap [84]
        //Clip 012
        "These are called \"Non-productive codes\".",                                                                                   //Cap [85]
        "When an operator selects an NPC, several features are disabled, including:",                                                   //Cap [86]    
        "Payload indicator, compressive fatigue, and bending fatigue.",                                                                 //Cap [87]
        "This means that bucket counting is disabled.",                                                                                 //Cap [88]
        "Rough Dig and Long Sideload alarms are also disabled.",                                                                        //Cap [89]
        "Every 5 minutes, the screen will flash,",                                                                                      //Cap [90]
        "reminding the operator that they have entered an NPC.",                                                                        //Cap [91]
        //Clip 013
        "It is important to change the Production Code once NPC digging is complete.",                                                  //Cap [92]
        "Otherwise, bucket and payload counting will not function.",                                                                    //Cap [93]
        //Clip 014
        "Delay Codes",                                                                                                                  //Cap [95]
        "When not in operation, delay codes are used to categorize the downtime.",                                                      //Cap [96]
        "Delay codes are prompted to be entered any time excitation is turned off,",                                                    //Cap [97]
        "or active digging has stopped for more than 2 minutes.",                                                                       //Cap [98]
        //Clip 015
        "The HMI prompts the operator to enter the delay code each time this state is entered.",                                        //Cap [99]
        "If a code is not entered, the time is identified as unknown.",                                                                 //Cap [100]
    };
 

	// Use this for initialization
	void Start ()
    {
        captionText.text = "";
        playAudio = GetComponent<AudioSource>();
        fadeScreenController = gameObject.GetComponent<Fade_Screen_Controller>();
        fadeToBlackController = gameObject.GetComponent<CaptionBKGNDFader>();
        videoPlayer.clip = videoClips[0];               //Load Boom BOunce Video
        playAudio.clip = audioClips[0];                 //Load audio Clip 001
        fadeImage.color = whiteScreen;                  //Light the screen
        hideCaptionBacking = false;
        fadeToBlackController.showFadeScreen = true;
    }
	
	// Update is called once per frame
	void Update ()
    {
        runTime = Time.time;
        UpdatePlayTime();
        CaptionandBackingManager();
        QueueTextUpdate();
        #region Queue_One (Boom Bounce Audio)
        //******************************************************************************************************************************
        //Queue One (Audio)
        if (queue == 0)
        {
            queue = 1;
            startTime = Time.time;         
            fadeImage.sprite = screenShots[0];          //Load the first screen shot
            fadeScreenController.showFadeScreen = true; // show screenshot screen           
            playAudio.Play();
        }
        //Queue one SFX
        //This module will discuss the different types of alarms through D M S, and the digging styles that cause these alarms.",        //Cap [0]
        //Boom Bounce",                                                                                                                  //Cap [1]
        //When a boom bounce occurs,",                                                                                                   //Cap [2]
        //the instantaneous compressive stress in the boom exceeds the threshold.",                                                      //Cap [3]
        //In other words, when the bucket rapidly bounces up and down,",                                                                 //Cap [4]
        //this causes the boom to bend down.",                                                                                           //Cap [5]
        //This results in high stresses in the boom and suspension ropes.",                                                              //Cap [6]
        if (queue == 1)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 9.3)
                {
                    captionText.text = captions[0];
                }
                if (playTime > 9.3 && playTime < 14.2)
                {
                    captionText.text = captions[1];
                }
                if (playTime > 14.2 && playTime < 18.1)
                {
                    captionText.text = captions[2];
                }
                if (playTime > 18.1 && playTime < 24.9)
                {
                    captionText.text = captions[3];
                }
                if (playTime > 24.9 && playTime < 30.6)
                {
                    captionText.text = captions[4];
                }
                if (playTime > 30.6 && playTime < 34.9)
                {
                    captionText.text = captions[5];
                }
                if (playTime > 34.9)
                {
                    captionText.text = captions[6];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);
                //hideCaptionBacking = true;                              //Hide the caption background

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Two(Boom Bounce Video)
        //******************************************************************************************************************************
        //Queue two (Video)
        if (queue == 1 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 2;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue two SFX
        if (queue == 2)
        {

            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Three (Boom Bounce Explanation)
        //******************************************************************************************************************************
        //Queue three (Audio)
        if (queue == 2 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime )
        {
            queue = 3;
            playAudio.clip = audioClips[1];
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue three SFX
        //When the boom bends down,  the upper chords are in tension",                                                                   //Cap [7]
        //and the lower chords are in compression.",                                                                                     //Cap [8]
        //When this happens, the HMI shows the instantaneous compressive stress.",                                                       //Cap [9]
        //This is shown on the HMI on the left boom compression indicator.",                                                             //Cap [10]
        //If the stress is high enough, it will exceed the threshold.",                                                                  //Cap [11]
        //This results in a Boom Bounce alarm",                                                                                          //Cap [12]
        //High stresses in the boom and ropes accelerate wear.",                                                                         //Cap [13]
        //This fatigue can lead to premature cracking and failure.",                                                                     //Cap [14]
        //Boom Bounce alarms can be avoided by reducing sudden vertical movements,",                                                     //Cap [15]
        //especially while engaged in the cut or while carrying large payloads.",                                                        //Cap [16]
        if (queue == 3)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.5)
                {
                    captionText.text = captions[7];
                }
                if (playTime > 5.5 && playTime < 10.2)
                {
                    captionText.text = captions[8];
                }
                if (playTime > 10.2 && playTime < 16.9)
                {
                    captionText.text = captions[9];
                }
                if (playTime > 16.9 && playTime < 23.4)
                {
                    captionText.text = captions[10];
                }
                if (playTime > 23.4 && playTime < 29.4)
                {
                    captionText.text = captions[11];
                }
                if (playTime > 29.4 && playTime < 34)
                {
                    captionText.text = captions[12];
                }
                if (playTime > 34 && playTime < 39.7)
                {
                    captionText.text = captions[13];
                }
                if (playTime > 39.7 && playTime < 45.6)
                {
                    captionText.text = captions[14];
                }
                if (playTime > 45.6 && playTime < 51.9)
                {
                    captionText.text = captions[15];
                }
                if (playTime > 51.9)
                {
                    captionText.text = captions[16];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_Four (Side Load Intro)
        //******************************************************************************************************************************
        //Queue four (Audio)
        if (queue == 3 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime * 2)
        {
            queue = 4;
            videoPlayer.clip = videoClips[1];               //Load Side Load Video
            fadeImage.sprite = screenShots[1];                  //Load the first screen shot
            //fadeScreenController.fadeToBlackTime = 1f;
            //fadeToBlackController.fadeToBlackTime = 1f;
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[2];     //Load Clip 003
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue four SFX
        //"Side Load",                                                                                                                    //Cap [17]
        //"When a side load occurs,",                                                                                                     //Cap [18]
        //"the instantaneous bending stress in the boom exceeds the threshold.",                                                          //Cap [19]
        //"In other words, when the bucket rapidly moves left or right,",                                                                 //Cap [20]
        //"this causes the boom to quickly bend.",                                                                                        //Cap [21]
        //"This results in high stresses in the boom and suspension ropes.",                                                              //Cap [22]
        if (queue == 4)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 4.9)
                {
                    captionText.text = captions[17];
                }
                if (playTime > 4.9 && playTime < 8.6)
                {
                    captionText.text = captions[18];
                }
                if (playTime > 8.6 && playTime < 14.9)
                {
                    captionText.text = captions[19];
                }
                if (playTime > 14.9 && playTime < 20.6)
                {
                    captionText.text = captions[20];
                }
                if (playTime > 20.6 && playTime < 25.3)
                {
                    captionText.text = captions[21];
                }
                if (playTime > 25.3)
                {
                    captionText.text = captions[22];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                //fadeScreenController.fadeToBlackTime = 2f;
                //fadeToBlackController.fadeToBlackTime = 2f;
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);
                //hideCaptionBacking = true;                              //Hide the caption background

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Five (Side Load Video)
        //******************************************************************************************************************************
        //Queue five (Video)
        if (queue == 4 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 5;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue five SFX
        if (queue == 5)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }
            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Six (Side Load Explanation)
        //******************************************************************************************************************************
        //Queue six (Audio)
        if (queue == 5 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 6;
            //fadeImage.sprite = screenShots[1];          //Load the first screen shot
            //fadeScreenController.showFadeScreen = true; // show fade screen    
            playAudio.clip = audioClips[3];     //Load Clip 004
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue six SFX
        //"When the boom bends to the left, the left chords are in compression",                                                          //Cap [23]
        //"and the right chords are in tension.",                                                                                         //Cap [24]
        //"The inverse is true when the boom bends to the right.",                                                                        //Cap [25]
        //When this happens, the HMI shows the instantaneous bending stress.",                                                           //Cap [26]
        //"This is shown on the HMI on the lower boom bending indicator.",                                                                //Cap [27]
        //"If the stress is high enough, it will exceed the threshold.",                                                                  //Cap [28]
        //"This results in a Side Load alarm",                                                                                            //Cap [29]
        //"High stresses in the boom and ropes accelerate wear.",                                                                         //Cap [30]
        //"This fatigue can lead to premature cracking and failure.",                                                                     //Cap [31]
        //"Side Load alarms can be avoided by keeping the bucket centered beneath the boom,",                                             //Cap [32]
        //"and avoiding sudden starts or stops while swinging,",                                                                          //Cap [33]
        //"especially while engaged in the cut or carrying large payloads.",                                                              //Cap [34]
        if (queue == 6)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.7)
                {
                    captionText.text = captions[23];
                }
                if (playTime > 5.7 && playTime < 10.1)
                {
                    captionText.text = captions[24];
                }
                if (playTime > 10.1 && playTime < 15.2)
                {
                    captionText.text = captions[25];
                }
                if (playTime > 15.2 && playTime < 22.1)
                {
                    captionText.text = captions[26];
                }
                if (playTime > 22.1 && playTime < 28.3)
                {
                    captionText.text = captions[27];
                }
                if (playTime > 28.3 && playTime < 34.4)
                {
                    captionText.text = captions[28];
                }
                if (playTime > 34.4 && playTime < 38.9)
                {
                    captionText.text = captions[29];
                }
                if (playTime > 38.9 && playTime < 44.6)
                {
                    captionText.text = captions[30];
                }
                if (playTime > 44.6 && playTime < 50.2)
                {
                    captionText.text = captions[31];
                }
                if (playTime > 50.2 && playTime < 57.1)
                {
                    captionText.text = captions[32];
                }
                if (playTime > 57.1 && playTime < 62)
                {
                    captionText.text = captions[33];
                }
                if (playTime > 62)
                {
                    captionText.text = captions[34];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_Seven (Rough Dig Intro)
        //******************************************************************************************************************************
        //Queue seven (Audio)
        if (queue == 6 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 7;
            videoPlayer.clip = videoClips[2];               //Load Rough Dig Video
            fadeImage.sprite = screenShots[2];                  //Load the first screen shot
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[4];     //Load Clip 005
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue seven SFX
        //"Fatigue Alarms",                                                                                                               //Cap [35]
        //"Fatigue alarms occur from the total stresses accrued during a single cycle.",                                                  //Cap [36]
        //"This is when the total bending or compressive stresses exceed the threshold.",                                                 //Cap [37]
        //"Fatigue is defined as the accumulation of small loads over time.",                                                             //Cap [38]
        //"For example, imagine that the boom is a paperclip.",                                                                           //Cap [39]
        //"If you bend the paperclip,",                                                                                                   //Cap [40]
        //"fatigue events are the small back and forth bending motions.",                                                                 //Cap [41]
        //"These don't break the paperclip but weaken the material.",                                                                     //Cap [42]
        //"Rough Dig",                                                                                                                    //Cap [43]
        //"When a rough dig cycle occurs,",                                                                                               //Cap [44]
        //"the cumulative compressive stress in the boom exceeds the threshold.",                                                         //Cap [45]
        //"In other words, when the bucket bounces up and down,",                                                                         //Cap [46]
        //"this causes the boom to bend up and down.",                                                                                    //Cap [47]
        //"This results in low, but repeated stress on the boom and suspension ropes.",                                                   //Cap [48]
        if (queue == 7)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.3)
                {
                    captionText.text = captions[35];
                }
                if (playTime > 5.3 && playTime < 12.4)
                {
                    captionText.text = captions[36];
                }
                if (playTime > 12.4 && playTime < 19.1)
                {
                    captionText.text = captions[37];
                }
                if (playTime > 19.1 && playTime < 25.6)
                {
                    captionText.text = captions[38];
                }
                if (playTime > 25.6 && playTime < 31.5)
                {
                    captionText.text = captions[39];
                }
                if (playTime > 31.5 && playTime < 35.3)
                {
                    captionText.text = captions[40];
                }
                if (playTime > 35.3 && playTime < 41.1)
                {
                    captionText.text = captions[41];
                }
                if (playTime > 41.1 && playTime < 46.2)
                {
                    captionText.text = captions[42];
                }
                if (playTime > 46.2 && playTime < 51.2)
                {
                    captionText.text = captions[43];
                }
                if (playTime > 51.2 && playTime < 55.4)
                {
                    captionText.text = captions[44];
                }
                if (playTime > 55.4 && playTime < 61)
                {
                    captionText.text = captions[45];
                }
                if (playTime > 61 && playTime < 66)
                {
                    captionText.text = captions[46];
                }
                if (playTime > 66 && playTime < 71)
                {
                    captionText.text = captions[47];
                }
                if (playTime > 71)
                {
                    captionText.text = captions[48];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length)//the audio clip has a 2 second silence at end
            {
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Eight (Rough Dig Video)
        //******************************************************************************************************************************
        //Queue eight (Video)
        if (queue == 7 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 8;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue five SFX
        if (queue == 8)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Nine (Rough Dig Explanation)
        //******************************************************************************************************************************
        //Queue nine (Audio)
        if (queue == 8 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 9;
            playAudio.clip = audioClips[5];     //Load Clip 006
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue nine SFX
        //"When the boom repeatedly bends back and forth,",                                                                               //Cap [49]
        //"total cycle stress increases.",                                                                                                //Cap [50]
        //"When this happens, the HMI shows the total compressive stress.",                                                               //Cap [51]
        //"This is shown on the HMI on the right boom compression indicator.",                                                            //Cap [52]
        //"If the stress total is high enough, it will exceed the threshold.",                                                            //Cap [53]
        //"This results in a Rough Dig alarm.",                                                                                           //Cap [54]
        //"Rough Dig alarms can be avoided by filling the bucket in a single pass,",                                                      //Cap [55]
        //"avoiding unnecessary vertical bucket movements,",                                                                              //Cap [56]
        //"and reducing cycle times.",                                                                                                    //Cap [57]
        if (queue == 9)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 4.6)
                {
                    captionText.text = captions[49];
                }
                if (playTime > 4.6 && playTime < 9.1)
                {
                    captionText.text = captions[50];
                }
                if (playTime > 9.1 && playTime < 15.5)
                {
                    captionText.text = captions[51];
                }
                if (playTime > 15.5 && playTime < 22)
                {
                    captionText.text = captions[52];
                }
                if (playTime > 22 && playTime < 28.2)
                {
                    captionText.text = captions[53];
                }
                if (playTime > 28.2 && playTime < 32.9)
                {
                    captionText.text = captions[54];
                }
                if (playTime > 32.9 && playTime < 39.1)
                {
                    captionText.text = captions[55];
                }
                if (playTime > 39.1 && playTime < 44.2)
                {
                    captionText.text = captions[56];
                }
                if (playTime > 44.2)
                {
                    captionText.text = captions[57];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_Ten (Long Side Load Intro)
        //******************************************************************************************************************************
        //Queue Ten (Audio)
        if (queue == 9 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 10;
            videoPlayer.clip = videoClips[3];               //Load Long Side Load Video
            fadeImage.sprite = screenShots[3];                  //Load Long Side Load screen shot
            //fadeScreenController.fadeToBlackTime = 1f;
            //fadeToBlackController.fadeToBlackTime = 1f;
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[6];     //Load Clip 007
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue ten SFX
        //"Long Sideload",                                                                                                                //Cap [58]
        //"When a long sideload cycle occurs,",                                                                                           //Cap [59]
        //"the cumulative bending stress in the boom exceeds the threshold.",                                                             //Cap [60]
        //"In other words, when the bucket swings left and right,",                                                                       //Cap [61]
        //"this causes the boom to bend.",                                                                                                //Cap [62]
        //"This results in low, but repeated stress on the boom and suspension ropes.",                                                   //Cap [63]
        if (queue == 10)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.2)
                {
                    captionText.text = captions[58];
                }
                if (playTime > 5.2 && playTime < 9.8)
                {
                    captionText.text = captions[59];
                }
                if (playTime > 9.8 && playTime < 15.9)
                {
                    captionText.text = captions[60];
                }
                if (playTime > 15.9 && playTime < 21.1)
                {
                    captionText.text = captions[61];
                }
                if (playTime > 21.1 && playTime < 25.5)
                {
                    captionText.text = captions[62];
                }
                if (playTime > 25.5)
                {
                    captionText.text = captions[63];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length)//the audio clip has a 2 second silence at end
            {
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Eleven (Long Side Load Video)
        //******************************************************************************************************************************
        //Queue eleven (Video)
        if (queue == 10 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 11;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue five SFX
        if (queue == 11)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Twelve (Long Side Load Explanation)
        //******************************************************************************************************************************
        //Queue twelve (Audio)
        if (queue == 11 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 12;
            playAudio.clip = audioClips[7];     //Load Clip 008
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue twelve SFX
        //"When the boom repeatedly bends back and forth,",                                                                               //Cap [64]
        //"total cycle stress increases.",                                                                                                //Cap [65]
        //"When this happens, the HMI shows the total bending stress.",                                                                   //Cap [66]
        //"This is shown on the HMI on the top boom bending indicator.",                                                                  //Cap [67]
        //"If the stress total is high enough, it will exceed the threshold.",                                                            //Cap [68]
        //"This results in a Long Sideload alarm.",                                                                                       //Cap [69]
        //"Long Sideload alarms can be avoided by reducing unnecessary swinging bucket movements,",                                       //Cap [70]
        //"keeping the bucket centered beneath the boom,",                                                                                //Cap [71]
        //"and reducing cycle times.",                                                                                                    //Cap [72]
        if (queue == 12)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 4.6)
                {
                    captionText.text = captions[64];
                }
                if (playTime > 4.6 && playTime < 9.2)
                {
                    captionText.text = captions[65];
                }
                if (playTime > 9.2 && playTime < 15.4)
                {
                    captionText.text = captions[66];
                }
                if (playTime > 15.4 && playTime < 21.6)
                {
                    captionText.text = captions[67];
                }
                if (playTime > 21.6 && playTime < 27.8)
                {
                    captionText.text = captions[68];
                }
                if (playTime > 27.8 && playTime < 32.8)
                {
                    captionText.text = captions[69];
                }
                if (playTime > 32.8 && playTime < 40.3)
                {
                    captionText.text = captions[70];
                }
                if (playTime > 40.3 && playTime < 44.9)
                {
                    captionText.text = captions[71];
                }
                if (playTime > 44.9)
                {
                    captionText.text = captions[72];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_Thirteen (Production Code Intro)
        //******************************************************************************************************************************
        //Queue Thirteen (Audio)
        if (queue == 12 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 13;
            videoPlayer.clip = videoClips[4];               //Load Long Side Load Video
            fadeImage.sprite = screenShots[4];                  //Load Long Side Load screen shot
            //fadeScreenController.fadeToBlackTime = 1f;
            //fadeToBlackController.fadeToBlackTime = 1f;
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[8];     //Load Clip 009
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue thirteen SFX
        //"Production Codes",                                                                                                             //Cap [73]
        //"Production codes are used to categorize the type of digging being performed.",                                                 //Cap [74]
        //"The operator selects the appropriate production code from the HMI.",                                                           //Cap [75]
        if (queue == 13)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.3)
                {
                    captionText.text = captions[73];
                }
                if (playTime > 5.3 && playTime < 12.1)
                {
                    captionText.text = captions[74];
                }
                if (playTime > 12.1)
                {
                    captionText.text = captions[75];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length)//the audio clip has a 2 second silence at end
            {
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Fourteen (Production Code Video)
        //******************************************************************************************************************************
        //Queue Fourteen (Video)
        if (queue == 13 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 14;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue fourteen SFX
        if (queue == 14)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Fifteen (Production Code Explanation)
        //******************************************************************************************************************************
        //Queue fifteen (Audio)
        if (queue == 14 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 15;
            playAudio.clip = audioClips[9];     //Load Clip 010
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue fifteen SFX
        //"Production code time begins to count once the bucket crossing the bucket counting threshold.",                                 //Cap [76]
        //"Production codes are held through delays, unless changed by the operator.",                                                    //Cap [77]
        //"This means that following the completion of a delay,",                                                                         //Cap [78]
        //"the production code used before the delay code was entered will be automatically used again,",                                 //Cap [79]
        //"unless changed by the operator.",                                                                                              //Cap [80]
        if (queue == 15)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 7.8)
                {
                    captionText.text = captions[76];
                }
                if (playTime > 7.8 && playTime < 14.6)
                {
                    captionText.text = captions[77];
                }
                if (playTime > 14.6 && playTime < 19.7)
                {
                    captionText.text = captions[78];
                }
                if (playTime > 19.7 && playTime < 27.2)
                {
                    captionText.text = captions[79];
                }
                if (playTime > 27.2)
                {
                    captionText.text = captions[80];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_Sixteen (NPC Intro)
        //******************************************************************************************************************************
        //Queue Sixteen (Audio)
        if (queue == 15 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 16;
            videoPlayer.clip = videoClips[5];               //Load Long Side Load Video
            fadeImage.sprite = screenShots[5];                  //Load Long Side Load screen shot
            //fadeScreenController.fadeToBlackTime = 1f;
            //fadeToBlackController.fadeToBlackTime = 1f;
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[10];     //Load Clip 011
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue Sixteen SFX
        //"Non-Productive Codes",                                                                                                         //Cap [81]
        //"Most digging operations follow the standard dig swing dump return cycle.",                                                     //Cap [82]
        //"For these operations, bucket counting and full alarming is desired.",                                                          //Cap [83]
        //"However, nonstandard operations can also occur, such as Stirring the Well.",                                                   //Cap [84]
        if (queue == 16)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.5)
                {
                    captionText.text = captions[81];
                }
                if (playTime > 5.5 && playTime < 15.2)
                {
                    captionText.text = captions[82];
                }
                if (playTime > 15.2 && playTime < 21.9)
                {
                    captionText.text = captions[83];
                }
                if (playTime > 21.9)
                {
                    captionText.text = captions[84];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length)//the audio clip has a 2 second silence at end
            {
                //fadeScreenController.fadeToBlackTime = 2f;
                //fadeToBlackController.fadeToBlackTime = 2f;
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);
                //hideCaptionBacking = true;                              //Hide the caption background

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Seventeen (Production Code Video)
        //******************************************************************************************************************************
        //Queue Seventeen (Video)
        if (queue == 16 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 17;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue Seventeen SFX
        if (queue == 17)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Eighteen (NPC Flash Intro)
        //******************************************************************************************************************************
        //Queue Eighteen (Audio)
        if (queue == 17 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 18;
            videoPlayer.clip = videoClips[6];               //Load Long Side Load Video
            fadeImage.sprite = screenShots[6];                  //Load Long Side Load screen shot
            //fadeScreenController.fadeToBlackTime = 1f;
            //fadeToBlackController.fadeToBlackTime = 1f;
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[11];     //Load Clip 012
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue Eighteen SFX
        //"These are called \"Non-productive codes\".",                                                                                   //Cap [85]
        //"When an operator selects an NPC, several features are disabled, including:",                                                   //Cap [86]    
        //"Payload indicator, compressive fatigue, and bending fatigue.",                                                                 //Cap [87]
        //"This means that bucket counting is disabled.",                                                                                 //Cap [88]
        //"Rough Dig and Long Sideload alarms are also disabled.",                                                                        //Cap [89]
        //"Every 5 minutes, the screen will flash,",                                                                                      //Cap [90]
        //"reminding the operator that they have entered an NPC.",                                                                        //Cap [91]
        if (queue == 18)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 4.4)
                {
                    captionText.text = captions[85];
                }
                if (playTime > 4.4 && playTime < 12)
                {
                    captionText.text = captions[86];
                }
                if (playTime > 12 && playTime < 18.5)
                {
                    captionText.text = captions[87];
                }
                if (playTime > 18.5 && playTime < 23.8)
                {
                    captionText.text = captions[88];
                }
                if (playTime > 23.8 && playTime < 29.5)
                {
                    captionText.text = captions[89];
                }
                if (playTime > 29.5 && playTime < 34.6)
                {
                    captionText.text = captions[90];
                }
                if (playTime > 34.6)
                {
                    captionText.text = captions[91];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                //fadeScreenController.fadeToBlackTime = 2f;
                //fadeToBlackController.fadeToBlackTime = 2f;
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);
                //hideCaptionBacking = true;                              //Hide the caption background

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_Ninteen (NPC Flash Video)
        //******************************************************************************************************************************
        //Queue Ninteen (Video)
        if (queue == 18 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 19;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue Seventeen SFX
        if (queue == 19)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_Twenty (NPC Flash Explanation)
        //******************************************************************************************************************************
        //Queue twenty (Audio)
        if (queue == 19 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 20;
            playAudio.clip = audioClips[12];     //Load Clip 013
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue twenty SFX
        //"It is important to change the Production Code once NPC digging is complete.",                                                  //Cap [92]
        //"Otherwise, bucket and payload counting will not function.",                                                                    //Cap [93]
        if (queue == 20)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 7.0)
                {
                    captionText.text = captions[92];
                }
                if (playTime > 7.0)
                {
                    captionText.text = captions[93];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_TwentyOne (Delay Code Intro)
        //******************************************************************************************************************************
        //Queue twenty one (Audio)
        if (queue == 20 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 21;
            videoPlayer.clip = videoClips[7];               //Load Rough Dig Video
            fadeImage.sprite = screenShots[7];                  //Load the first screen shot
            //fadeScreenController.fadeToBlackTime = 1f;
            //fadeToBlackController.fadeToBlackTime = 1f;
            fadeScreenController.showFadeScreen = true;         // show screenshot  
            fadeToBlackController.showFadeScreen = true;        //show black screen
            playAudio.clip = audioClips[13];     //Load Clip 014
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue twenty one SFX
        //"Delay Codes",                                                                                                                  //Cap [94]
        //"When not in operation, delay codes are used to categorize the downtime.",                                                      //Cap [95]
        //"Delay codes are prompted to be entered any time excitation is turned off,",                                                    //Cap [96]
        //"or active digging has stopped for more than 2 minutes.",                                                                       //Cap [97]
        if (queue == 21)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5.1)
                {
                    captionText.text = captions[94];
                }
                if (playTime > 5.1 && playTime < 11.9)
                {
                    captionText.text = captions[95];
                }
                if (playTime > 11.9 && playTime < 18.3)
                {
                    captionText.text = captions[96];
                }
                if (playTime > 18.3)
                {
                    captionText.text = captions[97];
                }
            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                //fadeScreenController.fadeToBlackTime = 2f;
                //fadeToBlackController.fadeToBlackTime = 2f;
                captionText.text = "";
                fadeScreenController.showFadeScreen = false;
                HideCaptionandBacking(true);
                //hideCaptionBacking = true;                              //Hide the caption background

            }
            if (playAudio.isPlaying == false
                && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime)
            {
                fadeToBlackController.showFadeScreen = false;
            }
        }
        #endregion
        #region Queue_TwentyTwo (Delay Code Video)
        //******************************************************************************************************************************
        //Queue twenty two (Video)
        if (queue == 21 && captionText.text == ""
            && Time.time > startTime + playAudio.clip.length + delayTime + screenDelayTime * 2 - videoPreLoadTime)
        {
            queue = 22;
            videoPlayer.Play();
            startTime = Time.time;
        }
        //Queue five SFX
        if (queue == 22)
        {
            if (videoPlayer.frame < (float)videoPlayer.frameCount)
            {
                videoPlayer.Pause();
                videoPlayer.StepForward();
            }

            if (videoPlayer.frame == (float)videoPlayer.frameCount && Time.time > startTime + videoPlayer.clip.length + delayTime)
            {
                captionText.text = "";
                HideCaptionandBacking(false);
                //hideCaptionBacking = false;     //Show the background for the captions again
            }

            //if (videoPlayer.isPlaying == false && Time.time > startTime + videoPlayer.clip.length + delayTime)
            //{
            //    captionText.text = "";
            //    HideCaptionandBacking(false);
            //    //hideCaptionBacking = false;     //Show the background for the captions again
            //}
        }
        #endregion
        #region Queue_TwentyThree (Delay Code Explanation)
        //******************************************************************************************************************************
        //Queue twenty three (Audio)
        if (queue == 22 && captionText.text == "" && Time.time > startTime + videoPlayer.clip.length + delayTime)
        {
            queue = 23;
            playAudio.clip = audioClips[14];     //Load Clip 015
            playAudio.Play();
            startTime = Time.time;
        }
        //Queue twenty three SFX
        //"The HMI prompts the operator to enter the delay code each time this state is entered.",                                        //Cap [98]
        //"If a code is not entered, the time is identified as unknown.",                                                                 //Cap [99]
        if (queue == 23)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 7.4)
                {
                    captionText.text = captions[98];
                }
                if (playTime > 7.4)
                {
                    captionText.text = captions[99];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                fadeToBlackController.showFadeScreen = true;    //Fade to black
                HideCaptionandBacking(true);
                //hideCaptionBacking = true;     //Hide the background for the captions again
            }
        }       
        #endregion
    }



    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
    //Toggle the visability of the caption background
    public void CaptionandBackingManager()
    {
        captionBackgroundColor = captionBackground.GetComponent<Image>().color;
        captionTextColor = captionText.color;
        if (hideCaptionBacking == true)
        {
            captionBackgroundColor.a = Mathf.Lerp(captionBackgroundColor.a, fadedAlpha, fadeTime * Time.deltaTime);
            captionTextColor.a = Mathf.Lerp(captionTextColor.a, fadedAlpha, fadeTime * Time.deltaTime);
            //captionBackground.SetActive(false);
        }
        else
        {
            captionBackgroundColor.a = Mathf.Lerp(captionBackgroundColor.a, solidAlpha, fadeTime * Time.deltaTime);
            captionTextColor.a = Mathf.Lerp(captionTextColor.a, solidTextAlpha, fadeTime * Time.deltaTime);
            //captionBackground.SetActive(true);
        }
        captionBackground.GetComponent<Image>().color = captionBackgroundColor;
        captionText.color = captionTextColor;
    }
    //A consolidated way to control the hiding and unhiding of the captions and backing as well as their speed.
    public void HideCaptionandBacking(bool hide)
    {
        if (hide)
        {
            //fadeTime = 3f;
            hideCaptionBacking = true;
        }
        else
        {
            //fadeTime = 3f;
            hideCaptionBacking = false;
        }
    }
    //For debugging to show the current Queue number on the screen.
    public void QueueTextUpdate()
    {
        queueTextParent.SetActive(showQueueText);
        //queueText.GetComponent<GameObject>().SetActive(showQueueText);
        queueText.text = queue.ToString();
    }
}
