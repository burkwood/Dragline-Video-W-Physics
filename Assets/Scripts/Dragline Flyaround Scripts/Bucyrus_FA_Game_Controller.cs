﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Bucyrus_FA_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    Fade_Screen_Controller fadeScreenController;
    Bucyrus_Interior_DL_SFX DL_SFX;
    private GameObject mainCamera;
    private GameObject virtualCamera;
    public float startTime;
    public float delayTime = 2;
    public float playTime;


    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Bucyrus_Interior_DL_SFX>();
        fadeScreenController = gameObject.GetComponent<Fade_Screen_Controller>();
        virtualCamera = GameObject.FindGameObjectWithTag("VirtualCamera");
        virtualCamera.SetActive(false);
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        mainCamera.transform.position = new Vector3(0, 5.45f, -13.68f);
        mainCamera.transform.rotation = Quaternion.Euler(14.55f, 0f, 0f);
        mainCamera.GetComponent<Camera>().fieldOfView = 60f;
	}
	
	// Update is called once per frame
	void Update ()
    {
        playTime = Time.time;
        if (Time.time < 2)
        {
            fadeScreenController.fadeMessage = "Bucyrus Dragline Interior";
            fadeScreenController.showFadeScreen = true;
            fadeScreenController.showFadeText = true;
            DL_SFX.hoistDispOn = false;
            DL_SFX.dragDispOn = false;
        }
        if (Time.time > 2 && queue == 0)
        {
            nextStage = true;
            queue = 1;
            startTime = Time.time;
        }
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            fadeScreenController.showFadeText = false;
            fadeScreenController.showFadeScreen = false;
            virtualCamera.SetActive(true);
            queue = 2;          
        }

        if (queue == 2)
        {
            if (Time.time > startTime + delayTime && Time.time < startTime + delayTime + 7)
            {
                DL_SFX.hoistMCtrl = MotorControl.Extend;
            }
            if (Time.time > startTime + delayTime + 5 && Time.time < startTime + delayTime + 12)
            {
                DL_SFX.hoistMCtrl = MotorControl.None;
                
            }
            if (Time.time > startTime + delayTime + 20 && Time.time < startTime + delayTime + 25)
            {
                DL_SFX.dragMCtrl = MotorControl.Extend;
                DL_SFX.hoistMCtrl = MotorControl.Retract;
            }
            if (Time.time > startTime + delayTime + 25)
            {
                DL_SFX.dragMCtrl = MotorControl.None;
                DL_SFX.hoistMCtrl = MotorControl.None;
            }
            if (Time.time > startTime + delayTime + 33)
            {
                fadeScreenController.showFadeScreen = true;
            }
        }



	}
}
