﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Marion_FA_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    Fade_Screen_Controller fadeScreenController;
    Marion_Interior_DL_SFX DL_SFX;
    private GameObject mainCamera;
    private GameObject virtualCamera;
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    // Use this for initialization
    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Marion_Interior_DL_SFX>();
        fadeScreenController = gameObject.GetComponent<Fade_Screen_Controller>();
        virtualCamera = GameObject.FindGameObjectWithTag("VirtualCamera");
        virtualCamera.SetActive(false);
        mainCamera = GameObject.FindGameObjectWithTag("MainCamera");
        mainCamera.transform.position = new Vector3(0.8f, 5.45f, 12.49f);
        mainCamera.transform.rotation = Quaternion.Euler(18.9f, 180f, 0f);
        mainCamera.GetComponent<Camera>().fieldOfView = 60f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        playTime = Time.time;
        if (Time.time < 2)
        {
            fadeScreenController.fadeMessage = "Marion Dragline Interior";
            fadeScreenController.showFadeScreen = true;
            fadeScreenController.showFadeText = true;
            DL_SFX.hoistDispOn = false;
            DL_SFX.dragDispOn = false;
        }
        if (Time.time > 2 && queue == 0)
        {
            nextStage = true;
            queue = 1;
            startTime = Time.time;
        }
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            fadeScreenController.showFadeText = false;
            fadeScreenController.showFadeScreen = false;
            virtualCamera.SetActive(true);
            queue = 2;
        }

        if (queue == 2)
        {
            if (Time.time > startTime + delayTime + 19 && Time.time < startTime + delayTime + 24)
            {
                DL_SFX.hoistMCtrl = MotorControl.Extend;
                DL_SFX.dragMCtrl = MotorControl.Extend;
            }
            if (Time.time > startTime + delayTime + 24 && Time.time < startTime + delayTime + 29)
            {
                DL_SFX.hoistMCtrl = MotorControl.None;
                DL_SFX.dragMCtrl = MotorControl.None;

            }
            //if (Time.time > startTime + delayTime + 24 && Time.time < startTime + delayTime + 29)
            //{
            //    DL_SFX.dragMCtrl = MotorControl.Extend;
            //    //DL_SFX.hoistMCtrl = MotorControl.Retract;
            //}
            //if (Time.time > startTime + delayTime + 32)
            //{
            //    DL_SFX.dragMCtrl = MotorControl.None;
            //    //DL_SFX.hoistMCtrl = MotorControl.None;
            //}
            if (Time.time > startTime + delayTime + 33)
            {
                fadeScreenController.showFadeScreen = true;
            }
        }
    }
}
