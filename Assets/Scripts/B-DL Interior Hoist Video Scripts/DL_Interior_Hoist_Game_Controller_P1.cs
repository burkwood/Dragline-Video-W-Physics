﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class DL_Interior_Hoist_Game_Controller_P1 : MonoBehaviour
{
    Interior_DL_SFX interiorDLSFX;                                                  
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    public float runTime;
    public int queue = 0;                                                                       //Index of SFX queues
    public AudioClip[] audioClip;                                                               //List of Audio Clips
    private AudioSource playAudio;
    public float startTime;
    public float delayTime = 2f;
    public float playTime;
    public Text captionText;
    private bool audioTriggered = false;
    private string[] captionsP1 = new string[]
    {
        "When the hoist up command is issued, several things occur.",                           //Cap [0]
        "First, the generators start generating power for the hoist motors.",                   //Cap [1]
        "The hoist motors power the hoist gearbox which turns the hoist drum.",                 //Cap [2]
        "The gearing ratio from the hoist motors to the drum can be seen here.",             //Cap [3]
        "As the hoist drum turns it spools in the hoist cables, raising the bucket.",          //Cap [4]
    };
 
    // Use this for initialization
    void Start ()
    {
        interiorDLSFX = GetComponent<Interior_DL_SFX>();                                        //Get reference to the special effects script
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                          //Get reference to the fade to black screen script
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();                //Get reference to the virtual joystick script
        playAudio = GetComponent<AudioSource>();                                              //Get reference to the audio player
        playAudio.clip = audioClip[0];
        captionText.text = "";                                                                   //Clear the caption text
	}
	
	// Update is called once per frame
	void Update ()
    {
        runTime = Time.time;
        UpdatePlayTime();
        //******************************************************************************************************************************
        //Scene Setup
        //Extend Rope, disable drag gen status, engage fade screen, display fade screen text
        interiorDLSFX.dragDispOn = false;                                                       //Turn off Drag Side Gen Status Display
        if (queue == 0)
        {
            //Extend Hoist rope, fade screen and show fade text
            if (Time.time < 6)
            {
                interiorDLSFX.hoistMCtrl = MotorControl.Extend;
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.showFadeText = true;
            }
            //Stop paying out hoist rope
            if (Time.time >6) // Shot any extending or retraction
            {
                interiorDLSFX.hoistMCtrl = MotorControl.None;
            }
            //Unfade screen and hide fade text
            if (Time.time > 9) //Lights up and set to first queue
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
            }
        }
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 10)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //"When the hoist up command is issued, several things occur." Cap [0]
        //Highlight right Joystick and move to Hoist Up Position
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            //Show Captions
            if (playAudio.isPlaying == true)
            {
                captionText.text = captionsP1[0];
            }
            //Highlight virtual joystick
            if (playAudio.isPlaying == true && playTime > 0.5)
            {
                virtualJoystickController.rightJoyHighlight = Status.Flash;
                virtualJoystickController.rightJoyVert = vJoyMove.Down;
            }
            //Clear captions after 2 secons and stop highlighting joystick
            if (playAudio.isPlaying == false && Time.time> startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                virtualJoystickController.rightJoyHighlight = Status.Off;
                //virtualJoystickController.rightJoyVert = vJoyMove.None;
            }
        }
        //******************************************************************************************************************************
        //Start the second Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 1 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[1];
            playAudio.Play();
        }
        //Caption & SFX Manager during the second Queue
        //"First, the generators start generating power for the hoist motors."  Cap [1]
        //Highlight generators and show gen engaged text
        if (queue == 2 )
        {
            //If audio is playing
            if (playAudio.isPlaying == true)
            {
                //Show the correct Caption
                captionText.text = captionsP1[1];
                //Highlight generators
                if (playTime > 1)
                {
                    interiorDLSFX.genHighlightStatus = Status.Flash;
                }
                //Engage generators
                if (playTime > 2)
                {
                    interiorDLSFX.gensetOn = true;
                }
            }
            //If audio clip ends, wait 2 secs then clear captions and stop highlighting
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                interiorDLSFX.genHighlightStatus = Status.Off;
                captionText.text = "";
            }

        }
        //******************************************************************************************************************************
        //Start the third Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 2 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 3;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[2];
            playAudio.Play();
        }
        //Caption and SFX Manager during the third Queue
        //"The hoist motors power the hoist gearbox which turns the hoist drum." Cap [2]
        //Highlight the Hoist Motors, the hoist gearbox and the Hoist drum
        if (queue == 3)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captionsP1[2];
                //Highlight hoist motor
                if (playTime > 0.3)
                {
                    interiorDLSFX.hoistMotorStatus = Status.Flash;
                }
                //Highlight hoist gearbox
                if (playTime > 1.6)
                {
                    interiorDLSFX.hoistGearboxStatus = Status.Flash;
                }
                //Highlight Hoist Drum
                if (playTime > 3.3)
                {
                    interiorDLSFX.hoistDrumStatus = Status.Flash;
                }
            }
            //If audio clip ends, wait 2 secs then clear captions and stop highlighting
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                interiorDLSFX.hoistMotorStatus = Status.Off;
                interiorDLSFX.hoistGearboxStatus = Status.Off;
                interiorDLSFX.hoistDrumStatus = Status.Off;
            }
        }
        //******************************************************************************************************************************
        //Start the forth Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 3 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 4;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[3];
            playAudio.Play();
        }
        //Caption and SFX Manager during the forthd Queue
        //"The gearing ratio from the hoist motors to the drum can be seen here." Cap [3]
        //Show the hoist gear movie P1
        if (queue == 4)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captionsP1[3];
            }
            if (playAudio.isPlaying == false)
            {
                //After audio clip but less then audio length + screen open time + video clip length
                if (Time.time > startTime + playAudio.clip.length && Time.time < startTime + playAudio.clip.length + interiorDLSFX.screenScaleTime + interiorDLSFX.hoistClips[0].length - 0.5)
                {
                    interiorDLSFX.allScreens.hoistScreen.open = true;
                    interiorDLSFX.allScreens.hoistScreen.play = true;
                }
                //After movie finished close screen
                if (Time.time > startTime + playAudio.clip.length + interiorDLSFX.screenScaleTime + interiorDLSFX.hoistClips[0].length - 0.5)
                {
                    interiorDLSFX.allScreens.hoistScreen.play = false;
                    interiorDLSFX.allScreens.hoistScreen.open = false;
                }
                //clear captions
                if (Time.time > startTime + playAudio.clip.length + interiorDLSFX.screenScaleTime * 2 + interiorDLSFX.hoistClips[0].length + delayTime)
                {
                    captionText.text = "";
                }
            }
        }
        //******************************************************************************************************************************
        //Start the fifth Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 4 && Time.time > startTime + delayTime * 2 + playAudio.clip.length + interiorDLSFX.screenScaleTime * 2 + interiorDLSFX.hoistClips[0].length && captionText.text == "")
        {
            queue = 5;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[4];
            playAudio.Play();
        }
        //Ca[tion and SFX Manager during the fifth Queue
        //"As the hoist drum turns it spools in the hoist cables, raising the bucket." Cap [4]
        //Retract hoist cable for 5s
        if (queue == 5)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captionsP1[4];
                //Reel in cable
                if (playTime > 1)
                {
                    interiorDLSFX.hoistMCtrl = MotorControl.Retract;
                    interiorDLSFX.hoistDrumStatus = Status.Flash;
                }
            }
            //Stop hoist drum and clear captions
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                interiorDLSFX.hoistMCtrl = MotorControl.None;
                interiorDLSFX.hoistDrumStatus = Status.Off;
                interiorDLSFX.gensetOn = false;
                virtualJoystickController.rightJoyVert = vJoyMove.None;
                captionText.text = "";
            }
        }
        //******************************************************************************************************************************
        //Start the sixth Queue 
        //No Camera Movement
        //log time, load next clip & start playing
        if (queue == 5 && Time.time > startTime + playAudio.clip.length + delayTime * 2 && captionText.text == "")
        {
            queue = 6;
            startTime = Time.time;
        }
        //Caption and SFX manager during the sixth queue
        if (queue == 6)
        {
            fadeScreenController.showFadeScreen = true;
        }
    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
