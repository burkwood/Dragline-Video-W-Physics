﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Obi_Extended_Exterior_P1_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClip;                                                                                               //Audio Player
    Obi_Bucyrus_DL_SFX DL_SFX;                                                                                                  //The script that controls all hiding and highlighting
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    Obi_Bucyrus_Controller obiBucyrusController;
    Operator_Chair_Controller operatorChairController;
    TrailCableFinder trailCableController;
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private float[] delayTimes = new float[] { 7, 2, 4, 6, 8 };                                                                    //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "The dragline is comprised of a series of multiple electromechanical systems.",
        "These systems and components work to raise and lower the bucket,",
        "pay in and pay out the bucket, swing the dragline, and walk the dragline.",
        "In this video the operator controls will be mirrored by virtual controls.",
        "Before moving the bucket, the operator must verify that excitation is ON,",
        "and the hoist and drag brakes are released.",
        "Let's look at how power is delivered to these systems to move the dragline bucket.",
        "The dragline is powered via 7600VAC, provided by the trail cable.",
        "This power comes from the local power company.",
        
        
        
        
        //Cap [0]

	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Obi_Bucyrus_DL_SFX>();                                                            //Get reference to the SFX script
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                                                      //Get reference to Fade Screen Controller
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();
        obiBucyrusController = GetComponent<Obi_Bucyrus_Controller>();
        operatorChairController = GetComponent<Operator_Chair_Controller>();
        trailCableController = gameObject.GetComponent<TrailCableFinder>();
        playAudio = GetComponent<AudioSource>();                                                                           //Get reference to the Audio Source
        playAudio.clip = audioClip[0];                                                                                         //Load the first clip
        captionText.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();
        #region Scene_Setup
        //******************************************************************************************************************************
        //Scene Setup
        //No Camera Movement, wait for the system to stabilize
        //start with black screen
        if (queue == 0)
        {
            //Fade Screen, show fade text, wait for stable
            if (Time.time < 6)
            {
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.fadeMessage = "Dragline Cab";
                fadeScreenController.showFadeText = true;
            }
            //Unfade Screen and hide text
            if (Time.time > 9)
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
            }
        }
        #endregion
        #region First_Queue
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 10)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //"The dragline is comprised of a series of multiple electromechanical systems."    Cap [0]
        //"These systems and components work to raise and lower the bucket,"                Cap [1]
        //"pay in and pay out the bucket, swing the dragline, and walk the dragline."       Cap [2]
        //Move left joy to drag in position and drag in bucket
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first Clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            //Show captions
            if (playAudio.isPlaying == true)
            {
                if (playTime < 7)
                {
                    captionText.text = captions[0];
                }
                if (playTime > 7 && playTime < 13)
                {
                    captionText.text = captions[1];
                }
                if (playTime > 13)
                {
                    captionText.text = captions[2];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Second_Queue
        //******************************************************************************************************************************
        //Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[1];
            playAudio.Play();
        }
        //Caption & SFX Manager during the second Queue
        //"In this video the operator controls will be mirrored by the virtual controls."   Cap [3]
        if (queue == 2)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[3];
                //Flash Operator Controls
                if (playTime > 1.5)
                {
                    operatorChairController.leftJoyHighlight = IntPublicClass.Status.Flash;
                    operatorChairController.rightJoyHighlight = IntPublicClass.Status.Flash;
                }
                //Flash virtual controls
                if (playTime > 3.5)
                {
                    virtualJoystickController.leftJoyHighlight = Status.Flash;
                    virtualJoystickController.rightJoyHighlight = Status.Flash;
                }
            }
            //clear SFX and captions 2s after audio clip ends
            if (playAudio.isPlaying == false && Time.time> startTime + delayTime + playAudio.clip.length)
            {
                captionText.text = "";
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Off;
                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Off;
                virtualJoystickController.leftJoyHighlight = Status.Off;
                virtualJoystickController.rightJoyHighlight = Status.Off;
            }
        }
        #endregion
        #region Third_Queue
        //******************************************************************************************************************************
        //Start the third Queue 
        //No Camera Movement
        if (queue == 2 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 3;
            startTime = Time.time;
            audioTriggered = false;
        }
        //Caption  & SFX Manager during the third Queue
        if (queue == 3)
        {
            //Flash Left VJoy and OpJoy && move left Sticks Up
            //ST to ST + 2
            if (Time.time < startTime + delayTime)
            {
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Flash;
                virtualJoystickController.leftJoyHighlight = Status.Flash;
                operatorChairController.leftJoyVert = vStickMove.Up;
                virtualJoystickController.leftJoyVert = vJoyMove.Up;
            }
            //Move Left Sticks Down
            //ST + 2 to ST + 4
            if (Time.time > startTime + delayTime && Time.time < startTime + delayTime * 2)
            {
                operatorChairController.leftJoyVert = vStickMove.Down;
                virtualJoystickController.leftJoyVert = vJoyMove.Down;
            }
            //Move Left Sticks left
            //ST + 4 to ST + 6
            if (Time.time > startTime + delayTime * 2 && Time.time < startTime + delayTime * 3)
            {
                operatorChairController.leftJoyVert = vStickMove.None;
                virtualJoystickController.leftJoyVert = vJoyMove.None;
                operatorChairController.leftJoyHor = hStickMove.Left;
                virtualJoystickController.leftJoyHor = hJoyMove.Left;
            }
            //Move Left Sticks Right
            //ST + 6 to ST + 8
            if (Time.time > startTime + delayTime * 3 && Time.time < startTime + delayTime * 4)
            {
                operatorChairController.leftJoyHor = hStickMove.Right;
                virtualJoystickController.leftJoyHor = hJoyMove.Right;
            }
            //Flash right VJoy and OpJoy && move right Sticks Up
            //ST + 8 to ST + 10
            if (Time.time > startTime + delayTime * 4 && Time.time < startTime + delayTime * 5)
            {
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Off;
                virtualJoystickController.leftJoyHighlight = Status.Off;
                operatorChairController.leftJoyHor = hStickMove.None;
                virtualJoystickController.leftJoyHor = hJoyMove.None;

                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Flash;
                virtualJoystickController.rightJoyHighlight = Status.Flash;
                operatorChairController.rightJoyVert = vStickMove.Up;
                virtualJoystickController.rightJoyVert = vJoyMove.Up;
            }
            //Move Right Sticks Down
            //ST + 10 to ST + 12
            if (Time.time > startTime + delayTime * 5 && Time.time < startTime + delayTime * 6)
            {
                operatorChairController.rightJoyVert = vStickMove.Down;
                virtualJoystickController.rightJoyVert = vJoyMove.Down;
            }
            //Move Right Sticks left
            //ST + 12 to ST + 14
            if (Time.time > startTime + delayTime * 6 && Time.time < startTime + delayTime * 7)
            {
                operatorChairController.rightJoyVert = vStickMove.None;
                virtualJoystickController.rightJoyVert = vJoyMove.None;
                operatorChairController.rightJoyHor = hStickMove.Left;
                virtualJoystickController.rightJoyHor = hJoyMove.Left;
            }
            //Move Right Sticks Right
            //ST + 14 to ST + 16
            if (Time.time > startTime + delayTime * 7 && Time.time < startTime + delayTime * 8)
            {
                operatorChairController.rightJoyHor = hStickMove.Right;
                virtualJoystickController.rightJoyHor = hJoyMove.Right;
            }
            //Stop all flashing and return all sticks to neutral position
            //ST + 16 to ST + 18
            if (Time.time > startTime + delayTime * 8 && Time.time < startTime + delayTime * 9)
            {
                operatorChairController.rightJoyHor = hStickMove.None;
                virtualJoystickController.rightJoyHor = hJoyMove.None;
                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Off;
                virtualJoystickController.rightJoyHighlight = Status.Off;
            }
        }
        #endregion
        #region Forth_Queue
        //******************************************************************************************************************************
        //Start the forth Queue 
        //No Camera Movement
        if (queue == 3 && Time.time > startTime + delayTime * 9)
        {
            queue = 4;
            startTime = Time.time;
            playAudio.clip = audioClip[2];
            playAudio.Play();
        }
        //Caption & SFX Manager during the forth Queue
        //"Before moving the bucket, the operator must verify that excitation is ON,"           Cap [4]
        //"and the hoist and drag brakes are released."                                         Cap [5]
        //"Let's look at how power is delivered to these systems to move the dragline bucket."  Cap [6]
        if (queue == 4)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 7 )
                {
                    captionText.text = captions[4];
                }
                if (playTime > 7 && playTime < 11)
                {
                    captionText.text = captions[5];
                }
                if (playTime > 11)
                {
                    captionText.text = captions[6];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Fifth_Queue
        //******************************************************************************************************************************
        //Start the fifth Queue 
        //Camera moves from cab to home view
        if (queue == 4 && nextStage == false && captionText.text == "")
        {
            nextStage = true;
            queue = 5;
            startTime = Time.time + delayTimes[0];
        }
        //Caption & SFX Manager during the fifth Queue
        //"The dragline is powered via 7600VAC, provided by the trail cable."   Cap [7]
        //"This power comes from the local power company."                      Cap [8]
        if (queue == 5 && nextStage == false && Time.time> startTime)
        {
            if (audioTriggered == false)
            {
                playAudio.clip = audioClip[3];
                playAudio.Play();
                audioTriggered = true;
            }
            if (playAudio.isPlaying == true)
            {
                //Caption Section
                if (playTime < 8.9)
                {
                    captionText.text = captions[7];
                }
                if (playTime > 8.9)
                {
                    captionText.text = captions[8];
                }
            }
            

            //if (playAudio.isPlaying == true && Time.time > startTime 
            //    && Time.time < startTime + 8.9)
            //{
            //    captionText.text = captions[7];
            //}
            if (playAudio.isPlaying == true && Time.time > startTime + 5.4)
            {
                trailCableController.fromPowerStationStatus.status = Status.Solid;
            }
            if (playAudio.isPlaying == true && Time.time > startTime + 6.4)
            {
                trailCableController.toDraglineStatus.status = Status.Solid;
            }
            //if (playAudio.isPlaying == true && Time.time > startTime + 8.9)
            //{
            //    captionText.text = captions[8];
            //}
            if (playAudio.isPlaying == false 
                && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                trailCableController.toDraglineStatus.status = Status.Off;
                trailCableController.fromPowerStationStatus.status = Status.Off;
                fadeScreenController.showFadeText = false;
                fadeScreenController.fadeMessage = "";
                fadeScreenController.showFadeScreen = true;
            }
            ////Fade to black
            //if (playAudio.isPlaying == false && captionText.text == "")
            //{
            //    fadeScreenController.showFadeText = false;
            //    fadeScreenController.fadeMessage = "";
            //    fadeScreenController.showFadeScreen = true;
            //}
        }
        #endregion
    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
