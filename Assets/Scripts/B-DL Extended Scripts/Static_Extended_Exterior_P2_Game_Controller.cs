﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Static_Extended_Exterior_P2_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClip;                                                                                               //Audio Player
    Bucyrus_DL_SFX DL_SFX;                                                                                                  //The script that controls all hiding and highlighting
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    Obi_Bucyrus_Controller obiBucyrusController;
    Operator_Chair_Controller operatorChairController;
    TrailCableFinder trailCableController;
    FairleadHighlighter fairleadHighlighter;

    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "From the drum to the boom point, there are 1-2 sets of deflection towers",         //Cap [0]
        "These towers have sheaves that guide the rope.",                                   //Cap [1]
        "They restrict the high force, high amplitude waves of the rope.",                  //Cap [2]
        "The drag ropes also pass through sheaves, restricting high amplitude movement.",   //Cap [3]
        "These sheaves and towers take high, uneven forces.",                               //Cap [4]
        "Without these, the dynamics would result in erratic movements of the bucket.",     //Cap [5]
	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Bucyrus_DL_SFX>();
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                                                      //Get reference to Fade Screen Controller
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();
        trailCableController = gameObject.GetComponent<TrailCableFinder>();
        fairleadHighlighter = gameObject.GetComponent<FairleadHighlighter>();
        playAudio = GetComponent<AudioSource>();                                                                           //Get reference to the Audio Source
        playAudio.clip = audioClip[0];                                                                                         //Load the first clip
        captionText.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();
        #region Scene_Setup
        //******************************************************************************************************************************
        //Scene Setup
        //No Camera Movement, wait for the system to stabilize
        //start with black screen
        if (queue == 0)
        {
            //Fade Screen, show fade text, wait for stable
            if (Time.time < 5)
            {
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.fadeMessage = "Dragline Exterior";
                fadeScreenController.showFadeText = true;
            }
            //Unfade Screen and hide text
            if (Time.time > 5)
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
            }
        }
        #endregion
        #region First_Queue
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 7)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //"From the drum to the boom point, there are 1-2 sets of deflection towers" //Cap [0]
        //"These towers have sheaves that guide the rope."                           //Cap [1]
        //"They restrict the high force, high amplitude waves of the rope."          //Cap [2]
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first Clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            //Show captions
            if (playAudio.isPlaying == true)
            {
                //Captions****************
                if (playTime < 6.8 )
                {
                    captionText.text = captions[0];
                }
                if (playTime > 6.8 && playTime < 11.4)
                {
                    captionText.text = captions[1];
                }
                if (playTime > 11.4)
                {
                    captionText.text = captions[2];
                }
                //Highlights***************
                if (playTime > 3.1)
                {
                    DL_SFX.highlighting.deflectionTowers.status = Status.Flash;
                    DL_SFX.highlighting.boomSheave.status = Status.Flash;
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                DL_SFX.highlighting.deflectionTowers.status = Status.Off;
                DL_SFX.highlighting.boomSheave.status = Status.Off;
            }
        }
        #endregion
        #region Second_Queue
        //******************************************************************************************************************************
        //Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && captionText.text == "" && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[1];
            playAudio.Play();
        }
        //Caption & SFX Manager during the second Queue
        //"The drag ropes also pass through sheaves, restricting high amplitude movement."  Cap [3]
        if (queue == 2)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[3];
                if (playTime > 2)
                {
                    fairleadHighlighter.fairleadStatus.status = Status.Flash;
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                fairleadHighlighter.fairleadStatus.status = Status.Off;
                captionText.text = "";
            }
        }
        #endregion
        //******************************************************************************************************************************
        //Start the Third Queue 
        //No Camera Movement
        if (queue == 2 && captionText.text == "" && Time.time > startTime + delayTime + playAudio.clip.length + delayTime )
        {
            queue = 3;
            startTime = Time.time;
            playAudio.clip = audioClip[2];
            playAudio.Play();
        }
        //Caption  & SFX Manager during the third Queue
        //"These sheaves and towers take high, uneven forces."                              //Cap [4]
        //"Without these, the dynamics would result in erratic movements of the bucket."    //Cap [5]
        if (queue == 3)
        {
            if (playAudio.isPlaying == true)
            {
                DL_SFX.highlighting.boomSheave.status = Status.Flash;
                DL_SFX.highlighting.deflectionTowers.status = Status.Flash;
                fairleadHighlighter.fairleadStatus.status = Status.Flash;
                if (playTime < 5.7)
                {
                    captionText.text = captions[4];
                }
                if (playTime > 5.7)
                {
                    captionText.text = captions[5];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                DL_SFX.highlighting.boomSheave.status = Status.Off;
                DL_SFX.highlighting.deflectionTowers.status = Status.Off;
                fairleadHighlighter.fairleadStatus.status = Status.Off;
                captionText.text = "";
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = true;
            }
        }
    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
