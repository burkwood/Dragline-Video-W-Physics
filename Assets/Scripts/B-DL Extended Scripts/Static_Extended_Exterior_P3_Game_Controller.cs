﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Static_Extended_Exterior_P3_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClip;                                                                                               //Audio Player
    Bucyrus_DL_SFX DL_SFX;                                                                                                  //The script that controls all hiding and highlighting
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    Obi_Bucyrus_Controller obiBucyrusController;
    Operator_Chair_Controller operatorChairController;
    TrailCableFinder trailCableController;
    FairleadHighlighter fairleadHighlighter;

    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "What does this mean to the operator while digging?",                                        //Cap [0]
        "Tightlining is one of the biggest concerns during operation.",                              //Cap [1]
        "Tightline occurs when hoist and drag ropes are pulled to their maximum limits.",          //Cap [2]
        "This draws the bucket towards the boom.",                                                   //Cap [3]
        "It creates high forces on the gearing systems, and the boom structure.",                    //Cap [4]
        "To prevent this, a tightline window is established with electrical limits.",                //Cap [5]
        "It prevents the operator from maneuvering the bucket too close to tightline condition.",    //Cap [6]
        "Limits prevent the bucket from interfering with the fairleads or point sheaves.",           //Cap [7]
	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Bucyrus_DL_SFX>();
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                                                      //Get reference to Fade Screen Controller
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();
        trailCableController = gameObject.GetComponent<TrailCableFinder>();
        fairleadHighlighter = gameObject.GetComponent<FairleadHighlighter>();
        playAudio = GetComponent<AudioSource>();                                                                           //Get reference to the Audio Source
        playAudio.clip = audioClip[0];                                                                                         //Load the first clip
        captionText.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();
        #region Scene_Setup
        //******************************************************************************************************************************
        //Scene Setup
        //No Camera Movement, wait for the system to stabilize
        //start with black screen
        if (queue == 0)
        {
            //Fade Screen, show fade text, wait for stable
            if (Time.time < 5)
            {
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.showFadeText = false;
            }
            //Unfade Screen and hide text
            if (Time.time > 5)
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
            }
        }
        #endregion
        #region First_Queue
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 7)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //"What does this mean to the operator while digging?"                           //Cap [0]
        //"Tightlining is one of the biggest concerns during operation."                 //Cap [1]
        //"Tightlining occurs when hoist and drag ropes are pulled to their maximum limits."
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            //Show captions
            if (playAudio.isPlaying == true)
            {
                if (playTime < 5)
                {
                    captionText.text = captions[0];
                }
                if (playTime > 5 && playTime < 11)
                {
                    captionText.text = captions[1];
                }
                if (playTime > 11)
                {
                    captionText.text = captions[2];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        //******************************************************************************************************************************
        //Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && captionText.text == "" && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClip[1];
            playAudio.Play();
        }
        //Caption & SFX Manager during the second Queue
        //"This draws the bucket towards the boom."                                                   //Cap [3]
        //"It creates high forces on the gearing systems, and the boom structure."                    //Cap [4]
        //"To prevent this, a tightline window is established with electrical limits."                //Cap [5]
        //"It prevents the operator from maneuvering the bucket too close to tightline condition."    //Cap [6]
        //"Limits prevent the bucket from interfering with the fairleads or point sheaves."           //Cap [7]
        if (queue == 2)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 4.3)
                {
                    captionText.text = captions[3];
                }
                if (playTime > 4.3 && playTime < 11)
                {
                    captionText.text = captions[4];
                }
                if (playTime > 11 && playTime < 18)
                {
                    captionText.text = captions[5];
                }
                if (playTime > 18 && playTime < 25)
                {
                    captionText.text = captions[6];
                }
                if (playTime > 25)
                {
                    captionText.text = captions[7];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length)
            {
                captionText.text = "";
                fadeScreenController.showFadeScreen = true;
            }
        }
    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
