﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Extended_Interior_HD_Game_Controller : MonoBehaviour
{
    Interior_Extended_DL_SFX iDLSFX;
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    RopeHighlighter ropeHighlighter;
    ImageController imageController;
    Camera_Zoom camZoom;

    public int queue = 0;																										//The index of text queues
	public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public float runTime;
    public AudioClip[] audioClips;
    private AudioSource playAudio;
    public float startTime;
    public float delayTime = 2f;
    public float playTime;
    public Text captionText;
    private bool audioTriggered = false;
    private string[] captions = new string[]
    {
        "The incoming electricity powers synchronous AC motors.",                                       //Cap [0]
        "These motors power the DC generators keeping a constant shaft speed, usually 1200RPM.",        //Cap [1]
        "The dragline has a total of 12 generators, 4 for hoist, 4 for drag and 4 for swing.",          //Cap [2]
        "The goal of the synch motors is to keep the shafts turning at constant speed.",                //Cap [3]
        "When the dragline moves, the necessary load tries to burden the generators,",                  //Cap [4]
        "but the synch motors ensure constant shaft speed, regardless of load.",                        //Cap [5]
        "Both the hoist and drag systems are arranged in 2 control loops:",                             //Cap [6]
        "a master loop, and a slave loop.",                                                             //Cap [7]
        "Each loop contains 2 motors and 2 generators.",                                                //Cap [8]
        "The generators provide DC voltage to the motors, based upon the operator controls.",           //Cap [9]
        "Moving the controls further from the neutral increases the output voltage.",                   //Cap [10]
        "An increase in output voltage increases motor speed.",                                         //Cap [11]
        "The drum rotation direction can be clockwise or counterclockwise,",                            //Cap [12]
        "depending on either positive or negative voltage output.",                                     //Cap [13]
        "To raise the bucket or pay in,",                                                               //Cap [14]
        "the respective generators provide a positive voltage,",                                        //Cap [15]
        "to either hoist motors or the drag motors.",                                                   //Cap [16]
        "Each of these motors rotate at the same speed,",                                               //Cap [17]
        "providing power to the gearbox via a high speed pinion and shaft.",                            //Cap [18]
        "M1 and M2 are tied to a single generator loop,",                                               //Cap [19]
        "because it's critical that they operate at the same voltage.",                                 //Cap [20]
        "This ensures identical rotational timing and speed across each motor pinion.",                 //Cap [21]
        "An imbalanced voltage would result in the motors turning at different speeds,",                //Cap [22]
        "resulting in an interference in the gearbox.",                                                 //Cap [23]
        "Each motor has a set of brakes, either drum brakes, or disc and caliper brakes.",              //Cap [24]
        "The brakes are intended to lockout motion of the motor shafts.",                               //Cap [25]
        "The brakes are hydraulically or pneumatically operated.",                                      //Cap [26]
        "They're designed that in the event of a power loss,",                                          //Cap [27]
        "the brakes automatically engage, stopping motion.",                                            //Cap [28]
        "Brakes normally want to be closed,",                                                           //Cap [29]
        "and a pneumatic or hydraulic force is needed to keep them open.",                              //Cap [30]
        "This is a designed safety mechanism,",                                                         //Cap [31]
        "locking motion in the event of a power failure.",                                              //Cap [32]
        "The hoist and drag gearboxes are comprised of a series of gears.",                             //Cap [33]
        "Gearing reduction is needed to convert high speed output shaft of the drag motors.",           //Cap [34]
        "Low speed torque is needed to rotate the drum and move the bucket.",                           //Cap [35]
        "Most gears are induction case-hardened.",                                                      //Cap [36]
        "This helps provide a strong exterior to the gear teeth,",                                      //Cap [37]
        "while still maintaining a softer, more flexible core structure.",                              //Cap [38]
        "To rotate the drum, four spur-pinions turn two larger, intermediate gears.",                   //Cap [39]
        "The 1st stage of the drag gearing operates at high speed.",                                    //Cap [40]
        "Because of this speed, the gears are case enclosed and oil lubricated.",                       //Cap [41]
        "It's important that the oil level and temperatures are maintained.",                           //Cap [42]
        "This is to prevent overheating and premature component failure.",                              //Cap [43]
        "2 intermediate gears rotate an intermediate shaft, each with a herringbone pinion.",           //Cap [44]
        "These pinions turn the larger bull gear.",                                                     //Cap [45]
        "The bull gear is directly connected to the drag drum, rotating the drum.",                     //Cap [46]
        "The herringbone teeth cut is necessary to prevent lateral thrust loads.",                      //Cap [47]
        "It allows for maximum delivery of torque to the drag drum.",                                   //Cap [48]
        "Low speed gears are lubricated with an open-gear type lubrication.",                           //Cap [49]
        "The higher viscosity grease is needed because the gears see higher force at lower speed.",     //Cap [50]
        "The output of the gearbox provides a low speed, high power turning force.",                    //Cap [51]
        "This is needed to turn the drum either direction.",                                            //Cap [52]
        "Drum torque must be greater than the force of the bucket weight and gravity.",                 //Cap [53]
        "The necessary torque can exceed 300,000lbs.",                                                  //Cap [54]
        "Drums are made of flame hardened steel.",                                                      //Cap [55]
        "This helps resist the frictional wear between the rope and drum lagging.",                     //Cap [56]
        "The hoist and drag ropes are secured to the drums via clamps.",                                //Cap [57]
        "Several wraps around the drum is required to prevent the ropes from slipping.",                //Cap [58]
        "Hoist and drag ropes are wire ropes called IWRC (Independent Wire Rope Core).",                //Cap [59]
        "This rope type allows for bending, both around the drum and general operation.",               //Cap [60]
        "There are 2 ropes for each hoist and drag.",                                                   //Cap [61]
        "Each rope has a different lay (right-hand lay vs left-hand lay).",                             //Cap [62]
        "This allows for proper winding around the drum.",                                              //Cap [63]
        "Wire ropes are designed for high tensile loads,",                                              //Cap [64]
        "without exhibiting excessive stretch or failure.",                                             //Cap [65]
        "Most wire ropes fail due to excessive bending and surface wear.",                              //Cap [66]
        "Generally, while hoist ropes experience higher forces, drag ropes experience higher wear.",    //Cap [67]
    };

	// Use this for initialization
	void Start ()
    {
        iDLSFX = gameObject.GetComponent<Interior_Extended_DL_SFX>();
        fadeScreenController = gameObject.GetComponent<Fade_Screen_Controller>();
        virtualJoystickController = gameObject.GetComponent<Virtual_Joystick_Controller>();
        ropeHighlighter = gameObject.GetComponent<RopeHighlighter>();
        imageController = gameObject.GetComponent<ImageController>();
        camZoom = gameObject.GetComponent<Camera_Zoom>();
        captionText.text = "";
        playAudio = GetComponent<AudioSource>();
        playAudio.clip = audioClips[0];
        iDLSFX.hoistDispOn = false;
        iDLSFX.dragDispOn = false;
	}
	
	// Update is called once per frame
	void Update ()
    {
        runTime = Time.time;
        UpdatePlayTime();
        #region Scene_Setup
        //******************************************************************************************************************************
        //Scene Setup
        if (queue == 0)
        {
            //Trigger Fade screen
            if (Time.time < 5)
            {
                fadeScreenController.fadeMessage = "Dragline interior";
                fadeScreenController.showFadeScreen = true;
                fadeScreenController.showFadeText = true;
            }
            if (Time.time > 5 )
            {
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = false;
                fadeScreenController.fadeMessage = "";
            }
        }
        #endregion
        #region First_Queue
        //******************************************************************************************************************************
        //Start the first Queue 
        //No Camera Movement
        if (queue == 0 && Time.time > 6)
        {
            queue = 1;
            startTime = Time.time;
        }
        //Caption & SFX Manager during the first Queue
        //The incoming electricity powers synchronous AC motors.                                       //Cap [0]
        //These motors power the DC generators, maintaining a constant shaft speed, usually 1200RPM.   //Cap [1]
        //The dragline has a total of 12 generators, 4 for hoist, 4 for drag and 4 for swing.          //Cap [2]
        //The goal of the synch motors is to keep the shafts turning at constant speed.                //Cap [3]
        //When the dragline moves, the necessary load tries to burden the generators,                  //Cap [4]
        //but the synch motors ensure constant shaft speed, regardless of load.                        //Cap [5]
        if (queue == 1 && Time.time > startTime + delayTime)
        {
            //Play first clip
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            
            if (playAudio.isPlaying == true)
            {
                //Caption Section
                if (playTime < 5.7)
                {
                    captionText.text = captions[0];
                }
                if (playTime > 5.7 && playTime < 15.5)
                {
                    captionText.text = captions[1];
                }
                if (playTime > 15.5 && playTime < 28.9)
                {
                    captionText.text = captions[2];
                }
                if (playTime > 28.9 && playTime < 35.6)
                {
                    captionText.text = captions[3];
                }
                if (playTime > 35.6 && playTime < 43)
                {
                    captionText.text = captions[4];
                }
                if (playTime > 43)
                {
                    captionText.text = captions[5];
                }
                //SFX Section
                //Flash the Sync motors
                if (playTime > 3.5 && playTime < 4)
                {
                    iDLSFX.syncMotorStatus = Status.Flash;
                }
                //Turn off sync and flash all generators
                if (playTime > 8.5 && playTime < 15.5 )
                {
                    iDLSFX.syncMotorStatus = Status.Off;
                    iDLSFX.innerGen.flashStatus = IntPublicClass.Status.Flash;
                    iDLSFX.middleGen.flashStatus = IntPublicClass.Status.Flash;
                    iDLSFX.outerGen.flashStatus = IntPublicClass.Status.Flash;
                }
                //Turn off all generators
                if (playTime > 15.5 && playTime < 18.9)
                {
                    iDLSFX.innerGen.flashStatus = IntPublicClass.Status.Off;
                    iDLSFX.middleGen.flashStatus = IntPublicClass.Status.Off;
                    iDLSFX.outerGen.flashStatus = IntPublicClass.Status.Off;
                }
                //Flash the Hoist Generators
                if (playTime > 20 && playTime < 22 )
                {
                    iDLSFX.innerGen.flashStatus = IntPublicClass.Status.Flash;
                }
                //Turn off the hoist generators
                if (playTime > 22 && playTime < 23 )
                {
                    iDLSFX.innerGen.flashStatus = IntPublicClass.Status.Off;
                }
                //Flash the Drag generators
                if (playTime > 23 && playTime < 25)
                {
                    iDLSFX.middleGen.flashStatus = IntPublicClass.Status.Flash;
                }
                //Turn off the Drag Generators
                if (playTime > 25 && playTime < 25.5)
                {
                    iDLSFX.middleGen.flashStatus = IntPublicClass.Status.Off;
                }
                //Flash the Swing generators
                if (playTime > 26.8 && playTime < 29)
                {
                    iDLSFX.outerGen.flashStatus = IntPublicClass.Status.Flash;
                }
                //Turn off the Swing generators
                if (playTime > 29 && playTime < 29.5)
                {
                    iDLSFX.outerGen.flashStatus = IntPublicClass.Status.Off;
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Second_Queue
        //******************************************************************************************************************************
        //Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && Time.time > startTime + delayTime + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClips[1];      //Load clip 006
            playAudio.Play();
        }
        //Caption & SFX Manager during the second Queue
        //Both the hoist and drag systems are arranged in 2 control loops: //Cap [6]
        //a master loop, and a slave loop.                                 //Cap [7]
        //Each loop contains 2 motors and 2 generators.                    //Cap [8]
        if (queue == 2)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 6.1)
                {
                    captionText.text = captions[6];
                }
                if (playTime > 6.1 && playTime < 10.9)
                {
                    captionText.text = captions[7];
                }
                if (playTime > 10.9)
                {
                    captionText.text = captions[8];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + delayTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Third_Queue
        //******************************************************************************************************************************
        //Start the third Queue 
        //camers move from motor view to default view
        if (queue == 2 && nextStage == false && captionText.text == "")
        {
            nextStage = true;   //Trigger camera movement
            queue = 3;
            startTime = Time.time + 7;
        }
        //Caption & SFX Manager during the third Queue
        //The generators provide DC voltage to the motors, based upon the operator controls.  //Cap [9]
        //Moving the controls further from the neutral increases the output voltage.          //Cap [10]
        //An increase in output voltage increases motor speed.                                //Cap [11]
        //The drum rotation can be clockwise or counterclockwise,                             //Cap [12]
        //depending on either positive or negative voltage output.                            //Cap [13]
        if (queue == 3 && nextStage == false && Time.time > startTime)
        {
            if (audioTriggered == false)
            {
                playAudio.clip = audioClips[2]; //Load audio clip 7
                playAudio.Play();
                audioTriggered = true;
            }
            if (playAudio.isPlaying == true)
            {
                //Caption Section
                if (playTime < 8)
                {
                    captionText.text = captions[9];
                }
                if (playTime > 8 && playTime < 14.5)
                {
                    captionText.text = captions[10];
                }
                if (playTime > 14.5 && playTime < 20)
                {
                    captionText.text = captions[11];
                }
                if (playTime > 20 && playTime < 26.9)
                {
                    captionText.text = captions[12];
                }
                if (playTime > 26.9)
                {
                    captionText.text = captions[13];
                }
                //SFX Section
                //move joystick controls, flash motors, start vibrate, start payout
                if (playTime > 6 && playTime < 7)
                {
                    virtualJoystickController.leftJoyVert = vJoyMove.Up;
                    virtualJoystickController.leftJoyHighlight = Status.Flash;
                    iDLSFX.dragMotorStatus = Status.Flash;
                    iDLSFX.dragMotorVibrate = true;
                    iDLSFX.dragMCtrl = MotorControl.Extend;
                }
                //Increase magnitude for 4 secs
                if (playTime > 7 && playTime < 11)
                {
                    iDLSFX.motorVibeMagnitude = new Vector3(0.05f, 0.05f, 0.05f);
                }
                //tone down vibration move joystick to neutral
                if (playTime > 11 && playTime < 12)
                {
                    virtualJoystickController.leftJoyVert = vJoyMove.None;
                    iDLSFX.motorVibeMagnitude = new Vector3(0.01f, 0.01f, 0.01f);
                    iDLSFX.dragMCtrl = MotorControl.None;
                }
                //Turn off vib.
                if (playTime > 12 && playTime < 13)
                {
                    iDLSFX.dragMotorVibrate = false;
                }
                //reverse direction
                if (playTime > 20 && playTime < 21)
                {
                    virtualJoystickController.leftJoyVert = vJoyMove.Down;
                    iDLSFX.dragMotorVibrate = true;
                    iDLSFX.dragMCtrl = MotorControl.Retract;
                }
                //increase mag for 3 secs
                if (playTime > 21 && playTime < 24)
                {
                    iDLSFX.motorVibeMagnitude = new Vector3(0.05f, 0.05f, 0.05f);
                }
                //tone down vib and move stick to neutral
                if (playTime > 24 && playTime < 25)
                {
                    virtualJoystickController.leftJoyVert = vJoyMove.None;
                    iDLSFX.motorVibeMagnitude = new Vector3(0.01f, 0.01f, 0.01f);
                    iDLSFX.dragMCtrl = MotorControl.None;
                }
                //Turn off vib.
                if (playTime > 25 && playTime < 26)
                {
                    iDLSFX.dragMotorVibrate = false;
                    virtualJoystickController.leftJoyHighlight = Status.Off;
                    iDLSFX.dragMotorStatus = Status.Off;
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Forth_Queue
        //******************************************************************************************************************************
        //Start the forth Queue 
        //No Camera Movement
        if (queue == 3 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 4;
            startTime = Time.time;
            audioTriggered = false;
            playAudio.clip = audioClips[3];      //Load clip 008
            playAudio.Play();
        }
        //Caption & SFX Manager during the Forth Queue
        //To raise the bucket or pay in,                                  //Cap [14]
        //the 2 respective generators provide a positive voltage,         //Cap [15]
        //to the 4 hoist motors of the 4 drag motors.                     //Cap [16]
        if (queue == 4)
        {
            if (playAudio.isPlaying == true)
            {
                //Caption Section
                if (playTime <4)
                {
                    captionText.text = captions[14];
                }
                if (playTime > 4 && playTime < 9.5)
                {
                    captionText.text = captions[15];
                }
                if (playTime > 9.5)
                {
                    captionText.text = captions[16];
                }
                //SFX section
                //Flash the Hoist Motors
                if (playTime > 10 && playTime < 11)
                {
                    iDLSFX.hoistMotorStatus = Status.Flash;
                }
                //Flash the drag motors
                if (playTime > 11)
                {
                    iDLSFX.hoistMotorStatus = Status.Off;
                    iDLSFX.dragMotorStatus = Status.Flash;
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                iDLSFX.dragMotorStatus = Status.Off;
            }
        }
        #endregion
        #region Fifth_Queue
        //******************************************************************************************************************************
        //Start the fifth Queue 
        //No Camera Movement
        if (queue == 4 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 5;
            startTime = Time.time;
            playAudio.clip = audioClips[4];      //Load clip 009
            playAudio.Play();
        }
        //Caption & SFX Manager during the Fifth Queue
        //Each of these motors rotate at the same speed,                                       //Cap [17]
        //providing power to the gearbox via a high speed pinion and shaft.                    //Cap [18]
        //M1 and M2 are tied to a single generator loop,                                       //Cap [19]
        //because it's critical that they operate at the same voltage.                         //Cap [20]
        //This ensures identical rotational timing and speed across each motor pinion.         //Cap [21]
        //An imbalanced voltage would result in the motors turning at different speeds,        //Cap [22]
        //resulting in an interference in the gearbox.                                         //Cap [23]
        if (queue == 5)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 5)
                {
                    captionText.text = captions[17];
                }
                if (playTime> 5 && playTime< 11.4)
                {
                    captionText.text = captions[18];
                }
                if (playTime > 11.4 && playTime< 16.8)
                {
                    captionText.text = captions[19];
                }
                if (playTime > 16.8 && playTime < 22.4)
                {
                    captionText.text = captions[20];
                }
                if (playTime> 22.4 && playTime< 29.4)
                {
                    captionText.text = captions[21];
                }
                if (playTime > 29.4 && playTime < 35.8)
                {
                    captionText.text = captions[22];
                }
                if (playTime > 35.8)
                {
                    captionText.text = captions[23];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Sixth_Queue
        //******************************************************************************************************************************
        //Start the sixth Queue
        //No camera movement
        if (queue == 5 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 6;
            startTime = Time.time;
            playAudio.clip = audioClips[5];      //Load clip 010
            playAudio.Play();
        }
        //Caption & SFX Manager during the Fifth Queue
        //Each motor has a set of brakes, either drum brakes, or disc and caliper brakes.  //Cap [24]
        if (queue == 6)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[24];
                //SFX Section
                if (playTime > 2)
                {
                    iDLSFX.dragMotorBreakStatus = Status.Flash;
                    imageController.imageStatus[0] = true;
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                imageController.imageStatus[0] = false;
                iDLSFX.dragMotorBreakStatus = Status.Off;
                captionText.text = "";
            }
        }
        #endregion
        #region Seventh_Queue
        //******************************************************************************************************************************
        //Start the seventh Queue
        //No Camera Movement
        if (queue == 6 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + imageController.screenScaleTime)
        {
            queue = 7;
            startTime = Time.time;
            playAudio.clip = audioClips[6];     //Load Clip 011
            playAudio.Play();
        }
        //Caption & SFX Manager during the Sixth Queue
        //The brakes are intended to lockout motion of the motor shafts.        //Cap [25]
        //The brakes are hydraulically or pneumatically operated.               //Cap [26]
        //They're designed that in the event of a power loss,                   //Cap [27]
        //the brakes automatically engage, stopping motion.                     //Cap [28]
        //Brakes normally want to be closed,                                    //Cap [29]
        //and a pneumatic or hydraulic force is needed to keep them open.       //Cap [30]
        //This is a designed safety mechanism,                                  //Cap [31]
        //locking motion in the event of a power failure.                       //Cap [32]
        if (queue == 7)
        {
            if (playAudio.isPlaying == true)
            {
                //Caption Section
                if (playTime < 5.7)
                {
                    captionText.text = captions[25];
                }
                if (playTime > 5.7 && playTime < 11)
                {
                    captionText.text = captions[26];
                }
                if (playTime > 11 && playTime < 15.8)
                {
                    captionText.text = captions[27];
                }
                if (playTime > 15.8 && playTime < 21.5)
                {
                    captionText.text = captions[28];
                }
                if (playTime > 21.5 && playTime < 25.7)
                {
                    captionText.text = captions[29];
                }
                if (playTime > 25.7 && playTime < 31.7)
                {
                    captionText.text = captions[30];
                }
                if (playTime > 31.7 && playTime < 36.3)
                {
                    captionText.text = captions[31];
                }
                if (playTime > 36.3)
                {
                    captionText.text = captions[32];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Eighth_Queue
        //******************************************************************************************************************************
        //Start the eighth Queue
        //No Camera Movement
        if (queue == 7 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 8;
            startTime = Time.time;
            playAudio.clip = audioClips[7];     //Load Clip 012
            playAudio.Play();
        }
        //Caption & SFX Manager during the Eighth Queue
        //The hoist and drag gearboxes are comprised of a series of gears.   //Cap [33]
        if (queue == 8)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[33];
                iDLSFX.hoistGearboxStatus = Status.Flash;
                iDLSFX.dragGearboxStatus = Status.Flash;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                iDLSFX.hoistGearboxStatus = Status.Off;
                iDLSFX.dragGearboxStatus = Status.Off;
            }
        }
        #endregion
        #region Ninth_Queue
        //******************************************************************************************************************************
        //Start the ninth Queue
        //No Camera Movement
        if (queue == 8 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 9;
            startTime = Time.time;
            playAudio.clip = audioClips[8];     //Load Clip 013
            playAudio.Play();
        }
        //Caption & SFX Manager during the ninth Queue
        //Gearing reduction is needed to convert high speed output shaft of the drag motors.    //Cap [34]
        //Low speed torque is needed to rotate the drum and move the bucket.                    //Cap [35]
        if (queue == 9)
        {
            if (playAudio.isPlaying == true)
            {
                //Captions Section
                if (playTime < 7.1)
                {
                    captionText.text = captions[34];
                }
                if (playTime > 7.1)
                {
                    captionText.text = captions[35];
                }
                //SFX Section
                iDLSFX.allScreens.dragScreen.open = true;
                iDLSFX.allScreens.dragScreen.play = true;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                iDLSFX.allScreens.dragScreen.open = false;
                iDLSFX.allScreens.dragScreen.play = false;
            }
        }
        #endregion
        #region Tenth_Queue
        //******************************************************************************************************************************
        //Start the Tenth Queue
        //No Camera Movement
        if (queue == 9 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + iDLSFX.screenScaleTime)
        {
            queue = 10;
            startTime = Time.time;
            playAudio.clip = audioClips[9];     //Load Clip 014
            playAudio.Play();
        }
        //Caption & SFX Manager during the tenth Queue
        //Most gears are induction case-hardened.        //Cap [36]
        if (queue == 10)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[36];
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Eleventh_Queue
        //******************************************************************************************************************************
        //Start the Eleventh Queue
        //No Camera Movement
        if (queue == 10 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 11;
            startTime = Time.time;
            playAudio.clip = audioClips[10];     //Load Clip 015
            playAudio.Play();
        }
        //Caption & SFX Manager during the eleventh Queue
        //This helps provide a strong exterior to the gear teeth,            //Cap [37]
        //while still maintaining a softer, more flexible core structure.    //Cap [38]
        if (queue == 11)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 5.4)
                {
                    captionText.text = captions[37];
                }
                if (playTime > 5.4)
                {
                    captionText.text = captions[38];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Twelfth_Queue
        //******************************************************************************************************************************
        //Start the Twelfth Queue
        //No Camera Movement
        if (queue == 11 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 12;
            startTime = Time.time;
            playAudio.clip = audioClips[11];     //Load Clip 016
            playAudio.Play();
        }
        //Caption & SFX Manager during the twelfth Queue
        //To rotate the drum, four spur-pinions turn two larger, intermediate gears.   //Cap [39]
        if (queue == 12)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[39];
                imageController.imageStatus[1] = true;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                imageController.imageStatus[1] = false;
            }
        }
        #endregion
        #region Thirteenth_Queue
        //******************************************************************************************************************************
        //Start the Thirteenth Queue
        //No Camera Movement
        if (queue == 12 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 13;
            startTime = Time.time;
            playAudio.clip = audioClips[12];     //Load Clip 017
            playAudio.Play();
        }
        //Caption & SFX Manager during the thirteenth Queue
        //The 1st stage of the drag gearing operates at high speed.                  //Cap [40]
        //Because of this speed, the gears are case enclosed and oil lubricated.     //Cap [41]
        //It's important that the oil level and temperatures are maintained.         //Cap [42]
        //This is to prevent overheating and premature component failure.            //Cap [43]
        if (queue == 13)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 6)
                {
                    captionText.text = captions[40];
                }
                if (playTime > 6 && playTime < 12.6)
                {
                    captionText.text = captions[41];
                }
                if (playTime > 12.6 && playTime < 18.7)
                {
                    captionText.text = captions[42];
                }
                if (playTime > 18.7)
                {
                    captionText.text = captions[43];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Fourteenth_Queue
        //******************************************************************************************************************************
        //Start the Fourteenth Queue
        //No Camera Movement
        if (queue == 13 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 14;
            startTime = Time.time;
            playAudio.clip = audioClips[13];     //Load Clip 018
            playAudio.Play();
        }
        //Caption & SFX Manager during the fourteenth Queue
        //2 intermediate gears rotate an intermediate shaft, each with a herringbone pinion.   //Cap [44]
        if (queue == 14)
        {
            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[44];
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Fifteenth_Queue
        //******************************************************************************************************************************
        //Start the Fifteenth Queue
        //No Camera Movement
        if (queue == 14 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 15;
            startTime = Time.time;
            playAudio.clip = audioClips[14];     //Load Clip 019
            playAudio.Play();
        }
        //Caption & SFX Manager during the fifteenth Queue
        //These pinions turn the larger bull gear.                                      //Cap [45]
        //The bull gear is directly connected to the drag drum, rotating the drum.      //Cap [46]
        if (queue == 15)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 4.9)
                {
                    captionText.text = captions[45];
                }
                if (playTime > 4.9)
                {
                    captionText.text = captions[46];
                }
                imageController.imageStatus[2] = true;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                imageController.imageStatus[2] = false;     //Hide bull gear
            }
        }
        #endregion
        #region Sixteenth_Queue
        //******************************************************************************************************************************
        //Start the Sixteenth Queue
        //No Camera Movement
        if (queue == 15 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + imageController.screenScaleTime)
        {
            queue = 16;
            startTime = Time.time;
            playAudio.clip = audioClips[15];     //Load Clip 020
            playAudio.Play();
        }
        //Caption & SFX Manager during the sixteenth Queue
        //The herringbone teeth cut is necessary to prevent lateral thrust loads.       //Cap [47]
        //It allows for maximum of torque to the drag drum.                             //Cap [48]
        //6.7
        if (queue == 16)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 6.7)
                {
                    captionText.text = captions[47];
                }
                if (playTime > 6.7)
                {
                    captionText.text = captions[48];
                }
                imageController.imageStatus[3] = true;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                imageController.imageStatus[3] = false;     //Hide herringbone
            }
        }
        #endregion
        #region Seventeenth_Queue
        //******************************************************************************************************************************
        //Start the Seventeenth Queue
        //No Camera Movement
        if (queue == 16 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + imageController.screenScaleTime)
        {
            queue = 17;
            startTime = Time.time;
            playAudio.clip = audioClips[16];     //Load Clip 021
            playAudio.Play();
        }
        //Caption & SFX Manager during the seventeenth Queue
        //Low speed gears are lubricated with an open-gear type lubrication.                           //Cap [49]
        //The higher viscosity grease is needed because the gears see higher force at lower speed.     //Cap [50]
        //6.2
        if (queue == 17)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 6.2)
                {
                    captionText.text = captions[49];
                }
                if (playTime > 6.2)
                {
                    captionText.text = captions[50];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Eighteenth_Queue
        //******************************************************************************************************************************
        //Start the Eighteenth Queue
        //No Camera Movement
        if (queue == 17 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 18;
            startTime = Time.time;
            playAudio.clip = audioClips[17];     //Load Clip 022
            playAudio.Play();
        }
        //Caption & SFX Manager during the eighteenth Queue
        //The output of the gearbox provides a low speed, high power turning force.      //Cap [51]
        //This is needed to turn the drum either direction.                              //Cap [52]
        //Drum torque must be greater than the force of the bucket weight and gravity.   //Cap [53]
        //The necessary torque can exceed 300,000lbs.                                    //Cap [54]
        if (queue == 18)
        {
            if (playAudio.isPlaying == true)
            {
                iDLSFX.dragDrumStatus = Status.Flash;
                iDLSFX.hoistDrumStatus = Status.Flash;
                if (playTime < 7.1)
                {
                    captionText.text = captions[51];
                }
                if (playTime > 7.1 && playTime < 12.2)
                {
                    captionText.text = captions[52];
                }
                if (playTime > 12.2 && playTime < 18.5)
                {
                    captionText.text = captions[53];
                }
                if (playTime > 18.5)
                {
                    captionText.text = captions[54];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                iDLSFX.dragDrumStatus = Status.Off;
                iDLSFX.hoistDrumStatus = Status.Off;
            }
        }
        #endregion
        #region Queue_Nineteen
        //******************************************************************************************************************************
        //Start the Nineteenth Queue
        //No Camera Movement
        if (queue == 18 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 19;
            startTime = Time.time;
            playAudio.clip = audioClips[18];     //Load Clip 023
            playAudio.Play();
        }
        //Caption & SFX Manager during the nineteenth Queue
        //Drums are made of flame hardened steel.                                   //Cap [55]
        //This helps resist the frictional wear between the rope and drum lagging.  //Cap [56]
        if (queue == 19)
        {
            if (playAudio.isPlaying == true)
            {
                if (playTime < 4.8 )
                {
                    captionText.text = captions[55];
                }
                if (playTime > 4.8)
                {
                    captionText.text = captions[56];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
            }
        }
        #endregion
        #region Queue_Twenty
        //******************************************************************************************************************************
        //Start Queue Twenty
        //No Camera Movement
        if (queue == 19 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + delayTime)
        {
            queue = 20;
            startTime = Time.time;
            playAudio.clip = audioClips[19];     //Load Clip 024
            playAudio.Play();
        }
        //Caption & SFX Manager during the Queue Twenty
        //The hoist and drag ropes are secured to the drums via clamps.                    //Cap [57]
        //Several wraps around the drum is required to prevent the ropes from slipping.    //Cap [58]
        //6.1
        if (queue == 20)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 6.1)
                {
                    captionText.text = captions[57];
                }
                if (playTime > 6.1)
                {
                    captionText.text = captions[58];
                }
                camZoom.zoom = true;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                camZoom.zoom = false;
            }
        }
        #endregion
        #region Queue_TwentyOne
        //******************************************************************************************************************************
        //Start Queue TwentyOne
        //No Camera Movement
        if (queue == 20 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + 5f)
        {
            queue = 21;
            startTime = Time.time;
            playAudio.clip = audioClips[20];     //Load Clip 025
            playAudio.Play();
        }
        //Caption & SFX Manager during the Queue TwentyOne
        //Hoist and drag ropes are wire ropes called IWRC (Independent Wire Rope Core).    //Cap [59]
        if (queue == 21)
        {
            if (playAudio.isPlaying)
            {
                ropeHighlighter.dragRopeStatus.status = Status.Flash;
                ropeHighlighter.hoistRopeStatus.status = Status.Flash;
                imageController.imageStatus[4] = true;
                captionText.text = captions[59];
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                imageController.imageStatus[4] = false;
            }
        }
        #endregion
        #region Queue_TwentyTwo
        //******************************************************************************************************************************
        //Start Queue TwentyTwo
        //No Camera Movement
        if (queue == 21 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + imageController.screenScaleTime)
        {
            queue = 22;
            startTime = Time.time;
            playAudio.clip = audioClips[21];     //Load Clip 026
            playAudio.Play();
        }
        //Caption & SFX Manager during the Queue TwentyTwo
        //This rope type allows for bending, both around the drum and general operation.   //Cap [60]
        //There are 2 ropes for each hoist and drag.                                       //Cap [61]
        //Each rope has a different lay (right-hand lay vs left-hand lay).                 //Cap [62]
        //This allows for proper winding around the drum.                                  //Cap [63]
        if (queue == 22)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 7)
                {
                    captionText.text = captions[60];
                }
                if (playTime > 7 && playTime < 12.1)
                {
                    captionText.text = captions[61];
                }
                if (playTime > 12.1 && playTime < 18.8)
                {
                    captionText.text = captions[62];
                }
                if (playTime > 18.8)
                {
                    captionText.text = captions[63];
                }
                imageController.imageStatus[5] = true;
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                imageController.imageStatus[5] = false;
            }
        }
        #endregion
        #region Queue_TwentyThree
        //******************************************************************************************************************************
        //Start Queue TwentyThree
        //No Camera Movement
        if (queue == 22 && captionText.text == "" && Time.time > startTime + playAudio.clip.length + imageController.screenScaleTime)
        {
            queue = 23;
            startTime = Time.time;
            playAudio.clip = audioClips[22];     //Load Clip 027
            playAudio.Play();
        }
        //Caption & SFX Manager during the Queue TwentyThree
        //Wire ropes are designed for high tensile loads,                                              //Cap [64]
        //without exhibiting excessive stretch or failure.                                             //Cap [65]
        //Most wire ropes fail due to excessive bending and surface wear.                              //Cap [66]
        //Generally, while hoist ropes experience higher forces, drag ropes experience higher wear.    //Cap [67]
        if (queue == 23)
        {
            if (playAudio.isPlaying)
            {
                if (playTime < 5)
                {
                    captionText.text = captions[64];
                }
                if (playTime > 5 && playTime < 10.1)
                {
                    captionText.text = captions[65];
                }
                if (playTime > 10.1 && playTime < 16.5)
                {
                    captionText.text = captions[66];
                }
                if (playTime > 16.5)
                {
                    captionText.text = captions[67];
                }
            }
            if (playAudio.isPlaying == false && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";
                ropeHighlighter.dragRopeStatus.status = Status.Off;
                ropeHighlighter.hoistRopeStatus.status = Status.Off;
                fadeScreenController.showFadeText = false;
                fadeScreenController.showFadeScreen = true;
            }
        }
        #endregion
    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
