﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Extended_Interior_HD_Camera_Controller : MonoBehaviour
{
    public GameObject[] cameras;
    public string[] cameraNames;
    public int stage;
    public bool nextStage;
    public float startTime;
    public float delayTime = 5;
    public float elapsedTime;

    private GameObject mainCamera;
    private string[] cameraOrder = new string[] { "First Move", "Second Move", "Third Move", "Fourth Move", "Fifth Move" };

    // Use this for initialization
    void Start ()
    {
        cameras = GameObject.FindGameObjectsWithTag("VirtualCamera");                               //Find all virtual cameras
        cameraNames = new string[cameras.Length];
        for (int i = 0; i < cameras.Length; i++)                                                    //disable all virtual cameras and record names
        {
            cameras[i].SetActive(false);
            cameraNames[i] = cameras[i].name;
        }

        mainCamera = GameObject.FindWithTag("MainCamera");                                          //Find Main Camera
        mainCamera.transform.position = new Vector3(6.14f, 6.21f, -13.93f);                           //Set Main Camera starting transform
        mainCamera.transform.rotation = Quaternion.Euler(45.3f, 301.88f, 0f);                         //Set Main Camera starting rotation
        mainCamera.GetComponent<Camera>().fieldOfView = 60f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        GameObject gameControllerNextStage = GameObject.FindWithTag("GameController");
        nextStage = gameControllerNextStage.GetComponent<Extended_Interior_HD_Game_Controller>().nextStage;
        elapsedTime = Time.time;

        //Start first camera movement
        //Motor view to Default view
        if (stage == 0 && nextStage == true)
        {
            MoveCamera(0);
            startTime = Time.time;
            gameControllerNextStage.GetComponent<Extended_Interior_HD_Game_Controller>().nextStage = false;
            stage = 1;
        }
	}
    
    //******************************************************************************************************************************
    //Function Section
    //Trigger camera movement
    void MoveCamera(int num)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            if (cameraNames[i] == cameraOrder[num])
            {
                cameras[i].SetActive(true);
            }
        }
    }
}
