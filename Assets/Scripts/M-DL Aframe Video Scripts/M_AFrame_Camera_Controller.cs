﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class M_AFrame_Camera_Controller : MonoBehaviour
{
    public GameObject[] cameras;
    public string[] cameraNames;
    public int stage;
    public bool nextStage;                                                                          //Start next camera move flag
    public float startTime;                                                                         //Camera movement start time
    public float delayTime = 5;                                                                     //Minimum time between camera movements	
    public float elapsedTime;

    private GameObject mainCamera;
    private string[] cameraOrder = new string[] { "First Move", "Second Move", "Third Move", "Fourth Move", "Fifth Move" };
    // Use this for initialization
    void Start ()
    {
        cameras = GameObject.FindGameObjectsWithTag("VirtualCamera");                               //Find all virtual cameras
        cameraNames = new string[cameras.Length];
        for (int i = 0; i < cameras.Length; i++)                                                    //disable all virtual cameras and record names
        {
            cameras[i].SetActive(false);
            cameraNames[i] = cameras[i].name;
        }

        mainCamera = GameObject.FindWithTag("MainCamera");                                          //Find Main Camera
        mainCamera.transform.position = new Vector3(71.8f, 23.6f, 36.1f);                           //Set Main Camera starting transform
        mainCamera.transform.rotation = Quaternion.Euler(0f, 270f, 0f);                         //Set Main Camera starting rotation
        mainCamera.GetComponent<Camera>().fieldOfView = 60f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        GameObject gameControllerNextStage = GameObject.FindWithTag("GameController");
        nextStage = gameControllerNextStage.GetComponent<M_AFrame_Game_Controller>().nextStage;
        elapsedTime = Time.time;

        //Start First camera movement
        //Home to Side Angle Down
        if (stage == 0 && nextStage == true)
        {
            MoveCamera(0);
            startTime = Time.time;
            gameControllerNextStage.GetComponent<M_AFrame_Game_Controller>().nextStage = false;
            stage = 1;
        }

        //Start second camera movement
        //Side Angle Down to Home
        if (stage == 1 && Time.time > startTime + delayTime && nextStage == true)
        {
            MoveCamera(1);
            gameControllerNextStage.GetComponent<M_AFrame_Game_Controller>().nextStage = false;
            startTime = Time.time;
            stage = 2;

        }
    }

//******************************************************************************************************************************
    //Function Section
    //Trigger camera movement
    void MoveCamera(int num)
    {
        for (int i = 0; i < cameras.Length; i++)
        {
            if (cameraNames[i] == cameraOrder[num])
            {
                cameras[i].SetActive(true);
            }
        }
    }//MoveCamera end
}
