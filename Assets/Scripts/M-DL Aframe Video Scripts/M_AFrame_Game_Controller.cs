﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class M_AFrame_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClips;                                                                                                   //Audio Player
    Marion_DL_SFX DL_SFX;                                                                                                      //The script that controls all hiding and highlighting
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private float[] delayTimes = new float[] { 5, 5 };                                                                          //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "The A-frame is comprised of main structural members called legs.",														//Cap [0]

		"The legs are structural members mounted through to the floor of the revolving frame.",									//Cap [1]

		"The A-frame is held in tension with suspension ropes and link plates.",											//Cap [2]

		"The mast and boom are anchored by these components."																//Cap [3]

	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = GameObject.FindWithTag("GameController").GetComponent<Marion_DL_SFX>();                                       //Get reference to the SFX script
        playAudio = GetComponent<AudioSource>();                                                                                //Get Reference to the Audio Source
        playAudio.clip = audioClips[0];                                                                                          //Load the first Audio Clip
        captionText.text = "";
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();

 //******************************************************************************************************************************
        //Start the first Queue 
        //Camera moves from Home to Side Angle Down
        if (queue == 0 && nextStage == false && Time.time > delayTime)                                                              //START Queue 1 
        {
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 1;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the Camera Movement was triggered
                                                                                                                                    //			DL_SFX.showHide.aFrameLegs = false;																						//Hide A Frame Legs
        }

        //Caption & SFX Manager during the first Queue
        //"The A-frame mast is comprised of main structural members called legs." Cap [0]
        //Highlight Legs
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0])                                          //Start Playing Audio after camera has stopped moving
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }

            if (playAudio.isPlaying == true) { captionText.text = captions[0]; }                                                        //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 3.6)
            {
 
                DL_SFX.highlighting.aFrame.status = Status.Flash;                                                               //Highlight A-Frame
                DL_SFX.fadeManager.house.enabled = true;                                                                        //Fade House
                DL_SFX.fadeManager.hoistRopes.enabled = true;                                                                   //Fade Hoist Ropes
                DL_SFX.fadeManager.dragRopes.enabled = true;                                                                    //Fade Drag Ropes

            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[0] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
            }
        }

 //******************************************************************************************************************************
        // Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 2
        {
            playAudio.clip = audioClips[1];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 2;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the second Queue
        //"The legs are structural members mounted through to the floor of the revolving frame." Cap [1]
        //Highlight Legs, leg Lugs,  fade out house lights and cab till end************************************************************
        if (queue == 2)
        {
            if (playAudio.isPlaying == true) { captionText.text = captions[1]; }                                                    //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 2.1)
            {
                DL_SFX.highlighting.aFrameLugs.status = Status.Flash;                                                           //Highlight Leg Lugs

            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.aFrame.status = Status.Off;                                                                 //Unhilight A-frame Legs
                DL_SFX.highlighting.aFrameLugs.status = Status.Off;                                                             //Un-Highlight Leg Lugs
                DL_SFX.fadeManager.house.enabled = false;                                                                       //Un-fade House Body
                DL_SFX.fadeManager.hoistRopes.enabled = false;                                                                  //Un-fade Hoist Ropes
                DL_SFX.fadeManager.dragRopes.enabled = false;                                                                   //Un-fade Drag Ropes
            }

        }

 //******************************************************************************************************************************
        // Start the Third Queue
        //Camera moves from Side Angle Down to Home
        if (queue == 2 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "")
        {
            nextStage = true;                                                                                                       //trigger next camera movement
            playAudio.clip = audioClips[2];                                                                                              //change clip
            queue = 3;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the Camera Movement was triggered
        }

        //Caption  & SFX Manager during the Third stage
        //"The A-frame is held in tension with suspension ropes and link plates." 		Cap [2]
        //"The mast and boom are anchored by these components."	Cap [3]
        //Highlight Mast and Boom till end******************************************************************************************
        if (queue == 3 && nextStage == false && Time.time > startTime + delayTimes[1])                                              //Start playing audio after camera has stopped moving
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
            }
            if (playAudio.isPlaying == true && playTime < 6.8)
            {
                captionText.text = captions[2];                                                                                 //If audio clip is playing, show captions
            }
            if (playAudio.isPlaying == true && playTime > 2)
            {
                DL_SFX.highlighting.upSusRopes.status = Status.Flash;                                                               //Highlight Upper Suspension Ropes
                DL_SFX.highlighting.loSusRopes.status = Status.Flash;                                                               //Highlight Lower Suspension Ropes
            }

            if (playAudio.isPlaying == true && playTime > 6.8)
            {
                captionText.text = captions[3];
            }

            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.upSusRopes.status = Status.Off;                                                                 //Un-highlight Upper Suspension Ropes
                DL_SFX.highlighting.loSusRopes.status = Status.Off;                                                                 //Un-highlight Lower Suspension Ropes
            }
        }

    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
