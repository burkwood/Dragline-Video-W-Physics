﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class DumpRopeTensioner : MonoBehaviour
{
    //public GameObject dumpRope;
    public ObiRope rope;
    public ObiRopeCursor cursor;
    public float ropeLength;
    public float strain;
    public bool changeShorter = false;
    public bool changeLonger = false;
    [Range(0.0f, 5.0f)]
    public float minusLength = 0.0f;
    

	// Use this for initialization
	void Start ()
    {
        //Define Objects
        rope = gameObject.GetComponent<ObiRope>();
        cursor = gameObject.GetComponent<ObiRopeCursor>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        ropeLength = rope.RestLength;
        strain = rope.CalculateLength() / ropeLength;
        if (changeShorter)
        {
            cursor.ChangeLength(rope.RestLength - minusLength);
            changeShorter = false;
        }
        if (changeLonger)
        {
            cursor.ChangeLength(rope.RestLength + minusLength);
            changeLonger = false;
        }

    }
}
