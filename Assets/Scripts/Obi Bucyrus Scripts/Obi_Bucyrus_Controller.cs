﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;
using IntPublicClass;
//**************************************************COMMENT CODE!!!!!!!!!!!!!!!!!!!!!!!!!!!!!
public class Obi_Bucyrus_Controller : MonoBehaviour
{
    //GameObjects
    private GameObject houseBody;
    private ObiRopeCursor[] cursor = new ObiRopeCursor[4];
    private ObiRope[] rope = new ObiRope[4];
    private GameObject leftHoistRope;
    private GameObject rightHoistRope;
    private GameObject leftDragRope;
    private GameObject rightDragRope;
    private GameObject[] lowBoomSheaves;
    //Numeric Variables
    private float leftHoistRopeLength;
    private float rightHoistRopeLength;
    private float leftDragRopeLength;
    private float rightDragRopeLength;
    private float longestRopeLength;
    private bool balance = false;
    private bool hoistRopesTensioned = false;
    private int ropeArrayLength = 0;

    // Public Variables
    public bool keyboard = false;
    public float speed = 6f;
    public float rotateSpeed = 0.3f;
    public float sheaveRotateSpeed = 0.002f;
    public MotorControl hoistMotor;
    public MotorControl dragMotor;
    public SwingControl swingMotor;

    // Use this for initialization
    void Start ()
    {
        //Define Game Objects
        houseBody = GameObject.FindGameObjectWithTag("Dragline_Parent");
        houseBody = GameObject.FindGameObjectWithTag("House_Body");
        lowBoomSheaves = GameObject.FindGameObjectsWithTag("LowBoomSheaves");
        leftHoistRope = GameObject.FindWithTag("LeftHoistRope");
        rightHoistRope = GameObject.FindWithTag("RightHoistRope");
        leftDragRope = GameObject.FindWithTag("LeftDragRope");
        rightDragRope = GameObject.FindWithTag("RightDragRope");
        //Get Obi Rope Cursors
        cursor[0] = leftHoistRope.GetComponent<ObiRopeCursor>();
        cursor[1] = rightHoistRope.GetComponent<ObiRopeCursor>();
        cursor[2] = leftDragRope.GetComponent<ObiRopeCursor>();
        cursor[3] = rightDragRope.GetComponent<ObiRopeCursor>();
        //Get Obi Rope References
        for (int i = 0; i < rope.Length; i++)
        {
            rope[i] = cursor[i].GetComponent<ObiRope>();
        }
        //Get Rope Rest Lengths
        leftHoistRopeLength = rope[0].RestLength;
        rightHoistRopeLength = rope[1].RestLength;
        leftDragRopeLength = rope[2].RestLength;
        rightDragRopeLength = rope[3].RestLength;
    }
	
	// Update is called once per frame
	void Update ()
    {
        //Update Rope Lengths
        leftHoistRopeLength = rope[0].RestLength;
        rightHoistRopeLength = rope[1].RestLength;
        leftDragRopeLength = rope[2].RestLength;
        rightDragRopeLength = rope[3].RestLength;
        //Ensuer that the hoist ropes are the same length once but can be called again repeatedly
        if (!balance)
        {
            cursor[1].ChangeLength(leftHoistRopeLength);
            cursor[3].ChangeLength(leftDragRopeLength);
            balance = true;
        }
        //Tension Hoist Ropes to prevent bucket hitting the floor on startup
        if (!hoistRopesTensioned)
        {
            if (leftHoistRopeLength > 125 && rightHoistRopeLength > 125)
            {
                for (int i = 0; i < 2; i++)
                {
                    cursor[i].ChangeLength(rope[i].RestLength - speed * Time.deltaTime);
                }
            }
            else
            {
                hoistRopesTensioned = true;
            }
        }
        //********************************Movement Section******************************************
        if (keyboard == true)
        {
            if (Input.GetKey(KeyCode.S))
            {
                hoistMotor = MotorControl.Extend;
            }
            else if (Input.GetKey(KeyCode.W))
            {
                hoistMotor = MotorControl.Retract;
            }
            else
            {
                hoistMotor = MotorControl.None;
            }

            if (Input.GetKey(KeyCode.Q))
            {
                dragMotor = MotorControl.Extend;
            }
            else if (Input.GetKey(KeyCode.E))
            {
                dragMotor = MotorControl.Retract;
            }
            else
            {
                dragMotor = MotorControl.None;
            }

            if (Input.GetKey(KeyCode.A))
            {
                swingMotor = SwingControl.CCW;
            }
            else if (Input.GetKey(KeyCode.D))
            {
                swingMotor = SwingControl.CW;
            }
            else
            {
                swingMotor = SwingControl.None;
            }
        }
        
        
        
        //Hoist Rope Movement
        if (hoistMotor == MotorControl.Retract)
        {
            if (leftHoistRopeLength > 59 && rightHoistRopeLength > 59)
            {
                //Reduce Rope Length
                for (int i = 0; i < 2; i++)
                {
                    cursor[i].ChangeLength(rope[i].RestLength - speed * Time.deltaTime);
                }                               
            }
            //Rotate boom sheaves for visual effect
            for (int i = 0; i < lowBoomSheaves.Length; i++)
            {
                lowBoomSheaves[i].transform.Rotate(-Time.deltaTime / sheaveRotateSpeed, 0, 0);
            }
        }
        if (hoistMotor == MotorControl.Extend)
        {
            //Extend Rope Length
            for (int i = 0; i < 2; i++)
            {
                cursor[i].ChangeLength(rope[i].RestLength + speed * Time.deltaTime);
            }
            //Rotate Boom SHeaves for visual effect
            for (int i = 0; i < lowBoomSheaves.Length; i++)
            {
                lowBoomSheaves[i].transform.Rotate(Time.deltaTime / sheaveRotateSpeed, 0, 0);
            }
        }
        //Drag Rope Movement
        if (dragMotor == MotorControl.Retract)
        {
            //Reduce Rope Length
            if (leftDragRopeLength > 1 && rightDragRopeLength > 1)
            {
                for (int i = 2; i < cursor.Length; i++)
                {
                    cursor[i].ChangeLength(rope[i].RestLength - speed * Time.deltaTime);
                }
            }
        }
        if (dragMotor == MotorControl.Extend)
        {
            //Extend Rope Length
            for (int i = 2; i < cursor.Length; i++)
            {
                cursor[i].ChangeLength(rope[i].RestLength + speed * Time.deltaTime);
            }
        }
        //Swing Movement
        if (swingMotor == SwingControl.CW)
        {
            houseBody.transform.Rotate(0, Time.deltaTime / rotateSpeed, 0);
        }
        if (swingMotor == SwingControl.CCW)
        {
            houseBody.transform.Rotate(0, -Time.deltaTime / rotateSpeed, 0);
        }
    }
}
