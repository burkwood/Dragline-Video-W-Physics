﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class ObiHoistRopeController : MonoBehaviour {

	public ObiRopeCursor[] cursor = new ObiRopeCursor[2];
	public ObiRope[] rope = new ObiRope[2];
	public GameObject leftHoistRope;
	public GameObject rightHoistRope;
	public float leftRopeLength;
	public float rightRopeLength;

	// Use this for initialization
	void Start () 
	{
		//Define Game Objects
		leftHoistRope = GameObject.FindWithTag ("LeftHoistRope");
		rightHoistRope = GameObject.FindWithTag ("RightHoistRope");
		cursor [0] = leftHoistRope.GetComponent<ObiRopeCursor> ();
		cursor [1] = rightHoistRope.GetComponent<ObiRopeCursor> ();
		for (int i = 0; i < rope.Length; i++) {
			rope [i] = cursor [i].GetComponent<ObiRope> ();
		}
	}
	
	// Update is called once per frame
	void Update () 
	{
		leftRopeLength = rope [0].RestLength;
		rightRopeLength = rope [1].RestLength;
		if (Input.GetKey(KeyCode.W)) {
			if (leftRopeLength > 18 && rightRopeLength > 18) {
				for (int i = 0; i < cursor.Length; i++) {
					cursor [i].ChangeLength (rope [i].RestLength - 1f * Time.deltaTime);
				}//For end
			}//If End
		}//If End

		if (Input.GetKey(KeyCode.S)) {
			for (int i = 0; i < cursor.Length; i++) {
				cursor[i].ChangeLength(rope[i].RestLength + 1f * Time.deltaTime);
			}//For End
		}//If End
	}//Update End
}//Monobehaviour End
