﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Obi;

public class ObiBucyrusController : MonoBehaviour
{
    public GameObject houseBody;
    public ObiRopeCursor[] cursor = new ObiRopeCursor[4];
    public ObiRope[] rope = new ObiRope[4];
    public GameObject leftHoistRope;
    public GameObject rightHoistRope;
    public GameObject leftDragRope;
    public GameObject rightDragRope;
    public float leftHoistRopeLength;
    public float rightHoistRopeLength;
    public float leftDragRopeLength;
    public float rightDragRopeLength;
    public float longestRopeLength;
    public bool balance = false;
    public bool hoistRopesTensioned = false;
    public int ropeArrayLength = 0;

    public GameObject[] lowBoomSheaves;

    public float speed = 6f;
    public float rotateSpeed = 0.3f;
    public float sheaveRotateSpeed = 0.002f;


    // Use this for initialization
    void Start ()
    {
        houseBody = GameObject.FindGameObjectWithTag("Dragline_Parent");
        houseBody = GameObject.FindGameObjectWithTag("House_Body");
        lowBoomSheaves = GameObject.FindGameObjectsWithTag("LowBoomSheaves");        

        //Define Game Objects
        leftHoistRope = GameObject.FindWithTag("LeftHoistRope");
        rightHoistRope = GameObject.FindWithTag("RightHoistRope");
        leftDragRope = GameObject.FindWithTag("LeftDragRope");
        rightDragRope = GameObject.FindWithTag("RightDragRope");
        cursor[0] = leftHoistRope.GetComponent<ObiRopeCursor>();
        cursor[1] = rightHoistRope.GetComponent<ObiRopeCursor>();
        cursor[2] = leftDragRope.GetComponent<ObiRopeCursor>();
        cursor[3] = rightDragRope.GetComponent<ObiRopeCursor>();

        ropeArrayLength = rope.Length;

        for (int i = 0; i < rope.Length; i++)
        {
            rope[i] = cursor[i].GetComponent<ObiRope>();
        }
        leftHoistRopeLength = rope[0].RestLength;
        rightHoistRopeLength = rope[1].RestLength;
        leftDragRopeLength = rope[2].RestLength;
        rightDragRopeLength = rope[3].RestLength;

    }
	
	// Update is called once per frame
	void Update ()
    {
        leftHoistRopeLength = rope[0].RestLength;
        rightHoistRopeLength = rope[1].RestLength;
        leftDragRopeLength = rope[2].RestLength;
        rightDragRopeLength = rope[3].RestLength;
        if (!balance)
        {
            cursor[1].ChangeLength(leftHoistRopeLength);
            cursor[3].ChangeLength(leftDragRopeLength);
            balance = true;
        }

        if (!hoistRopesTensioned)
        {
            if (leftHoistRopeLength > 125 && rightHoistRopeLength > 125)
            {
                for (int i = 0; i < 2; i++)
                {
                    cursor[i].ChangeLength(rope[i].RestLength - speed * Time.deltaTime);
                }//For end
     
            }//If End
            else
            {
                hoistRopesTensioned = true;
            }
            
        }//If

        if (Input.GetKey(KeyCode.W))
        {
            if (leftHoistRopeLength > 59 && rightHoistRopeLength > 59)
            {
                for (int i = 0; i < 2; i++)
                {
                    cursor[i].ChangeLength(rope[i].RestLength - speed * Time.deltaTime);
                }//For end                               

            }//If End

            for (int i = 0; i < lowBoomSheaves.Length; i++)
            {
                lowBoomSheaves[i].transform.Rotate(-Time.deltaTime / sheaveRotateSpeed, 0, 0);
            }
                       

        }//If End

        if (Input.GetKey(KeyCode.S))
        {
            for (int i = 0; i < 2; i++)
            {
                cursor[i].ChangeLength(rope[i].RestLength + speed * Time.deltaTime);
            }//For End

            for (int i = 0; i < lowBoomSheaves.Length; i++)
            {
                lowBoomSheaves[i].transform.Rotate(Time.deltaTime / sheaveRotateSpeed, 0, 0);
            }

        }//If End

        if (Input.GetKey(KeyCode.Q))
        {
            if (leftDragRopeLength > 1 && rightDragRopeLength > 1)
            {
                for (int i = 2; i < cursor.Length; i++)
                {
                    cursor[i].ChangeLength(rope[i].RestLength - speed * Time.deltaTime);
                }//For end
            }//If End
        }//If End

        if (Input.GetKey(KeyCode.E))
        {
            for (int i = 2; i < cursor.Length; i++)
            {
                cursor[i].ChangeLength(rope[i].RestLength + speed * Time.deltaTime);
            }//For End
        }//If End


        if (Input.GetKey(KeyCode.K)) //I wanna shorten just one drag rope
        {
            cursor[3].ChangeLength(rope[3].RestLength - speed * Time.deltaTime);            
        }

        //Rotate House Body
        if (Input.GetKey(KeyCode.A))
        {
            houseBody.transform.Rotate(0, Time.deltaTime / rotateSpeed, 0);
        }

        if (Input.GetKey(KeyCode.D))
        {
            houseBody.transform.Rotate(0, -Time.deltaTime / rotateSpeed, 0);
        }
    }
}
