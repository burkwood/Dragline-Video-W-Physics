﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class M_Tub_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClips;                                                                                                   //Audio Player
    Marion_DL_SFX DL_SFX;                                                                                                      //The script that controls all hiding and highlighting
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private float[] delayTimes = new float[] { 5, 3, 3, 5 };                                                                            //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "The tub is a welded steel structure consisting of multiple bulkheads to provide strength and rigidity.",				//Cap [0]

		"The tub supports the overall weight of the dragline, dispersing the weight across the pad.",							//Cap [1]

		"It's critical to have a flat, even pad.",																				//Cap [2]

		"On top of the tub, a flat machined rail is welded, called the lower roller rail.",										//Cap [3]

		"The roller circle consists of evenly spaced roller bearings.",															//Cap [4]

		"These bearings support the revolving frame.",																			//Cap [5]

		"Inward of the roller circle is the swing rack.",																		//Cap [6]

		"The swing rack is a large fixed gear that meshes with the swing pinions.",												//Cap [7]

		"The swing rack allows the revolving frame to rotate around the center pin.",											//Cap [8]

		"During propel the lifting hooks grab the lip of the tub, dragging the tub across the pad while walking.",				//Cap [9]

		"Note that ALL access to the swing rack, roller circle, and tub are confined spaces.",									//Cap [10]

		"Contact your supervisor before entering these areas to ensure that the necessary PPE and safety procedures are in place." //Cap [11]

	};
    // Use this for initialization
    void Start ()
    {
        DL_SFX = GameObject.FindWithTag("GameController").GetComponent<Marion_DL_SFX>();                                           //Get reference to the SFX script
        playAudio = GetComponent<AudioSource>();                                                                                    //Get Reference to the Audio Source
        playAudio.clip = audioClips[0];                                                                                              //Load the first Audio Clip
        captionText.text = "";                                                                                                      //Clear the Captions
        DL_SFX.showHide.upperRollerRail = false;                                                                                    //Hide Upper Roller Rail
        DL_SFX.showHide.revFrame = false;                                                                                           //Hide Revolving Frame
        DL_SFX.showHide.revFrameFloor = false;                                                                                      //Hide Revolving Frame Floor
        DL_SFX.showHide.aFrameLugs = false;																							//Hide A-Frame Lugs
        
    }
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();

        //******************************************************************************************************************************
        //Start the first Queue 
        //Camera moves from Home to Tub Closeup
        if (queue == 0 && nextStage == false && Time.time > delayTime)                                                              //START Queue 1 
        {
            nextStage = true;                                                                                                       //Trigger camera movement
            queue = 1;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the Camera Movement was triggered
        }

        //Caption & SFX Manager during the first Queue
        //"The tub is a welded steel structure consisting of multiple bulkheads to provide strength and rigidity." 	Cap [0]
        //Fade: HouseCab, HoistRopes, AFrame, Drag Ropes
        //Highlight Tab Bulkhead & fade out Tub  till end
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0])                                              //The camera has stopped moving
        {
            DL_SFX.fadeManager.hoistRopes.enabled = true;                                                                           //Fade Hoist Ropes
            DL_SFX.fadeManager.house.enabled = true;                                                                             //Fade House Cab
            DL_SFX.fadeManager.dragRopes.enabled = true;                                                                        //Fade Drag Ropes
            DL_SFX.fadeManager.aFrame.enabled = true;
        }
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0] + 3)                                      //Start Playing Audio after camera has stopped moving
        {                                                                                                                           //And Fade is Complete								 
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                                   //Play loaded clip
                audioTriggered = true;                                                                                              //Flag audio clip has been triggered
                DL_SFX.showHide.tub = false;                                                                                        //Hide the Tub
            }

            if (playAudio.isPlaying == true)
            {
                captionText.text = captions[0];                                                                                 //If audio clip is playing, show captions
            }
            if (playAudio.isPlaying == true && playTime > 0.2)
            {
                DL_SFX.highlighting.tubBulkhead.status = Status.Flash;                                                              //Flash Tub Bulkhead
            }
            if (!playAudio.isPlaying && Time.time > startTime + delayTimes[0] + playAudio.clip.length + delayTime + 1 + 3)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.showHide.tub = true;                                                                                         //Show Tub
                                                                                                                                    //				DL_SFX.fadeManager.tub.enabled = false;																				//Un-fade the tub
                DL_SFX.highlighting.tubBulkhead.status = Status.Off;                                                                //Un-highlight Tub Bulkhead
            }
        }
        //******************************************************************************************************************************
        // Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0] + playAudio.clip.length + delayTime + 1 + 3 && captionText.text == "") //Start Queue 2
        {
            playAudio.clip = audioClips[1];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 2;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the second Queue
        //"The tub supports the overall weight of the dragline, dispersing the weight across the pad.",	//Cap [1]
        //"It's critical to have a flat, even pad.",													//Cap [2]
        //Highlight Tub till end
        if (queue == 2)
        {

            if (playAudio.isPlaying == true && playTime > 0.2f) { DL_SFX.highlighting.tub.status = Status.Flash; }                  //Flash Tub
            if (playAudio.isPlaying == true && playTime < 7.5) { captionText.text = captions[1]; }                                  //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 7.5) { captionText.text = captions[2]; }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.tub.status = Status.Off;                                                                        //Un-highlight Tub
            }
        }
        //******************************************************************************************************************************
        // Start the Third Queue 
        //Tub Closeup To Tub Zoomed
        if (queue == 2 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 3
        {
            nextStage = true;
            playAudio.clip = audioClips[2];                                                                                              //change clip
            queue = 3;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the third Queue
        //"On top of the tub, a flat machined rail is welded, called the lower roller rail." Cap [3]
        //Highlight lower roller rail till end
        //Fade roller circle and upper roller rail till end
        if (queue == 3 && nextStage == false && Time.time > startTime + delayTimes[1])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
                DL_SFX.fadeManager.rollerCircle.enabled = true;                                                                     //Fade Roller Circle
            }

            if (playAudio.isPlaying == true) { captionText.text = captions[3]; }                                                    //If audio clip is playing, show captions												
            if (playAudio.isPlaying == true && playTime > 4.0)
            {
                DL_SFX.highlighting.rollerRailLower.status = Status.Flash;                                                          //Flash Lower Roller Rail
                                                                                                                                    //				DL_SFX.showHide.rollerCircle = false;																				//Hide Roller Circle
            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.rollerRailLower.status = Status.Off;                                                            //Un-highlight Lower ROller Rail
                                                                                                                                    //				DL_SFX.showHide.rollerCircle = true;																				//Un-Hide Roller Circle
                DL_SFX.fadeManager.rollerCircle.enabled = false;                                                                    //Un-fade Roller Circle
            }

        }
        //******************************************************************************************************************************
        // Start the Fourth Queue 
        //No Camera Movement
        if (queue == 3 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1] + 1 + 3 && captionText.text == "") //Start Queue 4
        {
            playAudio.clip = audioClips[3];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 4;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the fourth Queue
        //"The roller circle consists of evenly spaced roller bearings." Cap [4]
        //"These bearings support the revolving frame." Cap [5]
        //Highlight Roller Circle till end
        if (queue == 4)
        {
            if (playAudio.isPlaying == true && playTime > 0.2) { DL_SFX.highlighting.tubRollers.status = Status.Flash; }                //Highlight Roller Circle
            if (playAudio.isPlaying == true && playTime < 6.0) { captionText.text = captions[4]; }                                  //If audio clip is playing, show captions												
            if (playAudio.isPlaying == true && playTime > 6.0) { captionText.text = captions[5]; }                                  //Update Captions
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.tubRollers.status = Status.Off;                                                                 //Un-highlight Roller Circle
            }

        }
        //******************************************************************************************************************************
        // Start the Fifth Queue 
        //No Camera Movement
        if (queue == 4 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 4
        {
            playAudio.clip = audioClips[4];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 5;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the fourth Queue
        //"Inward of the roller circle is the swing rack." Cap [6]
        //Highlight Swing Rack
        if (queue == 5)
        {
            if (playAudio.isPlaying == true && playTime > 1.6) { DL_SFX.highlighting.swingRack.status = Status.Flash; }             //Highlight Swing Rack
            if (playAudio.isPlaying == true) { captionText.text = captions[6]; }                                                    //If audio clip is playing, show captions												
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
            }

        }
        //******************************************************************************************************************************
        // Start the Sixth Queue 
        //No Camera Movement
        if (queue == 5 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 5
        {
            playAudio.clip = audioClips[5];                                                                                              //change clip
            playAudio.Play();                                                                                                       //play clip
            queue = 6;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the Sixth Queue
        //"The swing rack is a large fixed gear that meshes with the swing pinions." Cap [7]
        //"The swing rack drives the revolving frame to rotate around the center pin." Cap [8]
        //Highlight Swing Rack, Swing pinions & center pin till end
        if (queue == 6)
        {
            if (playAudio.isPlaying == true && playTime > 3.1)
            {
                DL_SFX.highlighting.swingPinions.status = Status.Flash;                                                             //Highlight Swing Pinions
            }
            if (playAudio.isPlaying == true && playTime < 6.5) { captionText.text = captions[7]; }                                  //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 6.5) { captionText.text = captions[8]; }
            if (playAudio.isPlaying == true && playTime > 10) { DL_SFX.highlighting.centerPin.status = Status.Flash; }              //Highlight Center Pin
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.swingPinions.status = Status.Off;                                                               //Un-highlight Swing Pinions
                DL_SFX.highlighting.swingRack.status = Status.Off;                                                                  //Un-highlight Swing Rack
                DL_SFX.highlighting.centerPin.status = Status.Off;                                                                  //Un-highlight Center Pin
                                                                                                                                    //				DL_SFX.showHide.swingPinions = false;																				//Hide Swing Pinions.
            }

        }
        //******************************************************************************************************************************
        // Start the Seventh Queue 
        //Tub Zoomed To Tub Closeup
        if (queue == 6 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 3
        {
            nextStage = true;
            playAudio.clip = audioClips[6];                                                                                              //change clip
                                                                                                                                    //			playAudio.Play ();																										//play clip
            queue = 7;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the Seventh Queue
        //"During propel the lifting hooks grab the lip of the tub, dragging the tub across the pad while walking." Cap [9]
        //Lifting Hooks??
        if (queue == 7 && nextStage == false && Time.time > startTime + delayTimes[2])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }

            if (playAudio.isPlaying == true) { captionText.text = captions[9]; }                                                    //If audio clip is playing, show captions												
            if (playAudio.isPlaying == true && playTime > 0.9)
            {
                //				DL_SFX.showHide.liftingHooks = true;																				//Show Lifting Hooks
                DL_SFX.highlighting.liftingHooks.status = Status.Flash;                                                             //Highlight Lifting Hooks
            }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[2] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
                DL_SFX.highlighting.liftingHooks.status = Status.Off;                                                               //Un-highlight Lifting Hooks
                DL_SFX.fadeManager.hoistRopes.enabled = false;                                                                      //Un-fade Hoist Ropes
                DL_SFX.fadeManager.dragRopes.enabled = false;                                                                        //Un-fade Drag Ropes
                DL_SFX.fadeManager.aFrame.enabled = false;                                                                     //Un-fade AFrame Lights
                DL_SFX.fadeManager.house.enabled = false;                                                                   //Un-fade House Platform
            }

        }
        //******************************************************************************************************************************
        // Start the Eighth Queue 
        //Tub Closeup to Home
        if (queue == 7 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTimes[2] + delayTime + 2 + 3 && captionText.text == "") //Start Queue 3
        {
            nextStage = true;
            playAudio.clip = audioClips[7];                                                                                              //change clip
            queue = 8;                                                                                                              //Update the queue number
            startTime = Time.time;                                                                                                  //Store when the clip started playing
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the Eighth Queue
        //"Note that ALL access to the swing rack, roller circle, and tub are confined spaces." Cap [10]
        //"Contact your supervisor before entering these areas to ensure that the necessary PPE and safety procedures are in place." Cap [11]
        //No SFX
        if (queue == 8 && nextStage == false && Time.time > startTime + delayTimes[3])
        {
            if (audioTriggered == false)
            {
                playAudio.Play();
                audioTriggered = true;
            }
            if (playAudio.isPlaying == true && playTime < 7.9) { captionText.text = captions[10]; }                                 //If audio clip is playing, show captions
            if (playAudio.isPlaying == true && playTime > 7.9) { captionText.text = captions[11]; }
            if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[3] + 1)
            {
                captionText.text = "";                                                                                              //Don't clear caption till 2 Seconds after Audio ends
            }

        }
    }
    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
