﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Video;
using IntPublicClass;

public class ImageController : MonoBehaviour
{
    public GameObject[] images;
    public bool[] imageStatus;
    private Vector3 screenFolded = new Vector3(0f, 0f, 0f);
    //private Vector3[] expandedPositions;
    private Vector3 expandedPosition = new Vector3(4.440003f, 2.5f, 0.264711f);
    public float screenScaleTime = 5f;
    // Use this for initialization
    void Start ()
    {
        imageStatus = new bool[images.Length];
        //expandedPositions = new Vector3[images.Length];
        //if (expandedPositions.Length > 0)
        //{
        //    expandedPositions[0] = new Vector3(4.440003f, 2.5f, 0.264711f);
        //}
        
	}
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < images.Length; i++)
        {
            //Expand Image
            if (imageStatus[i] == true)
            {
                //iTween.ScaleTo(images[i], expandedPositions[i], screenScaleTime);
                iTween.ScaleTo(images[i], expandedPosition, screenScaleTime);
            }
            else
            {
                iTween.ScaleTo(images[i], screenFolded, screenScaleTime);
            }
        }
	}
}
