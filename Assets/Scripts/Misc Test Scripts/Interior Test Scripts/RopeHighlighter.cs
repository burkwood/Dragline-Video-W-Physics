﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//This code should be added to DL_SFX script but for time purposes it remains seperate
public class RopeHighlighter : MonoBehaviour
{
    //******************************************************************************************************************************
    //Public Variables
    public float flashDuration = 0.8f;
    public float colorLerp;
    public Highlight dragRopeStatus;
    public Highlight hoistRopeStatus;
    //******************************************************************************************************************************
    //Game Objects Section
    private GameObject[] dragRopes = new GameObject[2];
    private GameObject[] hoistRopes = new GameObject[2];
    //******************************************************************************************************************************
    //Materials Section
    private Material[] dragRopeMaterials = new Material[2];
    private Material[] hoistRopeMaterials = new Material[2];
    //******************************************************************************************************************************
    //Colors Section
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    private Color dragRopeOriginalColor;
    private Color dragRopeOriginalEColor;
    private Color dragRopeNewColor;
    private Color dragRopeNewEColor;
    private Color hoistRopeOriginalColor;
    private Color hoistRopeOriginalEColor;
    private Color hoistRopeNewColor;
    private Color hoistRopeNewEColor;
   
    
    // Use this for initialization
    void Start ()
    {
        //******************************************************************************************************************************
        //Define Game Objects
        dragRopes[0] = GameObject.FindGameObjectWithTag("LeftDragRope");
        dragRopes[1] = GameObject.FindGameObjectWithTag("RightDragRope");
        hoistRopes[0] = GameObject.FindGameObjectWithTag("LeftHoistRope");
        hoistRopes[1] = GameObject.FindGameObjectWithTag("RightHoistRope");
        //******************************************************************************************************************************
        //Define Materials
        for (int i = 0; i < dragRopes.Length; i++)
        {
            dragRopeMaterials[i] = dragRopes[i].GetComponent<Renderer>().material;
            hoistRopeMaterials[i] = hoistRopes[i].GetComponent<Renderer>().material;
        }
        //******************************************************************************************************************************
        //Define Colors
        dragRopeOriginalColor = dragRopeMaterials[0].color;
        dragRopeOriginalEColor = dragRopeMaterials[0].GetColor("_EmissionColor");
        hoistRopeOriginalColor = hoistRopeMaterials[0].color;
        hoistRopeOriginalEColor = hoistRopeMaterials[0].GetColor("_EmissionColor");
    }

    // Update is called once per frame
    void Update ()
    {
        colorLerp = Mathf.PingPong(Time.time, flashDuration) / flashDuration;
        //******************************************************************************************************************************
        //Highlight Section
        dragRopeStatus.highlight = FlashStatus((int)dragRopeStatus.status);
        hoistRopeStatus.highlight = FlashStatus((int)hoistRopeStatus.status);
        dragRopeNewColor = Color.Lerp(dragRopeOriginalColor, highlightColor, dragRopeStatus.highlight);
        dragRopeNewEColor = Color.Lerp(dragRopeOriginalEColor, highlightColor, dragRopeStatus.highlight);
        hoistRopeNewColor = Color.Lerp(hoistRopeOriginalColor, highlightColor, hoistRopeStatus.highlight);
        hoistRopeNewEColor = Color.Lerp(hoistRopeOriginalEColor, highlightColor, hoistRopeStatus.highlight);

        for (int i = 0; i < hoistRopes.Length; i++)
        {
            hoistRopeMaterials[i].SetColor("_Color", hoistRopeNewColor);
            hoistRopeMaterials[i].SetColor("_EmissionColor", hoistRopeNewEColor);
            dragRopeMaterials[i].SetColor("_Color", dragRopeNewColor);
            dragRopeMaterials[i].SetColor("_EmissionColor", dragRopeNewEColor);
        }
    }

    //******************************************************************************************************************************
    //Function Section
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
}
