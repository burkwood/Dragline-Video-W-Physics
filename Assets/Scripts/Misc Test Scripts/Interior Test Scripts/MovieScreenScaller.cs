﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Video;

public class MovieScreenScaller : MonoBehaviour
{
    public float scaleTime;
    private Vector3 shrunk = new Vector3(0f, 0f, 0f);
    private Vector3 expanded = new Vector3(4.44f, 2.5f, 0.2647108f);
    public GameObject movieScreen;
    public GameObject hoistPlayer;
    private VideoPlayer hoistMoviePlayer;
   
	// Use this for initialization
	void Start ()
    {
        iTween.ScaleTo(movieScreen, shrunk, 0.1f);
        hoistMoviePlayer = hoistPlayer.GetComponent<VideoPlayer>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.B))
        {
            iTween.ScaleTo(movieScreen, shrunk, scaleTime);
        }

        if (Input.GetKey(KeyCode.N))
        {
            iTween.ScaleTo(movieScreen, expanded, scaleTime);
        }

        if (Input.GetKey(KeyCode.M))
        {
            if (hoistMoviePlayer.isPlaying)
            {
                hoistMoviePlayer.Pause();
            }
            else
            {
                hoistMoviePlayer.Play();
            }
        }
	}
}
