﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class GetStatusTextController : MonoBehaviour {

    public Text statusText;
    private Color originalColor;
    //private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    private Color highlightColor = new Color(0f, 255f, 0f);
    private Color newColor;
    public float slowLerp;
    public float fastLerp;
    public float duration;

	// Use this for initialization
	void Start ()
    {
        originalColor = statusText.color;
	}
	
	// Update is called once per frame
	void Update ()
    {
        fastLerp = Mathf.PingPong(Time.time, duration) / duration;
        slowLerp = Mathf.PingPong(Time.time, duration * 2) / duration * 2;
        if (Input.GetKey(KeyCode.R))
        {
            newColor = highlightColor;
            newColor.a = fastLerp;
            statusText.text = "Engaged";
        }
        else
        {
            newColor = originalColor;
            newColor.a = slowLerp;
            statusText.text = "Idle";
        }

        //originalColor.a = lerpVal;
        statusText.color = newColor;
    }
}
