﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class kw_highlight_controller : MonoBehaviour
{
    //The sole purpose of this script is to activate/deactivate the kw highlighing behaviour.

    //public bool executed = false;
    public bool highlight = false;
    private string lastMessage;
	// Use this for initialization
	void Start ()
    {
        lastMessage = "";
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (highlight == true /*&& lastMessage != "Highlight"*/)
        {
            SendMessage("Highlight");
            lastMessage = "Highlight";
        }
        if (highlight == false && lastMessage != "DeleteHighlight")
        {
            SendMessage("DeleteHighlight");
            lastMessage = "DeleteHighlight";
        }
	}
}
