﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoistDrumController : MonoBehaviour
{

    
    public GameObject hoistRopeRight;
    public GameObject hoistRopeLeft;
    public GameObject dragRopeRight;
    public GameObject dragRopeLeft;
    public GameObject dragDrum;
    public GameObject hoistDrum;
    public UltimateRope rightHoistRope;
    public UltimateRope leftHoistRope;
    public UltimateRope leftDragRope;
    public UltimateRope rightDragRope;
    public float extensionSpeed;
    public float rotateSpeed;
    public float timeScale;
    //public float extensionFudge;


    public float hoistExtensionSpeed = 0;
//    private float hoistRotateSpeed = 0;
    private float hoistDirection;

    public float dragExtensionSpeed = 0;
    private float dragDirection;


    public float rightHoistRopeExtension;
    public float leftHoistRopeExtension;
    public float rightDragRopeExtension;
    public float leftDragRopeExtension;

    // Use this for initialization
    void Start()
    {
        hoistRopeRight = GameObject.FindWithTag("RightHoistRope");
        hoistRopeLeft = GameObject.FindWithTag("LeftHoistRope");
        dragRopeRight = GameObject.FindWithTag("RightDragRope");
        dragRopeLeft = GameObject.FindWithTag("LeftDragRope");
        hoistDrum = GameObject.FindWithTag("HoistDrum");
        dragDrum = GameObject.FindWithTag("DragDrum");
        rightHoistRope = hoistRopeRight.GetComponent<UltimateRope>();
        leftHoistRope = hoistRopeLeft.GetComponent<UltimateRope>();
        rightDragRope = dragRopeRight.GetComponent<UltimateRope>();
        leftDragRope = dragRopeLeft.GetComponent<UltimateRope>();
        rightHoistRopeExtension = rightHoistRope != null ? rightHoistRope.m_fCurrentExtension : 0.0f;
        leftHoistRopeExtension = leftHoistRope != null ? leftHoistRope.m_fCurrentExtension : 0.0f;
        rightDragRopeExtension = rightDragRope != null ? rightDragRope.m_fCurrentExtension : 0.0f;
        leftDragRopeExtension = leftDragRope != null ? leftDragRope.m_fCurrentExtension : 0.0f;
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.Z))
        {
            hoistDirection = 1;
            hoistExtensionSpeed = Mathf.Lerp( hoistExtensionSpeed, extensionSpeed, timeScale * Time.deltaTime);
            //hoistRotateSpeed = Mathf.Lerp(hoistRotateSpeed, rotateSpeed, timeScale * Time.deltaTime);
            rightHoistRopeExtension +=  Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension +=  Time.deltaTime * hoistExtensionSpeed;          
        }
        else if (Input.GetKey(KeyCode.X))
        {
            hoistDirection = -1;
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, extensionSpeed, timeScale * Time.deltaTime);
           // hoistRotateSpeed = Mathf.Lerp(hoistRotateSpeed, rotateSpeed, timeScale * Time.deltaTime);
            rightHoistRopeExtension -=  Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension -= Time.deltaTime * hoistExtensionSpeed;
        }
        else
        {
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, 0, timeScale * Time.deltaTime);
            //hoistRotateSpeed = Mathf.Lerp(hoistRotateSpeed, 0, timeScale * Time.deltaTime);
            rightHoistRopeExtension = rightHoistRopeExtension + hoistDirection * Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension = leftHoistRopeExtension + hoistDirection * Time.deltaTime * hoistExtensionSpeed;
        }

        //if (Input.GetKey(KeyCode.Z)) //Extend The Hoist Rope
        //{
        //    rightHoistRopeExtension += Time.deltaTime * extensionSpeed;
        //    leftHoistRopeExtension += Time.deltaTime * extensionSpeed;
        //    if (rightHoistRopeExtension < rightHoistRope.ExtensibleLength)
        //    {
        //        hoistDrum.transform.Rotate(Time.deltaTime * rotateSpeed, 0.0f, 0.0f);
        //    }
        //}

        //if (Input.GetKey(KeyCode.X)) //Retract The Hoist Rope
        //{
        //    rightHoistRopeExtension -= Time.deltaTime * extensionSpeed;
        //    leftHoistRopeExtension -= Time.deltaTime * extensionSpeed;
        //    if (rightHoistRopeExtension > 0)
        //    {
        //        hoistDrum.transform.Rotate(-Time.deltaTime * rotateSpeed, 0.0f, 0.0f);
        //    }
        //}

        //if (Input.GetKey(KeyCode.C)) //Extend the Drag Rope
        //{
        //    rightDragRopeExtension += Time.deltaTime * extensionSpeed;
        //    leftDragRopeExtension += Time.deltaTime * extensionSpeed;
        //    if (rightDragRopeExtension < rightDragRope.ExtensibleLength)
        //    {
        //        dragDrum.transform.Rotate(-Time.deltaTime * rotateSpeed, 0.0f, 0.0f);
        //    }
        //}

        //if (Input.GetKey(KeyCode.V))
        //{
        //    rightDragRopeExtension -= Time.deltaTime * extensionSpeed;
        //    leftDragRopeExtension -= Time.deltaTime * extensionSpeed;
        //    if (rightDragRopeExtension > 0)
        //    {
        //        dragDrum.transform.Rotate(Time.deltaTime * rotateSpeed, 0.0f, 0.0f);
        //    }
        //}

        if (Input.GetKey(KeyCode.C))
        {
            dragDirection = 1;
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, extensionSpeed, timeScale * Time.deltaTime);
            rightDragRopeExtension += Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension += Time.deltaTime * dragExtensionSpeed;
        }
        else if (Input.GetKey(KeyCode.V))
        {
            dragDirection = -1;
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, extensionSpeed, timeScale * Time.deltaTime);
            rightDragRopeExtension -= Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension -= Time.deltaTime * dragExtensionSpeed;
        }
        else
        {
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, 0, timeScale * Time.deltaTime);
            rightDragRopeExtension = rightDragRopeExtension + dragDirection * Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension = leftDragRopeExtension + dragDirection * Time.deltaTime * dragExtensionSpeed;
        }

        if (rightHoistRope != null) //Clamp max length and impose extension method
        {
            rightHoistRopeExtension = Mathf.Clamp(rightHoistRopeExtension, 0.0f, rightHoistRope.ExtensibleLength);
            rightHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightHoistRopeExtension - rightHoistRope.m_fCurrentExtension);
            
        }

        if (leftHoistRope != null) //Clamp max length and impose extension method
        {
            leftHoistRopeExtension = Mathf.Clamp(leftHoistRopeExtension, 0.0f, leftHoistRope.ExtensibleLength);
            leftHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftHoistRopeExtension - leftHoistRope.m_fCurrentExtension);
        }

        if (rightDragRope != null) //Clamp max length and impose extension method
        {
            rightDragRopeExtension = Mathf.Clamp(rightDragRopeExtension, 0.0f, rightDragRope.ExtensibleLength);
            rightDragRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightDragRopeExtension - rightDragRope.m_fCurrentExtension);
        }

        if (leftDragRope != null) //Clamp max length and impose extension method
        {
            leftDragRopeExtension = Mathf.Clamp(leftDragRopeExtension, 0.0f, leftDragRope.ExtensibleLength);
            leftDragRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftDragRopeExtension - leftDragRope.m_fCurrentExtension);
        }
    }
}
