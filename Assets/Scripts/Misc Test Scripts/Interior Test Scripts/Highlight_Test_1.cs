﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

//public enum Status { Off, Flash, Solid };

public class Highlight_Test_1 : MonoBehaviour
{


    private GameObject[] swingMotorGO;              //All Swing Motors Game Objects
    private GameObject[] hoistMotorGO;              //All Hoist Motor Game Objects
    private GameObject[] hoistGearboxGO;            //All Hoist Gearbox Game Objects
    private GameObject hoistDrumGO;                 //Hoist Drum
    private GameObject[] dragMotorGO;               //All Drag Motors Game Objects
    private GameObject[] dragGearboxGO;             //All Drag Gearbox Game Objects
    private GameObject dragDrumGO;                  //Drag Drum
    //private GameObject motorGenGO;                  //Need to highlight individual sync motors at once
    public GameObject[] motorGenGO;
    
    private float colorLerp;
    private Material[] swingMotorMaterial;          //All Swing Motor Materialss 
    private Material[] swingGearboxMaterial;        //All Swing Gearbox Materials
    private Material[] hoistMotorMaterial;         //All Hoist Motor Materials
    private Material[] hoistGearboxMaterial;       //All Hoist Gearbox Materials
    private Material hoistDrumMaterial;
    private Material[] dragMotorMaterial;           //All Drag Motor Material
    private Material[] dragGearboxMaterial;         //All Drag Gearbox Materials
    private Material dragDrumMaterial;
    private Material[] motorGenMaterial;
    //***************************************************************************
    //Color
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);

    private Color[] swingMotorOriginalColor = new Color[2];
    private Color[] swingGearOriginalColor = new Color[2];
    private Color[] swingMotorNewColor = new Color[2];
    private Color[] swingGearNewColor = new Color[2];

    private Color[] hoistMotorOriginalColor = new Color[2];
    private Color[] hoistGearboxOriginalColor = new Color[2];
    private Color[] hoistDrumOriginalColor = new Color[2];
    private Color[] hoistMotorNewColor = new Color[2];
    private Color[] hoistGearboxNewColor = new Color[2];
    private Color[] hoistDrumNewColor = new Color[2];

    private Color[] dragMotorOriginalColor = new Color[2];
    private Color[] dragGearboxOriginalColor = new Color[2];
    private Color[] dragDrumOriginalColor = new Color[2];
    private Color[] dragMotorNewColor = new Color[2];
    private Color[] dragGearboxNewColor = new Color[2];
    private Color[] dragDrumNewColor = new Color[2];

    private Color[] motorGenOriginalColor = new Color[2];
    private Color[] motorGenNewColor = new Color[2];


    public float highlightDuration;
    public Status swingMotorStatus;
    public Status swingGearboxStatus;
    public Status hoistMotorStatus;
    public Status hoistGearboxStatus;
    public Status hoistDrumStatus;
    public Status dragMotorStatus;
    public Status dragGearboxStatus;
    public Status dragDrumStatus;
    //for highlighting individual generators
    [Range(0, 11)]
    public int genIndex;
    public Status motorGenStatus;



    // Use this for initialization
    void Start ()
    {
        swingMotorGO = GameObject.FindGameObjectsWithTag("SwingMotor");
        hoistMotorGO = GameObject.FindGameObjectsWithTag("HoistMotor");
        hoistGearboxGO = GameObject.FindGameObjectsWithTag("HoistGearBoxes");
        hoistDrumGO = GameObject.FindWithTag("HoistDrum");
        dragMotorGO = GameObject.FindGameObjectsWithTag("DragMotor");
        dragGearboxGO = GameObject.FindGameObjectsWithTag("DragGearBoxes");
        dragDrumGO = GameObject.FindWithTag("DragDrum");
        swingMotorMaterial = new Material[swingMotorGO.Length];
        swingGearboxMaterial = new Material[swingMotorGO.Length];
        hoistMotorMaterial = new Material[hoistMotorGO.Length];        
        hoistGearboxMaterial = new Material[hoistGearboxGO.Length];        
        hoistDrumMaterial = hoistDrumGO.GetComponent<Renderer>().material;
        dragMotorMaterial = new Material[dragMotorGO.Length];
        dragGearboxMaterial = new Material[dragGearboxGO.Length];
        dragDrumMaterial = dragDrumGO.GetComponent<Renderer>().material;
        motorGenGO = GameObject.FindGameObjectsWithTag("Generators");
        motorGenMaterial = new Material[motorGenGO.Length];

        //Get materials of Generators
        for (int i = 0; i < motorGenGO.Length; i++)
        {
            motorGenMaterial[i] = motorGenGO[i].GetComponent<Renderer>().materials[0];
        }
        
        //Get all swing Motor  and gearbox materials
        for (int i = 0; i < swingMotorMaterial.Length; i++)
        {
            swingMotorMaterial[i] = swingMotorGO[i].GetComponent<Renderer>().materials[1];
            swingGearboxMaterial[i] = swingMotorGO[i].GetComponent<Renderer>().materials[0];
        }
        //Get all hoist Motor Materials
        for (int i = 0; i < hoistMotorMaterial.Length; i++)
        {
            hoistMotorMaterial[i] = hoistMotorGO[i].GetComponent<Renderer>().materials[0];
        }
        //Get all hoist gearbox materials
        for (int i = 0; i < hoistGearboxMaterial.Length; i++)
        {
            hoistGearboxMaterial[i] = hoistGearboxGO[i].GetComponent<Renderer>().material;
        }
        //Get All Drag Motor Materials
        for (int i = 0; i < dragMotorMaterial.Length; i++)
        {
            dragMotorMaterial[i] = dragMotorGO[i].GetComponent<Renderer>().materials[0];
        }
        //Get All Drag Gearbox Materials
        for (int i = 0; i < dragGearboxMaterial.Length; i++)
        {
            dragGearboxMaterial[i] = dragGearboxGO[i].GetComponent<Renderer>().material;
        }
        swingMotorOriginalColor[0] = swingMotorMaterial[0].color;
        swingMotorOriginalColor[1] = swingMotorMaterial[0].GetColor("_EmissionColor");
        swingGearOriginalColor[0] = swingGearboxMaterial[0].color;
        swingGearOriginalColor[1] = swingGearboxMaterial[0].GetColor("_EmissionColor");
        hoistMotorOriginalColor[0] = hoistMotorMaterial[0].color;
        hoistMotorOriginalColor[1] = hoistMotorMaterial[0].GetColor("_EmissionColor");
        hoistGearboxOriginalColor[0] = hoistGearboxMaterial[0].color;
        hoistGearboxOriginalColor[1] = hoistGearboxMaterial[0].GetColor("_EmissionColor");
        hoistDrumOriginalColor[0] = hoistDrumMaterial.color;
        hoistDrumOriginalColor[1] = hoistDrumMaterial.GetColor("_EmissionColor");
        dragMotorOriginalColor[0] = dragMotorMaterial[0].color;
        dragMotorOriginalColor[1] = dragMotorMaterial[0].GetColor("_EmissionColor");
        dragGearboxOriginalColor[0] = dragGearboxMaterial[0].color;
        dragGearboxOriginalColor[1] = dragGearboxMaterial[0].GetColor("_EmissionColor");
        dragDrumOriginalColor[0] = dragDrumMaterial.color;
        dragDrumOriginalColor[1] = dragDrumMaterial.GetColor("_EmissionColor");
        motorGenOriginalColor[0] = motorGenMaterial[0].color;
        motorGenOriginalColor[1] = motorGenMaterial[0].GetColor("_EmissionColor");
    }
	
	// Update is called once per frame
	void Update ()
    {
        colorLerp = Mathf.PingPong(Time.time, highlightDuration) / highlightDuration;

        swingMotorNewColor[0] = Color.Lerp(swingMotorOriginalColor[0], highlightColor, FlashStatus((int)swingMotorStatus));
        swingMotorNewColor[1] = Color.Lerp(swingMotorOriginalColor[1], highlightColor, FlashStatus((int)swingMotorStatus));
        swingGearNewColor[0] = Color.Lerp(swingGearOriginalColor[0], highlightColor, FlashStatus((int)swingGearboxStatus));
        swingGearNewColor[1] = Color.Lerp(swingGearOriginalColor[1], highlightColor, FlashStatus((int)swingGearboxStatus));
        hoistMotorNewColor[0] = Color.Lerp(hoistMotorOriginalColor[0], highlightColor, FlashStatus((int)hoistMotorStatus));
        hoistMotorNewColor[1] = Color.Lerp(hoistMotorOriginalColor[1], highlightColor, FlashStatus((int)hoistMotorStatus));
        hoistGearboxNewColor[0] = Color.Lerp(hoistGearboxOriginalColor[0], highlightColor, FlashStatus((int)hoistGearboxStatus));
        hoistGearboxNewColor[1] = Color.Lerp(hoistGearboxOriginalColor[1], highlightColor, FlashStatus((int)hoistGearboxStatus));
        hoistDrumNewColor[0] = Color.Lerp(hoistDrumOriginalColor[0], highlightColor, FlashStatus((int)hoistDrumStatus));
        hoistDrumNewColor[1] = Color.Lerp(hoistDrumOriginalColor[1], highlightColor, FlashStatus((int)hoistDrumStatus));
        dragMotorNewColor[0] = Color.Lerp(dragMotorOriginalColor[0], highlightColor, FlashStatus((int)dragMotorStatus));
        dragMotorNewColor[1] = Color.Lerp(dragMotorOriginalColor[1], highlightColor, FlashStatus((int)dragMotorStatus));
        dragGearboxNewColor[0] = Color.Lerp(dragGearboxOriginalColor[0], highlightColor, FlashStatus((int)dragGearboxStatus));
        dragGearboxNewColor[1] = Color.Lerp(dragGearboxOriginalColor[1], highlightColor, FlashStatus((int)dragGearboxStatus));
        dragDrumNewColor[0] = Color.Lerp(dragDrumOriginalColor[0], highlightColor, FlashStatus((int)dragDrumStatus));
        dragDrumNewColor[1] = Color.Lerp(dragDrumOriginalColor[1], highlightColor, FlashStatus((int)dragDrumStatus));
        motorGenNewColor[0] = Color.Lerp(motorGenOriginalColor[0], highlightColor, FlashStatus((int)motorGenStatus));
        motorGenNewColor[1] = Color.Lerp(motorGenOriginalColor[1], highlightColor, FlashStatus((int)motorGenStatus));

        //Highlight the generators
        //for (int i = 0; i < motorGenGO.Length; i++)
        //{
        //    motorGenMaterial[i].SetColor("_Color", motorGenNewColor[0]);
        //    motorGenMaterial[i].SetColor("_EmissionColor", motorGenNewColor[1]);
        //}
        motorGenMaterial[genIndex].SetColor("_Color", motorGenNewColor[0]);
        motorGenMaterial[genIndex].SetColor("_EmissionColor", motorGenNewColor[1]);



        for (int i = 0; i < swingMotorGO.Length; i++)
        {
            swingMotorMaterial[i].SetColor("_Color", swingMotorNewColor[0]);
            swingMotorMaterial[i].SetColor("_EmissionColor", swingMotorNewColor[1]);
            swingGearboxMaterial[i].SetColor("_Color", swingGearNewColor[0]);
            swingGearboxMaterial[i].SetColor("_EmissionColor", swingGearNewColor[1]);
        }

        for (int i = 0; i < hoistMotorGO.Length; i++)
        {
            hoistMotorMaterial[i].SetColor("_Color", hoistMotorNewColor[0]);
            hoistMotorMaterial[i].SetColor("_EmissionColor", hoistMotorNewColor[1]);

        }

        for (int i = 0; i < hoistGearboxGO.Length; i++)
        {
            hoistGearboxMaterial[i].SetColor("_Color", hoistGearboxNewColor[0]);
            hoistGearboxMaterial[i].SetColor("_EmissionColor", hoistGearboxNewColor[1]);
        }
        hoistDrumMaterial.SetColor("_Color", hoistDrumNewColor[0]);
        hoistDrumMaterial.SetColor("_EmissionColor", hoistDrumNewColor[1]);
        for (int i = 0; i < dragMotorGO.Length; i++)
        {
            dragMotorMaterial[i].SetColor("_Color", dragMotorNewColor[0]);
            dragMotorMaterial[i].SetColor("_EmissionColor", dragMotorNewColor[1]);
        }
        for (int i = 0; i < dragGearboxGO.Length; i++)
        {
            dragGearboxMaterial[i].SetColor("_Color", dragGearboxNewColor[0]);
            dragGearboxMaterial[i].SetColor("_EmissionColor", dragGearboxNewColor[1]);
        }
        dragDrumMaterial.SetColor("_Color", dragDrumNewColor[0]);
        dragDrumMaterial.SetColor("_EmissionColor", dragDrumNewColor[1]);

    }

    //Function Section
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        //THis replaces the following logic
        //**********Sample Code Start*******************
        //		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
        //**********Sample Code End********************
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
}
