﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Cinemachine;
//A quick and dirty hack 

public class Camera_Zoom : MonoBehaviour
{
    private float originalFOV = 60f;
    public float zoomedFOV = 24;
    public bool zoom;
    public float zoomRate = 0.5f;
    //public GameObject defaultView;
    public CinemachineVirtualCamera virtualCam;
	// Use this for initialization
	void Start ()
    {
        originalFOV = virtualCam.m_Lens.FieldOfView;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (zoom)
        {
            virtualCam.m_Lens.FieldOfView = Mathf.Lerp(virtualCam.m_Lens.FieldOfView, zoomedFOV, zoomRate * Time.deltaTime);
        }
        else
        {
            virtualCam.m_Lens.FieldOfView = Mathf.Lerp(virtualCam.m_Lens.FieldOfView, originalFOV, zoomRate * Time.deltaTime);
        }
	}
}
