﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Motor_Generator_Vibrate : MonoBehaviour
{

    public bool vibrate = false;
    public Vector3 magnitude;
    public float vibrateTime;
    public GameObject[] mGenArray;

    // Use this for initialization
    void Start()
    {

    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKey(KeyCode.R))
        {
            vibrate = true;
        }
        //iTween.ShakePosition(gameObject, magnitude, 0.5f);
        if (vibrate == true)
        {
            for (int i = 0; i < mGenArray.Length; i++)
            {
                iTween.ShakePosition(mGenArray[i], iTween.Hash("amount", magnitude, "time", vibrateTime));
            }
            //iTween.ShakePosition(gameObject, iTween.Hash("amount", magnitude, "time", vibrateTime));
            vibrate = false;
        }


    }
}
