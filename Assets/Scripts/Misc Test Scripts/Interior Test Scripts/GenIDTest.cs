﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntPublicClass;
using System;

public class GenIDTest : MonoBehaviour
{
    private string[] innerGenNames = new string[] {"Motor Gen 1.1" , "Motor Gen 2.1", "Motor Gen 3.1", "Motor Gen 4.1"};
    private string[] middleGenNames = new string[] { "Motor Gen 1.2", "Motor Gen 2.2", "Motor Gen 3.2", "Motor Gen 4.2" };
    private string[] outerGenNames = new string[] { "Motor Gen 1.3", "Motor Gen 2.3", "Motor Gen 3.3", "Motor Gen 4.3" };

    public int[] innerGenIndex = new int[4];
    public int[] middleGenIndex = new int[4];
    public int[] outerGenIndex = new int[4];
    public GameObject[] generators;
    public string[] generatorNames;
    // Use this for initialization
    void Start ()
    {
		generators = GameObject.FindGameObjectsWithTag("Generators");
        generatorNames = new string[generators.Length];
        for (int i = 0; i < generators.Length; i++)
        {
            generatorNames[i] = generators[i].name;
        }
    }
	
	// Update is called once per frame
	void Update ()
    {
        for (int i = 0; i < innerGenNames.Length; i++)
        {
            innerGenIndex[i] = System.Array.IndexOf(generatorNames, innerGenNames[i]);
            middleGenIndex[i] = System.Array.IndexOf(generatorNames, middleGenNames[i]);
            outerGenIndex[i] = System.Array.IndexOf(generatorNames, outerGenNames[i]);
        }
	}
}
