﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Playtime_Info_Script : MonoBehaviour
{
    public Text displayText;
    public bool hide;
    private Color textColor;
	// Use this for initialization
	void Start ()
    {
        textColor = displayText.GetComponent<Text>().color;
	}
	
	// Update is called once per frame
	void Update ()
    {
        displayText.text = "Time:" + Time.time;
        if (hide == true)
        {
            textColor.a = 0f;
        }
        else
        {
            textColor.a = 1.5f;
        }
        displayText.GetComponent<Text>().color = textColor;
	}
}
