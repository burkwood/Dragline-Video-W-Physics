﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using IntPublicClass;

//public enum hJoyMove { None = 0, Left = -80 , Right = 80};
//public enum vJoyMove { None = 0, Up = 80, Down = -80};

public class JoyStickController : MonoBehaviour
{
    public Image leftJoyHandle;
    public Image rightJoyHandle;
    private Vector3 leftJoyPos;
    private Vector3 rightJoyPos;
    public float scale;
    public hJoyMove leftJoyHor;
    public vJoyMove leftJoyVert;
    public hJoyMove rightJoyHor;
    public vJoyMove rightJoyVert;
    private float leftJoyX;
    private float leftJoyY;
    private float rightJoyX;
    private float rightJoyY;
    

	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        leftJoyPos = leftJoyHandle.transform.localPosition;
        rightJoyPos = rightJoyHandle.transform.localPosition;
        if (Input.GetKey(KeyCode.T)) // Left Joy Up
        {
            leftJoyVert = vJoyMove.Up;
        }
        else if (Input.GetKey(KeyCode.G)) // Left Joy Down
        {
            leftJoyVert = vJoyMove.Down;
        }
        else
        {
            leftJoyVert = vJoyMove.None;
        }

        if (Input.GetKey(KeyCode.F)) // Left Joy Left
        {
            leftJoyHor = hJoyMove.Left;
        }
        else if (Input.GetKey(KeyCode.H))
        {
            leftJoyHor = hJoyMove.Right;
        }
        else
        {
            leftJoyHor = hJoyMove.None;
        }

        if (Input.GetKey(KeyCode.I)) // Right Joy Up
        {
            rightJoyVert = vJoyMove.Up;
        }
        else if (Input.GetKey(KeyCode.K)) // Right Joy Down
        {
            rightJoyVert = vJoyMove.Down;
        }
        else
        {
            rightJoyVert = vJoyMove.None;
        }

        if (Input.GetKey(KeyCode.J)) // Right Joy Left
        {
            rightJoyHor = hJoyMove.Left;
        }
        else if (Input.GetKey(KeyCode.L)) //Right Joy RIght
        {
            rightJoyHor = hJoyMove.Right;
        }
        else
        {
            rightJoyHor = hJoyMove.None;
        }



        //leftJoyHandle.transform.localPosition = new Vector3 ( (float)leftJoyHor, (float)leftJoyVert, 0.0f);
        leftJoyHandle.transform.localPosition = Vector3.Lerp(leftJoyPos, new Vector3((float)leftJoyHor, (float)leftJoyVert, 0.0f), Time.deltaTime * scale);
        //rightJoyHandle.transform.localPosition = new Vector3((float)rightJoyHor, (float)rightJoyVert, 0.0f);
        rightJoyHandle.transform.localPosition = Vector3.Lerp(rightJoyPos, new Vector3((float)rightJoyHor, (float)rightJoyVert, 0.0f), Time.deltaTime * scale);

	}
 
}
