﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DLRopesMomentumTest : MonoBehaviour
{
    public GameObject hoistRopeRight;
    public GameObject hoistRopeLeft;
    //public GameObject dragRopeRight;
    //public GameObject dragRopeLeft;
    //public GameObject dragDrum;
    public GameObject hoistDrum;
    public UltimateRope rightHoistRope;
    public UltimateRope leftHoistRope;
    //public UltimateRope leftDragRope;
    //public UltimateRope rightDragRope;
    public float extensionSpeed;
    public float rotateSpeed;
    public float timeScale;
    public float extensionFudge;


    public float hoistExtensionSpeed = 0;
    public float hoistRotateSpeed = 0;
    public float hoistDirection;


    public float rightHoistRopeExtension;
    public float leftHoistRopeExtension;
    public float rightDragRopeExtension;
    public float leftDragRopeExtension;
    // Use this for initialization
    void Start ()
    {
        hoistRopeRight = GameObject.FindWithTag("RightHoistRope");
        hoistRopeLeft = GameObject.FindWithTag("LeftHoistRope");
        hoistDrum = GameObject.FindWithTag("HoistDrum");
        rightHoistRope = hoistRopeRight.GetComponent<UltimateRope>();
        leftHoistRope = hoistRopeLeft.GetComponent<UltimateRope>();
        rightHoistRopeExtension = rightHoistRope != null ? rightHoistRope.m_fCurrentExtension : 0.0f;
        leftHoistRopeExtension = leftHoistRope != null ? leftHoistRope.m_fCurrentExtension : 0.0f;
    }
	
	// Update is called once per frame
	void Update ()
    {
        if (Input.GetKey(KeyCode.Z)) //Extend The Hoist Rope
        {
            rightHoistRopeExtension += Time.deltaTime * extensionSpeed;
            leftHoistRopeExtension += Time.deltaTime * extensionSpeed;
            //if (rightHoistRopeExtension < rightHoistRope.ExtensibleLength)
            //{
            //    hoistDrum.transform.Rotate(Time.deltaTime * rotateSpeed, 0.0f, 0.0f);
            //}
        }

        if (Input.GetKey(KeyCode.X)) //Retract The Hoist Rope
        {
            rightHoistRopeExtension -= Time.deltaTime * extensionSpeed;
            leftHoistRopeExtension -= Time.deltaTime * extensionSpeed;
            //if (rightHoistRopeExtension > 0)
            //{
            //    hoistDrum.transform.Rotate(-Time.deltaTime * rotateSpeed, 0.0f, 0.0f);
            //}
        }





        if (rightHoistRope != null) //Clamp max length and impose extension method
        {
            rightHoistRopeExtension = Mathf.Clamp(rightHoistRopeExtension, 0.0f, rightHoistRope.ExtensibleLength);
            rightHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightHoistRopeExtension - rightHoistRope.m_fCurrentExtension);

        }

        if (leftHoistRope != null) //Clamp max length and impose extension method
        {
            leftHoistRopeExtension = Mathf.Clamp(leftHoistRopeExtension, 0.0f, leftHoistRope.ExtensibleLength);
            leftHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftHoistRopeExtension - leftHoistRope.m_fCurrentExtension);
        }
    }
}
