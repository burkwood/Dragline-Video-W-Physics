﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Marion_Rope_Align_Manip : MonoBehaviour
{
    Marion_DL_SFX DL_SFX;
    // Use this for initialization
    void Start ()
    {
        DL_SFX = GameObject.FindWithTag("GameController").GetComponent<Marion_DL_SFX>();
    }
	
	// Update is called once per frame
	void Update ()
    {
        DL_SFX.showHide.houseBody = false;
        DL_SFX.showHide.gantry = false;
        DL_SFX.showHide.mast = false;
	}
}
