﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class CamInfoScript : MonoBehaviour {

    public Camera mainCam;
    public Text displayText;
    public Vector3 camRotation;
    public Vector3 camTransform;
    public bool hide;

    private Color textColor;


	// Use this for initialization
	void Start ()
    {
        textColor = displayText.GetComponent<Text>().color;
	}
	
	// Update is called once per frame
	void Update ()
    {
        camTransform = mainCam.transform.position;
        camRotation = mainCam.transform.rotation.eulerAngles;
        displayText.text = "PosX:" + System.Math.Round(camTransform.x,2) + ", PosY:" + System.Math.Round(camTransform.y,2) + ", PosZ:" + System.Math.Round(camTransform.z,2) +
            "\nRotX:" + System.Math.Round(camRotation.x,2) + ", RotY:" + System.Math.Round(camRotation.y,2) + ", Rotz:" + System.Math.Round(camRotation.z,2);

        if (hide == true)
        {
            textColor.a = 0f;
        }
        else
        {
            textColor.a = 1.5f;
        }
        displayText.GetComponent<Text>().color = textColor;
	}
}
