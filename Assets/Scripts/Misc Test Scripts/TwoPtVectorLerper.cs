﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TwoPtVectorLerper : MonoBehaviour
{
    public GameObject startPt;
    public GameObject endPt;
    public Vector3 lerpPt;
    public bool extrap = false;

    [Range(0, 1)]
    public float lerp;
    public float distance = 0;

    private Vector3 unitVect;


	// Use this for initialization
	void Start ()
    {
		
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (extrap == false)
        {
            lerpPt = Vector3.Lerp(startPt.transform.position, endPt.transform.position, lerp);
            gameObject.transform.position = lerpPt;
        }
        if (extrap == true)
        {
            unitVect = Vector3.Normalize(endPt.transform.position - startPt.transform.position);
            gameObject.transform.position = startPt.transform.position + unitVect * distance;
        }
	}
}
