﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shader_Test_Script : MonoBehaviour
{
    public Material material;
    public float mode;
	// Use this for initialization
	void Start () {
        material = gameObject.GetComponent<Renderer>().material;
	}
	
	// Update is called once per frame
	void Update () {
        mode = material.GetFloat("_Mode");
	}
}
