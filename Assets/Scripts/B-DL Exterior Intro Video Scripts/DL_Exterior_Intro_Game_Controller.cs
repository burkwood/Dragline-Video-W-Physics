﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class DL_Exterior_Intro_Game_Controller : MonoBehaviour
{
    public int queue = 0;                                                                                                       //The index of text queues
    public bool nextStage = false;                                                                                              //a marker used to synch the camera movement with the audio, text display and SFX. 
                                                                                                                                //This script ONLY sets it to TRUE, the Camera controller will set it to False
    public AudioClip[] audioClip;                                                                                               //Audio Player
    Obi_Bucyrus_DL_SFX DL_SFX;                                                                                                  //The script that controls all hiding and highlighting
    Fade_Screen_Controller fadeScreenController;
    Virtual_Joystick_Controller virtualJoystickController;
    Obi_Bucyrus_Controller obiBucyrusController;
    Operator_Chair_Controller operatorChairController;
    public float startTime;
    public float delayTime = 2;
    public float playTime;
    public Text captionText;
    private float[] delayTimes = new float[] { 7, 2, 4, 6, 8 };                                                                    //Corresponds to the Hold + Blend times of the camera moves
    private AudioSource playAudio;
    private bool audioTriggered = false;                                                                                        //Used to flag if the Audio Clip has been played
    private string[] captions = new string[] {

        "In this video the operator controls will be mirrored by the virtual joysticks.",									//Cap [0]

	};

    void Start ()
    {
        DL_SFX = gameObject.GetComponent<Obi_Bucyrus_DL_SFX>();                                                            //Get reference to the SFX script
        fadeScreenController = GetComponent<Fade_Screen_Controller>();                                                      //Get reference to Fade Screen Controller
        virtualJoystickController = GetComponent<Virtual_Joystick_Controller>();                            
        obiBucyrusController = GetComponent<Obi_Bucyrus_Controller>();
        operatorChairController = GetComponent<Operator_Chair_Controller>();
        playAudio = GetComponent<AudioSource>();                                                                           //Get reference to the Audio Source
        playAudio.clip = audioClip[0];                                                                                         //Load the first clip
        captionText.text = "";                                                                                             //Clear the Captions
	}
	
	// Update is called once per frame
	void Update ()
    {
        UpdatePlayTime();                                                                                                  //If Audio Clip is playing Update playtime

        //******************************************************************************************************************************
        //Start the first Queue 
        //Camera moves from Home to Interior Cab View
        if (queue == 0 && nextStage == false && Time.time > 10)                                                             //Start Queue 1
        {
            nextStage = true;                                                                                              //Trigger Camera Movement
            queue = 1;                                                                                                     //Update queue number
            startTime = Time.time;                                                                                         //Store when camera movement was triggered
        }

        //Caption & SFX Manager during the first Queue
        //"In this video the operator controls will be mirrored by the virtual joysticks." Cap [0]
        //Highlight operator controls and virtual joysticks
        if (queue ==1 && nextStage == false && Time.time > startTime + delayTimes[0])                                      //Start playing audio after camera movement is complete
        {
            if (audioTriggered == false)
            {
                playAudio.Play();                                                                                          //Play Loaded clip
                audioTriggered = true;                                                                                     //Flag audio clip has been triggered
                //startTime = Time.time;                                                                                     //Log when clip started playing
            }

            if (playAudio.isPlaying == true) {captionText.text = captions[0];}                                              //Show captions
            //Flash the Operator Controls
            if (playAudio.isPlaying == true && playTime > 1.5)                                                              
            {
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Flash;
                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Flash;
            }
            //Flash the virtual controls
            if (playAudio.isPlaying == true && playTime > 3.6)                                                              
            {
                virtualJoystickController.leftJoyHighlight = Status.Flash;
                virtualJoystickController.rightJoyHighlight = Status.Flash;
            }
            //clear SFX and captions 2s after audio clip ends
            if (playAudio.isPlaying == false && Time.time > startTime + delayTimes[0] + playAudio.clip.length + delayTime)  
            {
                captionText.text = "";
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Off;
                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Off;
                virtualJoystickController.leftJoyHighlight = Status.Off;
                virtualJoystickController.rightJoyHighlight = Status.Off;
            }
        }

        //******************************************************************************************************************************
        // Start the Second Queue 
        //No Camera Movement
        if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0] + playAudio.clip.length + delayTime && captionText.text == "")
        {
            queue = 2;
            startTime = Time.time;
            audioTriggered = false;
        }

        //Caption  & SFX Manager during the second Queue
        if (queue == 2)
        {
            //Flash Left VJoy and OpJoy && move left Sticks Up
            //ST to ST + 2
            if (Time.time < startTime + delayTime)
            {
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Flash;
                virtualJoystickController.leftJoyHighlight = Status.Flash;
                operatorChairController.leftJoyVert = vStickMove.Up;
                virtualJoystickController.leftJoyVert = vJoyMove.Up;
            }
            //Move Left Sticks Down
            //ST + 2 to ST + 4
            if (Time.time > startTime + delayTime && Time.time < startTime + delayTime * 2)
            {
                operatorChairController.leftJoyVert = vStickMove.Down;
                virtualJoystickController.leftJoyVert = vJoyMove.Down;
            }
            //Move Left Sticks left
            //ST + 4 to ST + 6
            if (Time.time > startTime + delayTime * 2 && Time.time < startTime + delayTime * 3)
            {
                operatorChairController.leftJoyVert = vStickMove.None;
                virtualJoystickController.leftJoyVert = vJoyMove.None;
                operatorChairController.leftJoyHor = hStickMove.Left;
                virtualJoystickController.leftJoyHor = hJoyMove.Left;
            }
            //Move Left Sticks Right
            //ST + 6 to ST + 8
            if (Time.time > startTime + delayTime * 3 && Time.time < startTime + delayTime * 4)
            {
                operatorChairController.leftJoyHor = hStickMove.Right;
                virtualJoystickController.leftJoyHor = hJoyMove.Right;
            }
            //Flash right VJoy and OpJoy && move right Sticks Up
            //ST + 8 to ST + 10
            if (Time.time > startTime + delayTime * 4 && Time.time < startTime + delayTime * 5)
            {
                operatorChairController.leftJoyHighlight = IntPublicClass.Status.Off;
                virtualJoystickController.leftJoyHighlight = Status.Off;
                operatorChairController.leftJoyHor = hStickMove.None;
                virtualJoystickController.leftJoyHor = hJoyMove.None;

                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Flash;
                virtualJoystickController.rightJoyHighlight = Status.Flash;
                operatorChairController.rightJoyVert = vStickMove.Up;
                virtualJoystickController.rightJoyVert = vJoyMove.Up;
            }
            //Move Right Sticks Down
            //ST + 10 to ST + 12
            if (Time.time > startTime + delayTime * 5 && Time.time < startTime + delayTime * 6)
            {
                operatorChairController.rightJoyVert = vStickMove.Down;
                virtualJoystickController.rightJoyVert = vJoyMove.Down;
            }
            //Move Right Sticks left
            //ST + 12 to ST + 14
            if (Time.time > startTime + delayTime * 6 && Time.time < startTime + delayTime * 7)
            {
                operatorChairController.rightJoyVert = vStickMove.None;
                virtualJoystickController.rightJoyVert = vJoyMove.None;
                operatorChairController.rightJoyHor = hStickMove.Left;
                virtualJoystickController.rightJoyHor = hJoyMove.Left;
            }
            //Move Right Sticks Right
            //ST + 14 to ST + 16
            if (Time.time > startTime + delayTime * 7 && Time.time < startTime + delayTime * 8)
            {
                operatorChairController.rightJoyHor = hStickMove.Right;
                virtualJoystickController.rightJoyHor = hJoyMove.Right;
            }
            //Stop all flashing and return all sticks to neutral position
            //ST + 16 to ST + 18
            if (Time.time >  startTime + delayTime * 8 && Time.time < startTime + delayTime * 9)
            {
                operatorChairController.rightJoyHor = hStickMove.None;
                virtualJoystickController.rightJoyHor = hJoyMove.None;
                operatorChairController.rightJoyHighlight = IntPublicClass.Status.Off;
                virtualJoystickController.rightJoyHighlight = Status.Off;
            }
            //Fade to Black
            //ST + 18 to end
            if (Time.time > startTime + delayTime * 9)
            {
                fadeScreenController.fadeMessage = "";
                fadeScreenController.showFadeScreen = true;
            }
        }


    }

    //******************************************************************************************************************************
    //Function Section
    //Auto Update or reset play time
    void UpdatePlayTime()
    {
        if (playAudio.isPlaying)
        {
            playTime = playAudio.time;
        }
        else
        {
            playTime = 0;
        }
    }
}
