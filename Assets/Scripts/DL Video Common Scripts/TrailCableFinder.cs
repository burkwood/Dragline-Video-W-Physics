﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public class TrailCableFinder : MonoBehaviour
{
    public float flashDuration = 0.8f;
    public float colorLerp;
    public Highlight fromPowerStationStatus;
    public Highlight toDraglineStatus;
    public bool show; 
    //public bool highlightTrailCable1;
    //public bool highlightTrailCable2;
    //******************************************************************************************************************************
    //Game Objects Section
    private GameObject[] trailCables;
    //******************************************************************************************************************************
    //Materials Section
    private Material fromPowerStationMaterial;
    private Material toDraglineMaterial;
    //******************************************************************************************************************************
    //Colors Section
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    //Trail Cable
    private Color fromPowerStationOriginalColor;
    private Color fromPowerStationOriginalEColor;
    private Color fromPowerStationNewColor;
    private Color fromPowerStationNewEColor;

    private Color toDraglineOriginalColor;
    private Color toDraglineOriginalEColor;
    private Color toDraglineNewColor;
    private Color toDraglineNewEColor;

    // Use this for initialization
    void Start ()
    {
        //******************************************************************************************************************************
        //Define Game Objects
        trailCables = GameObject.FindGameObjectsWithTag("TrailCable");
        //******************************************************************************************************************************
        //Define Materials
        //for (int i = 0; i < trailCables.Length; i++)
        //{
        //    trailCableMaterials[i] = trailCables[i].GetComponent<Renderer>().material;
        //}
        fromPowerStationMaterial = trailCables[1].GetComponent<Renderer>().material;
        toDraglineMaterial = trailCables[0].GetComponent<Renderer>().material;
        //******************************************************************************************************************************
        //Define Colors
        //for (int i = 0; i < trailCables.Length; i++)
        //{
        //    trailCableOriginalColor[i] = trailCableMaterials[i].color;
        //    trailCableOriginalEColor[i] = trailCableMaterials[i].GetColor("_EmissionColor");
        //}
        fromPowerStationOriginalColor = fromPowerStationMaterial.color;
        fromPowerStationOriginalEColor = fromPowerStationMaterial.GetColor("_EmissionColor");
        toDraglineOriginalColor = toDraglineMaterial.color;
        toDraglineOriginalEColor = toDraglineMaterial.GetColor("_EmissionColor");
        //******************************************************************************************************************************
        //Set Defaults
        show = true;
    }

    // Update is called once per frame
    void Update ()
    {
        colorLerp = Mathf.PingPong(Time.time, flashDuration) / flashDuration;
        //******************************************************************************************************************************
        //Show/Hide Section
        for (int i = 0; i < trailCables.Length; i++)
        {
            trailCables[i].SetActive(show);
        }
        //******************************************************************************************************************************
        //Highlight Section
        fromPowerStationStatus.highlight = FlashStatus((int)fromPowerStationStatus.status);
        fromPowerStationNewColor = Color.Lerp(fromPowerStationOriginalColor, highlightColor, fromPowerStationStatus.highlight);
        fromPowerStationNewEColor = Color.Lerp(fromPowerStationOriginalEColor, highlightColor, fromPowerStationStatus.highlight);
        fromPowerStationMaterial.SetColor("_Color", fromPowerStationNewColor);
        fromPowerStationMaterial.SetColor("_EmissionColor", fromPowerStationNewEColor);

        toDraglineStatus.highlight = FlashStatus((int)toDraglineStatus.status);
        toDraglineNewColor = Color.Lerp(toDraglineOriginalColor, highlightColor, toDraglineStatus.highlight);
        toDraglineNewEColor = Color.Lerp(toDraglineOriginalEColor, highlightColor, toDraglineStatus.highlight);
        toDraglineMaterial.SetColor("_Color", toDraglineNewColor);
        toDraglineMaterial.SetColor("_EmissionColor", toDraglineNewEColor);

    }

    //******************************************************************************************************************************
    //Function Section
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
}
