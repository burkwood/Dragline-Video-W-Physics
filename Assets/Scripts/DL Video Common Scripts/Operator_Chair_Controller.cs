﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntPublicClass;
//THis script is used to control the operatos chair joysticks
public class Operator_Chair_Controller : MonoBehaviour
{
    //Game Objects Definition
    private GameObject leftJoystick;                                    //Joystick Parent for rotation purposes
    private GameObject[] leftJoychildren = new GameObject[1];           //Parent Children array used in finding the child
    private GameObject leftJoyChild;                                    //Joystick Actual for highlighting logic
    private GameObject rightJoystick;                                   //Joystick Parent for rotation purposes
    private GameObject[] rightJoyChildren = new GameObject[1];          //Parent Children array used in finding the child   
    private GameObject rightJoyChild;                                   //Joystick Actual for highlighting logic
    //Material Definition
    private Material[] leftJoyMaterial = new Material[2];
    private Material[] rightJoyMaterial = new Material[2];
    //Color Definition
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    private Color leftJoyOriginalColor;
    private Color leftJoyOriginalEColor;
    private Color leftJoyNewColor;
    private Color leftJoyNewEColor;
    private Color rightjoyOriginalColor;
    private Color rightJoyOriginalEColor;
    private Color rightJoyNewColor;
    private Color rightJoyNewEColor;
    //Numerical Constants
    private Vector3 leftStickRot;
    private Vector3 rightStickRot;
    private Vector3 leftStickTemp;
    private Vector3 rightStickTemp;
    private float leftJoyCount = 0;
    private float rightJoyCount = 0;
    private float colorLerp;
    //Public Variables
    public float stickMoveSpeed;                                    //joystick movement speed
    public float highlightDuration;                                 //Highlight duration
    public IntPublicClass.Status leftJoyHighlight;
    public hStickMove leftJoyHor;
    public vStickMove leftJoyVert;
    public IntPublicClass.Status rightJoyHighlight;
    public hStickMove rightJoyHor;
    public vStickMove rightJoyVert;




    // Use this for initialization
    void Start ()
    {
        //Find Left Joystick parent, actual, material and colors
        leftJoystick = GameObject.FindWithTag("LeftJoystick");
        foreach (Transform i in leftJoystick.transform)
        {
            leftJoychildren[(int)leftJoyCount] = i.gameObject;
            leftJoyCount++;
        }
        leftJoyChild = leftJoychildren[0];
        leftJoyMaterial = leftJoyChild.GetComponent<Renderer>().materials;
        //Note that of the two materials for the Joystick the first one is the body the second is the tip
        leftJoyOriginalColor = leftJoyMaterial[1].color;
        leftJoyOriginalEColor = leftJoyMaterial[1].GetColor("_EmissionColor");
        //Find Right Joystick parent, actual, material and colors
        rightJoystick = GameObject.FindWithTag("RightJoystick");
        foreach (Transform j in rightJoystick.transform)
        {
            rightJoyChildren[(int)rightJoyCount] = j.gameObject;
            rightJoyCount++;
        }
        rightJoyChild = rightJoyChildren[0];
        rightJoyMaterial = rightJoyChild.GetComponent<Renderer>().materials;
        //Note that of the two materials for the Joystick the first one is the body the second is the tip
        rightjoyOriginalColor = rightJoyMaterial[1].color;
        rightJoyOriginalEColor = rightJoyMaterial[1].GetColor("_EmissionColor");
    }

    // Update is called once per frame
    void Update ()
    {
        colorLerp = Mathf.PingPong(Time.time, highlightDuration) / highlightDuration;

        leftStickRot = leftJoystick.transform.localEulerAngles;                                                            //Get joystick current rotation
        leftStickTemp.x = Mathf.LerpAngle(leftStickRot.x, (float)leftJoyVert, Time.deltaTime * stickMoveSpeed);            //Interpolate between current angle and commanded one
        leftStickTemp.z = Mathf.LerpAngle(leftStickRot.z, (float)leftJoyHor, Time.deltaTime * stickMoveSpeed);             //Interpolate between current angle and commanded one
        leftJoystick.transform.localEulerAngles = leftStickTemp;
        leftJoyNewColor = Color.Lerp(leftJoyOriginalColor, highlightColor, FlashStatus((int)leftJoyHighlight));
        leftJoyNewEColor = Color.Lerp(leftJoyOriginalEColor, highlightColor, FlashStatus((int)leftJoyHighlight));
        leftJoyMaterial[1].SetColor("_Color", leftJoyNewColor);
        leftJoyMaterial[1].SetColor("_EmissionColor", leftJoyNewEColor);

        rightStickRot = rightJoystick.transform.localEulerAngles;                                                          //Get joystick current rotation
        rightStickTemp.x = Mathf.LerpAngle(rightStickRot.x, (float)rightJoyVert, Time.deltaTime * stickMoveSpeed);         //Interpolate between current angle and commanded one
        rightStickTemp.z = Mathf.LerpAngle(rightStickRot.z, (float)rightJoyHor, Time.deltaTime * stickMoveSpeed);          //Interpolate between current angle and commanded one
        rightJoystick.transform.localEulerAngles = rightStickTemp;
        rightJoyNewColor = Color.Lerp(rightjoyOriginalColor, highlightColor, FlashStatus((int)rightJoyHighlight));
        rightJoyNewEColor = Color.Lerp(rightJoyOriginalEColor, highlightColor, FlashStatus((int)rightJoyHighlight));
        rightJoyMaterial[1].SetColor("_Color", rightJoyNewColor);
        rightJoyMaterial[1].SetColor("_EmissionColor", rightJoyNewEColor);
    }

    //********************************************************************************
    //FUNCTION SECTION
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        //THis replaces the following logic
        //**********Sample Code Start*******************
        //		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
        //**********Sample Code End********************
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
}
