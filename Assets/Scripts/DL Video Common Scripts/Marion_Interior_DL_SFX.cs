﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Video;
using IntPublicClass;

public class Marion_Interior_DL_SFX : MonoBehaviour
{
    //The decleration section will be defined by object as opposed to class due to the large quantity of data types per object
    //Variables are declared here
    #region Hoist_Drag_Ropes
    //Hoist & Drag Ropes start
    private GameObject hoistRopeRight;
    private GameObject hoistRopeLeft;
    private GameObject dragRopeRight;
    private GameObject dragRopeLeft;

    private UltimateRope rightHoistRope;
    private UltimateRope leftHoistRope;
    private UltimateRope rightDragRope;
    private UltimateRope leftDragRope;

    private float hoistExtensionSpeed = 0f;
    private float hoistDirection;            //Either 1 or -1 indicates CW or CCW 
    private float dragExtensionSpeed = 0f;
    private float dragDirection;             //Either 1 or -1 indicates CW or CCW 
    private float rightHoistRopeExtension;
    private float leftHoistRopeExtension;
    private float rightDragRopeExtension;
    private float leftDragRopeExtension;

    public MotorControl hoistMCtrl;
    public MotorControl dragMCtrl;
    public float ropeExtensionSpeed;            //Max extension rate
    public float ropeTimeScale;
    #endregion
    #region Generator_Animation
    //Generator Set Text & Animation
    private GameObject[] genStatus;
    private GameObject generator;
    private GameObject[] genCanvasArray;

    private Color gensetOriginalColor;
    private Color gensetHighlightColor = new Color(0f, 255f, 0f);
    private Color gensetNewColor;

    private float textSlowLerp;
    private float textFastLerp;

    private string gensetStatusText;

    public bool gensetOn = false;
    public Status genHighlightStatus;
    public bool hoistDispOn = true;
    public bool dragDispOn = true;
    public float textDuration;
    public float genVibeDuration;
    public Vector3 genVibeMagnitude;
    #endregion
    #region Movie_Screens
    //Movie Screens
    private GameObject screenSource;
    private GameObject playerSource;
    private GameObject[] movieScreens;
    private GameObject[] moviePlayers;
    //private VideoPlayer[] moviePlayersActual;
    private float screenCount = 0;
    private float playerCount = 0;
    public AllScreens allScreens;
    private Vector3 screenFolded = new Vector3(0f, 0f, 0f);
    private Vector3 screenExpanded = new Vector3(4.44f, 2.5f, 0.2647108f);
    public float screenScaleTime;
    public VideoClip[] hoistClips;
    public VideoClip[] dragClips;
    #endregion
    #region Highlighting Static Objects
    //GameObject Section
    //private GameObject[] swingMotorGO;              //All Swing Motors Game Objects
    private GameObject hoistMotorGO;              //All Hoist Motor Game Objects
    private GameObject hoistGearboxGO;            //All Hoist Gearbox Game Objects
    private GameObject[] hoistDrumGO;                 //Hoist Drum
    private GameObject dragMotorGO;               //All Drag Motors Game Objects
    private GameObject dragGearboxGO;             //All Drag Gearbox Game Objects
    private GameObject[] dragDrumGO;                  //Drag Drum
    //Material Section
    
    //Color Section
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
   
    //Numerical Class Section
    private float colorLerp;
    public float highlightDuration;
    //Public Class Section
    //public Status swingMotorStatus;
    //public Status swingGearboxStatus;
    public Status hoistMotorStatus;
    public Status hoistGearboxStatus;
    public Status hoistDrumStatus;
    public Status dragMotorStatus;
    public Status dragGearboxStatus;
    public Status dragDrumStatus;
    #endregion
    // Use this for initialization


    void Start ()
    {
        //Find Object Reference
        //Generator Related
        genStatus = GameObject.FindGameObjectsWithTag("GenSetStatus");
        gensetOriginalColor = genStatus[0].GetComponent<Text>().color;
        generator = GameObject.FindWithTag("Generators");
        genCanvasArray = GameObject.FindGameObjectsWithTag("GenSetCanvas");

        //Rope Related
        hoistRopeRight = GameObject.FindWithTag("RightHoistRope");
        hoistRopeLeft = GameObject.FindWithTag("LeftHoistRope");
        dragRopeRight = GameObject.FindWithTag("RightDragRope");
        dragRopeLeft = GameObject.FindWithTag("LeftDragRope");
        rightHoistRope = hoistRopeRight.GetComponent<UltimateRope>();
        leftHoistRope = hoistRopeLeft.GetComponent<UltimateRope>();
        rightDragRope = dragRopeRight.GetComponent<UltimateRope>();
        leftDragRope = dragRopeLeft.GetComponent<UltimateRope>();
        rightHoistRopeExtension = rightHoistRope != null ? rightHoistRope.m_fCurrentExtension : 0.0f;
        leftHoistRopeExtension = leftHoistRope != null ? leftHoistRope.m_fCurrentExtension : 0.0f;
        rightDragRopeExtension = rightDragRope != null ? rightDragRope.m_fCurrentExtension : 0.0f;
        leftDragRopeExtension = leftDragRope != null ? leftDragRope.m_fCurrentExtension : 0.0f;

        //Movie Screen related
        screenSource = GameObject.FindWithTag("MovieScreenParent");
        playerSource = GameObject.FindWithTag("MoviePlayerParent");
        movieScreens = new GameObject[screenSource.transform.childCount];
        moviePlayers = new GameObject[playerSource.transform.childCount];
        //Find all movie screens
        foreach (Transform i in screenSource.transform) //Find all the screens under the Parent object
        {
            movieScreens[(int)screenCount] = i.gameObject;
            screenCount++;
        }
        //Find all movie Players
        foreach (Transform j in playerSource.transform)
        {
            moviePlayers[(int)playerCount] = j.gameObject;
            playerCount++;
        }
        //Shrink all movie screens
        for (int i = 0; i < movieScreens.Length; i++)
        {
            movieScreens[i].transform.localScale = new Vector3(0f, 0f, 0f);
        }

        //Highlighting Related
        //swingMotorGO = GameObject.FindGameObjectsWithTag("SwingMotor");
        hoistMotorGO = GameObject.FindGameObjectWithTag("HoistMotor");
        hoistGearboxGO = GameObject.FindGameObjectWithTag("HoistGearBoxes");
        hoistDrumGO = GameObject.FindGameObjectsWithTag("HoistDrum");
        dragMotorGO = GameObject.FindGameObjectWithTag("DragMotor");
        dragGearboxGO = GameObject.FindGameObjectWithTag("DragGearBoxes");
        dragDrumGO = GameObject.FindGameObjectsWithTag("DragDrum");
    }
	
	// Update is called once per frame
	void Update ()
    {
       
        //Text Flash Speed Calculations
        textFastLerp = Mathf.PingPong(Time.time, textDuration) / textDuration;
        textSlowLerp = Mathf.PingPong(Time.time, textDuration * 2) / textDuration * 2;

        //********************************************************************************
        //Hoist and Drag Rope Controls
        //Hoist Ropes
        if (hoistMCtrl == MotorControl.Extend)
        {
            hoistDirection = 1;
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime); //Ramp Up 
            rightHoistRopeExtension += Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension += Time.deltaTime * hoistExtensionSpeed;
        }
        else if (hoistMCtrl == MotorControl.Retract)
        {
            hoistDirection = -1;
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime); //Ramp Up
            rightHoistRopeExtension -= Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension -= Time.deltaTime * hoistExtensionSpeed;
        }
        else
        {
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, 0f, ropeTimeScale * Time.deltaTime); //Ramp Down
            rightHoistRopeExtension = rightHoistRopeExtension + hoistDirection * Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension = leftHoistRopeExtension + hoistDirection * Time.deltaTime * hoistExtensionSpeed;
        }
        //Drag Ropes
        if (dragMCtrl == MotorControl.Extend)
        {
            dragDirection = 1;
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime); //Ramp Up
            rightDragRopeExtension += Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension += Time.deltaTime * dragExtensionSpeed;
        }
        else if (dragMCtrl == MotorControl.Retract)
        {
            dragDirection = -1;
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime);
            rightDragRopeExtension -= Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension -= Time.deltaTime * dragExtensionSpeed;
        }
        else
        {
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, 0, ropeTimeScale * Time.deltaTime);
            rightDragRopeExtension = rightDragRopeExtension + dragDirection * Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension = leftDragRopeExtension + dragDirection * Time.deltaTime * dragExtensionSpeed;
        }

        //Required Code for Ultimate Rope Editor
        if (rightHoistRope != null) //Clamp max length and impose extension method
        {
            rightHoistRopeExtension = Mathf.Clamp(rightHoistRopeExtension, 0.0f, rightHoistRope.ExtensibleLength);
            rightHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightHoistRopeExtension - rightHoistRope.m_fCurrentExtension);

        }
        if (leftHoistRope != null) //Clamp max length and impose extension method
        {
            leftHoistRopeExtension = Mathf.Clamp(leftHoistRopeExtension, 0.0f, leftHoistRope.ExtensibleLength);
            leftHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftHoistRopeExtension - leftHoistRope.m_fCurrentExtension);
        }
        if (rightDragRope != null) //Clamp max length and impose extension method
        {
            rightDragRopeExtension = Mathf.Clamp(rightDragRopeExtension, 0.0f, rightDragRope.ExtensibleLength);
            rightDragRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightDragRopeExtension - rightDragRope.m_fCurrentExtension);
        }
        if (leftDragRope != null) //Clamp max length and impose extension method
        {
            leftDragRopeExtension = Mathf.Clamp(leftDragRopeExtension, 0.0f, leftDragRope.ExtensibleLength);
            leftDragRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftDragRopeExtension - leftDragRope.m_fCurrentExtension);
        }

        //********************************************************************************
        //Gen Set Text & Animation Controls
        //Modify Text Settings (color, text, flash speed) and Generator vibrate animation
        if (gensetOn == true)  //Set the "Generator Engaged" text, color and flash speed
        {
            gensetNewColor = gensetHighlightColor;
            gensetNewColor.a = textFastLerp;
            gensetStatusText = "Engaged";
            iTween.ShakePosition(generator, iTween.Hash("amount", genVibeMagnitude, "time", genVibeDuration));
            //for (int i = 0; i < genArray.Length; i++)
            //{
            //    iTween.ShakePosition(genArray[i], iTween.Hash("amount", genVibeMagnitude, "time", genVibeDuration));
            //}
        }
        else                   //Set the "Generator Idle" text, color and flash speed
        {
            gensetNewColor = gensetOriginalColor;
            gensetNewColor.a = textSlowLerp;
            gensetStatusText = "Idle";
        }
        //Write New Text Properties
        for (int i = 0; i < genStatus.Length; i++) //Write new text properties
        {
            genStatus[i].GetComponent<Text>().color = gensetNewColor;
            genStatus[i].GetComponent<Text>().text = gensetStatusText;
        }
        //Control visability of the generator Status
        genCanvasArray[1].SetActive(hoistDispOn);
        genCanvasArray[0].SetActive(dragDispOn);
        //Calculate generator new color
        KwHighlighter(generator, (int)genHighlightStatus);

        //********************************************************************************
        //VMovie Screen Controls

        if (allScreens.hoistScreen.open == true)
        {
            iTween.ScaleTo(movieScreens[0], screenExpanded, screenScaleTime);
        }
        else
        {
            iTween.ScaleTo(movieScreens[0], screenFolded, screenScaleTime);
        }

        if (allScreens.dragScreen.open == true)
        {
            iTween.ScaleTo(movieScreens[1], screenExpanded, screenScaleTime);
        }
        else
        {
            iTween.ScaleTo(movieScreens[1], screenFolded, screenScaleTime);
        }
        MovieController(0, allScreens.hoistScreen, hoistClips);
        MovieController(1, allScreens.dragScreen, dragClips);

        //********************************************************************************
        //Motor & Gearbox highlighting logic
        KwHighlighter(hoistMotorGO, (int)hoistMotorStatus);
        KwHighlighter(hoistGearboxGO, (int)hoistGearboxStatus);
        for (int i = 0; i < hoistDrumGO.Length; i++)
        {
            KwHighlighter(hoistDrumGO[i], (int)hoistDrumStatus);
        }
        KwHighlighter(dragMotorGO, (int)dragMotorStatus);
        KwHighlighter(dragGearboxGO, (int)dragGearboxStatus);
        for (int i = 0; i < dragDrumGO.Length; i++)
        {
            KwHighlighter(dragDrumGO[i], (int)dragDrumStatus);
        }
    }

    //********************************************************************************
    //FUNCTION SECTION

    void MovieController(int playerIndex, ScreenController screen, VideoClip[] clips)
    {
        VideoPlayer videoPlayer;
        videoPlayer = moviePlayers[playerIndex].GetComponent<VideoPlayer>();
        if (screen.secondClip == true && clips.Length > 1)              //If second clip is selected and there is a second clip
        {
            videoPlayer.clip = clips[1];                                //Load second clip
        }
        else
        {
            videoPlayer.clip = clips[0];                                //Load first clip
        }
        if (movieScreens[playerIndex].transform.localScale == screenExpanded)   //If the Screen is Expanded
        {
            if (screen.play == false)                                           //If play is not enabled
            {
                videoPlayer.Pause();                                            //Pause the player
                if (screen.secondClip == true && clips.Length > 1)              //If second clip is selected and there is a second clip
                {
                    videoPlayer.clip = clips[1];                                //Load second clip
                }
                else
                {
                    videoPlayer.clip = clips[0];                                //Load first clip
                }
            }
            else
            {

                videoPlayer.Play();                                             //Play video
            }
        }
        else
        {
            videoPlayer.Pause();                                                //Pause the video is the screen is not fully expanded
        }
    }
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        //THis replaces the following logic
        //**********Sample Code Start*******************
        //		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
        //**********Sample Code End********************
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
    void KwHighlighter (GameObject thing, int status)
    {
        //This function requires that the object passed to it has two scripts attached:
        //kw_HighlightableObject and kw_Highlight_controller
        //THis function is used for objects with PBR Substance textures where the main color parameters were not exposed during texture creation
        switch (status)
        {
            case 2: //Solid
                thing.GetComponent<kw_highlight_controller>().highlight = true;
                thing.GetComponent<kw_HighlightableObject>().bouncing = false;
                break;
            case 1: //Flash
                thing.GetComponent<kw_highlight_controller>().highlight = true;
                thing.GetComponent<kw_HighlightableObject>().bouncing = true;
                break;
            case 0: //Off
                thing.GetComponent<kw_highlight_controller>().highlight = false;
                break;
            default:
                thing.GetComponent<kw_highlight_controller>().highlight = false;
                break;

        }

    }
}
