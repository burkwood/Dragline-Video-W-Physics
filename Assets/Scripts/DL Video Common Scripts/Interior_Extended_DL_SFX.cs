﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Video;
using IntPublicClass;
//This file calls from Interior_Public_Classes file
//This file is intended to be a reusable script to be added to a game controller game object in any interior dragline scene
//It contains the logic for all interion SFX.

public class Interior_Extended_DL_SFX : MonoBehaviour
{
    //The decleration section will be defined by object as opposed to class due to the large quantity of data types per object
    //Variables are declared here
    #region Hoist_Drag_Ropes
    //Hoist & Drag Ropes start
    private GameObject hoistRopeRight;
    private GameObject hoistRopeLeft;
    private GameObject dragRopeRight;
    private GameObject dragRopeLeft;

    private UltimateRope rightHoistRope;
    private UltimateRope leftHoistRope;
    private UltimateRope rightDragRope;
    private UltimateRope leftDragRope;

    private float hoistExtensionSpeed = 0f;
    private float hoistDirection;            //Either 1 or -1 indicates CW or CCW 
    private float dragExtensionSpeed = 0f;
    private float dragDirection;             //Either 1 or -1 indicates CW or CCW 
    private float rightHoistRopeExtension;
    private float leftHoistRopeExtension;
    private float rightDragRopeExtension;
    private float leftDragRopeExtension;

    public MotorControl hoistMCtrl;
    public MotorControl dragMCtrl;
    public float ropeExtensionSpeed;            //Max extension rate
    public float ropeTimeScale;
    #endregion
    #region Generator_Animation
    //Generator Set Text & Animation
    private GameObject[] genStatus;
    private GameObject[] genArray;
    private GameObject[] genCanvasArray;

    private Material[] genMaterial;

    private Color gensetOriginalColor;
    private Color gensetHighlightColor = new Color(0f, 255f, 0f);
    private Color gensetNewColor;
    private Color genOriginalColor;
    private Color genOriginalEColor;
    private Color genNewColor;
    private Color genNewEColor;
    private Color innerGenNewColor, innerGenNewEColor;
    private Color middleGenNewColor, middleGenNewEColor;
    private Color outerGenNewColor, outerGenNewEColor;
   
    private float textSlowLerp;
    private float textFastLerp;

    private string gensetStatusText;
    private string[] innerGenNames = new string[] { "Motor Gen 1.1", "Motor Gen 2.1", "Motor Gen 3.1", "Motor Gen 4.1" };
    private string[] middleGenNames = new string[] { "Motor Gen 1.2", "Motor Gen 2.2", "Motor Gen 3.2", "Motor Gen 4.2" };
    private string[] outerGenNames = new string[] { "Motor Gen 1.3", "Motor Gen 2.3", "Motor Gen 3.3", "Motor Gen 4.3" };
    private string[] genArrayNames;

    public GeneratorSFX innerGen;
    public GeneratorSFX middleGen;
    public GeneratorSFX outerGen;

    public bool gensetOn = false;

    //private int[] innerGenIndex = new int[] { 0, 2, 4, 9 };
    //private int[] middleGenIndex = new int[] { 1, 3, 5, 10 };
    //private int[] outerGenIndex = new int[] { 6, 7, 8, 11 };
    private int[] innerGenIndex = new int[4];
    private int[] middleGenIndex = new int[4];
    private int[] outerGenIndex = new int[4];

    public bool hoistDispOn = true;
    public bool dragDispOn = true;
    public float textDuration;
    public float genVibeDuration;
    public Vector3 genVibeMagnitude;
    #endregion
    #region Virtual_Joysticks
    //Virtual Joysticks
    //private GameObject leftJoyHandle;
    //private GameObject rightJoyHandle;

    //private Vector3 leftJoyPos;
    //private Vector3 rightJoyPos;

    //public float joyMoveSpeed;
    //public hJoyMove leftJoyHor;
    //public vJoyMove leftJoyVert;
    //public hJoyMove rightJoyHor;
    //public vJoyMove rightJoyVert;

    #endregion
    #region Movie_Screens
    //Movie Screens
    private GameObject screenSource;
    private GameObject playerSource;
    private GameObject[] movieScreens;
    private GameObject[] moviePlayers;
    //private VideoPlayer[] moviePlayersActual;
    private float screenCount = 0;
    private float playerCount = 0;
    public AllScreens allScreens;
    private Vector3 screenFolded = new Vector3(0f, 0f, 0f);
    private Vector3 screenExpanded = new Vector3(4.44f, 2.5f, 0.2647108f);
    public float screenScaleTime;
    public VideoClip[] hoistClips;
    public VideoClip[] dragClips;
    #endregion
    #region Fade_Screen
    //Fade Screen
    //private GameObject fadeScreen;
    //private GameObject fadeText;
    //private float fadedAlpha = 0f;
    //private float solidAlpha = 1.5f;
    //private Color fadeScreenColor;
    //private Color fadeTextColor;
    //public string fadeMessage;
    //public bool showFadeScreen = false;
    //public bool showFadeText = false;
    //public float fadeToBlackTime;

    #endregion
    #region Highlighting Static Objects
    //GameObject Section
    private GameObject[] swingMotorGO;              //All Swing Motors Game Objects
    private GameObject[] hoistMotorGO;              //All Hoist Motor Game Objects
    private GameObject[] hoistGearboxGO;            //All Hoist Gearbox Game Objects
    private GameObject hoistDrumGO;                 //Hoist Drum
    private GameObject[] dragMotorGO;               //All Drag Motors Game Objects
    private GameObject[] dragGearboxGO;             //All Drag Gearbox Game Objects
    private GameObject dragDrumGO;                  //Drag Drum
    private GameObject[] syncMotors;                //Sync Motors
    //Material Section
    private Material[] swingMotorMaterial;          //All Swing Motor Materialss 
    private Material[] swingGearboxMaterial;        //All Swing Gearbox Materials
    private Material[] hoistMotorMaterial;         //All Hoist Motor Materials
    private Material[] hoistGearboxMaterial;       //All Hoist Gearbox Materials
    private Material hoistDrumMaterial;
    private Material[] dragMotorMaterial;           //All Drag Motor Material
    private Material[] dragMotorBreakMaterial;      //ALl Drag Motor Brake Material
    private Material[] dragGearboxMaterial;         //All Drag Gearbox Materials
    private Material dragDrumMaterial;
    private Material[] syncMotorMaterials;
    //Color Section
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    private Color[] swingMotorOriginalColor = new Color[2];
    private Color[] swingGearOriginalColor = new Color[2];
    private Color[] swingMotorNewColor = new Color[2];
    private Color[] swingGearNewColor = new Color[2];

    private Color[] hoistMotorOriginalColor = new Color[2];
    private Color[] hoistGearboxOriginalColor = new Color[2];
    private Color[] hoistDrumOriginalColor = new Color[2];
    private Color[] hoistMotorNewColor = new Color[2];
    private Color[] hoistGearboxNewColor = new Color[2];
    private Color[] hoistDrumNewColor = new Color[2];

    private Color[] dragMotorOriginalColor = new Color[2];
    private Color[] dragMotorBreakOriginalColor = new Color[2];
    private Color[] dragGearboxOriginalColor = new Color[2];
    private Color[] dragDrumOriginalColor = new Color[2];
    private Color[] dragMotorNewColor = new Color[2];
    private Color[] dragMotorBreakNewColor = new Color[2];
    private Color[] dragGearboxNewColor = new Color[2];
    private Color[] dragDrumNewColor = new Color[2];

    private Color[] syncMotorOriginalColor = new Color[2];
    private Color[] syncMotorNewColor = new Color[2];
    //private Color syncMotorOriginalColor, syncMotorOriginalEColor;
    //private Color syncMotorNewColor, syncMotorNewEColor;
    //Numerical Class Section
    private float colorLerp;
    public float highlightDuration;
    //Public Class Section
    public float motorVibeDuration;
    public Vector3 motorVibeMagnitude;
    public Status syncMotorStatus;
    public Status swingMotorStatus;
    public Status swingGearboxStatus;
    public bool hoistMotorVibrate;
    public Status hoistMotorStatus;
    public Status hoistGearboxStatus;
    public Status hoistDrumStatus;
    public bool dragMotorVibrate;
    public Status dragMotorStatus;
    public Status dragMotorBreakStatus;
    public Status dragGearboxStatus;
    public Status dragDrumStatus;
    #endregion


    void Start()
    {
        //Find Object References
        //Rope Related
        hoistRopeRight = GameObject.FindWithTag("RightHoistRope");
        hoistRopeLeft = GameObject.FindWithTag("LeftHoistRope");
        dragRopeRight = GameObject.FindWithTag("RightDragRope");
        dragRopeLeft = GameObject.FindWithTag("LeftDragRope");
        rightHoistRope = hoistRopeRight.GetComponent<UltimateRope>();
        leftHoistRope = hoistRopeLeft.GetComponent<UltimateRope>();
        rightDragRope = dragRopeRight.GetComponent<UltimateRope>();
        leftDragRope = dragRopeLeft.GetComponent<UltimateRope>();
        rightHoistRopeExtension = rightHoistRope != null ? rightHoistRope.m_fCurrentExtension : 0.0f;
        leftHoistRopeExtension = leftHoistRope != null ? leftHoistRope.m_fCurrentExtension : 0.0f;
        rightDragRopeExtension = rightDragRope != null ? rightDragRope.m_fCurrentExtension : 0.0f;
        leftDragRopeExtension = leftDragRope != null ? leftDragRope.m_fCurrentExtension : 0.0f;
        //Generator Related
        genStatus = GameObject.FindGameObjectsWithTag("GenSetStatus");
        gensetOriginalColor = genStatus[0].GetComponent<Text>().color;
        genArray = GameObject.FindGameObjectsWithTag("Generators");
        genArrayNames = new string[genArray.Length];
        for (int i = 0; i < genArray.Length; i++)
        {
            genArrayNames[i] = genArray[i].name;
        }
        for (int i = 0; i < innerGenNames.Length; i++)
        {
            innerGenIndex[i] = System.Array.IndexOf(genArrayNames, innerGenNames[i]);
            middleGenIndex[i] = System.Array.IndexOf(genArrayNames, middleGenNames[i]);
            outerGenIndex[i] = System.Array.IndexOf(genArrayNames, outerGenNames[i]);
        }
        genCanvasArray = GameObject.FindGameObjectsWithTag("GenSetCanvas");
        genMaterial = new Material[genArray.Length];
        for (int i = 0; i < genArray.Length; i++)
        {
            genMaterial[i] = genArray[i].GetComponent<Renderer>().material;
        }
        genOriginalColor = genMaterial[0].color;
        genOriginalEColor = genMaterial[0].GetColor("_EmissionColor");
        screenSource = GameObject.FindWithTag("MovieScreenParent");
        playerSource = GameObject.FindWithTag("MoviePlayerParent");
        //Movie Screen Related
        //Define Array Lengths
        movieScreens = new GameObject[screenSource.transform.childCount];
        moviePlayers = new GameObject[playerSource.transform.childCount];
        //Find all movie screens
        foreach (Transform i in screenSource.transform) //Find all the screens under the Parent object
        {
            movieScreens[(int)screenCount] = i.gameObject;
            screenCount++;
        }
        //Find all movie Players
        foreach (Transform j in playerSource.transform)
        {
            moviePlayers[(int)playerCount] = j.gameObject;
            playerCount++;
        }
        //Shrink all movie screens
        for (int i = 0; i < movieScreens.Length; i++)
        {
            movieScreens[i].transform.localScale = new Vector3(0f, 0f, 0f);
        }
        //Highlighting Related
        syncMotors = GameObject.FindGameObjectsWithTag("SyncMotor");
        swingMotorGO = GameObject.FindGameObjectsWithTag("SwingMotor");
        hoistMotorGO = GameObject.FindGameObjectsWithTag("HoistMotor");
        hoistGearboxGO = GameObject.FindGameObjectsWithTag("HoistGearBoxes");
        hoistDrumGO = GameObject.FindWithTag("HoistDrum");
        dragMotorGO = GameObject.FindGameObjectsWithTag("DragMotor");
        dragGearboxGO = GameObject.FindGameObjectsWithTag("DragGearBoxes");
        dragDrumGO = GameObject.FindWithTag("DragDrum");
        syncMotorMaterials = new Material[syncMotors.Length];
        swingMotorMaterial = new Material[swingMotorGO.Length];
        swingGearboxMaterial = new Material[swingMotorGO.Length];
        hoistMotorMaterial = new Material[hoistMotorGO.Length];
        hoistGearboxMaterial = new Material[hoistGearboxGO.Length];
        hoistDrumMaterial = hoistDrumGO.GetComponent<Renderer>().material;
        dragMotorMaterial = new Material[dragMotorGO.Length];
        dragMotorBreakMaterial = new Material[dragMotorGO.Length];
        dragGearboxMaterial = new Material[dragGearboxGO.Length];
        dragDrumMaterial = dragDrumGO.GetComponent<Renderer>().material;
        //Get all swing Motor  and gearbox materials
        for (int i = 0; i < syncMotorMaterials.Length; i++)
        {
            syncMotorMaterials[i] = syncMotors[i].GetComponent<Renderer>().material;
        }
        for (int i = 0; i < swingMotorMaterial.Length; i++)
        {
            swingMotorMaterial[i] = swingMotorGO[i].GetComponent<Renderer>().materials[1];
            swingGearboxMaterial[i] = swingMotorGO[i].GetComponent<Renderer>().materials[0];
        }
        //Get all hoist Motor Materials
        for (int i = 0; i < hoistMotorMaterial.Length; i++)
        {
            hoistMotorMaterial[i] = hoistMotorGO[i].GetComponent<Renderer>().materials[0];
        }
        //Get all hoist gearbox materials
        for (int i = 0; i < hoistGearboxMaterial.Length; i++)
        {
            hoistGearboxMaterial[i] = hoistGearboxGO[i].GetComponent<Renderer>().material;
        }
        //Get All Drag Motor Materials
        for (int i = 0; i < dragMotorMaterial.Length; i++)
        {
            dragMotorMaterial[i] = dragMotorGO[i].GetComponent<Renderer>().materials[0];
            dragMotorBreakMaterial[i] = dragMotorGO[i].GetComponent<Renderer>().materials[1];
        }
        //Get All Drag Gearbox Materials
        for (int i = 0; i < dragGearboxMaterial.Length; i++)
        {
            dragGearboxMaterial[i] = dragGearboxGO[i].GetComponent<Renderer>().material;
        }
        syncMotorOriginalColor[0] = syncMotorMaterials[0].color;
        syncMotorOriginalColor[1] = syncMotorMaterials[0].GetColor("_EmissionColor");
        swingMotorOriginalColor[0] = swingMotorMaterial[0].color;
        swingMotorOriginalColor[1] = swingMotorMaterial[0].GetColor("_EmissionColor");
        swingGearOriginalColor[0] = swingGearboxMaterial[0].color;
        swingGearOriginalColor[1] = swingGearboxMaterial[0].GetColor("_EmissionColor");
        hoistMotorOriginalColor[0] = hoistMotorMaterial[0].color;
        hoistMotorOriginalColor[1] = hoistMotorMaterial[0].GetColor("_EmissionColor");
        hoistGearboxOriginalColor[0] = hoistGearboxMaterial[0].color;
        hoistGearboxOriginalColor[1] = hoistGearboxMaterial[0].GetColor("_EmissionColor");
        hoistDrumOriginalColor[0] = hoistDrumMaterial.color;
        hoistDrumOriginalColor[1] = hoistDrumMaterial.GetColor("_EmissionColor");
        dragMotorOriginalColor[0] = dragMotorMaterial[0].color;
        dragMotorOriginalColor[1] = dragMotorMaterial[0].GetColor("_EmissionColor");
        dragMotorBreakOriginalColor[0] = dragMotorBreakMaterial[0].color;
        dragMotorBreakOriginalColor[1] = dragMotorBreakMaterial[0].GetColor("_EmissionColor");
        dragGearboxOriginalColor[0] = dragGearboxMaterial[0].color;
        dragGearboxOriginalColor[1] = dragGearboxMaterial[0].GetColor("_EmissionColor");
        dragDrumOriginalColor[0] = dragDrumMaterial.color;
        dragDrumOriginalColor[1] = dragDrumMaterial.GetColor("_EmissionColor");

    }


    // Update is called once per frame
    void Update()
    {
        //Text Flash Speed Calculations
        textFastLerp = Mathf.PingPong(Time.time, textDuration) / textDuration;
        textSlowLerp = Mathf.PingPong(Time.time, textDuration * 2) / textDuration * 2;

        //********************************************************************************
        #region Hoist_Drag_Rope_Controls
        //Hoist and Drag Rope Controls
        //Hoist Ropes
        if (hoistMCtrl == MotorControl.Extend)
        {
            hoistDirection = 1;
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime); //Ramp Up 
            rightHoistRopeExtension += Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension += Time.deltaTime * hoistExtensionSpeed;
        }
        else if (hoistMCtrl == MotorControl.Retract)
        {
            hoistDirection = -1;
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime); //Ramp Up
            rightHoistRopeExtension -= Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension -= Time.deltaTime * hoistExtensionSpeed;
        }
        else
        {
            hoistExtensionSpeed = Mathf.Lerp(hoistExtensionSpeed, 0f, ropeTimeScale * Time.deltaTime); //Ramp Down
            rightHoistRopeExtension = rightHoistRopeExtension + hoistDirection * Time.deltaTime * hoistExtensionSpeed;
            leftHoistRopeExtension = leftHoistRopeExtension + hoistDirection * Time.deltaTime * hoistExtensionSpeed;
        }
        //Drag Ropes
        if (dragMCtrl == MotorControl.Extend)
        {
            dragDirection = 1;
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime); //Ramp Up
            rightDragRopeExtension += Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension += Time.deltaTime * dragExtensionSpeed;
        }
        else if (dragMCtrl == MotorControl.Retract)
        {
            dragDirection = -1;
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, ropeExtensionSpeed, ropeTimeScale * Time.deltaTime);
            rightDragRopeExtension -= Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension -= Time.deltaTime * dragExtensionSpeed;
        }
        else
        {
            dragExtensionSpeed = Mathf.Lerp(dragExtensionSpeed, 0, ropeTimeScale * Time.deltaTime);
            rightDragRopeExtension = rightDragRopeExtension + dragDirection * Time.deltaTime * dragExtensionSpeed;
            leftDragRopeExtension = leftDragRopeExtension + dragDirection * Time.deltaTime * dragExtensionSpeed;
        }

        //Required Code for Ultimate Rope Editor
        if (rightHoistRope != null) //Clamp max length and impose extension method
        {
            rightHoistRopeExtension = Mathf.Clamp(rightHoistRopeExtension, 0.0f, rightHoistRope.ExtensibleLength);
            rightHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightHoistRopeExtension - rightHoistRope.m_fCurrentExtension);

        }
        if (leftHoistRope != null) //Clamp max length and impose extension method
        {
            leftHoistRopeExtension = Mathf.Clamp(leftHoistRopeExtension, 0.0f, leftHoistRope.ExtensibleLength);
            leftHoistRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftHoistRopeExtension - leftHoistRope.m_fCurrentExtension);
        }
        if (rightDragRope != null) //Clamp max length and impose extension method
        {
            rightDragRopeExtension = Mathf.Clamp(rightDragRopeExtension, 0.0f, rightDragRope.ExtensibleLength);
            rightDragRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, rightDragRopeExtension - rightDragRope.m_fCurrentExtension);
        }
        if (leftDragRope != null) //Clamp max length and impose extension method
        {
            leftDragRopeExtension = Mathf.Clamp(leftDragRopeExtension, 0.0f, leftDragRope.ExtensibleLength);
            leftDragRope.ExtendRope(UltimateRope.ERopeExtensionMode.LinearExtensionIncrement, leftDragRopeExtension - leftDragRope.m_fCurrentExtension);
        }
        #endregion
        //********************************************************************************
        //Gen Set Text & Animation Controls
        //Modify Text Settings (color, text, flash speed) and Generator vibrate animation

        //*****************************Generator Vibration Section*****************************
        //Inner Generators
        if (innerGen.vibrate == true)
        {
            for (int i = 0; i < innerGenIndex.Length; i++)
            {
                iTween.ShakePosition(genArray[innerGenIndex[i]], iTween.Hash("amount", genVibeMagnitude, "time", genVibeDuration));
            }
        }
        if (middleGen.vibrate == true)
        {
            for (int i = 0; i < middleGenIndex.Length; i++)
            {
                iTween.ShakePosition(genArray[middleGenIndex[i]], iTween.Hash("amount", genVibeMagnitude, "time", genVibeDuration));
            }
        }
        if (outerGen.vibrate == true)
        {
            for (int i = 0; i < outerGenIndex.Length; i++)
            {
                iTween.ShakePosition(genArray[outerGenIndex[i]], iTween.Hash("amount", genVibeMagnitude, "time", genVibeDuration));
            }
        }
        //*****************************Generator Highlight Section***************************** 
        innerGenNewColor = Color.Lerp(genOriginalColor, highlightColor, FlashStatus((int)innerGen.flashStatus));
        innerGenNewEColor = Color.Lerp(genOriginalEColor, highlightColor, FlashStatus((int)innerGen.flashStatus));
        middleGenNewColor = Color.Lerp(genOriginalColor, highlightColor, FlashStatus((int)middleGen.flashStatus));
        middleGenNewEColor = Color.Lerp(genOriginalEColor, highlightColor, FlashStatus((int)middleGen.flashStatus));
        outerGenNewColor = Color.Lerp(genOriginalColor, highlightColor, FlashStatus((int)outerGen.flashStatus));
        outerGenNewEColor = Color.Lerp(genOriginalEColor, highlightColor, FlashStatus((int)outerGen.flashStatus));
        //Cycle through the inner generators
        for (int i = 0; i < innerGenIndex.Length; i++)
        {
            genMaterial[innerGenIndex[i]].SetColor("_Color", innerGenNewColor);
            genMaterial[innerGenIndex[i]].SetColor("_EmissionColor", innerGenNewEColor);
        }
        //Cycle through the middle generators
        for (int i = 0; i < middleGenIndex.Length; i++)
        {
            genMaterial[middleGenIndex[i]].SetColor("_Color", middleGenNewColor);
            genMaterial[middleGenIndex[i]].SetColor("_EmissionColor", middleGenNewEColor);
        }
        //Cycle through the outer generators
        for (int i = 0; i < outerGenIndex.Length; i++)
        {
            genMaterial[outerGenIndex[i]].SetColor("_Color", outerGenNewColor);
            genMaterial[outerGenIndex[i]].SetColor("_EmissionColor", outerGenNewEColor);
        }


        if (gensetOn == true)  //Set the "Generator Engaged" text, color and flash speed
        {
            gensetNewColor = gensetHighlightColor;
            gensetNewColor.a = textFastLerp;
            gensetStatusText = "Engaged";
        }
        else                   //Set the "Generator Idle" text, color and flash speed
        {
            gensetNewColor = gensetOriginalColor;
            gensetNewColor.a = textSlowLerp;
            gensetStatusText = "Idle";
        }
        //Write New Text Properties
        for (int i = 0; i < genStatus.Length; i++) //Write new text properties
        {
            genStatus[i].GetComponent<Text>().color = gensetNewColor;
            genStatus[i].GetComponent<Text>().text = gensetStatusText;
        }
        //Control visability of the generator Status
        genCanvasArray[1].SetActive(hoistDispOn);
        genCanvasArray[0].SetActive(dragDispOn);


        //********************************************************************************
        //Virtual Joystick Controls
        //leftJoyPos = leftJoyHandle.transform.localPosition;
        //rightJoyPos = rightJoyHandle.transform.localPosition;

        //leftJoyHandle.transform.localPosition = Vector3.Lerp(leftJoyPos, new Vector3((float)leftJoyHor, (float)leftJoyVert, 0.0f), Time.deltaTime * joyMoveSpeed);
        //rightJoyHandle.transform.localPosition = Vector3.Lerp(rightJoyPos, new Vector3((float)rightJoyHor, (float)rightJoyVert, 0.0f), Time.deltaTime * joyMoveSpeed);

        //********************************************************************************
        //VMovie Screen Controls

        if (allScreens.hoistScreen.open == true)
        {
            iTween.ScaleTo(movieScreens[0], screenExpanded, screenScaleTime);
        }
        else
        {
            iTween.ScaleTo(movieScreens[0], screenFolded, screenScaleTime);
        }

        if (allScreens.dragScreen.open == true)
        {
            iTween.ScaleTo(movieScreens[1], screenExpanded, screenScaleTime);
        }
        else
        {
            iTween.ScaleTo(movieScreens[1], screenFolded, screenScaleTime);
        }
        MovieController(0, allScreens.hoistScreen, hoistClips);
        MovieController(1, allScreens.dragScreen, dragClips);

        //********************************************************************************
        //Fade Screen
        //fadeText.GetComponent<Text>().text = fadeMessage;
        //fadeScreenColor = fadeScreen.GetComponent<Image>().color;
        //fadeTextColor = fadeText.GetComponent<Text>().color;
        ////Fade Screen Logic
        //if (showFadeScreen == true)
        //{
        //    fadeScreenColor.a = Mathf.Lerp(fadeScreenColor.a, solidAlpha, fadeToBlackTime * Time.deltaTime);
        //}
        //else
        //{
        //    fadeScreenColor.a = Mathf.Lerp(fadeScreenColor.a, fadedAlpha, fadeToBlackTime * Time.deltaTime);
        //}
        //fadeScreen.GetComponent<Image>().color = fadeScreenColor;
        ////Fade Text Logic
        //if (showFadeText == true)
        //{
        //    fadeTextColor.a = Mathf.Lerp(fadeTextColor.a, solidAlpha, fadeToBlackTime * Time.deltaTime * 2);
        //}
        //else
        //{
        //    fadeTextColor.a = Mathf.Lerp(fadeTextColor.a, fadedAlpha, fadeToBlackTime * Time.deltaTime * 2);
        //}
        //fadeText.GetComponent<Text>().color = fadeTextColor;

        //********************************************************************************
        //Motor & Gearbox highlighting logic
        colorLerp = Mathf.PingPong(Time.time, highlightDuration) / highlightDuration;
        //Calculate object new color
        syncMotorNewColor[0] = Color.Lerp(syncMotorOriginalColor[0], highlightColor, FlashStatus((int)syncMotorStatus));
        syncMotorNewColor[1] = Color.Lerp(syncMotorOriginalColor[1], highlightColor, FlashStatus((int)syncMotorStatus));
        swingMotorNewColor[0] = Color.Lerp(swingMotorOriginalColor[0], highlightColor, FlashStatus((int)swingMotorStatus));
        swingMotorNewColor[1] = Color.Lerp(swingMotorOriginalColor[1], highlightColor, FlashStatus((int)swingMotorStatus));
        swingGearNewColor[0] = Color.Lerp(swingGearOriginalColor[0], highlightColor, FlashStatus((int)swingGearboxStatus));
        swingGearNewColor[1] = Color.Lerp(swingGearOriginalColor[1], highlightColor, FlashStatus((int)swingGearboxStatus));
        hoistMotorNewColor[0] = Color.Lerp(hoistMotorOriginalColor[0], highlightColor, FlashStatus((int)hoistMotorStatus));
        hoistMotorNewColor[1] = Color.Lerp(hoistMotorOriginalColor[1], highlightColor, FlashStatus((int)hoistMotorStatus));
        hoistGearboxNewColor[0] = Color.Lerp(hoistGearboxOriginalColor[0], highlightColor, FlashStatus((int)hoistGearboxStatus));
        hoistGearboxNewColor[1] = Color.Lerp(hoistGearboxOriginalColor[1], highlightColor, FlashStatus((int)hoistGearboxStatus));
        hoistDrumNewColor[0] = Color.Lerp(hoistDrumOriginalColor[0], highlightColor, FlashStatus((int)hoistDrumStatus));
        hoistDrumNewColor[1] = Color.Lerp(hoistDrumOriginalColor[1], highlightColor, FlashStatus((int)hoistDrumStatus));
        dragMotorNewColor[0] = Color.Lerp(dragMotorOriginalColor[0], highlightColor, FlashStatus((int)dragMotorStatus));
        dragMotorNewColor[1] = Color.Lerp(dragMotorOriginalColor[1], highlightColor, FlashStatus((int)dragMotorStatus));
        dragMotorBreakNewColor[0] = Color.Lerp(dragMotorBreakOriginalColor[0], highlightColor, FlashStatus((int)dragMotorBreakStatus));
        dragMotorBreakNewColor[1] = Color.Lerp(dragMotorBreakOriginalColor[1], highlightColor, FlashStatus((int)dragMotorBreakStatus));
        dragGearboxNewColor[0] = Color.Lerp(dragGearboxOriginalColor[0], highlightColor, FlashStatus((int)dragGearboxStatus));
        dragGearboxNewColor[1] = Color.Lerp(dragGearboxOriginalColor[1], highlightColor, FlashStatus((int)dragGearboxStatus));
        dragDrumNewColor[0] = Color.Lerp(dragDrumOriginalColor[0], highlightColor, FlashStatus((int)dragDrumStatus));
        dragDrumNewColor[1] = Color.Lerp(dragDrumOriginalColor[1], highlightColor, FlashStatus((int)dragDrumStatus));
        //Apply new color to object
        for (int i = 0; i < syncMotors.Length; i++)
        {
            syncMotorMaterials[i].SetColor("_Color", syncMotorNewColor[0]);
            syncMotorMaterials[i].SetColor("_EmissionColor", syncMotorNewColor[1]);
        }
        for (int i = 0; i < swingMotorGO.Length; i++)
        {
            swingMotorMaterial[i].SetColor("_Color", swingMotorNewColor[0]);
            swingMotorMaterial[i].SetColor("_EmissionColor", swingMotorNewColor[1]);
            swingGearboxMaterial[i].SetColor("_Color", swingGearNewColor[0]);
            swingGearboxMaterial[i].SetColor("_EmissionColor", swingGearNewColor[1]);
        }
        for (int i = 0; i < hoistMotorGO.Length; i++)
        {
            hoistMotorMaterial[i].SetColor("_Color", hoistMotorNewColor[0]);
            hoistMotorMaterial[i].SetColor("_EmissionColor", hoistMotorNewColor[1]);
        }
        for (int i = 0; i < hoistGearboxGO.Length; i++)
        {
            hoistGearboxMaterial[i].SetColor("_Color", hoistGearboxNewColor[0]);
            hoistGearboxMaterial[i].SetColor("_EmissionColor", hoistGearboxNewColor[1]);
        }
        hoistDrumMaterial.SetColor("_Color", hoistDrumNewColor[0]);
        hoistDrumMaterial.SetColor("_EmissionColor", hoistDrumNewColor[1]);
        for (int i = 0; i < dragMotorGO.Length; i++)
        {
            dragMotorMaterial[i].SetColor("_Color", dragMotorNewColor[0]);
            dragMotorMaterial[i].SetColor("_EmissionColor", dragMotorNewColor[1]);
            dragMotorBreakMaterial[i].SetColor("_Color", dragMotorBreakNewColor[0]);
            dragMotorBreakMaterial[i].SetColor("_EmissionColor", dragMotorBreakNewColor[1]);
        }
        for (int i = 0; i < dragGearboxGO.Length; i++)
        {
            dragGearboxMaterial[i].SetColor("_Color", dragGearboxNewColor[0]);
            dragGearboxMaterial[i].SetColor("_EmissionColor", dragGearboxNewColor[1]);
        }
        dragDrumMaterial.SetColor("_Color", dragDrumNewColor[0]);
        dragDrumMaterial.SetColor("_EmissionColor", dragDrumNewColor[1]);

        if (hoistMotorVibrate)
        {
            for (int i = 0; i < hoistMotorGO.Length; i++)
            {
                iTween.ShakePosition(hoistMotorGO[i], iTween.Hash("amount", motorVibeMagnitude, "time", motorVibeDuration));
            }
        }
        if (dragMotorVibrate)
        {
            for (int i = 0; i < dragMotorGO.Length; i++)
            {
                iTween.ShakePosition(dragMotorGO[i], iTween.Hash("amount", motorVibeMagnitude, "time", motorVibeDuration));
            }
        }
    }

    //********************************************************************************
    //FUNCTION SECTION

    void MovieController(int playerIndex, ScreenController screen, VideoClip[] clips)
    {
        VideoPlayer videoPlayer;
        videoPlayer = moviePlayers[playerIndex].GetComponent<VideoPlayer>();
        if (screen.secondClip == true && clips.Length > 1)              //If second clip is selected and there is a second clip
        {
            videoPlayer.clip = clips[1];                                //Load second clip
        }
        else
        {
            videoPlayer.clip = clips[0];                                //Load first clip
        }
        if (movieScreens[playerIndex].transform.localScale == screenExpanded)   //If the Screen is Expanded
        {
            if (screen.play == false)                                           //If play is not enabled
            {
                videoPlayer.Pause();                                            //Pause the player
                if (screen.secondClip == true && clips.Length > 1)              //If second clip is selected and there is a second clip
                {
                    videoPlayer.clip = clips[1];                                //Load second clip
                }
                else
                {
                    videoPlayer.clip = clips[0];                                //Load first clip
                }
            }
            else
            {

                videoPlayer.Play();                                             //Play video
            }
        }
        else
        {
            videoPlayer.Pause();                                                //Pause the video is the screen is not fully expanded
        }
    }
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        //THis replaces the following logic
        //**********Sample Code Start*******************
        //		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
        //**********Sample Code End********************
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End

}

