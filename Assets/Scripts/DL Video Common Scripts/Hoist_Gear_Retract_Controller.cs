﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntPublicClass;

public class Hoist_Gear_Retract_Controller : MonoBehaviour
{
    Gear_Speed_Control gearSpeedControl;
    public bool spool;

	// Use this for initialization
	void Start ()
    {
        gearSpeedControl = GetComponent<Gear_Speed_Control>();
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (spool == true)
        {
            gearSpeedControl.motorControl = MotorControl.Extend;
        }
        else if (spool == false)
        {
            gearSpeedControl.motorControl = MotorControl.Retract;
        }
        else
        {
            gearSpeedControl.motorControl = MotorControl.None;
        }
	}
}
