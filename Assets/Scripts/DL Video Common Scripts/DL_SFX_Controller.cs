﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DL_SFX_Controller : MonoBehaviour 
{

	public bool showLights = true;
	public bool showPlatform = true;
	public bool upSusRopesVisable = true;
	public bool intSusRopesVisable = true;
	public bool lowerSusRopesVisable = true;
	public bool housePlatformVisable = true;
	public bool houseLightsVisable = true;
	public bool houseCabVisable = true;
	public bool houseBodyVisable = true;
	public bool mastChordsHighlight;
	public bool mastLacingsHighlights;
	public bool mastFeetHighlight;
	public bool aFrameHighlight;
	public bool trunnionHighlight;


	[Range(0.0f, 1.0f)]
	public float chordsAlpha;
	[Range(0.0f,1.0f)]
	public float chordsHighlight;
	[Range(0.0f, 1.0f)]
	public float lacingsAlpha;
	[Range(0.0f,1.0f)]
	public float lacingsHighlight;
	[Range(0.0f, 1.0f)]
	public float boomTipHighlight;
	[Range(0.0f, 1.0f)]
	public float mastHeadHighlight;
	[Range (0.0f, 1.0f)]
	public float boomFeetHighlight;
	[Range (0.0f,1.0f)]
	public float deflectionTowerHighlight;
	[Range (0.0f,1.0f)]
	public float upperSusRopesHighlight;
	[Range(0.0f,1.0f)]
	public float intermediateSusRopesHighlight;
	[Range(0.0f,1.0f)]
	public float suspensionRopesHighlight;
	[Range(0.0f, 1.0f)]
	public float bucketTeethHighlight;
	[Range(0.0f,1.0f)]
	public float spreaderBarHighlight;
	[Range(0.0f,1.0f)]
	public float hoistChainHighlight;
	[Range(0.0f,0.1f)]
	public float dragChainHighlight;
	[Range(0.0f,1.0f)]
	public float dumpChainHighlight;
	[Range(0.0f,1.0f)]
	public float dumpRopeHighlight;
	[Range(0.0f,1.0f)]
	public float clevisHighlight;
	[Range(0.0f,1.0f)]
	public float dragRopeHighlight;
	[Range(0.0f,1.0f)]
	public float hoistRopeHighlight;
	[Range(0.0f,1.0f)]
	public float dumpBlockHighlight;
	[Range(0.0f,1.0f)]
	public float bucketHighlight;



	private GameObject[] straightRopes;
	private GameObject boomPlatform;
	private GameObject boomLights;
	private GameObject boomBeams;
	private GameObject housePlatform;
	private GameObject houseCab;
	private GameObject boomTip;
	private GameObject mastHead;
	private GameObject boomFeet;
	private GameObject deflectionTowers;
	private GameObject[] suspensionRopes = new GameObject[4];
	private GameObject[] intermediateSuspensionRopes = new GameObject[4];
	private GameObject[] upperSuspensionRopes = new GameObject[4] ;
	private GameObject mastChord;
	private GameObject mastLacing;
	private GameObject mastFeet;
	private GameObject aFrame;
	private GameObject houseBody;
	private GameObject houseLights;
	private GameObject bucketTeeth;
	private GameObject spreaderBar;
	private GameObject[] hoistChains;
	private GameObject[] dragChains;
	private GameObject[] dumpChains;
	private GameObject dumpRopeParent;
//	public GameObject dumpRopeParent2;
	private GameObject[] dumpRopes = new GameObject[1];
	private GameObject dumpRope;
	private GameObject[] clevis;
	private GameObject trunnion;

	private GameObject leftDragRopeParent;
	private GameObject[] leftDragRopes = new GameObject[1];
	private GameObject leftDragRope;

	private GameObject rightDragRopeParent;
	private GameObject[] rightDragRopes = new GameObject[1];
	private GameObject rightDragRope;

	private GameObject leftHoistRopeParent;
	private GameObject[] leftHoistRopes = new GameObject[1];
	private GameObject leftHoistRope;

	private GameObject rightHoistRopeParent;
	private GameObject[] rightHoistRopes = new GameObject[1];
	private GameObject rightHoistRope;

	private GameObject dumpBlock;
	private GameObject[] bucket;


	private Color lacingsOriginalColor;
	private Color chordsOriginalColor;
	private Color lacingsColor;
	private Color chordsColor;
	private Color boomTipOriginalColor;
//	private Color highlightColor = new Color (255f, 0.0f, 0.0f);
	private Color highlightColor = new Color (0f, 255f, 0f);
	private Color boomTipNewColor;
	private Color mastHeadNewColor;
	private Color mastHeadOriginalColor;
	private Color boomFeetOriginalColor;
	private Color boomFeetNewColor;
	private Color deflectionTowerOriginalColor;
	private Color deflectionTowerNewColor;
	private Color upperSusRopesOriginalColor;
	private Color upperSusRopesNewColor;
	private Color intermediateSusRopesOriginalColor;
	private Color intermediateSusRopesNewColor;
	private Color suspensionRopesOriginalColor;
	private Color suspensionRopesNewColor;
	private Color bucketTeethOriginalColor;
	private Color bucketTeethNewColor;
	private Color spreaderBarOriginalColor;
	private Color spreaderBarNewColor;
	private Color hoistChainOriginalColor;
	private Color hoistChainNewColor;
	private Color dragChainOriginalColor;
	private Color dragChainNewColor;
	private Color dumpChainOriginalColor;
	private Color dumpChainNewColor;
	private Color dumpRopeOriginalColor;
	private Color dumpRopeNewColor;
	private Color clevisOriginalColor;
	private Color clevisNewColor;
	private Color dragRopeOriginalColor;
	private Color dragRopeNewColor;
	private Color hoistRopeOriginalColor;
	private Color hoistRopeNewColor;
	private Color dumpBlockOriginalColor;
	private Color dumpBlockNewColor;
	private Color bucketOriginalColor;
	private Color bucketNewColor;


	private Renderer renderers;
	private Material[] materials;
	private Material boomTipMaterial;
	private Material mastHeadMaterial;
	private Material boomFeetMaterial;
	private Material deflectionTowerMaterial;
	private Material bucketFeetMaterial;
	private Material spreaderBarMaterial;
	private Material hoistChainMaterial;
	private Material dragChainMaterial;
	private Material dumpChainMaterial;
	private Material dumpRopeMaterial;
	private Material clevisMaterial;
	private Material dragRopeMaterial;
	private Material hoistRopeMaterial;
	private Material dumpBlockMaterial;
	private Material bucketMaterial;


	private float dumpRopeChildCount;
	private float leftDragRopeChildCount;
	private float rightDragRopeChildCount;
	private float leftHoistRopeChildCount;
	private float rightHoistRopeChildCount;

	private bool gotRopes = false;
	private bool dumpRopeFound = false;
	private bool leftDragRopeFound = false;
	private bool rightDragRopeFound = false;
	private bool leftHoistRopeFound = false;
	private bool rightHoistRopeFound = false;

	private int dumpCount;
	private int leftDragCount;
	private int rightDragCount;
	private int leftHoistCount;
	private int rightHoistCount;

	// Use this for initialization
	void Start () 
	{
		boomPlatform = GameObject.FindWithTag ("BoomPlatform");
		boomLights = GameObject.FindWithTag ("BoomLights");
		boomBeams = GameObject.FindWithTag ("BoomBeamsDisplay");
		renderers = boomBeams.GetComponent<Renderer> ();
		materials = renderers.materials;
		lacingsOriginalColor = materials[1].color;
		chordsOriginalColor = materials[0].color;
		straightRopes = GameObject.FindGameObjectsWithTag ("StraightRopePrefab");
		housePlatform = GameObject.FindWithTag ("HousePlatform");
		houseCab = GameObject.FindWithTag ("HouseCab");
		boomTip = GameObject.FindWithTag ("BoomTip");
		mastHead = GameObject.FindWithTag ("MastHead");
		boomFeet = GameObject.FindWithTag ("BoomFeet");
		deflectionTowers = GameObject.FindWithTag ("DeflectionTowers");
		boomTipMaterial = boomTip.GetComponent<Renderer> ().material;
		boomTipOriginalColor = boomTipMaterial.color;
		mastHeadMaterial = mastHead.GetComponent<Renderer> ().material;
		mastHeadOriginalColor = mastHeadMaterial.color;
		boomFeetMaterial = boomFeet.GetComponent<Renderer> ().material;
		boomFeetOriginalColor = boomFeetMaterial.color;
		deflectionTowerMaterial = deflectionTowers.GetComponent<Renderer> ().material;
		deflectionTowerOriginalColor = deflectionTowerMaterial.color;
		mastChord = GameObject.FindWithTag ("MastChords");
		mastLacing = GameObject.FindWithTag ("MastLacings");
		mastFeet = GameObject.FindWithTag ("MastFeetHighlight");
		aFrame = GameObject.FindWithTag ("AFrameHighlight");
		houseBody = GameObject.FindWithTag ("HouseBody");
		houseLights = GameObject.FindWithTag ("HouseLights");

		bucketTeeth = GameObject.FindWithTag ("BucketTeeth");
		bucketFeetMaterial = bucketTeeth.GetComponent<Renderer> ().material;
		bucketTeethOriginalColor = bucketFeetMaterial.color;

		spreaderBar = GameObject.FindWithTag ("SpreaderBar");
		spreaderBarMaterial = spreaderBar.GetComponent<Renderer> ().material;
		spreaderBarOriginalColor = spreaderBarMaterial.color;

		hoistChains = GameObject.FindGameObjectsWithTag ("HoistChain");
		hoistChainMaterial = hoistChains [0].GetComponent<Renderer> ().material;
		hoistChainOriginalColor = hoistChainMaterial.color;

		dragChains = GameObject.FindGameObjectsWithTag ("DragChain");
		dragChainMaterial = dragChains [0].GetComponent<Renderer> ().material;
		dragChainOriginalColor = dragChainMaterial.color;

		dumpChains = GameObject.FindGameObjectsWithTag ("DumpChain");
		dumpChainMaterial = dumpChains [0].GetComponent<Renderer> ().material;
		dumpChainOriginalColor = dumpChainMaterial.color;

		dumpRopeParent = GameObject.FindWithTag ("DumpRopeParent");
//		dumpRopeParent2 = GameObject.FindWithTag ("DumpRopeParent");

		clevis = GameObject.FindGameObjectsWithTag ("Clevis");
		clevisMaterial = clevis [0].GetComponent<Renderer> ().material;
		clevisOriginalColor = clevisMaterial.color;

		trunnion = GameObject.FindWithTag ("TrunionHighlight");

		leftDragRopeParent = GameObject.FindWithTag ("LeftDragRopeParent");
		rightDragRopeParent = GameObject.FindWithTag ("RightDragRopeParent");

		leftHoistRopeParent = GameObject.FindWithTag ("LeftHoistRopeParent");
		rightHoistRopeParent = GameObject.FindWithTag ("RightHoistRopeParent");

		dumpBlock = GameObject.FindWithTag ("DumpBlock");
		dumpBlockMaterial = dumpBlock.GetComponent<Renderer> ().material;
		dumpBlockOriginalColor = dumpBlockMaterial.color;

		bucket = GameObject.FindGameObjectsWithTag ("Bucket");
		bucketMaterial = bucket[0].GetComponent<Renderer> ().material;
		bucketOriginalColor = bucketMaterial.color;


	}

	// Update is called once per frame
	void Update () 
	{
		boomPlatform.SetActive (showPlatform); 
		boomLights.SetActive (showLights);
		lacingsColor = Color.Lerp (lacingsOriginalColor, highlightColor, lacingsHighlight);
		lacingsColor.a = lacingsAlpha;
		chordsColor = Color.Lerp (chordsOriginalColor, highlightColor, chordsHighlight);
		chordsColor.a = chordsAlpha;
		materials [1].SetColor ("_Color", lacingsColor);
		materials [0].SetColor ("_Color", chordsColor);
		housePlatform.SetActive (housePlatformVisable);
		houseCab.SetActive (houseCabVisable);
		boomTipNewColor = Color.Lerp (boomTipOriginalColor, highlightColor, boomTipHighlight);
		boomTipMaterial.SetColor ("_Color", boomTipNewColor);
		mastHeadNewColor = Color.Lerp (mastHeadOriginalColor, highlightColor, mastHeadHighlight);
		mastHeadMaterial.SetColor ("_Color", mastHeadNewColor);
		boomFeetNewColor = Color.Lerp (boomFeetOriginalColor, highlightColor, boomFeetHighlight);
		boomFeetMaterial.SetColor ("_Color", boomFeetNewColor);
		deflectionTowerNewColor = Color.Lerp (deflectionTowerOriginalColor, highlightColor, deflectionTowerHighlight);
		deflectionTowerMaterial.SetColor ("_Color", deflectionTowerNewColor);
		upperSusRopesNewColor = Color.Lerp (upperSusRopesOriginalColor, highlightColor, upperSusRopesHighlight);
		intermediateSusRopesNewColor = Color.Lerp (intermediateSusRopesOriginalColor, highlightColor, intermediateSusRopesHighlight);
		suspensionRopesNewColor = Color.Lerp (suspensionRopesOriginalColor, highlightColor, suspensionRopesHighlight);

		bucketTeethNewColor = Color.Lerp (bucketTeethOriginalColor, highlightColor, bucketTeethHighlight);
		bucketFeetMaterial.SetColor ("_Color", bucketTeethNewColor);

		spreaderBarNewColor = Color.Lerp (spreaderBarOriginalColor, highlightColor, spreaderBarHighlight);
		spreaderBarMaterial.SetColor ("_Color", spreaderBarNewColor);

		hoistChainNewColor = Color.Lerp (hoistChainOriginalColor, highlightColor, hoistChainHighlight);

		dragChainNewColor = Color.Lerp (dragChainOriginalColor, highlightColor, dragChainHighlight);

		dumpChainNewColor = Color.Lerp (dumpChainOriginalColor, highlightColor, dumpChainHighlight);

		clevisNewColor = Color.Lerp (clevisOriginalColor, highlightColor, clevisHighlight);

		trunnion.SetActive (trunnionHighlight);

		dumpBlockNewColor = Color.Lerp (dumpBlockOriginalColor, highlightColor, dumpBlockHighlight);
		dumpBlockMaterial.SetColor ("_Color", dumpBlockNewColor);

		bucketNewColor = Color.Lerp (bucketOriginalColor, highlightColor, bucketHighlight);

		//If dumpRope children are found
		dumpRopeChildCount =dumpRopeParent.transform.childCount;
		if (dumpRopeChildCount !=0 && dumpRopeFound == false) 
		{
			dumpCount = 0;
			foreach (Transform i in dumpRopeParent.transform) 
			{
				dumpRopes [dumpCount] = i.gameObject;
				dumpCount++;
			}
			dumpRope = dumpRopes [0];
			dumpRopeMaterial = dumpRope.GetComponent<Renderer> ().material;
			dumpRopeOriginalColor = dumpRopeMaterial.color;
			dumpRopeFound = true;
		}

		if (dumpRopeFound) 
		{
			dumpRopeNewColor = Color.Lerp (dumpRopeOriginalColor, highlightColor, dumpRopeHighlight);
			dumpRope.GetComponent<Renderer> ().material.SetColor ("_Color", dumpRopeNewColor);
		}

		leftDragRopeChildCount = leftDragRopeParent.transform.childCount;
		if (leftDragRopeChildCount != 0 && leftDragRopeFound == false) 
		{
			leftDragCount = 0;
			foreach (Transform i in leftDragRopeParent.transform) 
			{
				leftDragRopes [leftDragCount] = i.gameObject;
				leftDragCount++;
			}
			leftDragRope = leftDragRopes [0];
			dragRopeMaterial = leftDragRope.GetComponent<Renderer> ().material;
			dragRopeOriginalColor = dragRopeMaterial.color;
			leftDragRopeFound = true;
		}

		dragRopeNewColor = Color.Lerp (dragRopeOriginalColor, highlightColor, dragRopeHighlight);
		leftDragRope.GetComponent<Renderer> ().material.SetColor ("_Color", dragRopeNewColor);

		rightDragRopeChildCount = rightDragRopeParent.transform.childCount;
		if (rightDragRopeChildCount != 0 && rightDragRopeFound == false) 
		{
			rightDragCount = 0;
			foreach (Transform i in rightDragRopeParent.transform) 
			{
				rightDragRopes [rightDragCount] = i.gameObject;
				rightDragCount++;
			}
			rightDragRope = rightDragRopes [0];
			rightDragRopeFound = true;
		}

		rightDragRope.GetComponent<Renderer> ().material.SetColor ("_Color", dragRopeNewColor);

		leftHoistRopeChildCount = leftHoistRopeParent.transform.childCount;
		if (leftHoistRopeChildCount != 0 && leftHoistRopeFound == false) 
		{
			leftHoistCount = 0;
			foreach (Transform i in leftHoistRopeParent.transform) 
			{
				leftHoistRopes [leftHoistCount] = i.gameObject;
				leftHoistCount++;
			}
			leftHoistRope = leftHoistRopes [0];
			hoistRopeMaterial = leftHoistRope.GetComponent<Renderer> ().material;
			hoistRopeOriginalColor = hoistRopeMaterial.color;
			leftHoistRopeFound = true;
		}

		hoistRopeNewColor = Color.Lerp (hoistRopeOriginalColor, highlightColor, hoistRopeHighlight);
		leftHoistRope.GetComponent<Renderer> ().material.SetColor ("_Color", hoistRopeNewColor);

		rightHoistRopeChildCount = rightHoistRopeParent.transform.childCount;
		if (rightHoistRopeChildCount != 0 && rightHoistRopeFound == false) 
		{
			rightHoistCount = 0;
			foreach (Transform i in rightHoistRopeParent.transform) 
			{
				rightHoistRopes [rightHoistCount] = i.gameObject;
				rightHoistCount++;
			}
			rightHoistRope = rightHoistRopes [0];
			rightHoistRopeFound = true;
		}

		rightHoistRope.GetComponent<Renderer> ().material.SetColor ("_Color", hoistRopeNewColor);

		mastChord.SetActive (mastChordsHighlight);
		mastLacing.SetActive (mastLacingsHighlights);
		mastFeet.SetActive (mastFeetHighlight);
		aFrame.SetActive (aFrameHighlight);
		houseBody.SetActive (houseBodyVisable);
		houseLights.SetActive (houseLightsVisable);

		//wait till ropes have been drawn before attempting to get references
		if (straightRopes.Length < 2) 
		{
			straightRopes = GameObject.FindGameObjectsWithTag ("StraightRopePrefab");
		}

		// Populate suspension rope groups
		if (straightRopes.Length ==12 && gotRopes == false) 
		{
			for (int i = 0; i < 4; i++) 
			{
				suspensionRopes [i] = straightRopes [i];
				intermediateSuspensionRopes [i] = straightRopes [i+4];
				upperSuspensionRopes [i] = straightRopes [i+8];
			}
			upperSusRopesOriginalColor = upperSuspensionRopes [0].GetComponent<Renderer> ().material.color;
			intermediateSusRopesOriginalColor = intermediateSuspensionRopes [0].GetComponent<Renderer> ().material.color;
			suspensionRopesOriginalColor = suspensionRopes [0].GetComponent<Renderer> ().material.color;
			gotRopes = true;
		}

		if (gotRopes) 
		{
			for (int i = 0; i < 4; i++) 
			{
				suspensionRopes [i].SetActive (lowerSusRopesVisable);
				suspensionRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", suspensionRopesNewColor);
				intermediateSuspensionRopes [i].SetActive (intSusRopesVisable);
				intermediateSuspensionRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", intermediateSusRopesNewColor);
				upperSuspensionRopes [i].SetActive (upSusRopesVisable);
				upperSuspensionRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", upperSusRopesNewColor);
			}
		}


		for (int i = 0; i < hoistChains.Length; i++) 
		{
			hoistChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", hoistChainNewColor);
		}

		for (int i = 0; i < dragChains.Length; i++) 
		{
			dragChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", dragChainNewColor);
		}

		for (int i = 0; i < dumpChains.Length; i++) 
		{
			dumpChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", dumpChainNewColor);
		}

		for (int i = 0; i < clevis.Length; i++) 
		{
			clevis [i].GetComponent<Renderer> ().material.SetColor ("_Color", clevisNewColor);
		}

		for (int i = 0; i < bucket.Length; i++) 
		{
			bucket[i].GetComponent<Renderer>().material.SetColor("_Color", bucketNewColor);
		}



	}
}
