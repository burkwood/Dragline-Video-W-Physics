﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Fade_Screen_Controller : MonoBehaviour
{
    #region Fade_Screen
    //Fade Screen
    private GameObject fadeScreen;
    private GameObject fadeText;
    private float fadedAlpha = 0f;
    private float solidAlpha = 1.5f;
    private Color fadeScreenColor;
    private Color fadeTextColor;
    public string fadeMessage;
    public bool showFadeScreen = false;
    public bool showFadeText = false;
    public float fadeToBlackTime;

    #endregion
    // Use this for initialization
    void Start ()
    {
        fadeScreen = GameObject.FindWithTag("Fade Image");
        fadeText = GameObject.FindWithTag("Fade Text");
    }
	
	// Update is called once per frame
	void Update ()
    {
        //********************************************************************************
        //Fade Screen
        fadeText.GetComponent<Text>().text = fadeMessage;
        fadeScreenColor = fadeScreen.GetComponent<Image>().color;
        fadeTextColor = fadeText.GetComponent<Text>().color;
        //Fade Screen Logic
        if (showFadeScreen == true)
        {
            fadeScreenColor.a = Mathf.Lerp(fadeScreenColor.a, solidAlpha, fadeToBlackTime * Time.deltaTime);
        }
        else
        {
            fadeScreenColor.a = Mathf.Lerp(fadeScreenColor.a, fadedAlpha, fadeToBlackTime * Time.deltaTime);
        }
        fadeScreen.GetComponent<Image>().color = fadeScreenColor;
        //Fade Text Logic
        if (showFadeText == true)
        {
            fadeTextColor.a = Mathf.Lerp(fadeTextColor.a, solidAlpha, fadeToBlackTime * Time.deltaTime * 2);
        }
        else
        {
            fadeTextColor.a = Mathf.Lerp(fadeTextColor.a, fadedAlpha, fadeToBlackTime * Time.deltaTime * 2);
        }
        fadeText.GetComponent<Text>().color = fadeTextColor;
    }
}
