﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//This script is incharge of all the Special Effects for the Bucyrus Dragline.
//In this script the logic for making components/ parts of the Dragline Appear/Disappear as well as highlighting are contained.
//This script should be attached to the Game Controller in any scene containing a Bucyrus Dragline
//The intention is that when a SFX is needed, the Game Controller script will change the appropriate public variable in this script to achieve the desired effect.

public class Bucyrus_DL_SFX : MonoBehaviour 
{
	public float flashDuration = 0.8f;
	public float colorLerp;
	public ShowHide showHide;
	public BucycrusFadeManager fadeManager;
	public BucyrusHighlighting highlighting;


//******************************************************************************************************************************
	//Game Objects Section
	private GameObject aFrameTopBar;
	private GameObject aFrameLegs;
	private GameObject aFrameLugs;

	private GameObject boomLights;
	private GameObject boomPlatform;
	private GameObject boomLacings;
	private GameObject boomChords;
	private GameObject[] boomSheave;
	private GameObject boomFeet;
	private GameObject boomTip;
	private GameObject[] bucket;
	private GameObject bucketTeeth;

	private GameObject[] clevis;
	private GameObject centerPin;

	private GameObject deflectionTowers;
	private GameObject[] dragChains;
	private GameObject leftDragRopeParent;
	private GameObject[] leftDragRopes = new GameObject[1];
	private GameObject leftDragRope;
	private GameObject rightDragRopeParent;
	private GameObject[] rightDragRopes = new GameObject[1];
	private GameObject rightDragRope;
	private GameObject dumpBlock;
	private GameObject[] dumpChains;
	private GameObject dumpRopeParent;
	private GameObject[] dumpRopes = new GameObject[1];
	private GameObject dumpRope;

	private GameObject housePlatform;
	private GameObject houseLights;
	private GameObject houseCab;
	private GameObject houseBody;
	private GameObject[] hoistChains;
	private GameObject leftHoistRopeParent;
	private GameObject[] leftHoistRopes = new GameObject[1];
	private GameObject leftHoistRope;
	private GameObject rightHoistRopeParent;
	private GameObject[] rightHoistRopes = new GameObject[1];
	private GameObject rightHoistRope;

	private GameObject[] liftingHooks;

	private GameObject[] mastChords;
	private GameObject[] mastFeet;
	private GameObject mastHead;
	private GameObject[] mastLacings;

	private GameObject lowerRollerRail;
	private GameObject upperRollerRail;
	private GameObject revFrame;
	private GameObject revFrameFloor;

	private GameObject[] swingPinions;
	private GameObject swingRack;
	private GameObject spreaderBar;
	public GameObject[] straightRopes;
	private GameObject[] upperSusRopes = new GameObject[4];
	private GameObject[] intSusRopes = new GameObject[4];
	private GameObject[] lowerSusRopes = new GameObject[4];

	private GameObject tub;
	private GameObject tubBulkhead;
	private GameObject tubRollers;
	private GameObject[] trunion;

	//******************************************************************************************************************************
	//Materials Section
	private Material[] boomChordsMaterial;
	private Material[] boomLacingsMaterial;
	private Material[] hoistRopeMaterial = new Material[2];
	private Material[] houseBodyMaterial = new Material[3];
	private Material[] houseCabMaterial = new Material[7];
	private Material[] houseLightsMaterial = new Material[2];
	private Material[] housePlatformMaterial = new Material[3];
	private Material[] revFrameMaterial = new Material[1];
	private Material[] tubRollersMaterial = new Material[1];
	private Material[] tubMaterial = new Material[1];

	//******************************************************************************************************************************
	//Colors Section
	private Color highlightColor = new Color (212/255f, 208/255f, 15/255f);
	//Boom Chords
	private Color[] boomChordsOriginalColor = new Color[2];
	private Color[] boomChordsOriginalEColor= new Color[2];
	private Color[] boomChordsNewColor = new Color[2];
	private Color[] boomChordsNewEColor = new Color[2];
	//Boom Lacings
	private Color[] boomLacingsOriginalColor = new Color[2];
	private Color[] boomLacingsOriginalEColor = new Color[2];
	private Color[] boomLacingsNewColor = new Color[2];
	private Color[] boomLacingsNewEColor = new Color[2];
	//Suspension Ropes
	private Color upperSusRopesOriginalColor;
	private Color upperSusRopesOriginalEColor;
	private Color upperSusRopesNewColor;
	private Color upperSusRopesNewEColor;
	private Color intSusRopesOriginalColor;
	private Color intSusRopesOriginalEColor;
	private Color intSusRopesNewColor;
	private Color intSusRopesNewEColor;
	private Color lowerSusRopesOriginalColor;
	private Color lowerSusRopesOriginalEColor;
	private Color lowerSusRopesNewColor;
	private Color lowerSusRopesNewEColor;
	//A-frame
	private Color aFrameTopBarOriginalColor;
	private Color aFrameTopBarNewColor;
	//Trunion
	private Color trunionOriginalColor;
	private Color trunionNewColor;
	//Boom Sheave
	private Color boomSheaveOriginalColor;
	private Color boomSheaveNewColor;
	//Boom Feet
	private Color boomFeetOriginalColor;
	private Color boomFeetOriginalEColor;
	private Color boomFeetNewColor;
	private Color boomFeetNewEColor;
	//Boom Tip
	private Color boomTipOriginalColor;
	private Color boomTipOriginalEColor;
	private Color boomTipNewColor;
	private Color boomTipNewEColor;
	//Bucket
	private Color bucketOriginalColor;
	private Color bucketNewColor;
	private Color bucketBitsOriginalColor;
	private Color bucketBitsNewColor;
	//Bucket Teeth
	private Color bucketTeethOriginalColor;
	private Color bucketTeethNewColor;
	//Clevis
	private Color clevisOriginalColor;
	private Color clevisNewColor;
	//Deflection Towers
	private Color deflectionTowersOriginalColor;
	private Color deflectionTowersOriginalEColor;
	private Color deflectionTowersNewColor;
	private Color deflectionTowersNewEColor;
	//Drag Chains
	private Color dragChainsOriginalColor;
	private Color dragChainsOriginalEColor;
	private Color dragChainsNewColor;
	private Color dragChainsNewEColor;
	//Drag Ropes
	private Color dragRopeOriginalColor;
	private Color dragRopeOriginalEColor;
	private Color dragRopeNewColor;
	private Color dragRopeNewEColor;
	//Dump Block
	private Color dumpBlockOriginalColor;
	private Color dumpBlockOriginalEColor;
	private Color dumpBlockNewColor;
	private Color dumpBlockNewEColor;
	//Dump Chain
	private Color dumpChainOriginalColor;
	private Color dumpChainOriginalEColor;
	private Color dumpChainNewColor;
	private Color dumpChainNewEColor;
	//Dump Rope
	private Color dumpRopeOriginalColor;
	private Color dumpRopeOriginalEColor;
	private Color dumpRopeNewColor;
	private Color dumpRopeNewEColor;
	//Hoist Chains
	private Color hoistChainOriginalColor;
	private Color hoistChainOriginalEColor;
	private Color hoistChainNewColor;
	private Color hoistChainNewEColor;
	//Hoist Ropes
	private Color hoistRopeOriginalColor;
	private Color hoistRopeOriginalEColor;
	private Color hoistRopeNewColor;
	private Color hoistRopeNewEColor;
	//House Body
	private Color[] houseBodyOriginalColors = new Color[3];
	private Color[] houseBodyNewColors;
	//House Cab
	private Color[] houseCabOriginalColors = new Color[7];
	private Color[] houseCabNewColors;
	//House Lights
	private Color[] houseLightsOriginalColors = new Color[2];
	private Color[] houseLightsNewColors;
	//House Platform
	private Color[] housePlatformOriginalColor = new Color[3];
	private Color[] housePlatformNewColor;
	//Mast Chords
	private Color mastChordsOriginalColor;
	private Color mastChordsNewColor;
	//Mast Feet
	private Color mastFeetOriginalColor;
	private Color mastFeetNewColor;
	//Mast Head
	private Color mastHeadOriginalColor;
	private Color mastHeadOriginalEColor;
	private Color mastHeadNewColor;
	private Color mastHeadNewEColor;
	//Mast Lacings
	private Color mastLacingsOriginalColor;
	private Color mastLacingsNewColor;
	//Spreader Bar
	private Color spreaderBarOriginalColor;
	private Color spreaderBarOriginalEColor;
	private Color spreaderBarNewColor;
	private Color spreaderBarNewEColor;
	//Center Pin
	private Color centerPinOriginalColor;
	private Color centerPinNewColor;
	//Tub
	private Color tubOriginalColor;
	private Color tubOriginalEColor;
	private Color tubNewColor;
	private Color tubNewEColor;
	//Tub Bulkhead
	private Color tubBulkheadOriginalColor;
	private Color tubBulkheadOriginalEColor;
	private Color tubBulkheadNewColor;
	private Color tubBulkheadNewEColor;
	//Tub Rollers
	private Color tubRollersOriginalColor;
	private Color tubRollersOriginalEColor;
	private Color tubRollersNewColor;
	private Color tubRollersNewEColor;
	//Swing Rack
	private Color swingRackOriginalColor;
	private Color swingRackOriginalEColor;
	private Color swingRackNewColor;
	private Color swingRackNewEColor;
	//Roller Rails
	private Color lowerRollerRailOriginalColor;
	private Color lowerRollerRailOriginalEColor;
	private Color lowerRollerRailNewColor;
	private Color lowerRollerRailNewEColor;
	private Color upperRollerRailOriginalColor;
	private Color upperRollerRailOriginalEColor;
	private Color upperRollerRailNewColor;
	private Color upperRollerRailNewEColor;
	//Swing Pinions
	private Color swingPinionOriginalColor;
	private Color swingPinionOriginalEColor;
	private Color swingPinionNewColor;
	private Color swingPinionNewEColor;
	//Lifting Hooks
	private Color liftingHooksOriginalColor;
	private Color liftingHooksOriginalEColor;
	private Color liftingHooksNewColor;
	private Color liftingHooksNewEColor;
	//A Frame Legs
	private Color aFrameLegsOriginalColor;
	private Color aFrameLegsOriginalEColor;
	private Color aFrameLegsNewColor;
	private Color aFrameLegsNewEColor;
	//A Frame Lugs
	private Color aFrameLugsOriginalColor;
	private Color aFrameLugsOriginalEColor;
	private Color aFrameLugsNewColor;
	private Color aFrameLugsNewEColor;
	//Revolving Frame
	private Color revFrameOriginalColor;
	private Color revFrameOriginalEColor;
	private Color revFrameNewColor;
	private Color revFrameNewEColor;
	//Revolving Frame Floor
	private Color revFrameFloorOriginalColor;
	private Color revFrameFloorOriginalEColor;
	private Color revFrameFloorNewColor;
	private Color revFrameFloorNewEColor;

	//******************************************************************************************************************************
	//Bools and Floats Section
	private bool gotSuspensionRopes = false;
	private bool leftDragRopeFound = false;
	private bool rightDragRopeFound = false;
	private bool dumpRopeFound = false;
	private bool leftHoistRopeFound = false;
	private bool rightHoistRopeFound = false;

	private float leftDragRopeChildCount;
	private float rightDragRopeChildCount;
	private float dumpRopeChildCount;
	private float leftHoistRopeChildCount;
	private float rightHoistRopeChildCount;

    private float fadeValueCapHouseCabWindow;

    private int leftDragCount;
	private int rightDragCount;
	private int dumpRopeCount;
	private int leftHoistCount;
	private int rightHoistCount;

	// Use this for initialization
	void Start () 
	{
		//******************************************************************************************************************************
		//Define Game Objects
		boomLights = GameObject.FindWithTag("BoomLights");
		boomPlatform = GameObject.FindWithTag ("BoomPlatform");
		boomLacings = GameObject.FindWithTag ("BoomLacings");
		boomChords = GameObject.FindWithTag ("BoomChords");

		housePlatform = GameObject.FindWithTag ("HousePlatform");
		houseCab = GameObject.FindWithTag ("HouseCab");
		houseLights = GameObject.FindWithTag ("HouseLights");
		houseBody = GameObject.FindWithTag ("HouseBody");
		aFrameTopBar = GameObject.FindWithTag ("AFrameHighlight");
		trunion = GameObject.FindGameObjectsWithTag ("TrunionHighlight");
		boomSheave = GameObject.FindGameObjectsWithTag ("BoomSheave");
		boomFeet = GameObject.FindWithTag ("BoomFeet");
		boomTip = GameObject.FindWithTag ("BoomTip");
		bucket = GameObject.FindGameObjectsWithTag ("Bucket");
		bucketTeeth = GameObject.FindWithTag ("BucketTeeth");
		clevis = GameObject.FindGameObjectsWithTag ("Clevis");
		deflectionTowers = GameObject.FindWithTag ("DeflectionTowers");
		dragChains = GameObject.FindGameObjectsWithTag ("DragChain");
		leftDragRopeParent = GameObject.FindWithTag ("LeftDragRopeParent");
		rightDragRopeParent = GameObject.FindWithTag ("RightDragRopeParent");
		dumpBlock = GameObject.FindWithTag ("DumpBlock");
		dumpChains = GameObject.FindGameObjectsWithTag ("DumpChain");
		dumpRopeParent = GameObject.FindWithTag ("DumpRopeParent");
		hoistChains = GameObject.FindGameObjectsWithTag ("HoistChain");
		leftHoistRopeParent = GameObject.FindWithTag ("LeftHoistRopeParent");
		rightHoistRopeParent = GameObject.FindWithTag ("RightHoistRopeParent");
		mastChords = GameObject.FindGameObjectsWithTag ("MastChords");
		mastFeet = GameObject.FindGameObjectsWithTag ("MastFeetHighlight");
		mastHead = GameObject.FindWithTag ("MastHead");
		mastLacings = GameObject.FindGameObjectsWithTag ("MastLacings");
		spreaderBar = GameObject.FindWithTag ("SpreaderBar");
		centerPin = GameObject.FindWithTag ("CenterPinHighlight");
		tub = GameObject.FindWithTag ("Tub");
		tubBulkhead = GameObject.FindWithTag("TubBulkhead");
		tubRollers = GameObject.FindWithTag ("TubRollers");
		swingRack = GameObject.FindWithTag ("SwingRack");
		lowerRollerRail = GameObject.FindWithTag ("LowerRollerRail");
		upperRollerRail = GameObject.FindWithTag ("UpperRollerRail");
		swingPinions = GameObject.FindGameObjectsWithTag ("SwingPinion");
		liftingHooks = GameObject.FindGameObjectsWithTag ("LiftingHook");
		aFrameLegs = GameObject.FindWithTag ("AFrameLegs");
		aFrameLugs = GameObject.FindWithTag ("AFrameLugs");
		revFrame = GameObject.FindWithTag ("RevFrame");
		revFrameFloor = GameObject.FindWithTag ("RevFrameFloor");

		//******************************************************************************************************************************
		//Define Materials
		boomChordsMaterial = boomChords.GetComponent<Renderer> ().materials;
		boomLacingsMaterial = boomLacings.GetComponent<Renderer> ().materials;
		houseBodyMaterial = houseBody.GetComponent<Renderer> ().materials;
		houseCabMaterial = houseCab.GetComponent<Renderer> ().materials;
		houseLightsMaterial = houseLights.GetComponent<Renderer> ().materials;
		housePlatformMaterial = housePlatform.GetComponent<Renderer> ().materials;
		revFrameMaterial[0] = revFrame.GetComponent<Renderer> ().material;
		tubRollersMaterial[0] = tubRollers.GetComponent<Renderer> ().material;
		tubMaterial [0] = tub.GetComponent<Renderer> ().material;

		//******************************************************************************************************************************
		//Define Colors
		for (int i = 0; i < boomChordsMaterial.Length; i++) 
		{
			boomChordsOriginalColor [i] = boomChordsMaterial [i].color;
			boomChordsOriginalEColor [i] = boomChordsMaterial [i].GetColor ("_EmissionColor");
		}

		for (int i = 0; i < boomLacingsMaterial.Length; i++) 
		{
			boomLacingsOriginalColor [i] = boomLacingsMaterial [i].color;
			boomLacingsOriginalEColor [i] = boomLacingsMaterial [i].GetColor ("_EmissionColor");
		}

		aFrameTopBarOriginalColor = aFrameTopBar.GetComponent<Renderer> ().material.color;
		trunionOriginalColor = trunion [0].GetComponent<Renderer> ().material.color;
		boomSheaveOriginalColor = boomSheave [0].GetComponent<Renderer> ().material.color;
		boomFeetOriginalColor = boomFeet.GetComponent<Renderer> ().material.color;
		boomFeetOriginalEColor = boomFeet.GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		boomTipOriginalColor = boomTip.GetComponent<Renderer> ().material.color;
		boomTipOriginalEColor = boomTip.GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		bucketBitsOriginalColor = bucket [0].GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		bucketOriginalColor = bucket [1].GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		bucketTeethOriginalColor = bucketTeeth.GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		clevisOriginalColor = clevis [0].GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		deflectionTowersOriginalColor = deflectionTowers.GetComponent<Renderer> ().material.color;
		deflectionTowersOriginalEColor = deflectionTowers.GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		dragChainsOriginalColor = dragChains [0].GetComponent<Renderer> ().material.color;
		dragChainsOriginalEColor = dragChains [0].GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		dumpBlockOriginalColor = dumpBlock.GetComponent<Renderer> ().material.color;
		dumpBlockOriginalEColor = dumpBlock.GetComponent<Renderer> ().material.GetColor("_EmissionColor");
		dumpChainOriginalColor = dumpChains [0].GetComponent<Renderer> ().material.color;
		dumpChainOriginalEColor = dumpChains [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		hoistChainOriginalColor = hoistChains [0].GetComponent<Renderer> ().material.color;
		hoistChainOriginalEColor = hoistChains [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		for (int i = 0; i < houseBodyMaterial.Length; i++) 
		{
			houseBodyOriginalColors [i] = houseBodyMaterial [i].color;
		}
		for (int i = 0; i < houseCabMaterial.Length; i++) 
		{
			houseCabOriginalColors [i] = houseCabMaterial [i].color;
		}
		for (int i = 0; i < houseLightsMaterial.Length; i++) {
			houseLightsOriginalColors [i] = houseLightsMaterial [i].color;
		}
		for (int i = 0; i < housePlatformMaterial.Length; i++) {
			housePlatformOriginalColor [i] = housePlatformMaterial [i].color;
		}
		mastChordsOriginalColor = mastChords [0].GetComponent<Renderer> ().material.color;
		mastFeetOriginalColor = mastFeet [0].GetComponent<Renderer> ().material.color;
		mastHeadOriginalColor = mastHead.GetComponent<Renderer> ().material.color;
		mastHeadOriginalEColor = mastHead.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		mastLacingsOriginalColor = mastLacings [0].GetComponent<Renderer> ().material.color;
		spreaderBarOriginalColor = spreaderBar.GetComponent<Renderer> ().material.color;
		spreaderBarOriginalEColor = spreaderBar.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		centerPinOriginalColor = centerPin.GetComponent<Renderer> ().material.color;
		tubOriginalColor = tub.GetComponent<Renderer> ().material.color;
		tubOriginalEColor = tub.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		tubBulkheadOriginalColor = tubBulkhead.GetComponent<Renderer> ().material.color;
		tubBulkheadOriginalEColor = tubBulkhead.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		tubRollersOriginalColor = tubRollers.GetComponent<Renderer> ().material.color;
		tubRollersOriginalEColor = tubRollers.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		swingRackOriginalColor = swingRack.GetComponent<Renderer> ().material.color;
		swingRackOriginalEColor = swingRack.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		lowerRollerRailOriginalColor = lowerRollerRail.GetComponent<Renderer> ().material.color;
		lowerRollerRailOriginalEColor = lowerRollerRail.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		upperRollerRailOriginalColor = upperRollerRail.GetComponent<Renderer> ().material.color;
		upperRollerRailOriginalEColor = upperRollerRail.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		swingPinionOriginalColor = swingPinions [0].GetComponent<Renderer> ().material.color;
		swingPinionOriginalEColor = swingPinions [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		liftingHooksOriginalColor = liftingHooks [0].GetComponent<Renderer> ().material.color;
		liftingHooksOriginalEColor = liftingHooks [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		aFrameLegsOriginalColor = aFrameLegs.GetComponent<Renderer> ().material.color;
		aFrameLegsOriginalEColor = aFrameLegs.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		aFrameLugsOriginalColor = aFrameLugs.GetComponent<Renderer> ().material.color;
		aFrameLugsOriginalEColor = aFrameLugs.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		revFrameOriginalColor = revFrame.GetComponent<Renderer> ().material.color;
		revFrameOriginalEColor = revFrame.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		revFrameFloorOriginalColor = revFrameFloor.GetComponent<Renderer> ().material.color;
		revFrameFloorOriginalEColor = revFrameFloor.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");


		//******************************************************************************************************************************
		//Set Defaults
		showHide.boomLights = true;
		showHide.boomPlatform = true;
		showHide.boomChords = true;
		showHide.boomLacings = true;
		showHide.houseCab = true;
		showHide.houseLights = true;
		showHide.housePlatform = true;
		showHide.houseBody = true;
		showHide.upperSuspensionRopes = true;
		showHide.intSuspensionRopes = true;
		showHide.lowerSuspensionRopes = true;
		showHide.tub = true;
		showHide.hoistRopes = true;
		showHide.upperRollerRail = true;
		showHide.rollerCircle = true;
		showHide.swingPinions = true;
		showHide.liftingHooks = true;
		showHide.aFrameLegs = true;
		showHide.aFrameLugs = true;
		showHide.revFrame = true;
		showHide.revFrameFloor = true;
		fadeManager.hoistRopes.fade = 1;
		fadeManager.houseBody.fade = 1;
		fadeManager.houseCab.fade = 1;
		fadeManager.houseLights.fade = 1;
		fadeManager.housePlatform.fade = 1;
		fadeManager.tub.fade = 1;
        fadeManager.houseCabWindows.fade = houseCabMaterial[4].color.a; //Put this in as a hotfix
        fadeValueCapHouseCabWindow = fadeManager.houseCabWindows.fade;

    }//Start End
	
	// Update is called once per frame
	void Update () 
	{
		colorLerp = Mathf.PingPong (Time.time, flashDuration) / flashDuration;																	//this value ping pongs between 0 and 1 according to Time.time


		//******************************************************************************************************************************
		//Delayed Object Definition Section
		if (straightRopes.Length < 2) {straightRopes = GameObject.FindGameObjectsWithTag ("StraightRopePrefab");}								//Search for suspension ropes until they are found
		if (straightRopes.Length == 12 && gotSuspensionRopes == false) 																			//Define upper int and lower suspension ropes
		{
			for (int i = 0; i < 4; i++) 
			{
				lowerSusRopes [i] = straightRopes [i];																							//Lower Suspension Ropes are the first 4 
				intSusRopes [i] = straightRopes [i + 4];																						//Intermediate Sus Ropes are the middle 4
				upperSusRopes [i] = straightRopes [i + 8];																						//Upper Sus Ropes are the last 4
			}
			upperSusRopesOriginalEColor = upperSusRopes [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			upperSusRopesOriginalColor = upperSusRopes [0].GetComponent<Renderer> ().material.color;
			intSusRopesOriginalEColor = intSusRopes [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			intSusRopesOriginalColor = intSusRopes [0].GetComponent<Renderer> ().material.color;
			lowerSusRopesOriginalColor = lowerSusRopes [0].GetComponent<Renderer> ().material.color;
			lowerSusRopesOriginalEColor = lowerSusRopes [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			gotSuspensionRopes = true;																											//Signal that all suspension ropes have been found
		}

		leftDragRopeChildCount = leftDragRopeParent.transform.childCount;																		//Count all children of parent object that have transforms
		if (leftDragRopeChildCount != 0 && leftDragRopeFound == false) 
		{
			leftDragCount = 0;
			foreach (Transform i in leftDragRopeParent.transform) 																				//For each child transform
			{
				leftDragRopes [leftDragCount] = i.gameObject;																					//Add the game object attached to the transform to the array
				leftDragCount++;
			}
			leftDragRope = leftDragRopes [0];																									//Drag ropes only have one child
			dragRopeOriginalColor = leftDragRope.GetComponent<Renderer>().material.color;														//Get the original color
			dragRopeOriginalEColor = leftDragRope.GetComponent<Renderer>().material.GetColor ("_EmissionColor");
			leftDragRopeFound = true;																											//Flag that the left drag ropes have been found
		}

		rightDragRopeChildCount = rightDragRopeParent.transform.childCount;																		//Count all children of parent object that have transforms
		if (rightDragRopeChildCount != 0 && rightDragRopeFound == false) 
		{
			rightDragCount = 0;
			foreach (Transform i in rightDragRopeParent.transform) 																				//For each child transform
			{
				rightDragRopes [rightDragCount] = i.gameObject;																					//Add the game object attached to the transform to the array
				rightDragCount++;
			}
			rightDragRope = rightDragRopes [0];
			rightDragRopeFound = true;																											//Flag that the left drag ropes have been found
		}

		dumpRopeChildCount = dumpRopeParent.transform.childCount;
		if (dumpRopeChildCount != 0 && dumpRopeFound == false) 
		{
			dumpRopeCount = 0;
			foreach (Transform i in dumpRopeParent.transform) 
			{
				dumpRopes [dumpRopeCount] = i.gameObject;
				dumpRopeCount++;
			}
			dumpRope = dumpRopes [0];
			dumpRopeOriginalColor = dumpRope.GetComponent<Renderer> ().material.color;
			dumpRopeOriginalEColor = dumpRope.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			dumpRopeFound = true;

		}

		leftHoistRopeChildCount = leftHoistRopeParent.transform.childCount;
		if (leftHoistRopeChildCount != 0 && leftHoistRopeFound == false) 
		{
			leftHoistCount = 0;
			foreach (Transform i in leftHoistRopeParent.transform) 
			{
				leftHoistRopes [leftHoistCount] = i.gameObject;
				leftHoistCount++;
			}
			leftHoistRope = leftHoistRopes [0];
			hoistRopeMaterial [0] = leftHoistRope.GetComponent<Renderer> ().material;
			hoistRopeOriginalColor = leftHoistRope.GetComponent<Renderer> ().material.color;
			hoistRopeOriginalEColor = leftHoistRope.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			leftHoistRopeFound = true;
		}

		rightHoistRopeChildCount = rightHoistRopeParent.transform.childCount;
		if (rightHoistRopeChildCount != 0 && rightHoistRopeFound == false) 
		{
			rightHoistCount = 0;
			foreach (Transform i in rightHoistRopeParent.transform) 
			{
				rightHoistRopes [rightHoistCount] = i.gameObject;
				rightHoistCount++;
			}
			rightHoistRope = rightHoistRopes [0];
			hoistRopeMaterial [1] = rightHoistRope.GetComponent<Renderer> ().material;
			rightHoistRopeFound = true;
		}

		//******************************************************************************************************************************
		//Show/Hide Section
		aFrameLegs.SetActive(showHide.aFrameLegs);
		boomLights.SetActive (showHide.boomLights);
		boomPlatform.SetActive (showHide.boomPlatform);
		//To Show/Hide Boom Chords, see Highlight Section
		//To Show/Hide Boom Lacings, see Highlight Section
		houseCab.SetActive (showHide.houseCab);
		houseBody.SetActive (showHide.houseBody);
		houseLights.SetActive (showHide.houseLights);
		housePlatform.SetActive (showHide.housePlatform);
		//To Show/Hide Upper, Intermediate/Lower Suspension Ropes, see Highlight Section
		tub.SetActive(showHide.tub);
		leftHoistRopeParent.SetActive (showHide.hoistRopes);
		rightHoistRopeParent.SetActive (showHide.hoistRopes);
		tubRollers.SetActive (showHide.rollerCircle);
		upperRollerRail.SetActive (showHide.upperRollerRail);

		for (int i = 0; i < swingPinions.Length; i++) 
		{
			swingPinions [i].SetActive (showHide.swingPinions);
		}

		for (int i = 0; i < liftingHooks.Length; i++) 
		{
			liftingHooks [i].SetActive (showHide.liftingHooks);
		}

		aFrameLugs.SetActive (showHide.aFrameLugs);
		revFrame.SetActive (showHide.revFrame);
		revFrameFloor.SetActive (showHide.revFrameFloor);

		//******************************************************************************************************************************
		//Fade Section

		//Hoist Ropes
		fadeManager.hoistRopes.fade = FadeController(fadeManager.hoistRopes.enabled, fadeManager.hoistRopes.fade);									//Update the fade value based on enabled state
		ShaderChanger(hoistRopeMaterial, fadeManager.hoistRopes.enabled, fadeManager.hoistRopes.fade);
		//For  the writing of the hoist rope alpha see Highlight section

		//House Body
		fadeManager.houseBody.fade = FadeController(fadeManager.houseBody.enabled, fadeManager.houseBody.fade);
		ShaderChanger (houseBodyMaterial, fadeManager.houseBody.enabled, fadeManager.houseBody.fade);
		houseBodyNewColors = houseBodyOriginalColors;
		for (int i = 0; i < houseBodyNewColors.Length; i++) 
		{
			houseBodyNewColors [i].a = fadeManager.houseBody.fade;
			houseBodyMaterial [i].SetColor ("_Color", houseBodyNewColors [i]);
		}


        //House Cab
        //fadeManager.houseCab.fade = FadeController(fadeManager.houseCab.enabled, fadeManager.houseCab.fade);
        //ShaderChanger (houseCabMaterial, fadeManager.houseCab.enabled, fadeManager.houseCab.fade);
        //houseCabNewColors = houseCabOriginalColors;
        //for (int i = 0; i < houseCabNewColors.Length; i++) 
        //{
        //	houseCabNewColors [i].a = fadeManager.houseCab.fade;
        //	houseCabMaterial [i].SetColor ("_Color", houseCabNewColors [i]);
        //}
        fadeManager.houseCab.fade = FadeController(fadeManager.houseCab.enabled, fadeManager.houseCab.fade);

        fadeManager.houseCabWindows.fade = FadeControllerForWindows(fadeManager.houseCab.enabled, fadeManager.houseCabWindows.fade, fadeValueCapHouseCabWindow);

        ShaderChanger(houseCabMaterial, fadeManager.houseCab.enabled, fadeManager.houseCab.fade);
        houseCabNewColors = houseCabOriginalColors;
        for (int i = 0; i < houseCabNewColors.Length; i++)
        {
            if (i == 4 || i == 6)
                houseCabNewColors[i].a = fadeManager.houseCabWindows.fade;
            else houseCabNewColors[i].a = fadeManager.houseCab.fade;
            houseCabMaterial[i].SetColor("_Color", houseCabNewColors[i]);

            //Debug.Log("colors are " + houseCabNewColors[i].ToString());

        }

        //House Lights
        fadeManager.houseLights.fade = FadeController(fadeManager.houseLights.enabled, fadeManager.houseLights.fade);
		ShaderChanger (houseLightsMaterial, fadeManager.houseLights.enabled, fadeManager.houseLights.fade);
		houseLightsNewColors = houseLightsOriginalColors;
		for (int i = 0; i < houseLightsNewColors.Length; i++) 
		{
			houseLightsNewColors [i].a = fadeManager.houseLights.fade;
			houseLightsMaterial [i].SetColor ("_Color", houseLightsNewColors [i]);
		}

		//House Platform
		fadeManager.housePlatform.fade = FadeController(fadeManager.housePlatform.enabled, fadeManager.housePlatform.fade);
		ShaderChanger (housePlatformMaterial, fadeManager.housePlatform.enabled, fadeManager.housePlatform.fade);
		housePlatformNewColor = housePlatformOriginalColor;
		for (int i = 0; i < housePlatformNewColor.Length; i++) 
		{
			housePlatformNewColor [i].a = fadeManager.housePlatform.fade;
			housePlatformMaterial [i].SetColor ("_Color", housePlatformNewColor [i]);
		}

		//Rev Frame
		fadeManager.revFrame.fade = FadeController(fadeManager.revFrame.enabled, fadeManager.revFrame.fade);
		ShaderChanger (revFrameMaterial, fadeManager.revFrame.enabled, fadeManager.revFrame.fade);
		//For  the writing of the hoist rope alpha see Highlight section

		//Tub Rollers
		fadeManager.rollerCircle.fade = FadeController(fadeManager.rollerCircle.enabled, fadeManager.rollerCircle.fade);
		ShaderChanger (tubRollersMaterial, fadeManager.rollerCircle.enabled, fadeManager.rollerCircle.fade);
		//For  the writing of the hoist rope alpha see Highlight section

		//Tub
		fadeManager.tub.fade = FadeController(fadeManager.tub.enabled, fadeManager.tub.fade);
		ShaderChanger (tubMaterial, fadeManager.tub.enabled, fadeManager.tub.fade);
		//For the writing of the tub alpha see Highlight section

		//******************************************************************************************************************************
		//Highlight Section

		//A-Frame
		highlighting.aFrameTopBar.highlight = FlashStatus((int)highlighting.aFrameTopBar.status);													//Read the Highlight status of the Object
		aFrameTopBarNewColor = aFrameTopBarOriginalColor;																							//
		aFrameTopBarNewColor.a = highlighting.aFrameTopBar.highlight;																				//For the Afrmae only modify the Alpha value
		aFrameTopBar.GetComponent<Renderer> ().material.SetColor ("_Color", aFrameTopBarNewColor);													//Update the actual color of the object

		//Boom Chords
		highlighting.boomChords.highlight = FlashStatus((int)highlighting.boomChords.status);														//Read the Highlight status of the Object
		for (int i = 0; i < boomChordsMaterial.Length; i++) 
		{
			boomChordsNewColor[i] = Color.Lerp (boomChordsOriginalColor[i], highlightColor, highlighting.boomChords.highlight);
			boomChordsNewEColor[i] = Color.Lerp (boomChordsOriginalEColor[i], highlightColor, highlighting.boomChords.highlight);
			if (showHide.boomChords == true) {boomChordsNewColor[i].a = 1f;}																		//If commanded to show set alpha to 1
			if (showHide.boomChords == false) {boomChordsNewColor[i].a = 0f;}																		//If commanded to Hide set alpha to 0
		}

		if (boomChordsMaterial [0].color != boomChordsNewColor [0]) 																				//Only Set the color if current color != comanded color
		{
			for (int i = 0; i < boomChordsMaterial.Length; i++) 
			{
				boomChordsMaterial [i].SetColor ("_Color", boomChordsNewColor [i]);
				boomChordsMaterial [i].SetColor ("_EmissionColor", boomChordsNewEColor [i]);
			}
		}

		//Boom Feet
		highlighting.boomFeet.highlight = FlashStatus((int)highlighting.boomFeet.status);															//Read the Highlight status of the Object
		boomFeetNewColor = Color.Lerp (boomFeetOriginalColor, highlightColor, highlighting.boomFeet.highlight);
		boomFeetNewEColor = Color.Lerp (boomFeetOriginalEColor, highlightColor, highlighting.boomFeet.highlight);
		if (boomFeet.GetComponent<Renderer>().material.color != boomFeetNewColor) 
		{
			boomFeet.GetComponent<Renderer> ().material.SetColor ("_Color", boomFeetNewColor);
			boomFeet.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", boomFeetNewEColor);
		}

		//Boom Lacings
		highlighting.boomLacings.highlight = FlashStatus((int)highlighting.boomLacings.status);														//Read the Highlight status of the Object

		for (int i = 0; i < boomLacingsMaterial.Length; i++) 
		{
			boomLacingsNewColor[i] = Color.Lerp (boomLacingsOriginalColor[i], highlightColor, highlighting.boomLacings.highlight);
			boomLacingsNewEColor[i] = Color.Lerp (boomLacingsOriginalEColor[i], highlightColor, highlighting.boomLacings.highlight);
			if (showHide.boomLacings == true) {boomLacingsNewColor[i].a = 1f;}																		//If commanded to show set alpha to 1
			if (showHide.boomLacings == false){boomLacingsNewColor[i].a = 0f;}																		//If commanded to Hide set alpha to 0
		}

		if (boomLacingsMaterial [0].color != boomLacingsNewColor [0]) 																				//Only Set the color if current color != comanded color
		{
			for (int i = 0; i < boomLacingsMaterial.Length; i++) 
			{
				boomLacingsMaterial [i].SetColor ("_Color", boomLacingsNewColor [i]);
				boomLacingsMaterial [i].SetColor ("_EmissionColor", boomLacingsNewEColor [i]);
			}
		}

		//Boom Sheaves
		highlighting.boomSheave.highlight = FlashStatus((int)highlighting.boomSheave.status);														//Read the Highlight status of the Object
		boomSheaveNewColor = Color.Lerp (boomSheaveOriginalColor, highlightColor, highlighting.boomSheave.highlight);
		for (int i = 0; i < boomSheave.Length; i++) 
		{
			boomSheave [i].GetComponent<Renderer> ().material.SetColor ("_Color", boomSheaveNewColor);
		}

		//Boom Tip
		highlighting.boomTip.highlight = FlashStatus((int)highlighting.boomTip.status);																//Read the Highlight status of the Object
		boomTipNewColor = Color.Lerp (boomTipOriginalColor, highlightColor, highlighting.boomTip.highlight);
		boomTipNewEColor = Color.Lerp (boomTipOriginalEColor, highlightColor, highlighting.boomTip.highlight);
		if (boomTip.GetComponent<Renderer>().material.color != boomTipNewColor) 
		{
			boomTip.GetComponent<Renderer> ().material.SetColor ("_Color", boomTipNewColor);
			boomTip.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", boomTipNewEColor);
		}

		//Bucket
		highlighting.bucket.highlight = FlashStatus((int)highlighting.bucket.status);																//Read the Highlight status of the Object
		bucketNewColor = Color.Lerp (bucketOriginalColor, highlightColor, highlighting.bucket.highlight);
		bucketBitsNewColor = Color.Lerp (bucketBitsOriginalColor, highlightColor, highlighting.bucket.highlight);
		bucket [0].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", bucketBitsNewColor);
		bucket [1].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", bucketNewColor);


		//Bucket Teeth
		highlighting.bucketTeeth.highlight = FlashStatus((int)highlighting.bucketTeeth.status);														//Read the Highlight status of the Object
		bucketTeethNewColor = Color.Lerp (bucketTeethOriginalColor, highlightColor, highlighting.bucketTeeth.highlight);
		bucketTeeth.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", bucketTeethNewColor);

		//Clevis
		highlighting.clevis.highlight = FlashStatus((int)highlighting.clevis.status);																//Read the Highlight status of the Object
		clevisNewColor = Color.Lerp (clevisOriginalColor, highlightColor, highlighting.clevis.highlight);
		for (int i = 0; i < clevis.Length; i++) 
		{
			clevis [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", clevisNewColor);
		}

		//Deflection Towers
		highlighting.deflectionTowers.highlight = FlashStatus((int)highlighting.deflectionTowers.status);											//Read the Highlight status of the Object
		deflectionTowersNewColor = Color.Lerp (deflectionTowersOriginalColor, highlightColor, highlighting.deflectionTowers.highlight);
		deflectionTowersNewEColor = Color.Lerp (deflectionTowersOriginalEColor, highlightColor, highlighting.deflectionTowers.highlight);
		deflectionTowers.GetComponent<Renderer> ().material.SetColor ("_Color", deflectionTowersNewColor);
		deflectionTowers.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", deflectionTowersNewEColor);

		//Drag Chains
		highlighting.dragChain.highlight = FlashStatus((int)highlighting.dragChain.status);															//Read the Highlight status of the Object
		dragChainsNewColor = Color.Lerp (dragChainsOriginalColor, highlightColor, highlighting.dragChain.highlight);
		dragChainsNewEColor = Color.Lerp (dragChainsOriginalEColor, highlightColor, highlighting.dragChain.highlight);
		for (int i = 0; i < dragChains.Length; i++) 
		{
			dragChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", dragChainsNewColor);
			dragChains [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dragChainsNewEColor);
		}

		//Drag Ropes
		highlighting.dragRope.highlight = FlashStatus((int)highlighting.dragRope.status);															//Read the Highlight status of the Object
		dragRopeNewColor = Color.Lerp (dragRopeOriginalColor, highlightColor, highlighting.dragRope.highlight);
		dragRopeNewEColor = Color.Lerp (dragRopeOriginalEColor, highlightColor, highlighting.dragRope.highlight);
		leftDragRope.GetComponent<Renderer> ().material.SetColor ("_Color", dragRopeNewColor);
		leftDragRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dragRopeNewEColor);
		rightDragRope.GetComponent<Renderer> ().material.SetColor ("_Color", dragRopeNewColor);
		rightDragRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dragRopeNewEColor);

		//Dump Block
		highlighting.dumpBlock.highlight = FlashStatus((int)highlighting.dumpBlock.status);															//Read the Highlight status of the Object
		dumpBlockNewColor = Color.Lerp (dumpBlockOriginalColor, highlightColor, highlighting.dumpBlock.highlight);
		dumpBlockNewEColor = Color.Lerp (dumpBlockOriginalEColor, highlightColor, highlighting.dumpBlock.highlight);
		dumpBlock.GetComponent<Renderer> ().material.SetColor ("_Color", dumpBlockNewColor);
		dumpBlock.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dumpBlockNewEColor);

		//Dump Chain
		highlighting.dumpChain.highlight = FlashStatus((int)highlighting.dumpChain.status);															//Read the Highlight status of the Object
		dumpChainNewColor = Color.Lerp (dumpChainOriginalColor, highlightColor, highlighting.dumpChain.highlight);
		dumpChainNewEColor = Color.Lerp (dumpChainOriginalEColor, highlightColor, highlighting.dumpChain.highlight);
		for (int i = 0; i < dumpChains.Length; i++) 
		{
			dumpChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", dumpChainNewColor);
			dumpChains [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dumpChainNewEColor);
		}

		//Dump Rope
		highlighting.dumpRope.highlight = FlashStatus((int)highlighting.dumpRope.status);															//Read the Highlight status of the Object
		dumpRopeNewColor = Color.Lerp (dumpRopeOriginalColor, highlightColor, highlighting.dumpRope.highlight);
		dumpRopeNewEColor = Color.Lerp (dumpRopeOriginalEColor, highlightColor, highlighting.dumpRope.highlight);

		dumpRope.GetComponent<Renderer> ().material.SetColor ("_Color", dumpRopeNewColor);
		dumpRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dumpRopeNewEColor);

		//Hoist Chains
		highlighting.hoistChain.highlight = FlashStatus((int)highlighting.hoistChain.status);														//Read the Highlight status of the Object
		hoistChainNewColor = Color.Lerp (hoistChainOriginalColor, highlightColor, highlighting.hoistChain.highlight);
		hoistChainNewEColor = Color.Lerp (hoistChainOriginalEColor, highlightColor, highlighting.hoistChain.highlight);

		for (int i = 0; i < hoistChains.Length; i++) 
		{
			hoistChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", hoistChainNewColor);
			hoistChains [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", hoistChainNewEColor);
		}

		//Hoist Ropes
		highlighting.hoistRope.highlight = FlashStatus((int)highlighting.hoistRope.status);															//Read the Highlight status of the Object
		hoistRopeNewColor = Color.Lerp (hoistRopeOriginalColor, highlightColor, highlighting.hoistRope.highlight);
		hoistRopeNewColor.a = fadeManager.hoistRopes.fade;
		hoistRopeNewEColor = Color.Lerp (hoistRopeOriginalEColor, highlightColor, highlighting.hoistRope.highlight);

		leftHoistRope.GetComponent<Renderer> ().material.SetColor ("_Color", hoistRopeNewColor);
		leftHoistRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", hoistRopeNewEColor);
		rightHoistRope.GetComponent<Renderer> ().material.SetColor ("_Color", hoistRopeNewColor);
		rightHoistRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", hoistRopeNewEColor);

		//Mast Chords
		highlighting.mastChords.highlight = FlashStatus((int)highlighting.mastChords.status);														//Read the Highlight status of the Object
		mastChordsNewColor = mastChordsOriginalColor;
		mastChordsNewColor.a = highlighting.mastChords.highlight;
		for (int i = 0; i < mastChords.Length; i++) 
		{
			mastChords [i].GetComponent<Renderer> ().material.SetColor ("_Color", mastChordsNewColor);
		}

		//Mast Feet
		highlighting.mastFeet.highlight = FlashStatus((int)highlighting.mastFeet.status);															//Read the Highlight status of the Object
		mastFeetNewColor = mastFeetOriginalColor;
		mastFeetNewColor.a = highlighting.mastFeet.highlight;

		for (int i = 0; i < mastFeet.Length; i++) 
		{
			mastFeet [i].GetComponent<Renderer> ().material.SetColor ("_Color", mastFeetNewColor);
		}

		//Mast Head
		highlighting.mastHead.highlight = FlashStatus((int)highlighting.mastHead.status);															//Read the Highlight status of the Object
		mastHeadNewColor = Color.Lerp (mastHeadOriginalColor, highlightColor, highlighting.mastHead.highlight);
		mastHeadNewEColor = Color.Lerp (mastHeadOriginalEColor, highlightColor, highlighting.mastHead.highlight);
		mastHead.GetComponent<Renderer> ().material.SetColor ("_Color", mastHeadNewColor);
		mastHead.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", mastHeadNewEColor);

		//Mast Lacings
		highlighting.mastLacings.highlight = FlashStatus((int)highlighting.mastLacings.status);														//Read the Highlight status of the Object
		mastLacingsNewColor = mastLacingsOriginalColor;
		mastLacingsNewColor.a = highlighting.mastLacings.highlight;

		for (int i = 0; i < mastLacings.Length; i++) 
		{
			mastLacings [i].GetComponent<Renderer> ().sharedMaterial.SetColor ("_Color", mastLacingsNewColor);
		}

		//Spreader Bar
		highlighting.spreaderBar.highlight = FlashStatus((int)highlighting.spreaderBar.status);														//Read the Highlight status of the Object
		spreaderBarNewColor = Color.Lerp (spreaderBarOriginalColor, highlightColor, highlighting.spreaderBar.highlight);
		spreaderBarNewEColor = Color.Lerp (spreaderBarOriginalEColor, highlightColor, highlighting.spreaderBar.highlight);

		spreaderBar.GetComponent<Renderer> ().material.SetColor ("_Color", spreaderBarNewColor);
		spreaderBar.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", spreaderBarNewEColor);


		//Suspension Ropes
		//Upper
		highlighting.upSusRopes.highlight = FlashStatus((int)highlighting.upSusRopes.status);														//Read the Highlight status of the Object
		upperSusRopesNewColor = Color.Lerp (upperSusRopesOriginalColor, highlightColor, highlighting.upSusRopes.highlight);
		upperSusRopesNewEColor = Color.Lerp (upperSusRopesOriginalEColor, highlightColor, highlighting.upSusRopes.highlight);
		//Intermediate
		highlighting.intSusRopes.highlight = FlashStatus((int)highlighting.intSusRopes.status);														//Read the Highlight status of the Object
		intSusRopesNewColor = Color.Lerp (intSusRopesOriginalColor, highlightColor, highlighting.intSusRopes.highlight);
		intSusRopesNewEColor = Color.Lerp (intSusRopesOriginalEColor, highlightColor, highlighting.intSusRopes.highlight);
		//Lower
		highlighting.loSusRopes.highlight = FlashStatus((int)highlighting.loSusRopes.status);														//Read the Highlight status of the Object
		lowerSusRopesNewColor = Color.Lerp (lowerSusRopesOriginalColor, highlightColor, highlighting.loSusRopes.highlight);
		lowerSusRopesNewEColor = Color.Lerp (lowerSusRopesOriginalEColor, highlightColor, highlighting.loSusRopes.highlight);

		if (gotSuspensionRopes) 
		{
			for (int i = 0; i < 4; i++) 
			{
				lowerSusRopes [i].SetActive (showHide.lowerSuspensionRopes);
				lowerSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", lowerSusRopesNewColor);
				lowerSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", lowerSusRopesNewEColor);
				intSusRopes [i].SetActive (showHide.intSuspensionRopes);
				intSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", intSusRopesNewEColor);
				intSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", intSusRopesNewColor);
				upperSusRopes [i].SetActive (showHide.upperSuspensionRopes);
				upperSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", upperSusRopesNewEColor);
				upperSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", upperSusRopesNewColor);
			}
		}

		//Trunion
		highlighting.trunion.highlight = FlashStatus((int)highlighting.trunion.status);																//Read the Highlight status of the Object
		trunionNewColor = trunionOriginalColor;
		trunionNewColor.a = highlighting.trunion.highlight;																							//For Trunion Highlights only modify the Alpha value
		for (int i = 0; i < trunion.Length; i++) 
		{
			trunion [i].GetComponent<Renderer> ().material.SetColor ("_Color", trunionNewColor);
		}

		//Center Pin
		highlighting.centerPin.highlight = FlashStatus((int)highlighting.centerPin.status);															//Read the Highlight status of the Object
		centerPinNewColor = centerPinOriginalColor;
		centerPinNewColor.a = highlighting.centerPin.highlight;
		centerPin.GetComponent<Renderer> ().material.SetColor ("_Color", centerPinNewColor);

		//Tub
		highlighting.tub.highlight = FlashStatus((int)highlighting.tub.status);																		//Read the Highlight status of the Object
		tubNewColor = Color.Lerp (tubOriginalColor, highlightColor, highlighting.tub.highlight);
		tubNewColor.a = fadeManager.tub.fade;
		tubNewEColor = Color.Lerp (tubOriginalEColor, highlightColor, highlighting.tub.highlight);
		tub.GetComponent<Renderer>().material.SetColor ("_Color", tubNewColor);
		tub.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", tubNewEColor);

		//Tub Bulkhead
		highlighting.tubBulkhead.highlight = FlashStatus((int)highlighting.tubBulkhead.status);														//Read the Highlight status of the Object
		tubBulkheadNewColor = Color.Lerp (tubBulkheadOriginalColor, highlightColor, highlighting.tubBulkhead.highlight);
		tubBulkheadNewEColor = Color.Lerp (tubBulkheadOriginalEColor, highlightColor, highlighting.tubBulkhead.highlight);
		tubBulkhead.GetComponent<Renderer>().material.SetColor ("_Color", tubBulkheadNewColor);
		tubBulkhead.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", tubBulkheadNewEColor);

		//Tub Rollers
		highlighting.tubRollers.highlight = FlashStatus((int)highlighting.tubRollers.status);														//Read the Highlight status of the Object
		tubRollersNewColor = Color.Lerp (tubRollersOriginalColor, highlightColor, highlighting.tubRollers.highlight);
		tubRollersNewEColor = Color.Lerp (tubRollersOriginalEColor, highlightColor, highlighting.tubRollers.highlight);
		tubRollersNewColor.a = fadeManager.rollerCircle.fade;
		tubRollers.GetComponent<Renderer>().material.SetColor ("_Color", tubRollersNewColor);
		tubRollers.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", tubRollersNewEColor);

		//Swing Rack
		highlighting.swingRack.highlight = FlashStatus((int)highlighting.swingRack.status);															//Read the Highlight Status of the Object
		swingRackNewColor = Color.Lerp (swingRackOriginalColor, highlightColor, highlighting.swingRack.highlight);
		swingRackNewEColor = Color.Lerp (swingRackOriginalEColor, highlightColor, highlighting.swingRack.highlight);
		swingRack.GetComponent<Renderer>().material.SetColor ("_Color", swingRackNewColor);
		swingRack.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", swingRackNewEColor);

		//Upper Roller Rail
		highlighting.rollerRailUpper.highlight = FlashStatus((int)highlighting.rollerRailUpper.status);												//Read the Highlight status of the Object
		upperRollerRailNewColor = Color.Lerp (upperRollerRailOriginalColor, highlightColor, highlighting.rollerRailUpper.highlight);
		upperRollerRailNewEColor = Color.Lerp (upperRollerRailOriginalEColor, highlightColor, highlighting.rollerRailUpper.highlight);
		upperRollerRail.GetComponent<Renderer>().material.SetColor ("_Color", upperRollerRailNewColor);
		upperRollerRail.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", upperRollerRailNewEColor);

		//Lower Roller Rail
		highlighting.rollerRailLower.highlight = FlashStatus((int)highlighting.rollerRailLower.status);												//Read the Highlight status of the Object
		lowerRollerRailNewColor = Color.Lerp (lowerRollerRailOriginalColor, highlightColor, highlighting.rollerRailLower.highlight);
		lowerRollerRailNewEColor = Color.Lerp (lowerRollerRailOriginalEColor, highlightColor, highlighting.rollerRailLower.highlight);
		lowerRollerRail.GetComponent<Renderer>().material.SetColor ("_Color", lowerRollerRailNewColor);
		lowerRollerRail.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", lowerRollerRailNewEColor);

		//Swing Pinion
		highlighting.swingPinions.highlight = FlashStatus((int)highlighting.swingPinions.status);													//Read the Highlight status of the Object
		swingPinionNewColor = Color.Lerp (swingPinionOriginalColor, highlightColor, highlighting.swingPinions.highlight);
		swingPinionNewEColor = Color.Lerp (swingPinionOriginalEColor, highlightColor, highlighting.swingPinions.highlight);
		for (int i = 0; i < swingPinions.Length; i++) 
		{
			swingPinions [i].GetComponent<Renderer> ().material.SetColor ("_Color", swingPinionNewColor);
			swingPinions [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", swingPinionNewEColor);
		}

		//Lifting Hooks
		highlighting.liftingHooks.highlight = FlashStatus( (int)highlighting.liftingHooks.status);													//Read the Highlight status of the Object
		liftingHooksNewColor = Color.Lerp(liftingHooksOriginalColor, highlightColor, highlighting.liftingHooks.highlight);
		liftingHooksNewEColor = Color.Lerp (liftingHooksOriginalEColor, highlightColor, highlighting.liftingHooks.highlight);
		for (int i = 0; i < liftingHooks.Length; i++) 
		{
			liftingHooks [i].GetComponent<Renderer> ().material.SetColor ("_Color", liftingHooksNewColor);
			liftingHooks [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", liftingHooksNewEColor);
		}

		//A Frame Legs
		highlighting.aFrameLegs.highlight = FlashStatus( (int)highlighting.aFrameLegs.status);														//Read the Highlight status of the Object
		aFrameLegsNewColor = Color.Lerp(aFrameLegsOriginalColor, highlightColor, highlighting.aFrameLegs.highlight);
		aFrameLegsNewEColor = Color.Lerp (aFrameLegsOriginalEColor, highlightColor, highlighting.aFrameLegs.highlight);
		aFrameLegs.GetComponent<Renderer> ().material.SetColor ("_Color", aFrameLegsNewColor);
		aFrameLegs.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", aFrameLegsNewEColor);

		//A Frame Lugs
		highlighting.aFrameLugs.highlight = FlashStatus((int)highlighting.aFrameLugs.status);
		aFrameLugsNewColor = Color.Lerp (aFrameLugsOriginalColor, highlightColor, highlighting.aFrameLugs.highlight);
		aFrameLugsNewEColor = Color.Lerp (aFrameLugsOriginalEColor, highlightColor, highlighting.aFrameLugs.highlight);
		aFrameLugs.GetComponent<Renderer> ().material.SetColor ("_Color", aFrameLugsNewColor);
		aFrameLugs.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", aFrameLugsNewEColor);

		//Rev Frame
		highlighting.revolvingFrame.highlight = FlashStatus((int)highlighting.revolvingFrame.status);
		revFrameNewColor = Color.Lerp (revFrameOriginalColor, highlightColor, highlighting.revolvingFrame.highlight);
		revFrameNewEColor = Color.Lerp (revFrameOriginalEColor, highlightColor, highlighting.revolvingFrame.highlight);
		revFrameNewColor.a = fadeManager.revFrame.fade;
		revFrame.GetComponent<Renderer> ().material.SetColor ("_Color", revFrameNewColor);
		revFrame.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", revFrameNewEColor);

		//Rev Frame Floor
		highlighting.revolvingFrameFloor.highlight = FlashStatus((int)highlighting.revolvingFrameFloor.status);
		revFrameFloorNewColor = Color.Lerp (revFrameFloorOriginalColor, highlightColor, highlighting.revolvingFrameFloor.highlight);
		revFrameFloorNewEColor = Color.Lerp (revFrameFloorOriginalEColor, highlightColor, highlighting.revolvingFrameFloor.highlight);
		revFrameFloor.GetComponent<Renderer> ().material.SetColor ("_Color", revFrameFloorNewColor);
		revFrameFloor.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", revFrameFloorNewEColor);


	}//Update End

//******************************************************************************************************************************
	//Function Section
	float FlashStatus (int status)
	{
		//This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
		//The returned value should be sent to highlighting."ComponentName".highlight
		//THis replaces the following logic
		//**********Sample Code Start*******************
		//		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
		//		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
		//		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
		//**********Sample Code End********************
		float finalStatus;
		switch (status) 
		{
		case 2:
			finalStatus = 1;																														//If Status is "Solid"
			break;
		case 1:
			finalStatus = colorLerp;																												//If Status is "Flash"
			break;
		case 0:
			finalStatus = 0;																														//If Status is "Off"
			break;
		default:
			finalStatus = 0;																														//Default case
			break;
		}//Switch End
		return finalStatus;
	}//FlashStatus End

	//This function allows the gradual fading out of components. 
	float FadeController(bool enable, float fadeValue)
	{
		if (enable == true && fadeValue > 0.001) 
		{
			fadeValue = Mathf.Lerp (fadeValue, 0, Time.deltaTime);
		}
		if (enable == false && fadeValue < 0.99) 
		{
			fadeValue = Mathf.Lerp (fadeValue, 1, Time.deltaTime);
		}
		return fadeValue;
	}//FadeController End

    float FadeControllerForWindows(bool enable, float fadeValue, float fadeValueCap)
    {
        if (enable == true && fadeValue > 0.001)
        {
            fadeValue = Mathf.Lerp(fadeValue, 0, Time.deltaTime);
        }
        if (enable == false && fadeValue < fadeValueCap)
        {
            fadeValue = Mathf.Lerp(fadeValue, fadeValueCap, Time.deltaTime);
        }
        return fadeValue;
    }//FadeController End

    //   void ShaderChanger(Material[] materials, bool enable, float fadeValue)
    //{
    //	if (enable == true) 
    //	{
    //		for (int i = 0; i < materials.Length; i++) 
    //		{
    //			materials [i].EnableKeyword ("_ALPHABLEND_ON");
    //			materials [i].DisableKeyword ("_ALPHATEST_ON");
    //			materials [i].DisableKeyword ("_ALPHAPREMULTIPLY_ON");
    //			materials [i].SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
    //			materials [i].SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
    //			materials [i].SetFloat ("_Mode", 2);
    //			materials [i].SetInt ("_ZWrite", 0);
    //			materials [i].renderQueue = 3000;
    //		}//For End
    //	}//If End

    //	if (enable == false && fadeValue > 0.9) 
    //	{
    //		for (int i = 0; i < materials.Length; i++) 
    //		{
    //			materials [i].DisableKeyword ("_ALPHABLEND_ON");
    //			materials [i].DisableKeyword ("_ALPHATEST_ON");
    //			materials [i].DisableKeyword ("_ALPHAPREMULTIPLY_ON");
    //			materials [i].SetInt ("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
    //			materials [i].SetInt ("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
    //			materials [i].SetFloat ("_Mode", 0);
    //			materials [i].SetInt ("_ZWrite", 1);
    //			materials [i].renderQueue = -1;
    //		}//For End
    //	}//If End
    //}//ShaderChanger End
    void ShaderChanger(Material[] materials, bool enable, float fadeValue)
    {
        if (enable == true)
        {
            for (int i = 0; i < materials.Length; i++)
            {
                materials[i].EnableKeyword("_ALPHABLEND_ON");
                materials[i].DisableKeyword("_ALPHATEST_ON");
                materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");

                materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

                //Debug.Log("Materials name is " + materials[i].name);

                if (String.Equals(materials[i].name, "MOSAIC_Windows (Instance)"))
                {
                    //materials[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].SetFloat("_Mode", 3);
                    //Debug.Log("Mode for mosaic windows set");
                }
                else
                {
                    //materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].SetFloat("_Mode", 2);
                }

                materials[i].SetInt("_ZWrite", 0);
                materials[i].renderQueue = 3000;
            }//For End
        }//If End

        if (enable == false && fadeValue > 0.9)
        {
            for (int i = 0; i < materials.Length; i++)
            {
                materials[i].DisableKeyword("_ALPHABLEND_ON");
                materials[i].DisableKeyword("_ALPHATEST_ON");



                //Debug.Log("Materials name after disablin is " + materials[i].name);

                if (String.Equals(materials[i].name, "MOSAIC_Windows (Instance)"))
                {
                    materials[i].SetFloat("_Mode", 3);
                    //Debug.Log("After disabling, mode for mosaic windows set");

                    materials[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");

                    materials[i].renderQueue = 3000;
                    materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

                    //Debug.Log("windows color is " + materials[i].color.ToString());
                }
                else
                {
                    materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    materials[i].SetFloat("_Mode", 0);
                    materials[i].renderQueue = -1;
                }

                materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);

                materials[i].SetInt("_ZWrite", 1);

            }//For End
        }//If End
    }//ShaderChanger End
}//Monobehaviour End
