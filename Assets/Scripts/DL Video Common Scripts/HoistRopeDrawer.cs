﻿using UnityEngine;
using System.Collections;

public class HoistRopeDrawer : MonoBehaviour {
	public bool selfUpdate;
	public Transform[] sheaves;
	public float[] radii;
	public int sheaveInterpolate;
	public splineRendererArray spline;

	public Vector3 between, cross, betweenBack, crossBack;

	// Use this for initialization
	void Start () {
		spline.points = new Vector3[2+(sheaves.Length-2)*sheaveInterpolate];
	}
	
	// Update is called once per frame
	void Update () {
		if (selfUpdate) {
			UpdateRopes();
		}
	}

	public void UpdateRopes(){
		spline.points [0] = sheaves [0].position;
		for (int i = 1; i < sheaves.Length-1; i++) {
			between = sheaves [i].position - sheaves [i - 1].position;
			cross = Vector3.Cross (between, sheaves [i].right).normalized*radii[i];
			betweenBack = sheaves [i].position - sheaves [i + 1].position;
			crossBack = Vector3.Cross (betweenBack, sheaves [i].right).normalized*radii[i];
			spline.points [2*i-1] = sheaves [i].position+cross;
			spline.points [2*i] = sheaves [i].position-crossBack;
		}
		spline.points [spline.points.Length-1] = sheaves [sheaves.Length-1].position;
		spline.updateMesh ();
	}
}
