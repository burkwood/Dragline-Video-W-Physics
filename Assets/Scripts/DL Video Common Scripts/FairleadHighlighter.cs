﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//This script is used to highlight the fairleads.
//Later this should be moved into the appropriate DL_SFX scripts
public class FairleadHighlighter : MonoBehaviour
{
    //******************************************************************************************************************************
    //Public Variables
    public float flashDuration = 0.8f;
    public float colorLerp;
    public Highlight fairleadStatus;
    //******************************************************************************************************************************
    //Game Objects Section
    private GameObject[] fairleads;
    //******************************************************************************************************************************
    //Materials Section
    private Material[] fairleadMaterials = new Material[2];
    //******************************************************************************************************************************
    //Colors Section
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    private Color fairleadOriginalColor;
    private Color fairleadOriginalEColor;
    private Color fairleadNewColor;
    private Color fairleadNewEColor;

    // Use this for initialization
    void Start ()
    {
        //******************************************************************************************************************************
        //Define Game Objects
        fairleads = GameObject.FindGameObjectsWithTag("Fairlead");
        //******************************************************************************************************************************
        //Define Materials
        for (int i = 0; i < fairleads.Length; i++)
        {
            fairleadMaterials[i] = fairleads[i].GetComponent<Renderer>().material;
        }
        //******************************************************************************************************************************
        //Define Colors
        fairleadOriginalColor = fairleadMaterials[0].color;
        fairleadOriginalEColor = fairleadMaterials[0].GetColor("_EmissionColor");
    }

    // Update is called once per frame
    void Update ()
    {
        colorLerp = Mathf.PingPong(Time.time, flashDuration) / flashDuration;
        //******************************************************************************************************************************
        //Highlight Section
        fairleadStatus.highlight = FlashStatus((int)fairleadStatus.status);
        fairleadNewColor = Color.Lerp(fairleadOriginalColor, highlightColor, fairleadStatus.highlight);
        fairleadNewEColor = Color.Lerp(fairleadOriginalEColor, highlightColor, fairleadStatus.highlight);
        for (int i = 0; i < fairleads.Length; i++)
        {
            fairleadMaterials[i].SetColor("_Color", fairleadNewColor);
            fairleadMaterials[i].SetColor("_EmissionColor", fairleadNewEColor);
        }
    }

    //******************************************************************************************************************************
    //Function Section
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
}
