﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

public enum Status {Off, Flash, Solid};

[Serializable]
public class ShowHide
{
	public bool aFrameLegs;
	public bool aFrameLugs;
	public bool boomChords;
	public bool boomLacings;
	public bool boomLights;
	public bool boomPlatform;
	public bool hoistRopes;
	public bool housePlatform;
	public bool houseLights;
	public bool houseCab;
	public bool houseBody;
	public bool liftingHooks;
	public bool upperRollerRail;
	public bool rollerCircle;
	public bool revFrame;
	public bool revFrameFloor;
	public bool upperSuspensionRopes;
	public bool intSuspensionRopes;
	public bool lowerSuspensionRopes;
	public bool swingPinions;
	public bool tub;

    public bool gantry; //This is for the Marion machines
}

[Serializable]
public class Highlight
{
	[Range(0.0f,1.0f)]
	public float highlight;
	public Status status;
}

//Currently Not Used
[Serializable]
public class Fader
{
	[Range(0.0f,1.0f)]
	public float fade;
	public bool enabled;
}

[Serializable]
public class BucyrusHighlighting
{
	public Highlight aFrameTopBar;
	public Highlight aFrameLegs;
	public Highlight aFrameLugs;
	public Highlight boomChords;
	public Highlight boomFeet;
	public Highlight boomLacings;
	public Highlight boomSheave;
	public Highlight boomTip;
	public Highlight bucket;
	public Highlight bucketTeeth;
	public Highlight centerPin;
	public Highlight clevis;
	public Highlight deflectionTowers;
	public Highlight dragChain;
	public Highlight dragRope;
	public Highlight dumpBlock;
	public Highlight dumpChain;
	public Highlight dumpRope;
	public Highlight hoistChain;
	public Highlight hoistRope;
	public Highlight liftingHooks;
	public Highlight mastChords;
	public Highlight mastFeet;
	public Highlight mastHead;
	public Highlight mastLacings;
	public Highlight revolvingFrame;
	public Highlight revolvingFrameFloor;
	public Highlight rollerRailLower;
	public Highlight rollerRailUpper;
	public Highlight spreaderBar;
	public Highlight swingPinions;
	public Highlight swingRack;
	public Highlight tubRollers;
	public Highlight tub;
	public Highlight tubBulkhead;
	public Highlight upSusRopes;
	public Highlight intSusRopes;
	public Highlight loSusRopes;
	public Highlight trunion;
}


[Serializable]
public class BucycrusFadeManager
{
	public Fader hoistRopes;
	public Fader houseBody;
	public Fader houseCab;

    public Fader houseCabWindows;

	public Fader houseLights;
	public Fader housePlatform;
	public Fader revFrame;
	public Fader rollerCircle;
	public Fader tub;
}

[Serializable]
public class MarionFadeManager
{
    public Fader aFrame;
    public Fader boomTip;
    public Fader dragRopes;
    public Fader gantry;
    public Fader hoistRopes;  
    public Fader house;
    public Fader linkPlate;
    //public Fader cab;
    public Fader revFrame;
    public Fader rollerCircle;
    public Fader tub;
}

[Serializable]
public class MarionShowHide
{
    //	public bool aFrameLegs;
    //	public bool aFrameLugs;

    //public bool boomFeet;

	public bool boomChords;
	public bool boomLacings;
	public bool boomLights;
	public bool boomPlatform;
    public bool mast;
	public bool hoistRopes;
	//public bool housePlatform;
//	public bool houseLights;
	//public bool houseCab;
	public bool houseBody;
	public bool liftingHooks;
	public bool upperRollerRail;
	public bool rollerCircle;
	public bool revFrame;
	public bool revFrameFloor;
	public bool upperSuspensionRopes;
	public bool intSuspensionRopes;
	public bool lowerSuspensionRopes;
	public bool swingPinions;
    public bool swingRack;

	public bool tub;

    public bool aFrame;
    public bool aFrameLugs;
    public bool gantry;
    public bool bucks;
}

[Serializable]
public class MarionHighlighting
{
    //	public Highlight aFrameTopBar;
    //	public Highlight aFrameLegs;
    //	public Highlight aFrameLugs;

    public Highlight aFrame;
    public Highlight aFrameLugs;
	public Highlight boomChords;
	public Highlight boomFeet;
	public Highlight boomLacings;
	public Highlight boomSheave;
	public Highlight boomTip;
	public Highlight bucket;
	public Highlight bucketTeeth;
	public Highlight centerPin;
	public Highlight clevis;
//	public Highlight deflectionTowers;
	public Highlight dragChain;
	public Highlight dragRope;
	public Highlight dumpBlock;
	public Highlight dumpChain;
	public Highlight dumpRope;
	public Highlight hoistChain;
	public Highlight hoistRope;
	public Highlight liftingHooks;

    //public Highlight mast;
    
    public Highlight mastChords;
    public Highlight mastFeet;
    public Highlight mastHead;
    public Highlight mastLacings;

    public Highlight revolvingFrame;
	public Highlight revolvingFrameFloor;
	public Highlight rollerRailLower;
	public Highlight rollerRailUpper;
	public Highlight spreaderBar;
	public Highlight swingPinions;
	public Highlight swingRack;
	public Highlight tubRollers;
	public Highlight tub;
	public Highlight tubBulkhead;
	public Highlight upSusRopes;
	public Highlight intSusRopes;
	public Highlight loSusRopes;
	public Highlight trunion;

    public Highlight gantry;
 
}



public class PublicClasses : MonoBehaviour 
{

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {
		
	}
}
