﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

//This script is incharge of all the Special Effects for the Marion Dragline.
//In this script the logic for making components/ parts of the Dragline Appear/Disappear as well as highlighting are contained.
//This script should be attached to the Game Controller in any scene containing a Bucyrus Dragline
//The intention is that when a SFX is needed, the Game Controller script will change the appropriate public variable in this script to achieve the desired effect.
//THis scripts refrences Classes defined in the "PublicClasses" script
public class Marion_DL_SFX : MonoBehaviour 
{
	public float flashDuration = 0.8f;
	public float colorLerp;
	public MarionShowHide showHide;
	public MarionHighlighting highlighting;
    public MarionFadeManager fadeManager;

    //******************************************************************************************************************************
    //Game Objects Section
    #region Game-Objects

    private GameObject aFrame;
    private GameObject[] aFrameLugs;
    private GameObject boomChords;
	private GameObject boomLacings;
	private GameObject boomLights;
	private GameObject[] boomPlatform;
	private GameObject boomTip;
	private GameObject boomSheave;
	private GameObject[] bucket;
	private GameObject bucketTeeth;
    private GameObject[] boomFeet;
    private GameObject buckParent;

    private GameObject centerPin;
    private GameObject[] clevis;

    private GameObject[] dragChains;
    private GameObject[] leftDragRopeParents;
    private GameObject[] leftDragRopes = new GameObject[2];
    private GameObject[] rightDragRopeParents;
    private GameObject[] rightDragRopes = new GameObject[2];
    private GameObject dumpBlock;
    private GameObject[] dumpChain;
    private GameObject dumpRopeParent;
    private GameObject[] dumpRopes = new GameObject[1];
    private GameObject dumpRope;

    private GameObject gantry;

    private GameObject[] hoistChains;
	private GameObject housePlatform;
    private GameObject[] houseCab;
	private GameObject leftHoistRopeParent;
	private GameObject[] leftHoistRopes = new GameObject[1];
	private GameObject leftHoistRope;
	private GameObject rightHoistRopeParent;
	private GameObject[] rightHoistRopes = new GameObject[1];
	private GameObject rightHoistRope;

	private GameObject[] liftingHooks;

    private GameObject mast;
    private GameObject[] mastFeet;

    public GameObject[] straightRopes;																											//Needs to be public to avoid error
	private GameObject[] upperSusRopes = new GameObject[4];
	private GameObject[] intSusRopes = new GameObject[2];
    private GameObject[] lowSusRopes;
    private GameObject revFrameFloor;
    private GameObject revFrame;
    private GameObject lowerRollerRail;
    private GameObject upperRollerRail;
    private GameObject spreaderBar;
    private GameObject[] swingPinions;
    private GameObject swingRack;
    private GameObject tub;
    private GameObject tubBulkhead;
    private GameObject tubRollers;
    private GameObject[] trunion;
    #endregion

    //Materials
    private Material[] boomChordsMaterial;
    private Material[] boomLacingsMaterial;
    private Material[] boomTipMaterial;
    private Material[] hoistRopeMaterial = new Material[2];
    private Material[] dragRopeMaterial = new Material[4];
    //private Material[] houseBodyMaterial = new Material[3];
    private Material[] houseCabMaterial = new Material[2];
    //private Material[] housePlatformMaterial = new Material[3];
    private Material[] housePlatformMaterial;
    private Material[] revFrameMaterial = new Material[1];
    private Material[] tubRollersMaterial = new Material[1];
    private Material[] tubMaterial = new Material[1];

    private Material[] mastMaterial;    
    private Material[] mastFeetMaterial;
    
    private Material[] aFrameMaterial;
    private Material[] gantryMaterial;
    private Material[] linkPlateMaterial;

    //******************************************************************************************************************************
    //Colors Section
    private Color highlightColor = new Color (212/255f, 208/255f, 15/255f);
	//Boom Chords
	private Color boomChordsOriginalColor;
	private Color boomChordsOriginalEColor;
	private Color boomChordsNewColor;
	private Color boomChordsNewEColor;
	//Boom Lacings
	private Color boomLacingsOriginalColor;
	private Color boomLacingsOriginalEColor;
	private Color boomLacingsNewColor;
	private Color boomLacingsNewEColor;
    //Boom Sheave
    private Color boomSheaveOriginalColor;
	private Color boomSheaveOriginalEColor;
	private Color boomSheaveNewColor;
	private Color boomSheaveNewEColor;
	//Boom Tip
	private Color boomTipOriginalColor;
	private Color boomTipOriginalEColor;
	private Color boomTipNewColor;
	private Color boomTipNewEColor;
	//Bucket
	private Color bucketOriginalColor;
	private Color bucketOriginalEColor;
	private Color bucketNewColor;
	private Color bucketNewEColor;
	//Bucket Teeth
	private Color bucketTeethOriginalColor;
	private Color bucketTeethOriginalEColor;
	private Color bucketTeethNewColor;
	private Color bucketTeethNewEColor;
	//Center Pin
	private Color centerPinOriginalColor;
	private Color centerPinNewColor;
	//Clevis
	private Color clevisOriginalColor;
	private Color clevisOriginalEColor;
	private Color clevisNewColor;
	private Color clevisNewEColor;
    //Spreader Bar
    private Color spreaderBarOriginalColor;
    private Color spreaderBarOriginalEColor;
    private Color spreaderBarNewColor;
    private Color spreaderBarNewEColor;
    //Drag Chains
    private Color dragChainsOriginalColor;
	private Color dragChainsOriginalEColor;
	private Color dragChainsNewColor;
	private Color dragChainsNewEColor;
	//Dump Block
	private Color dumpBlockOriginalColor;
	private Color dumpBlockOriginalEColor;
	private Color dumpBlockNewColor;
	private Color dumpBlockNewEColor;
	//Drag Ropes
	private Color dragRopesOriginalColor;
	private Color dragRopesOriginalEColor;
	private Color dragRopesNewColor;
	private Color dragRopesNewEColor;
	//Dump Chain
	private Color dumpChainOriginalColor;
	private Color dumpChainOriginalEColor;
	private Color dumpChainNewColor;
	private Color dumpChainNewEColor;
	//Dump Rope
	private Color dumpRopeOriginalColor;
	private Color dumpRopeOriginalEColor;
	private Color dumpRopeNewColor;
	private Color dumpRopeNewEColor;
	//Hoist Ropes
	private Color hoistRopeOriginalColor;
	private Color hoistRopeOriginalEColor;
	private Color hoistRopeNewColor;
	private Color hoistRopeNewEColor;
	//Hoist Chains
	private Color hoistChainOriginalColor;
	private Color hoistChainOriginalEColor;
	private Color hoistChainNewColor;
	private Color hoistChainNewEColor;
    //House Platform
    private Color housePlatformOriginalColor;
    private Color housePlatformOriginalEColor;
    private Color housePlatformNewColor;
    private Color housePlatformNewEColor;
    //House Cab
    private Color[] houseCabOriginalColor;
    //private Color[] houseCabOriginalEColor;
    private Color[] houseCabNewColor;
    //private Color[] houseCabNewEColor;
    //Mast
    private Color[] mastOriginalColor = new Color[4];
    private Color[] mastOriginalEColor = new Color[4];
    private Color[] mastNewColor = new Color[4];
    private Color[] mastNewEColor = new Color[4];
    //Mast Feet
    private Color mastFeetOriginalColor;
    private Color mastFeetNewColor;

    //Lifting Hooks
    private Color liftingHooksOriginalColor;
	private Color liftingHooksOriginalEColor;
	private Color liftingHooksNewColor;
	private Color liftingHooksNewEColor;
    //Swing Pinions
    private Color swingPinionOriginalColor;
    private Color swingPinionOriginalEColor;
    private Color swingPinionNewColor;
    private Color swingPinionNewEColor;

    //Swing Rack
    private Color swingRackOriginalColor;
    private Color swingRackOriginalEColor;
    private Color swingRackNewColor;
    private Color swingRackNewEColor;

    //Boom Feet
    private Color boomFeetOriginalColor;
    private Color boomFeetOriginalEColor;
    private Color boomFeetNewColor;
    private Color boomFeetNewEColor;

    //Rev Frame Floor
    private Color revFrameFloorOriginalColor;
	private Color revFrameFloorOriginalEColor;
	private Color revFrameFloorNewColor;
	private Color revFrameFloorNewEColor;
    //Rev Frame 
    private Color revFrameOriginalColor;
    private Color revFrameOriginalEColor;
    private Color revFrameNewColor;
    private Color revFrameNewEColor;
    //Suspension Ropes
    private Color upperSusRopesOriginalColor;
	private Color upperSusRopesOriginalEColor;
	private Color upperSusRopesNewColor;
	private Color upperSusRopesNewEColor;
	private Color intSusRopesOriginalColor;
	private Color intSusRopesOriginalEColor;
	private Color intSusRopesNewColor;
	private Color intSusRopesNewEColor;
    private Color lowSusRopesOriginalColor;
    private Color lowSusRopesOriginalEColor;
    private Color lowSusRopesNewColor;
    private Color lowSusRopesNewEColor;
	//Trunion
	private Color trunionOriginalColor;
	private Color trunionNewColor;
    //AFrame
    private Color aFrameOriginalColor;
    private Color aFrameOriginalEColor;
    private Color aFrameNewColor;
    private Color aFrameNewEColor;
    //AFrame Lugs
    private Color aFrameLugsOriginalColor;
    private Color aFrameLugsOriginalEColor;
    private Color aFrameLugsNewColor;
    private Color aFrameLugsNewEColor;
    //Gantry
    private Color gantryOriginalColor;
    private Color gantryOriginalEColor;
    private Color gantryNewColor;
    private Color gantryNewEColor;

    private Color tubOriginalColor;
    private Color tubOriginalEColor;
    private Color tubNewColor;
    private Color tubNewEColor;

    private Color tubBulkheadOriginalColor;
    private Color tubBulkheadOriginalEColor;
    private Color tubBulkheadNewColor;
    private Color tubBulkheadNewEColor;

    private Color tubRollersOriginalColor;
    private Color tubRollersOriginalEColor;
    private Color tubRollersNewColor;
    private Color tubRollersNewEColor;

    //Roller Rails
    private Color lowerRollerRailOriginalColor;
    private Color lowerRollerRailOriginalEColor;
    private Color lowerRollerRailNewColor;
    private Color lowerRollerRailNewEColor;
    private Color upperRollerRailOriginalColor;
    private Color upperRollerRailOriginalEColor;
    private Color upperRollerRailNewColor;
    private Color upperRollerRailNewEColor;


    //******************************************************************************************************************************
    //Bools and Floats Section
    private bool gotSuspensionRopes = false;
	private bool leftDragRopeFound = false;
	private bool rightDragRopeFound = false;
	private bool dumpRopeFound = false;
	private bool leftHoistRopeFound = false;
	private bool rightHoistRopeFound = false;

	private float leftDragRopeChildCount;
	private float rightDragRopeChildCount;
	private float dumpRopeChildCount;
	private float leftHoistRopeChildCount;
	private float rightHoistRopeChildCount;

	private int leftDragCount;
	private int rightDragCount;
	private int dumpRopeCount;
	private int leftHoistCount;
	private int rightHoistCount;

    private Color tempColor;
    //private Material tempMaterial;

    void Start () 																																//Run at startup
	{
		//******************************************************************************************************************************
		//Define Game Objects
		boomChords = GameObject.FindWithTag ("BoomChords");
		boomLacings = GameObject.FindWithTag ("BoomLacings");
		boomLights = GameObject.FindWithTag ("BoomLights");
		boomPlatform = GameObject.FindGameObjectsWithTag ("BoomPlatform");
		housePlatform = GameObject.FindWithTag ("HousePlatform");
        houseCab = GameObject.FindGameObjectsWithTag("HouseCab");
		leftHoistRopeParent = GameObject.FindWithTag ("LeftHoistRopeParent");
		rightHoistRopeParent = GameObject.FindWithTag ("RightHoistRopeParent");
		boomSheave = GameObject.FindWithTag ("BoomSheave");
		boomTip = GameObject.FindWithTag ("BoomTip");
		bucket = GameObject.FindGameObjectsWithTag ("Bucket");
		bucketTeeth = GameObject.FindWithTag ("BucketTeeth");
        spreaderBar = GameObject.FindWithTag("SpreaderBar");
        centerPin = GameObject.FindWithTag ("CenterPinHighlight");
		clevis = GameObject.FindGameObjectsWithTag ("Clevis");
		dragChains = GameObject.FindGameObjectsWithTag ("DragChain");
		leftDragRopeParents = GameObject.FindGameObjectsWithTag ("LeftDragRopeParent");
		rightDragRopeParents = GameObject.FindGameObjectsWithTag ("RightDragRopeParent");
		dumpBlock = GameObject.FindWithTag ("DumpBlock");
		dumpChain = GameObject.FindGameObjectsWithTag ("DumpChain");
		dumpRopeParent = GameObject.FindWithTag ("DumpRopeParent");
		hoistChains = GameObject.FindGameObjectsWithTag ("HoistChain");
        lowSusRopes = GameObject.FindGameObjectsWithTag("LinkPlate");
        boomFeet = GameObject.FindGameObjectsWithTag("BoomFeet");

        swingPinions = GameObject.FindGameObjectsWithTag("SwingPinion");
        swingRack = GameObject.FindWithTag("SwingRack");
        mast = GameObject.FindGameObjectWithTag("Mast");
        mastFeet = GameObject.FindGameObjectsWithTag("MastFeetHighlight");

        revFrame = GameObject.FindWithTag("RevFrame");
        revFrameFloor = GameObject.FindWithTag ("RevFrameFloor");

        tub = GameObject.FindWithTag("Tub");
        tubRollers = GameObject.FindWithTag("TubRollers");
        tubBulkhead = GameObject.FindWithTag("TubBulkhead");

        lowerRollerRail = GameObject.FindWithTag("LowerRollerRail");
        upperRollerRail = GameObject.FindWithTag("UpperRollerRail");

        liftingHooks = GameObject.FindGameObjectsWithTag("LiftingHook");

        trunion = GameObject.FindGameObjectsWithTag ("TrunionHighlight");
        aFrame = GameObject.FindGameObjectWithTag("AFrameLegs");
        aFrameLugs = GameObject.FindGameObjectsWithTag("AFrameLugs");
        gantry = GameObject.FindGameObjectWithTag("Gantry");

        buckParent = GameObject.FindGameObjectWithTag("BuckParent");
        boomTipMaterial = new Material[1];
        boomTipMaterial[0] = boomTip.GetComponent<Renderer>().material;
        boomChordsMaterial = boomChords.GetComponent<Renderer>().materials;
        boomLacingsMaterial = boomLacings.GetComponent<Renderer>().materials;        
        housePlatformMaterial = housePlatform.GetComponent<Renderer>().materials;
        houseCabMaterial = new Material[houseCab.Length];
        for (int i = 0; i < houseCabMaterial.Length; i++)
        {
            houseCabMaterial[i] = houseCab[i].GetComponent<Renderer>().material;
        }
        revFrameMaterial[0] = revFrame.GetComponent<Renderer>().material;
        tubRollersMaterial[0] = tubRollers.GetComponent<Renderer>().material;
        tubMaterial[0] = tub.GetComponent<Renderer>().material;
        aFrameMaterial = aFrame.GetComponent<Renderer>().materials;
        gantryMaterial = gantry.GetComponent<Renderer>().materials;
        mastMaterial = mast.GetComponent<Renderer>().materials;
        linkPlateMaterial = new Material[lowSusRopes.Length];
        for (int i = 0; i < linkPlateMaterial.Length; i++)
        {
            linkPlateMaterial[i] = lowSusRopes[i].GetComponent<Renderer>().material;
        }
                

        //******************************************************************************************************************************
        //Define Colors
        boomChordsOriginalColor = boomChords.GetComponent<Renderer>().material.color;
		boomChordsOriginalEColor = boomChords.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		boomLacingsOriginalColor = boomLacings.GetComponent<Renderer> ().material.color;
		boomLacingsOriginalEColor = boomLacings.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		boomSheaveOriginalColor = boomSheave.GetComponent<Renderer> ().material.color;
		boomSheaveOriginalEColor = boomSheave.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		boomTipOriginalColor = boomTip.GetComponent<Renderer> ().material.color;
		boomTipOriginalEColor = boomTip.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		bucketOriginalColor = bucket [0].GetComponent<Renderer> ().material.color;
		bucketOriginalEColor = bucket [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		bucketTeethOriginalColor = bucketTeeth.GetComponent<Renderer> ().material.color;
		bucketTeethOriginalEColor = bucketTeeth.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		clevisOriginalColor = clevis [0].GetComponent<Renderer> ().material.color;
		clevisOriginalEColor = clevis [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		dragChainsOriginalColor = dragChains [0].GetComponent<Renderer> ().material.color;
		dragChainsOriginalEColor = dragChains [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		dumpBlockOriginalColor = dumpBlock.GetComponent<Renderer> ().material.color;
		dumpBlockOriginalEColor = dumpBlock.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		dumpChainOriginalColor = dumpChain [0].GetComponent<Renderer> ().material.color;
		dumpChainOriginalEColor = dumpChain [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
		hoistChainOriginalColor = hoistChains [0].GetComponent<Renderer> ().material.color;
		hoistChainOriginalEColor = hoistChains [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
        revFrameOriginalColor = revFrame.GetComponent<Renderer>().material.color;
        revFrameOriginalEColor = revFrame.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        revFrameFloorOriginalColor = revFrameFloor.GetComponent<Renderer> ().material.color;
		revFrameFloorOriginalEColor = revFrameFloor.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
        lowSusRopesOriginalColor = lowSusRopes[0].GetComponent<Renderer>().material.color;
        lowSusRopesOriginalEColor = lowSusRopes[0].GetComponent<Renderer>().material.GetColor("_EmissionColor");
        housePlatformOriginalColor = housePlatform.GetComponent<Renderer>().material.color;
        houseCabOriginalColor = new Color[houseCab.Length];
        //houseCabOriginalEColor = new Color[houseCab.Length];
        houseCabNewColor = new Color[houseCab.Length];
        //houseCabNewEColor = new Color[houseCab.Length];
        for (int i = 0; i < houseCab.Length; i++)
        {
            houseCabOriginalColor[i] = houseCabMaterial[i].color;
            //houseCabOriginalEColor[i] = houseCabMaterial[i].GetColor("_EmissionColor");
        }


        swingPinionOriginalColor = swingPinions[0].GetComponent<Renderer>().material.color;
        swingPinionOriginalEColor = swingPinions[0].GetComponent<Renderer>().material.GetColor("_EmissionColor");
        swingRackOriginalColor = swingRack.GetComponent<Renderer>().material.color;
        swingRackOriginalEColor = swingRack.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        boomFeetOriginalColor = boomFeet[0].GetComponent<Renderer>().material.color;
        boomFeetOriginalEColor = boomFeet[0].GetComponent<Renderer>().material.GetColor("_EmissionColor");

        liftingHooksOriginalColor = liftingHooks[0].GetComponent<Renderer>().material.color;
        liftingHooksOriginalEColor = liftingHooks[0].GetComponent<Renderer>().material.GetColor("_EmissionColor");

        trunionOriginalColor = trunion [0].GetComponent<Renderer> ().material.color;

        aFrameOriginalColor = aFrame.GetComponent<Renderer>().material.color;
        aFrameOriginalEColor = aFrame.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        aFrameLugsOriginalColor = aFrameLugs[0].GetComponent<Renderer>().material.color;
        aFrameLugsOriginalEColor = aFrameLugs[0].GetComponent<Renderer>().material.GetColor("_EmissionColor");
        gantryOriginalColor = gantry.GetComponent<Renderer>().material.color;
        gantryOriginalEColor = gantry.GetComponent<Renderer>().material.GetColor("_EmissionColor");

        for(int i = 0; i < mast.GetComponent<Renderer>().materials.Length; i++)
        {
            mastOriginalColor[i] = mast.GetComponent<Renderer>().materials[i].color;
            mastOriginalEColor[i] = mast.GetComponent<Renderer>().materials[i].GetColor("_EmissionColor");

            //Debug.Log(mast.GetComponent<Renderer>().materials[i].name + " " + mast.GetComponent<Renderer>().materials.Length + " i = " + i);
        }
        
        
        mastFeetOriginalColor = mastFeet[0].GetComponent<Renderer>().material.color;

        spreaderBarOriginalColor = spreaderBar.GetComponent<Renderer>().material.color;
        spreaderBarOriginalEColor = spreaderBar.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        centerPinOriginalColor = centerPin.GetComponent<Renderer>().material.color;

        tubOriginalColor = tub.GetComponent<Renderer>().material.color;
        tubOriginalEColor = tub.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        tubBulkheadOriginalColor = tubBulkhead.GetComponent<Renderer>().material.color;
        tubBulkheadOriginalEColor = tubBulkhead.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        tubRollersOriginalColor = tubRollers.GetComponent<Renderer>().material.color;
        tubRollersOriginalEColor = tubRollers.GetComponent<Renderer>().material.GetColor("_EmissionColor");

        lowerRollerRailOriginalColor = lowerRollerRail.GetComponent<Renderer>().material.color;
        lowerRollerRailOriginalEColor = lowerRollerRail.GetComponent<Renderer>().material.GetColor("_EmissionColor");
        upperRollerRailOriginalColor = upperRollerRail.GetComponent<Renderer>().material.color;
        upperRollerRailOriginalEColor = upperRollerRail.GetComponent<Renderer>().material.GetColor("_EmissionColor");

        //******************************************************************************************************************************
        //Set Defaults
        showHide.aFrame = true;
        showHide.aFrameLugs = true;
        showHide.boomChords = true;																												//Show Boom CHords
		showHide.boomLacings = true;																											//Show Boom Lacings
		showHide.boomLights = true;																												//Show Boom Lights
		showHide.boomPlatform = true;																											//Show Boom Platform
        showHide.mast = true;
		showHide.houseBody = true;																												//Show House Body
        //showHide.houseCab = true;
		showHide.hoistRopes = true;																												//Show Hoist Ropes
		showHide.upperSuspensionRopes = true;																									//Show Upper Suspension Ropes
		showHide.intSuspensionRopes = true;																										//Show Intermediate Suspension Ropes
        showHide.lowerSuspensionRopes = true;
		showHide.revFrameFloor = true;																											//Show Rev Frame Floor
        showHide.tub = true;
        showHide.aFrame = true;
        showHide.gantry = true;
        //showHide.housePlatform = true;
        showHide.upperRollerRail = true;
        showHide.rollerCircle = true;
        showHide.liftingHooks = true;
        showHide.revFrame = true;
        showHide.revFrameFloor = true;

        showHide.swingPinions = true;
        showHide.swingRack = true;
        showHide.bucks = true;

        fadeManager.hoistRopes.fade = 1;
        fadeManager.house.fade = 1;
        fadeManager.tub.fade = 1;
        fadeManager.revFrame.fade = 1;
        fadeManager.dragRopes.fade = 1;
        fadeManager.aFrame.fade = 1;
        fadeManager.gantry.fade = 1;
        fadeManager.linkPlate.fade = 1;
        fadeManager.boomTip.fade = 1;
        


    }//Start End
	
	// Update is called once per frame
	void Update () 
	{
		colorLerp = Mathf.PingPong (Time.time, flashDuration) / flashDuration;																	//this value ping pongs between 0 and 1 according to Time.time

		//******************************************************************************************************************************
		//Delayed Object Definition Section
		//Objects in this section cannot be found at startup as have yet to be instantiated

		leftDragRopeChildCount = leftDragRopeParents [0].transform.childCount;																	//How many child objects are there
		if (leftDragRopeChildCount != 0 && leftDragRopeFound == false) 																			//If left drag ropes exist
		{
			leftDragCount = 0;																													//Zero counter
			for (int i = 0; i < leftDragRopeParents.Length; i++) 																				//For each parent (there are two)
			{
				foreach (Transform j in leftDragRopeParents[i].transform) 
				{
					leftDragRopes [leftDragCount] = j.gameObject;																				//Add game object to array
					leftDragCount ++;
				}//Foreach end
			}//For End
            dragRopeMaterial[0] = leftDragRopes[0].GetComponent<Renderer>().material;
            dragRopeMaterial[1] = leftDragRopes[1].GetComponent<Renderer>().material;
			dragRopesOriginalColor = leftDragRopes[0].GetComponent<Renderer>().material.color;
			dragRopesOriginalEColor = leftDragRopes [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			leftDragRopeFound = true;
		}//If End

		rightDragRopeChildCount = rightDragRopeParents [0].transform.childCount;																//How many child objects are there
		if (rightDragRopeChildCount != 0 && rightDragRopeFound == false) 																		//If right drag ropes exist
		{
			rightDragCount = 0;																													//Zero Counter
			for (int i = 0; i < rightDragRopeParents.Length; i++) 																				//For each parent (there are 2)
			{
				foreach (Transform j in rightDragRopeParents[i].transform) 
				{
					rightDragRopes [rightDragCount] = j.gameObject;																				//Add game objcet to array
					rightDragCount ++;																											//Increment counter
				}//Foreach end
			}//For End
			rightDragRopeFound = true;
            dragRopeMaterial[2] = rightDragRopes[0].GetComponent<Renderer>().material;
            dragRopeMaterial[3] = rightDragRopes[1].GetComponent<Renderer>().material;
		}//If End

		dumpRopeChildCount = dumpRopeParent.transform.childCount;																				//How many child objects are there
		if (dumpRopeChildCount != 0 && dumpRopeFound == false) 																					//If Dump Rope exists
		{
			dumpRopeCount = 0;																													//Zero counter
			foreach (Transform i in dumpRopeParent.transform) 																					//For each child transfrom in parent
			{
				dumpRopes [dumpRopeCount] = i.gameObject;																						//Add gameobject of each child transform to array
				dumpRopeCount ++;																												//Increment counter
			}//Foreach End
			dumpRope = dumpRopes[0];																											//There is only one dump rope 
			dumpRopeOriginalColor = dumpRope.GetComponent<Renderer> ().material.color;
			dumpRopeOriginalEColor = dumpRope.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			dumpRopeFound = true;																												//Flag dump rope as found
		}//If End


		leftHoistRopeChildCount = leftHoistRopeParent.transform.childCount;																		//Find the number of child objects with transforms
		if (leftHoistRopeChildCount != 0 && leftHoistRopeFound == false) 																		//If there are children and they have not been found
		{
			leftHoistCount = 0;																													//Set count to zero
			foreach (Transform i in leftHoistRopeParent.transform) 																				//For each child transfrom under the parent object
			{
				leftHoistRopes [leftHoistCount] = i.gameObject;																					//Add gameobject of the transform to list
				leftHoistCount++;																												//Increment count
			}//foreach End
			leftHoistRope = leftHoistRopes[0];
            hoistRopeMaterial[0] = leftHoistRope.GetComponent<Renderer>().material;
            hoistRopeOriginalColor = leftHoistRope.GetComponent<Renderer> ().material.color;
			hoistRopeOriginalEColor = leftHoistRope.GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			leftHoistRopeFound = true;
		}//If End

		rightHoistRopeChildCount = rightHoistRopeParent.transform.childCount;																		//Find the number of child objects with transforms
		if (rightHoistRopeChildCount != 0 && rightHoistRopeFound == false) 																		//If there are children and they have not been found
		{
			rightHoistCount = 0;																													//Set count to zero
			foreach (Transform i in rightHoistRopeParent.transform) 																				//For each child transfrom under the parent object
			{
				rightHoistRopes [rightHoistCount] = i.gameObject;																					//Add gameobject of the transform to list
				rightHoistCount++;																												//Increment count
			}//foreach End
			rightHoistRope = rightHoistRopes[0];
            hoistRopeMaterial[1] = rightHoistRope.GetComponent<Renderer>().material;
            rightHoistRopeFound = true;
		}//If End



		if (straightRopes.Length < 2) {straightRopes = GameObject.FindGameObjectsWithTag ("StraightRopePrefab");}								//Search for suspension ropes
		if (straightRopes.Length == 6 && gotSuspensionRopes == false) 
		{
			for (int i = 0; i < 4; i++) 
			{
				upperSusRopes [i] = straightRopes [i];
			}
			for (int j = 4; j < 6; j++) 
			{
				intSusRopes [j - 4] = straightRopes [j];
			}
			upperSusRopesOriginalColor = upperSusRopes [0].GetComponent<Renderer> ().material.color;
			upperSusRopesOriginalEColor = upperSusRopes [0].GetComponent<Renderer> ().material.GetColor ("_EmissionColor");
			intSusRopesOriginalColor = intSusRopes [0].GetComponent<Renderer> ().material.color;
			intSusRopesOriginalEColor = intSusRopes [0].GetComponent<Renderer> ().sharedMaterial.GetColor ("_EmissionColor");
			gotSuspensionRopes = true;
		}


		//******************************************************************************************************************************
		//Show/Hide Section
		boomChords.SetActive(showHide.boomChords);
		boomLacings.SetActive (showHide.boomLacings);
		boomLights.SetActive (showHide.boomLights);
        mast.SetActive(showHide.mast);
        aFrame.SetActive(showHide.aFrame);
        for (int i = 0; i < aFrameLugs.Length; i++)
        {
            aFrameLugs[i].SetActive(showHide.aFrameLugs);
        }
        gantry.SetActive(showHide.gantry);

        for (int i = 0; i < swingPinions.Length; i++)
        {
            swingPinions[i].SetActive(showHide.swingPinions);
        }

        swingRack.SetActive(showHide.swingRack);

        for (int i = 0; i < liftingHooks.Length; i++)
        {
            liftingHooks[i].SetActive(showHide.liftingHooks);
        }

        for (int i = 0; i < boomPlatform.Length; i++) 
		{
			boomPlatform [i].SetActive (showHide.boomPlatform);
		}
		housePlatform.SetActive (showHide.houseBody);
        for (int i = 0; i < houseCab.Length; i++)
        {
            houseCab[i].SetActive(showHide.houseBody);
        }
		leftHoistRopeParent.SetActive (showHide.hoistRopes);
		rightHoistRopeParent.SetActive (showHide.hoistRopes);
        //For suspension Ropes see Highlight section
        for (int i = 0; i < lowSusRopes.Length; i++)
        {
            lowSusRopes[i].SetActive(showHide.lowerSuspensionRopes);
        }
		revFrameFloor.SetActive(showHide.revFrameFloor);
        revFrame.SetActive(showHide.revFrame);
        tub.SetActive(showHide.tub);
        tubRollers.SetActive(showHide.rollerCircle);
        upperRollerRail.SetActive(showHide.upperRollerRail);

        buckParent.SetActive(showHide.bucks);

        //******************************************************************************************************************************
        //Fade Section
        //A Frame
        fadeManager.aFrame.fade = FadeController(fadeManager.aFrame.enabled, fadeManager.aFrame.fade);
        ShaderChanger(aFrameMaterial, fadeManager.aFrame.enabled, fadeManager.aFrame.fade);
        //For the writing for the aFrame alpha, see highlighting section

        //Boom Tip
        fadeManager.boomTip.fade = FadeController(fadeManager.boomTip.enabled, fadeManager.boomTip.fade);
        ShaderChanger(boomTipMaterial, fadeManager.boomTip.enabled, fadeManager.boomTip.fade);
        //For the writing of the Boom Tip alpha, see Highlighting Section
        
        //Hoist Ropes
        fadeManager.hoistRopes.fade = FadeController(fadeManager.hoistRopes.enabled, fadeManager.hoistRopes.fade);                                  //Update the fade value based on enabled state
        ShaderChanger(hoistRopeMaterial, fadeManager.hoistRopes.enabled, fadeManager.hoistRopes.fade);
        //For  the writing of the hoist rope alpha see Highlight section

        //Drag Ropes
        fadeManager.dragRopes.fade = FadeController(fadeManager.dragRopes.enabled, fadeManager.dragRopes.fade);
        ShaderChanger(dragRopeMaterial, fadeManager.dragRopes.enabled, fadeManager.dragRopes.fade);
        //For the writing of the drag rope alpha see Highlight Section

        //Gantry
        fadeManager.gantry.fade = FadeController(fadeManager.gantry.enabled, fadeManager.gantry.fade);
        ShaderChanger(gantryMaterial, fadeManager.gantry.enabled, fadeManager.gantry.fade);
        //For the writing of the Gantry alpha see Highlight Section

        //House Platform
        fadeManager.house.fade = FadeController(fadeManager.house.enabled, fadeManager.house.fade);
        ShaderChanger(housePlatformMaterial, fadeManager.house.enabled, fadeManager.house.fade);
        housePlatformNewColor = housePlatformOriginalColor;
        housePlatformNewColor.a = fadeManager.house.fade;
        for (int i = 0; i < housePlatformMaterial.Length; i++)
        {        
            housePlatformMaterial[i].SetColor("_Color", housePlatformNewColor);
        }
        foreach(Transform j in housePlatform.transform)
        {
            if(true) {
                // Render mode for childed objects here is cutout, so I can just set the alpha here
                tempColor = j.gameObject.GetComponent<Renderer>().material.color;
                tempColor.a = fadeManager.house.fade;
                j.gameObject.GetComponent<Renderer>().material.SetColor("_Color", tempColor);
                //Debug.Log(j.gameObject.name + " " + j.gameObject.GetComponent<Renderer>().material.name);
            }            
        }
        //House Cab
        ShaderChanger(houseCabMaterial, fadeManager.house.enabled, fadeManager.house.fade);
        for (int i = 0; i < houseCab.Length; i++)
        {
            houseCabNewColor[i] = houseCabOriginalColor[i];
            houseCabNewColor[i].a = fadeManager.house.fade;
            houseCabMaterial[i].SetColor("_Color", houseCabNewColor[i]);
        }

        //Link Plate
        fadeManager.linkPlate.fade = FadeController(fadeManager.linkPlate.enabled, fadeManager.linkPlate.fade);
        ShaderChanger(linkPlateMaterial, fadeManager.linkPlate.enabled, fadeManager.linkPlate.fade);
        //For the writing of link plate alpha see Highlight Section

        //Rev Frame
        fadeManager.revFrame.fade = FadeController(fadeManager.revFrame.enabled, fadeManager.revFrame.fade);
        ShaderChanger(revFrameMaterial, fadeManager.revFrame.enabled, fadeManager.revFrame.fade);

        //Tub Rollers
        fadeManager.rollerCircle.fade = FadeController(fadeManager.rollerCircle.enabled, fadeManager.rollerCircle.fade);
        ShaderChanger(tubRollersMaterial, fadeManager.rollerCircle.enabled, fadeManager.rollerCircle.fade);

        //Tub
        fadeManager.tub.fade = FadeController(fadeManager.tub.enabled, fadeManager.tub.fade);
        ShaderChanger(tubMaterial, fadeManager.tub.enabled, fadeManager.tub.fade);



        //******************************************************************************************************************************
        //Highlight Section

        //A-Frame
        highlighting.aFrame.highlight = FlashStatus((int)highlighting.aFrame.status);  //Read the Highlight status of the Object
        aFrameNewColor = Color.Lerp(aFrameOriginalColor, highlightColor, highlighting.aFrame.highlight);
        aFrameNewEColor = Color.Lerp(aFrameOriginalEColor, highlightColor, highlighting.aFrame.highlight);  //Get the New Emission Color
        aFrameNewColor.a = fadeManager.aFrame.fade;
        aFrame.GetComponent<Renderer>().material.SetColor("_Color", aFrameNewColor);													//Update the actual color of the object
        aFrame.GetComponent<Renderer>().material.SetColor("_EmissionColor", aFrameNewEColor);

        //AFrame Lugs
        highlighting.aFrameLugs.highlight = FlashStatus((int)highlighting.aFrameLugs.status);
        aFrameLugsNewColor = Color.Lerp(aFrameLugsOriginalColor, highlightColor, highlighting.aFrameLugs.highlight);
        aFrameLugsNewEColor = Color.Lerp(aFrameLugsOriginalEColor, highlightColor, highlighting.aFrameLugs.highlight);
        for (int i = 0; i < aFrameLugs.Length; i++)
        {
            aFrameLugs[i].GetComponent<Renderer>().material.SetColor("_Color", aFrameLugsNewColor);
            aFrameLugs[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", aFrameLugsNewEColor);
        }

        //Gantry
        highlighting.gantry.highlight = FlashStatus((int)highlighting.gantry.status);  //Read the Highlight status of the Object
        gantryNewColor = Color.Lerp(gantryOriginalColor, highlightColor, highlighting.gantry.highlight);
        gantryNewEColor = Color.Lerp(gantryOriginalEColor, highlightColor, highlighting.gantry.highlight);  //Get the New Emission Color
        gantryNewColor.a = fadeManager.gantry.fade;
        gantry.GetComponent<Renderer>().material.SetColor("_Color", gantryNewColor);													//Update the actual color of the object
        gantry.GetComponent<Renderer>().material.SetColor("_EmissionColor", gantryNewEColor);

        //Mast
        //highlighting.mast.highlight = FlashStatus((int)highlighting.mast.status);  //Read the Highlight status of the Object
        highlighting.mastHead.highlight = FlashStatus((int)highlighting.mastHead.status);
        highlighting.mastChords.highlight = FlashStatus((int)highlighting.mastChords.status);        
        highlighting.mastLacings.highlight = FlashStatus((int)highlighting.mastLacings.status);

        mastNewColor[0] = Color.Lerp(mastOriginalColor[0], highlightColor, highlighting.mastHead.highlight);
        mastNewEColor[0] = Color.Lerp(mastOriginalEColor[0], highlightColor, highlighting.mastHead.highlight);  //Get the New Emission Color      

        mastNewColor[1] = Color.Lerp(mastOriginalColor[1], highlightColor, highlighting.mastChords.highlight);
        mastNewEColor[1] = Color.Lerp(mastOriginalEColor[1], highlightColor, highlighting.mastChords.highlight);  //Get the New Emission Color      

        mastNewColor[2] = Color.Lerp(mastOriginalColor[2], highlightColor, highlighting.mastLacings.highlight);
        mastNewEColor[2] = Color.Lerp(mastOriginalEColor[2], highlightColor, highlighting.mastLacings.highlight);  //Get the New Emission Color      

        mastNewColor[3] = Color.Lerp(mastOriginalColor[3], highlightColor, highlighting.mastLacings.highlight);
        mastNewEColor[3] = Color.Lerp(mastOriginalEColor[3], highlightColor, highlighting.mastLacings.highlight);  //

        for (int i = 0; i < mast.GetComponent<Renderer>().materials.Length; i++)
        {
            mast.GetComponent<Renderer>().materials[i].SetColor("_Color", mastNewColor[i]); //Update the actual color of the object
            mast.GetComponent<Renderer>().materials[i].SetColor("_EmissionColor", mastNewEColor[i]);
        }
        
        //Mast Feet
        highlighting.mastFeet.highlight = FlashStatus((int)highlighting.mastFeet.status);                                                           //Read the Highlight status of the Object
        mastFeetNewColor = Color.Lerp(mastFeetOriginalColor, highlightColor, highlighting.mastFeet.highlight); ;
        mastFeetNewColor.a = highlighting.mastFeet.highlight;

        for (int i = 0; i < mastFeet.Length; i++)
        {
            mastFeet[i].GetComponent<Renderer>().material.SetColor("_Color", mastFeetNewColor);
        }

        //Boom Chords
        highlighting.boomChords.highlight = FlashStatus((int)highlighting.boomChords.status);													//Read the highlight status of the object
		boomChordsNewColor = Color.Lerp(boomChordsOriginalColor, highlightColor, highlighting.boomChords.highlight);							//Get the New Color
		boomChordsNewEColor = Color.Lerp(boomChordsOriginalEColor, highlightColor, highlighting.boomChords.highlight);							//Get the New Emission Color
		boomChords.GetComponent<Renderer> ().material.SetColor ("_Color", boomChordsNewColor);													//Set the New Color
		boomChords.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", boomChordsNewEColor);											//Set the New Emission Color

		//Boom Lacings
		highlighting.boomLacings.highlight = FlashStatus((int)highlighting.boomLacings.status);
		boomLacingsNewColor = Color.Lerp (boomLacingsOriginalColor, highlightColor, highlighting.boomLacings.highlight);
		boomLacingsNewEColor = Color.Lerp (boomLacingsOriginalEColor, highlightColor, highlighting.boomLacings.highlight);
		boomLacings.GetComponent<Renderer> ().material.SetColor ("_Color", boomLacingsNewColor);
		boomLacings.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", boomLacingsNewEColor);

        //Boom Feet
        highlighting.boomFeet.highlight = FlashStatus((int)highlighting.boomFeet.status);                                                           //Read the Highlight status of the Object
        boomFeetNewColor = Color.Lerp(boomFeetOriginalColor, highlightColor, highlighting.boomFeet.highlight);
        boomFeetNewEColor = Color.Lerp(boomFeetOriginalEColor, highlightColor, highlighting.boomFeet.highlight);
        if (boomFeet[0].GetComponent<Renderer>().material.color != boomFeetNewColor)
        {
            boomFeet[0].GetComponent<Renderer>().material.SetColor("_Color", boomFeetNewColor);
            boomFeet[0].GetComponent<Renderer>().material.SetColor("_EmissionColor", boomFeetNewEColor);
        }
        if (boomFeet[1].GetComponent<Renderer>().material.color != boomFeetNewColor)
        {
            boomFeet[1].GetComponent<Renderer>().material.SetColor("_Color", boomFeetNewColor);
            boomFeet[1].GetComponent<Renderer>().material.SetColor("_EmissionColor", boomFeetNewEColor);
        }

        //Boom Sheave
        highlighting.boomSheave.highlight = FlashStatus((int)highlighting.boomSheave.status);
		boomSheaveNewColor = Color.Lerp (boomSheaveOriginalColor, highlightColor, highlighting.boomSheave.highlight);
		boomSheaveNewEColor = Color.Lerp (boomSheaveOriginalEColor, highlightColor, highlighting.boomSheave.highlight);
		boomSheave.GetComponent<Renderer> ().material.SetColor ("_Color", boomSheaveNewColor);
		boomSheave.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", boomSheaveNewEColor);

		//Boom Tip
		highlighting.boomTip.highlight = FlashStatus((int)highlighting.boomTip.status);
		boomTipNewColor = Color.Lerp (boomTipOriginalColor, highlightColor, highlighting.boomTip.highlight);
		boomTipNewEColor = Color.Lerp (boomTipOriginalEColor, highlightColor, highlighting.boomTip.highlight);
        boomTipNewColor.a = fadeManager.boomTip.fade;
        boomTip.GetComponent<Renderer> ().material.SetColor ("_Color", boomTipNewColor);
		boomTip.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", boomTipNewEColor);

		//Bucket
		highlighting.bucket.highlight = FlashStatus((int)highlighting.bucket.status);
		bucketNewColor = Color.Lerp (bucketOriginalColor, highlightColor, highlighting.bucket.highlight);
		bucketNewEColor = Color.Lerp (bucketOriginalEColor, highlightColor, highlighting.bucket.highlight);
		for (int i = 0; i < bucket.Length; i++) {
			bucket [i].GetComponent<Renderer> ().material.SetColor ("_Color", bucketNewColor);
			bucket [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", bucketNewEColor);
		}

		//Bucket Teeth
		highlighting.bucketTeeth.highlight = FlashStatus((int)highlighting.bucketTeeth.status);
		bucketTeethNewColor = Color.Lerp (bucketTeethOriginalColor, highlightColor, highlighting.bucketTeeth.highlight);
		bucketTeethNewEColor = Color.Lerp (bucketTeethOriginalEColor, highlightColor, highlighting.bucketTeeth.highlight);
		bucketTeeth.GetComponent<Renderer> ().material.SetColor ("_Color", bucketTeethNewColor);
		bucketTeeth.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", bucketTeethNewEColor);

		//Center Pin
		highlighting.centerPin.highlight = FlashStatus((int)highlighting.centerPin.status);
		centerPinNewColor = centerPinOriginalColor;
		centerPinNewColor.a = highlighting.centerPin.highlight;
		centerPin.GetComponent<Renderer> ().material.SetColor ("_Color", centerPinNewColor);

		//Clevis
		highlighting.clevis.highlight = FlashStatus((int)highlighting.clevis.status);
		clevisNewColor = Color.Lerp (clevisOriginalColor, highlightColor, highlighting.clevis.highlight);
		clevisNewEColor = Color.Lerp (clevisOriginalEColor, highlightColor, highlighting.clevis.highlight);
		for (int i = 0; i < clevis.Length; i++) {
			clevis [i].GetComponent<Renderer> ().material.SetColor ("_Color", clevisNewColor);
			clevis [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", clevisNewEColor);
		}

		//Drag Chains
		highlighting.dragChain.highlight = FlashStatus((int)highlighting.dragChain.status);
		dragChainsNewColor = Color.Lerp (dragChainsOriginalColor, highlightColor, highlighting.dragChain.highlight);
		dragChainsNewEColor = Color.Lerp (dragChainsOriginalEColor, highlightColor, highlighting.dragChain.highlight);
		for (int i = 0; i < dragChains.Length; i++) {
			dragChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", dragChainsNewColor);
			dragChains [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dragChainsNewEColor);
		}

		//Drag Ropes
		highlighting.dragRope.highlight = FlashStatus((int)highlighting.dragRope.status);
		dragRopesNewColor = Color.Lerp (dragRopesOriginalColor, highlightColor, highlighting.dragRope.highlight);
		dragRopesNewEColor = Color.Lerp (dragRopesOriginalEColor, highlightColor, highlighting.dragRope.highlight);

        dragRopesNewColor.a = fadeManager.dragRopes.fade;

		for (int i = 0; i < leftDragRopes.Length; i++) {
			leftDragRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", dragRopesNewColor);
			leftDragRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dragRopesNewEColor);
			rightDragRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", dragRopesNewColor);
			rightDragRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dragRopesNewEColor);
		}

		//Dump Block
		highlighting.dumpBlock.highlight = FlashStatus((int)highlighting.dumpBlock.status);
		dumpBlockNewColor = Color.Lerp (dumpBlockOriginalColor, highlightColor, highlighting.dumpBlock.highlight);
		dumpBlockNewEColor = Color.Lerp (dumpBlockOriginalEColor, highlightColor, highlighting.dumpBlock.highlight);
		dumpBlock.GetComponent<Renderer> ().material.SetColor ("_Color", dumpBlockNewColor);
		dumpBlock.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dumpBlockNewEColor);

		//Dump Chain
		highlighting.dumpChain.highlight = FlashStatus((int)highlighting.dumpChain.status);
		dumpChainNewColor = Color.Lerp (dumpChainOriginalColor, highlightColor, highlighting.dumpChain.highlight);
		dumpChainNewEColor = Color.Lerp (dumpChainOriginalEColor, highlightColor, highlighting.dumpChain.highlight);
		for (int i = 0; i < dumpChain.Length; i++) {
			dumpChain [i].GetComponent<Renderer> ().material.SetColor ("_Color", dumpChainNewColor);
			dumpChain [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dumpChainNewEColor);
		}

		//Dump Rope
		highlighting.dumpRope.highlight = FlashStatus((int)highlighting.dumpRope.status);
		dumpRopeNewColor = Color.Lerp (dumpRopeOriginalColor, highlightColor, highlighting.dumpRope.highlight);
		dumpRopeNewEColor = Color.Lerp (dumpRopeOriginalEColor, highlightColor, highlighting.dumpRope.highlight);
		dumpRope.GetComponent<Renderer> ().material.SetColor ("_Color", dumpRopeNewColor);
		dumpRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", dumpRopeNewEColor);

        //Spreader bar
        highlighting.spreaderBar.highlight = FlashStatus((int)highlighting.spreaderBar.status);
        spreaderBarNewColor = Color.Lerp(spreaderBarOriginalColor, highlightColor, highlighting.spreaderBar.highlight);
        spreaderBarNewEColor = Color.Lerp(spreaderBarOriginalEColor, highlightColor, highlighting.spreaderBar.highlight);
        spreaderBar.GetComponent<Renderer>().material.SetColor("_Color", spreaderBarNewColor);
        spreaderBar.GetComponent<Renderer>().material.SetColor("_EmissionColor", spreaderBarNewEColor);
        
        //Hoist Ropes
        highlighting.hoistRope.highlight = FlashStatus((int)highlighting.hoistRope.status);
		hoistRopeNewColor = Color.Lerp (hoistRopeOriginalColor, highlightColor, highlighting.hoistRope.highlight);
		hoistRopeNewEColor = Color.Lerp (hoistRopeOriginalEColor, highlightColor, highlighting.hoistRope.highlight);

        hoistRopeNewColor.a = fadeManager.hoistRopes.fade;

        leftHoistRope.GetComponent<Renderer> ().material.SetColor ("_Color", hoistRopeNewColor);
		rightHoistRope.GetComponent<Renderer> ().material.SetColor ("_Color", hoistRopeNewColor);
		leftHoistRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", hoistRopeNewEColor);
		rightHoistRope.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", hoistRopeNewEColor);

		//Hoist Chains
		highlighting.hoistChain.highlight = FlashStatus((int)highlighting.hoistChain.status);
		hoistChainNewColor = Color.Lerp (hoistChainOriginalColor, highlightColor, highlighting.hoistChain.highlight);
		hoistChainNewEColor = Color.Lerp (hoistChainOriginalEColor, highlightColor, highlighting.hoistChain.highlight);
		for (int i = 0; i < hoistChains.Length; i++) {
			hoistChains [i].GetComponent<Renderer> ().material.SetColor ("_Color", hoistChainNewColor);
			hoistChains [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", hoistChainNewEColor);
		}

        //Rev Frame
        highlighting.revolvingFrame.highlight = FlashStatus((int)highlighting.revolvingFrame.status);
        revFrameNewColor = Color.Lerp(revFrameOriginalColor, highlightColor, highlighting.revolvingFrame.highlight);
        revFrameNewEColor = Color.Lerp(revFrameOriginalEColor, highlightColor, highlighting.revolvingFrame.highlight);
        revFrameNewColor.a = fadeManager.revFrame.fade;
        revFrame.GetComponent<Renderer>().material.SetColor("_Color", revFrameNewColor);
        revFrame.GetComponent<Renderer>().material.SetColor("_EmissionColor", revFrameNewEColor);

        //Rev Frame Floor
        highlighting.revolvingFrameFloor.highlight = FlashStatus((int)highlighting.revolvingFrameFloor.status);
		revFrameFloorNewColor = Color.Lerp (revFrameFloorOriginalColor, highlightColor, highlighting.revolvingFrameFloor.highlight);
		revFrameFloorNewEColor = Color.Lerp (revFrameFloorOriginalEColor, highlightColor, highlighting.revolvingFrameFloor.highlight);
		revFrameFloor.GetComponent<Renderer> ().material.SetColor ("_Color", revFrameFloorNewColor);
		revFrameFloor.GetComponent<Renderer> ().material.SetColor ("_EmissionColor", revFrameFloorNewEColor);

        //Swing Pinion
        highlighting.swingPinions.highlight = FlashStatus((int)highlighting.swingPinions.status);                                                   //Read the Highlight status of the Object
        swingPinionNewColor = Color.Lerp(swingPinionOriginalColor, highlightColor, highlighting.swingPinions.highlight);
        swingPinionNewEColor = Color.Lerp(swingPinionOriginalEColor, highlightColor, highlighting.swingPinions.highlight);
        for (int i = 0; i < swingPinions.Length; i++)
        {
            swingPinions[i].GetComponent<Renderer>().material.SetColor("_Color", swingPinionNewColor);
            swingPinions[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", swingPinionNewEColor);
        }

        //Swing Rack
        highlighting.swingRack.highlight = FlashStatus((int)highlighting.swingRack.status);
        swingRackNewColor = Color.Lerp(swingRackOriginalColor, highlightColor, highlighting.swingRack.highlight);
        swingRackNewEColor = Color.Lerp(swingRackOriginalEColor, highlightColor, highlighting.swingRack.highlight);
        swingRack.GetComponent<Renderer>().material.SetColor("_Color", swingRackNewColor);
        swingRack.GetComponent<Renderer>().material.SetColor("_EmissionColor", swingRackNewEColor);

        //Suspension Ropes
        //Prep Upper Ropes New Color
        highlighting.upSusRopes.highlight = FlashStatus((int)highlighting.upSusRopes.status);
		upperSusRopesNewColor = Color.Lerp (upperSusRopesOriginalColor, highlightColor, highlighting.upSusRopes.highlight);
		upperSusRopesNewEColor = Color.Lerp (upperSusRopesOriginalEColor, highlightColor, highlighting.upSusRopes.highlight);
		//Prep Int Ropes New Color
		highlighting.intSusRopes.highlight = FlashStatus((int)highlighting.intSusRopes.status);
		intSusRopesNewColor = Color.Lerp (intSusRopesOriginalColor, highlightColor, highlighting.intSusRopes.highlight);
		intSusRopesNewEColor = Color.Lerp (intSusRopesOriginalEColor, highlightColor, highlighting.intSusRopes.highlight);
		//Set Colors
		if (gotSuspensionRopes) 
		{
			for (int i = 0; i < upperSusRopes.Length; i++) 
			{
				upperSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", upperSusRopesNewColor);
				upperSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", upperSusRopesNewEColor);
				upperSusRopes [i].SetActive (showHide.upperSuspensionRopes);
			}
			for (int i = 0; i < intSusRopes.Length; i++) {
				intSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_Color", intSusRopesNewColor);
				intSusRopes [i].GetComponent<Renderer> ().material.SetColor ("_EmissionColor", intSusRopesNewEColor);
				intSusRopes [i].SetActive (showHide.intSuspensionRopes);
			}
		}//If End
        highlighting.loSusRopes.highlight = FlashStatus((int)highlighting.loSusRopes.status);
        lowSusRopesNewColor = Color.Lerp(lowSusRopesOriginalColor, highlightColor, highlighting.loSusRopes.highlight);
        lowSusRopesNewEColor = Color.Lerp(lowSusRopesOriginalEColor, highlightColor, highlighting.loSusRopes.highlight);
        lowSusRopesNewColor.a = fadeManager.linkPlate.fade;
        for (int i = 0; i < lowSusRopes.Length; i++)
        {
            lowSusRopes[i].GetComponent<Renderer>().material.SetColor("_Color", lowSusRopesNewColor);
            lowSusRopes[i].GetComponent<Renderer>().material.SetColor("_EmissionCOlor", lowSusRopesNewEColor);
        }

		//Trunion
		highlighting.trunion.highlight = FlashStatus((int)highlighting.trunion.status);
		trunionNewColor = trunionOriginalColor;
		trunionNewColor.a = highlighting.trunion.highlight;
		for (int i = 0; i < trunion.Length; i++) {
			trunion [i].GetComponent<Renderer> ().material.SetColor ("_Color", trunionNewColor);
		}

        //Tub 
        highlighting.tub.highlight = FlashStatus((int)highlighting.tub.status);       //Read the Highlight status of the Object
        tubNewColor = Color.Lerp(tubOriginalColor, highlightColor, highlighting.tub.highlight);
        tubNewEColor = Color.Lerp(tubOriginalEColor, highlightColor, highlighting.tub.highlight);
        tub.GetComponent<Renderer>().material.SetColor("_Color", tubNewColor);
        tub.GetComponent<Renderer>().material.SetColor("_EmissionColor", tubNewEColor);

        //Tub Bulkhead
        highlighting.tubBulkhead.highlight = FlashStatus((int)highlighting.tubBulkhead.status);                                                     //Read the Highlight status of the Object
        tubBulkheadNewColor = Color.Lerp(tubBulkheadOriginalColor, highlightColor, highlighting.tubBulkhead.highlight);
        tubBulkheadNewEColor = Color.Lerp(tubBulkheadOriginalEColor, highlightColor, highlighting.tubBulkhead.highlight);
        tubBulkhead.GetComponent<Renderer>().material.SetColor("_Color", tubBulkheadNewColor);
        tubBulkhead.GetComponent<Renderer>().material.SetColor("_EmissionColor", tubBulkheadNewEColor);

        //Tub Rollers
        highlighting.tubRollers.highlight = FlashStatus((int)highlighting.tubRollers.status);                                                       //Read the Highlight status of the Object
        tubRollersNewColor = Color.Lerp(tubRollersOriginalColor, highlightColor, highlighting.tubRollers.highlight);
        tubRollersNewEColor = Color.Lerp(tubRollersOriginalEColor, highlightColor, highlighting.tubRollers.highlight);
        tubRollersNewColor.a = fadeManager.rollerCircle.fade;
        tubRollers.GetComponent<Renderer>().material.SetColor("_Color", tubRollersNewColor);
        tubRollers.GetComponent<Renderer>().material.SetColor("_EmissionColor", tubRollersNewEColor);

        //Lifting Hooks
        highlighting.liftingHooks.highlight = FlashStatus((int)highlighting.liftingHooks.status);                                                   //Read the Highlight status of the Object
        liftingHooksNewColor = Color.Lerp(liftingHooksOriginalColor, highlightColor, highlighting.liftingHooks.highlight);
        liftingHooksNewEColor = Color.Lerp(liftingHooksOriginalEColor, highlightColor, highlighting.liftingHooks.highlight);
        for (int i = 0; i < liftingHooks.Length; i++)
        {
            liftingHooks[i].GetComponent<Renderer>().material.SetColor("_Color", liftingHooksNewColor);
            liftingHooks[i].GetComponent<Renderer>().material.SetColor("_EmissionColor", liftingHooksNewEColor);
        }

        //Upper Roller Rail
        highlighting.rollerRailUpper.highlight = FlashStatus((int)highlighting.rollerRailUpper.status);                                             //Read the Highlight status of the Object
        upperRollerRailNewColor = Color.Lerp(upperRollerRailOriginalColor, highlightColor, highlighting.rollerRailUpper.highlight);
        upperRollerRailNewEColor = Color.Lerp(upperRollerRailOriginalEColor, highlightColor, highlighting.rollerRailUpper.highlight);
        upperRollerRail.GetComponent<Renderer>().material.SetColor("_Color", upperRollerRailNewColor);
        upperRollerRail.GetComponent<Renderer>().material.SetColor("_EmissionColor", upperRollerRailNewEColor);

        //Lower Roller Rail
        highlighting.rollerRailLower.highlight = FlashStatus((int)highlighting.rollerRailLower.status);                                             //Read the Highlight status of the Object
        lowerRollerRailNewColor = Color.Lerp(lowerRollerRailOriginalColor, highlightColor, highlighting.rollerRailLower.highlight);
        lowerRollerRailNewEColor = Color.Lerp(lowerRollerRailOriginalEColor, highlightColor, highlighting.rollerRailLower.highlight);
        lowerRollerRail.GetComponent<Renderer>().material.SetColor("_Color", lowerRollerRailNewColor);
        lowerRollerRail.GetComponent<Renderer>().material.SetColor("_EmissionColor", lowerRollerRailNewEColor);

    }//Update End

//******************************************************************************************************************************
	//Function Section
	float FlashStatus (int status)
	{
		//This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
		//The returned value should be sent to highlighting."ComponentName".highlight
		//THis replaces the following logic
		//**********Sample Code Start*******************
		//		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
		//		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
		//		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
		//**********Sample Code End********************
		float finalStatus;
		switch (status) 
		{
		case 2:
			finalStatus = 1;																														//If Status is "Solid"
			break;
		case 1:
			finalStatus = colorLerp;																												//If Status is "Flash"
			break;
		case 0:
			finalStatus = 0;																														//If Status is "Off"
			break;
		default:
			finalStatus = 0;																														//Default case
			break;
		}//Switch End
		return finalStatus;
	}//FlashStatus End


    //This function allows the gradual fading out of components. 
    float FadeController(bool enable, float fadeValue)
    {
        if (enable == true && fadeValue > 0.001)
        {
            fadeValue = Mathf.Lerp(fadeValue, 0, Time.deltaTime);
        }
        if (enable == false && fadeValue < 0.99)
        {
            fadeValue = Mathf.Lerp(fadeValue, 1, Time.deltaTime);
        }
        return fadeValue;
    }//FadeController End

    void ShaderChanger(Material[] materials, bool enable, float fadeValue)
    {
        if (enable == true)
        {
            for (int i = 0; i < materials.Length; i++)
            {
                materials[i].EnableKeyword("_ALPHABLEND_ON");
                materials[i].DisableKeyword("_ALPHATEST_ON");
                materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");

                materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.SrcAlpha);
                materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);

                //Debug.Log("Materials name is " + materials[i].name);

                if (String.Equals(materials[i].name, "MOSAIC_Windows (Instance)"))
                {
                    //materials[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].SetFloat("_Mode", 3);
                    //Debug.Log("Mode for mosaic windows set");
                }
                else
                {
                    //materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].SetFloat("_Mode", 2);
                }

                materials[i].SetInt("_ZWrite", 0);
                materials[i].renderQueue = 3000;
            }//For End
        }//If End

        if (enable == false && fadeValue > 0.9)
        {
            //Debug.Log(materials.Length);

            for (int i = 0; i < materials.Length; i++)
            {
                //Debug.Log(materials[i].name);

                materials[i].DisableKeyword("_ALPHABLEND_ON");
                materials[i].DisableKeyword("_ALPHATEST_ON");

                if (String.Equals(materials[i].name, "MOSAIC_Windows (Instance)"))
                {
                    materials[i].SetFloat("_Mode", 3);
                    materials[i].EnableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].renderQueue = 3000;
                    materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.OneMinusSrcAlpha);
                }
                else
                {
                    materials[i].DisableKeyword("_ALPHAPREMULTIPLY_ON");
                    materials[i].SetInt("_DstBlend", (int)UnityEngine.Rendering.BlendMode.Zero);
                    materials[i].SetFloat("_Mode", 0);
                    materials[i].renderQueue = -1;
                }

                materials[i].SetInt("_SrcBlend", (int)UnityEngine.Rendering.BlendMode.One);
                materials[i].SetInt("_ZWrite", 1);

            }//For End
        }//If End
    }//ShaderChanger End


}//MonoBehaviour End
