﻿using UnityEngine;
using System.Collections;

public class RopeDrawer : MonoBehaviour {
	public bool selfUpdate;
	public GameObject[] ropeStarts;
	public GameObject[] RopeEnds;
	public splineRendererArray[] splines;
	public bool overrideLength;

	public float relaxationFactor, aplha, g, precision;
	public int nbPoints;
	public float[] lengths;

	private float vertDist, horzDist, a, T, x1, x2, y1, dx;
	private Vector3 start, distance, direction;

	// Use this for initialization
	void Start () {
		lengths = new float[ropeStarts.Length];
		for (int i = 0; i < splines.Length; i++) {
			splines[i].points = new Vector3[nbPoints];
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (selfUpdate) {
			calcRopes();
		}
	}

	void calcRopes(){
		for (int i = 0; i < ropeStarts.Length; i++) {
			start = ropeStarts [i].transform.position;
			distance = (RopeEnds [i].transform.position - start);
			direction = Vector3.Normalize (new Vector3 (distance.x, 0f, distance.z));
			vertDist = distance.y;
			horzDist = Vector3.Magnitude (new Vector3 (distance.x, 0f, distance.z));
			if (overrideLength) {
				lengths [i] = distance.magnitude * relaxationFactor;
			}
			if (lengths [i] > distance.magnitude) {
				a = solveA (horzDist, lengths [i], vertDist, precision);
				T = a * aplha * g;
				x1 = solveX1 (lengths [i], a, horzDist);
				x2 = horzDist + x1;
				y1 = catCurve (x1, a);
				dx = horzDist / ((float)nbPoints - 1f);
				if (!float.IsNaN (x1)) {
					for (int j = 0; j < nbPoints; j++) {
						if (vertDist >= 0f) {
							splines [i].points [j] = start + ((dx * (float)j) * direction) + (Vector3.up * (catCurve (x1 + dx * ((float)j), a) - y1));
						} else {
							splines [i].points [j] = RopeEnds [i].transform.position + ((-dx * (float)j) * direction) + (Vector3.up * (catCurve (x1 + dx * ((float)j), a) - y1));
						}
					}
				}
				splines [i].updateMesh ();
			} else {
				Debug.Log ("Too Short");
			}
		}
	}

	float solveA(float horizontalDist, float ropeLength, float vertDist, float precision){
		float goal = Mathf.Sqrt (Mathf.Pow (ropeLength, 2) - Mathf.Pow (vertDist, 2));
		float low = 1f;
		float high = 75000f;
		while (Mathf.Abs(high-low) > precision) {
			float mid = (high + low) / 2f;
			float midO = iterate (mid, horizontalDist) - goal;
			if (midO > 0f) {
				low = mid;
			} else {
				high = mid;
			}
		}
		return low;
	}

	float solveX1(float ropeLength, float a, float horizontalDist){
		float ha = horizontalDist / a;
		return a * Mathf.Log((Mathf.Exp (-ha)*(Mathf.Sqrt(-1f*Mathf.Exp (ha)*(-2f*a*a*Mathf.Exp (ha)+a*a*Mathf.Exp (2f*ha)+a*a-ropeLength*ropeLength*Mathf.Exp (ha)))+ropeLength * Mathf.Exp (ha)))/(a*(Mathf.Exp (ha)-1f)));
	}

	float iterate(float a, float h){
		return a * (Mathf.Exp (h / (2f * a)) - Mathf.Exp (-h / (2f * a)));
	}

	float catCurve(float x, float a){
		return a * 0.5f * (Mathf.Exp (x / a) + Mathf.Exp (-x / a));
	}
}
