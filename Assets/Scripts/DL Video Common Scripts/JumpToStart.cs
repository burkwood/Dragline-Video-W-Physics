﻿using UnityEngine;
using System.Collections;

public class JumpToStart : MonoBehaviour {
	public Transform target;
	public splineRendererArray dumpRope;
	public bool jumped;

	// Use this for initialization
	void Start () {
		jumped = true; // test code!!
	
	}
	
	// Update is called once per frame
	void Update () {
		if (!jumped) {
			this.transform.parent = target;
			this.transform.localPosition = new Vector3 ();
			dumpRope.resetChildren ();
			jumped = true;
		}
	}
}
