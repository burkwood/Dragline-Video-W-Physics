﻿using UnityEngine;
using System.Collections;

public class StraightRopeDrawer : MonoBehaviour {
	public bool selfUpdate;
	public Transform prefab, houseParent;
	public float[] diameters;
	public Transform[] startPositions, endPositions, ropes;
	public bool createdRopes;


	// Use this for initialization
	void Start () {
		createRopes ();
	}
	
	// Update is called once per frame
	void Update () {
		if (selfUpdate && createdRopes) {
			updateRopes ();
		}
	}

	public void createRopes(){
		ropes = new Transform[startPositions.Length];
		for (int i = 0; i < startPositions.Length; i++) {
			ropes [i] = Transform.Instantiate (prefab);
			ropes [i].parent = houseParent;
		}
		updateRopes ();
		createdRopes = true;
	}

	public void updateRopes(){
		for (int i = 0; i < ropes.Length; i++) {
			cylinderPtPt (ropes [i], startPositions [i].position, endPositions [i].position, diameters [i]);
		}
	}

	void cylinderPtPt(Transform cylinder, Vector3 p1, Vector3 p2, float diameter){
		cylinder.position = (p1 + p2)/2.0f;
		Quaternion direction = Quaternion.LookRotation((p1 - p2).normalized);
		cylinder.transform.rotation = direction;
		float dist = Vector3.Distance(p1, p2);
		cylinder.transform.localScale = new Vector3 (diameter, dist/2.0f, diameter);
		cylinder.transform.Rotate (90f, 0f, 0f);
	}
}
