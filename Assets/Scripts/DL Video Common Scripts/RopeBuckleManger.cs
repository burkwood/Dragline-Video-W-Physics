﻿using UnityEngine;
using System.Collections;

public class RopeBuckleManger : MonoBehaviour {
	public Transform[] starts, ends, objects;
	public float[] relativePos;
	public Vector3 startNode, endNode, vect;

	public bool liveUpdate;

	// Use this for initialization
	void Start () {
		startNode = vectCentroid (starts);
		endNode = vectCentroid (ends);
		vect = endNode - startNode;
		relativePos = new float[objects.Length];
		for (int i = 0; i < objects.Length; i++) {
			relativePos[i] = Vector3.Dot (objects [i].position - startNode, vect.normalized)/vect.magnitude;
		}
	}
	
	// Update is called once per frame
	void Update () {
		if (liveUpdate) {
			startNode = vectCentroid (starts);
			endNode = vectCentroid (ends);
			vect = endNode - startNode;
			for (int i = 0; i < objects.Length; i++) {
				objects [i].position = startNode + relativePos [i] * vect;
			}
		}
	}

	Vector3 vectCentroid(Transform[] points){
		Vector3 centroid = new Vector3 ();
		float count = 0f;
		for (int i = 0; i < points.Length; i++) {
			centroid += points [i].position;
			count++;
		}
		return new Vector3 (centroid.x / count, centroid.y / count, centroid.z / count);
	}
}
