﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using IntPublicClass;

public class Gear_Speed_Control : MonoBehaviour
{
    public GameObject gear;
    GearDriver gearDriver;
    public float motorSpeed;
    [Range(-100f, 100f)]
    public float comSpeed;
    public bool auto = false;
    public bool keyboard = false;
    public MotorControl motorControl;
    public float scale;
    public float slowScale;
    public float motorDir;
    public float tempSpeed;
    public float actualSpeed;




    // Use this for initialization
	void Start ()
    {
        gearDriver = gear.GetComponent<GearDriver>();
        //motorSpeed = gearDriver.motorSpeedRPM;
	}
	
	// Update is called once per frame
	void Update ()
    {
        if (keyboard == true)
        {
            if (Input.GetKey(KeyCode.Q))
            {
                motorControl = MotorControl.Extend;
            }
            else if (Input.GetKey(KeyCode.E))
            {
                motorControl = MotorControl.Retract;
            }
            else
            {
                motorControl = MotorControl.None;
            }
        }
        


        if (auto)
        {
            if (motorControl == MotorControl.Extend)
            {
                motorDir = 1;
                tempSpeed = Mathf.Lerp(tempSpeed, motorSpeed, scale * Time.deltaTime);
                actualSpeed += Time.deltaTime * tempSpeed;
            }
            else if (motorControl == MotorControl.Retract)
            {
                motorDir = -1;
                tempSpeed = Mathf.Lerp(tempSpeed, motorSpeed, scale * Time.deltaTime);
                actualSpeed -= Time.deltaTime * tempSpeed;
            }
            else if (motorControl == MotorControl.None)
            {
                tempSpeed = Mathf.Lerp(tempSpeed, 0f, slowScale * Time.deltaTime);
                actualSpeed = Mathf.Lerp(actualSpeed, 0f, slowScale * Time.deltaTime);
                //actualSpeed = actualSpeed + motorDir * Time.deltaTime * tempSpeed;
            }
            actualSpeed = Mathf.Clamp(actualSpeed, -motorSpeed, motorSpeed);
            gearDriver.motorSpeedRPM = actualSpeed;
        }



        if (auto == false)
        {
            motorSpeed = gearDriver.motorSpeedRPM;
            gearDriver.motorSpeedRPM = comSpeed;
        }
        
    }
}
