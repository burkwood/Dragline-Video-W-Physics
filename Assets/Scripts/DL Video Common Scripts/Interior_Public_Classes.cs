﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using System;
using UnityEngine.Video;

namespace IntPublicClass
{
    public enum MotorControl { None = 0, Extend = 1, Retract = -1 };
    public enum SwingControl { None = 0, CW = 1, CCW = -1 };
    public enum hJoyMove { None = 0, Left = -80, Right = 80 };
    public enum vJoyMove { None = 0, Up = 80, Down = -80 };
    public enum hStickMove { None = 0, Left = 40, Right = -40 };
    public enum vStickMove { None = 0, Up = 40, Down = -40 };
    public enum Status { Off, Flash, Solid };

    [Serializable]
    public class ScreenController
    {
        //public string name;
        public bool open;
        public bool secondClip;
        public bool play;
    }

    [Serializable]
    public class ImageController
    {
        //public string name;
        public bool open;
    }

    [Serializable]
    public class AllScreens
    {
        public ScreenController hoistScreen;
        public ScreenController dragScreen;
    }

    [Serializable]
    public class GeneratorSFX
    {
        public bool vibrate;
        public Status flashStatus;
    }



    public class Interior_Public_Classes : MonoBehaviour
    {

	    // Use this for initialization
	    void Start () {
		
	    }
	
	    // Update is called once per frame
	    void Update () {
		
	    }


    }

}
