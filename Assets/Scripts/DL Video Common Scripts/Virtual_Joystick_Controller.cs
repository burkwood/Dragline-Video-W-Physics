﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using IntPublicClass;

public class Virtual_Joystick_Controller : MonoBehaviour
{
    //Virtual Joysticks
    //Game Objects
    private GameObject leftJoyHandle;
    private GameObject rightJoyHandle;

    //Color
    private Color highlightColor = new Color(212 / 255f, 208 / 255f, 15 / 255f);
    private Color leftJoyOriginalColor;
    private Color leftJoyNewColor;
    private Color rightjoyOriginalColor;
    private Color rightJoyNewColor;

    //Numerical Variables
    private Vector3 leftJoyPos;
    private Vector3 rightJoyPos;
    private float colorLerp;

    //Public Variables
    public float joyMoveSpeed;
    public float highlightDuration;
    public Status leftJoyHighlight;
    public hJoyMove leftJoyHor;
    public vJoyMove leftJoyVert;
    public Status rightJoyHighlight;
    public hJoyMove rightJoyHor;
    public vJoyMove rightJoyVert;
    // Use this for initialization
    void Start ()
    {
        leftJoyHandle = GameObject.FindWithTag("LeftJoyHandle");
        leftJoyOriginalColor = leftJoyHandle.GetComponent<Image>().color;
        rightJoyHandle = GameObject.FindWithTag("RightJoyHandle");
        rightjoyOriginalColor = rightJoyHandle.GetComponent<Image>().color;
    }
	
	// Update is called once per frame
	void Update ()
    {
        colorLerp = Mathf.PingPong(Time.time, highlightDuration) / highlightDuration;

        leftJoyPos = leftJoyHandle.transform.localPosition;
        rightJoyPos = rightJoyHandle.transform.localPosition;
        leftJoyHandle.transform.localPosition = Vector3.Lerp(leftJoyPos, new Vector3((float)leftJoyHor, (float)leftJoyVert, 0.0f), Time.deltaTime * joyMoveSpeed);
        rightJoyHandle.transform.localPosition = Vector3.Lerp(rightJoyPos, new Vector3((float)rightJoyHor, (float)rightJoyVert, 0.0f), Time.deltaTime * joyMoveSpeed);

        leftJoyNewColor = Color.Lerp(leftJoyOriginalColor, highlightColor, FlashStatus((int)leftJoyHighlight));
        leftJoyHandle.GetComponent<Image>().color = leftJoyNewColor;
        rightJoyNewColor = Color.Lerp(rightjoyOriginalColor, highlightColor, FlashStatus((int)rightJoyHighlight));
        rightJoyHandle.GetComponent<Image>().color = rightJoyNewColor;
    }

    //********************************************************************************
    //FUNCTION SECTION
    float FlashStatus(int status)
    {
        //This function will take the value of the enum "Status" cast as an integer as an input, 0 for off, 1 for flashing, 2 for solid color
        //The returned value should be sent to highlighting."ComponentName".highlight
        //THis replaces the following logic
        //**********Sample Code Start*******************
        //		if (highlighting.aFrameLegs.flash == true && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = colorLerp;}//If flash is enabled set the highlight transition to colorLerp
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == true) {highlighting.aFrameLegs.highlight = 1;}		//If Solid is enabled set to solid highlight color
        //		if (highlighting.aFrameLegs.flash == false && highlighting.aFrameLegs.solid == false) {highlighting.aFrameLegs.highlight = 0;}		//If both flash and solid are off return to original color
        //**********Sample Code End********************
        float finalStatus;
        switch (status)
        {
            case 2:
                finalStatus = 1;                                                                                                                        //If Status is "Solid"
                break;
            case 1:
                finalStatus = colorLerp;                                                                                                                //If Status is "Flash"
                break;
            case 0:
                finalStatus = 0;                                                                                                                        //If Status is "Off"
                break;
            default:
                finalStatus = 0;                                                                                                                        //Default case
                break;
        }//Switch End
        return finalStatus;
    }//FlashStatus End
}
