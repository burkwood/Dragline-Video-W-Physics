﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class M_BnR_Game_Controller : MonoBehaviour 
{
	public int queue = 0;																										//The index of text queues
	public bool nextStage = false;																								//a marker used to synch the camera movement with the audio, text display and SFX. 
	//This script ONLY sets it to TRUE, the Camera controller will set it to False
	public AudioClip[] audio;																									//Audio Player
	Marion_DL_SFX DL_SFX;																										//The script that controls all hiding and highlighting
	public float startTime;
	public float delayTime = 2;
	public float playTime;
	public Text captionText;
	private float[] delayTimes = new float[] {3,5,5,3};																			//Corresponds to the Hold + Blend times of the camera moves
	private AudioSource playAudio;
	private bool audioTriggered = false;																						//Used to flag if the Audio Clip has been played
	private string[] captions = new string[] {
		
		"The dragline bucket is the tool that moves the dirt from one location to another.",									//Cap [0]

		"It is a welded steel, open faced structure, meant to resist the wear of being dragged across the ground.",				//Cap [1]

		"The bucket has several components:",																					//Cap [2]

		"The drag ropes, which move the bucket in and out.",																	//Cap [3]

		"The hoist ropes, which move the bucket up and down.",																	//Cap [4]

		"The hoist chains, which are connected to the hoist ropes.",															//Cap [5]

		"The drag chains, which are connected to the drag ropes.",																//Cap [6]

		"The dump chains, which connect the dump rope to the drag ropes.",														//Cap [7]

		"The dump rope, which is attached to the dump chains and the center of the bucket arch.",								//Cap [8]

		"The dump rope runs through the dump block.",																			//Cap [9]

		"This allows the bucket to pivot and dump when the drag chains are slacked.",											//Cap [10]

		"The clevis, which connects the drag chains to the bucket.",															//Cap [11]

		"The trunnion, which connects the hoist chains to the bucket.",															//Cap [12]

		"The spreader bar ensures the hoist chains remain separated during the dig phase.",										//Cap [13]

		"This prevents entanglement and allows for an evenly lowered bucket.",													//Cap [14]

		"The bucket teeth engage with the material while digging.",																//Cap [15]

		"These components are regularly replaced due to the high wear and breakage during digging."								//Cap [16]

	};

	// Use this for initialization
	void Start () 
	{
		DL_SFX = GameObject.FindWithTag ("GameController").GetComponent<Marion_DL_SFX> ();										//Get reference to the SFX script
		playAudio = GetComponent<AudioSource> ();																				//Get Reference to the Audio Source
		playAudio.clip = audio [0]; 																							//Load the first Audio Clip
		captionText.text = "";																									//Clear the Captions
	}

	
	// Update is called once per frame
	void Update () 
	{
		UpdatePlayTime ();

		if (Time.time < delayTime) 
		{
			//DL_SFX.showHide.aFrame = false;																					//Hide A-frame Legs
		}
//******************************************************************************************************************************
		//Start the first Queue 
		//Camera moves from Home to Whole Mast Left
		if (queue == 0 && nextStage == false && Time.time > delayTime)																//START Queue 1 
		{ 																
			nextStage = true;																										//Trigger camera movement
			queue = 1;																												//Update the queue number
			startTime = Time.time;																									//Store when the Camera Movement was triggered
		}

		//Caption & SFX Manager during the first Queue
		//"The dragline bucket is the tool that moves the dirt from one location to another." Cap [0]
		//No Highlight
		if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes [0]) 											//Start Playing Audio after camera has stopped moving
		{										 
			if (audioTriggered == false) 
			{
				playAudio.Play ();																									//Play loaded clip
				audioTriggered = true;																								//Flag audio clip has been triggered
				startTime = Time.time;																								//Store when the clip started playing
			}

			if (playAudio.isPlaying == true) {captionText.text = captions [0];}														//If audio clip is playing, show captions
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 1) 
			{
				captionText.text = "";																								//Don't clear caption till 2 Seconds after Audio ends
			}			
		}

//******************************************************************************************************************************
		// Start the Second Queue 
		//No Camera Movement
		if (queue ==1 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 2
		{
			playAudio.clip = audio [1];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 2;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the second Queue
		//"It is a welded steel, open faced structure, meant to resist the wear of being dragged across the ground." Cap [1]
		if (queue == 2) 
		{
			if (playAudio.isPlaying == true ) {captionText.text = captions [1];}													//If audio clip is playing, show captions
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) {captionText.text = "";}			//Don't clear caption till 2 Seconds after Audio ends 

		}


//******************************************************************************************************************************
		// Start the Third Queue 
		//No Camera Movement
		if (queue ==2 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 3
		{
			playAudio.clip = audio [2];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 3;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Third Queue
		//"The bucket has several components:" Cap [2]
		if (queue == 3) 
		{
			if (playAudio.isPlaying == true ) {captionText.text = captions [2];}													//If audio clip is playing, show captions
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) {captionText.text = "";}			//Don't clear caption till 2 Seconds after Audio ends 
		}


//******************************************************************************************************************************
		// Start the Fourth Queue
		//No Camera Movement
		if (queue == 3 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [3];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 4;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
		}

		//Caption  & SFX Manager during the Fourth stage
		//"The drag ropes, which move the bucket in and out." Cap [3]
		//Highlight drag ropes till the end
		if (queue == 4) 
		{
			if (playAudio.isPlaying == true ) {captionText.text = captions [3];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime>0.2) 																		//Delay highlighting the chords
			{
				DL_SFX.highlighting.dragRope.status = Status.Flash;																	//Highlight Drag Ropes
			}
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.dragRope.status = Status.Off;																	//Un-highlight Drag Ropes
			}			
		}


//******************************************************************************************************************************
		// Start the Fifth Queue
		//No Camera Movement
		if (queue == 4 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [4];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 5;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
		}

		//Caption  & SFX Manager during the Fourth stage
		//"The hoist ropes, which move the bucket up and down." Cap [4]
		//Highlight hoist ropes till the end
		if (queue == 5) 
		{
			if (playAudio.isPlaying == true ) {captionText.text = captions [4];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime>0.2) 																		//Delay highlighting the chords
			{
				DL_SFX.highlighting.hoistRope.status = Status.Flash;																//Highlight Drag Ropes
			}
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.hoistRope.status = Status.Off;																	//Un-highlight Drag Ropes

			}			
		}

//******************************************************************************************************************************
		// Start the Sixth Queue
		//Camera moves from Whole Boom Left to Bucket Closeup
		if (queue==5 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			nextStage = true;																										//trigger next camera movement
			playAudio.clip = audio [5];																								//change clip
			queue = 6;																												//Update the queue number
			startTime = Time.time;																									//Store when the Camera Movement was triggered
		}
		//*********************************************************************************************************************************
		//Caption  & SFX Manager during the Sixth stage
		//"The hoist chains, which are connected to the hoist ropes." Cap [5]
		//Highlight Hoist Chains Till the end
		if (queue == 6 && nextStage == false && Time.time > startTime + delayTimes[1]) 
		{
			if (playAudio.isPlaying == true) {captionText.text = captions [5];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) {DL_SFX.highlighting.hoistChain.status = Status.Flash;}				//Highlight Hoist Chains
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.hoistChain.status = Status.Off;																	//Un-highlight Hoist Chains
			}			
		}




//******************************************************************************************************************************
		// Start the Seventh Queue
		//No Camera Movement
		if (queue == 6 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [6];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 7;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Seventh stage
		//"The drag chains, which are connected to the drag ropes." Cap [6]
		//Highlight Drag Chains Till the end
		if (queue == 7) 
		{
			if (playAudio.isPlaying == true) {captionText.text = captions [6];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) {DL_SFX.highlighting.dragChain.status = Status.Flash;}				//Highlight Drag Chains
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.dragChain.status = Status.Off;																	//Un-highlight Drag Chains
			}			
		}


//******************************************************************************************************************************
		// Start the Eighth Queue
		//No Camera Movement
		if (queue == 7 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [7];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 8;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Seventh stage
		//"The dump chains, which connect the dump rope to the drag ropes." Cap [15]
		//Highlight Dump Chains Till the end
		if (queue == 8) 
		{
			if (playAudio.isPlaying == true) {captionText.text = captions [7];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) {DL_SFX.highlighting.dumpChain.status = Status.Flash;}				//Highlight Dump Chains
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.dumpChain.status = Status.Off;																	//Un-highlight Dump Chains
			}			
		}

//******************************************************************************************************************************
		// Start the Nineth Queue
		//No Camera Movement
		if (queue == 8 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [8];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 9;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Ninth stage
		//"The dump rope, which is attached to the dump chains and the center of the bucket arch." Cap [5]
		//Highlight Dump Rope
		if (queue == 9)																 												//Start playing audio after camera has stopped moving
		{
			if (audioTriggered == false) 
			{
				playAudio.Play ();																									//Play loaded clip
				audioTriggered = true;																								//Flag audio clip has been triggered
			}
			if (playAudio.isPlaying == true ) {captionText.text = captions [8];}													//If audio clip is playing, show captions
			if(playAudio.isPlaying == true && playTime > 0.2)
			{							
				DL_SFX.highlighting.dumpRope.status = Status.Flash;																	//Highlight Dump Rope
			}
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes[1]) {captionText.text = "";}			//Don't clear caption till 2 Seconds after Audio ends
		}

//******************************************************************************************************************************
		// Start the Tenth Queue
		//No Camera Movement
		if (queue == 9 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [9];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 10;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Tenth stage
		//"The dump rope runs through the dump block."			 Cap [9]
		//"This allows the bucket to pivot and dump when the drag chains are slacked."				Cap [10]
		//Highlight Dump Rope till the end
		if (queue == 10) 
		{
			if (playAudio.isPlaying == true && playTime < 4.5 ) {captionText.text = captions [9];} 									//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 1.5) 
			{
				DL_SFX.highlighting.dumpBlock.status = Status.Flash;																//Highlight Dump Block
				DL_SFX.highlighting.dumpRope.status = Status.Off;																	//Un-highlight Dump Ropes

			}
			if (playAudio.isPlaying == true && playTime > 4.5) {captionText.text = captions [10];}
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.dumpBlock.status = Status.Off;																	//Un-highlight Dump Block

			}			
		}

//******************************************************************************************************************************
		// Start the Eleventh Queue
		//No Camera Movement
		if (queue == 10 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [10];																							//change clip
			playAudio.Play ();																										//play clip
			queue = 11;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Eleventh stage
		//"The clevis, which connects the drag chains to the bucket." Cap [11]
		//Highlight Dump Rope till the end
		if (queue == 11) 
		{
			if (playAudio.isPlaying == true ) {captionText.text = captions [11];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) 
			{
				DL_SFX.highlighting.clevis.status = Status.Flash;																	//Highlight clevis
			}						
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.clevis.status = Status.Off;																		//Un-highlight Clevis

			}			
		}

//******************************************************************************************************************************
		// Start the Twelfth Queue
		//No Camera Movement
		if (queue == 11 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [11];																							//change clip
			playAudio.Play ();																										//play clip
			queue = 12;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Twelfth stage
		//"The trunnion, which connects the hoist chains to the bucket." Cap [12]
		//Highlight Trunnion till the end
		if (queue == 12) 
		{
			if (playAudio.isPlaying == true ) {captionText.text = captions [12];} 													//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) 
			{
				DL_SFX.highlighting.trunion.status = Status.Flash;																	//Highlight Trunnion
			}						
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.trunion.status = Status.Off;																	//Un-highlight Trunnion

			}			
		}

//******************************************************************************************************************************
		// Start the Thirteenth Queue
		//No Camera Movement
		if (queue == 12 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [12];																							//change clip
			playAudio.Play ();																										//play clip
			queue = 13;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Thirteenth stage
		//"The spreader bar ensures the hoist chains remain seperated during the dig phase." Cap [13]
		//"This prevents entanglement and allows for an evenly lowered bucket." Cap [14]
		//Highlight Spreader Bar Till the end
		if (queue == 13) 
		{
			if (playAudio.isPlaying == true  && playTime < 6.9 ) {captionText.text = captions [13];} 								//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) {DL_SFX.highlighting.spreaderBar.status = Status.Flash;}				//Highlight Spreader Bar
			if (playAudio.isPlaying == true  && playTime > 6.9 ) {captionText.text = captions [14];} 
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.spreaderBar.status = Status.Off;																//Un-highlight Spreader Bar
			}			
		}

//******************************************************************************************************************************
		// Start the Fourteenth Queue
		//No Camera Movement
		if (queue == 13 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			playAudio.clip = audio [13];																							//change clip
			playAudio.Play ();																										//play clip
			queue = 14;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Fourteenth stage
		//"The bucket teeth engage with the material while digging." Cap [15]
		//"These components are regularly replaced due to the high wear and breakage during digging." Cap [16]
		//Highlight Bucket Teeth Till the end
		if (queue == 14) 
		{
			if (playAudio.isPlaying == true  && playTime < 5.5 ) {captionText.text = captions [15];} 								//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 0.2) {DL_SFX.highlighting.bucketTeeth.status = Status.Flash;}				//Highlight Bucket Teeth
			if (playAudio.isPlaying == true  && playTime > 5.5 ) {captionText.text = captions [16];} 
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 									//Don't clear caption till 2 Seconds after Audio ends
			{
				captionText.text = "";
				DL_SFX.highlighting.bucketTeeth.status = Status.Off;																//Un-highlight Bucket Teeth
			}			
		}

//******************************************************************************************************************************	
		//Start the Fifteenth Queue
		//Camera Moves from Bucket Closeup to Whole Boom Left
		if (queue == 14 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") 
		{
			queue = 15;																												//Update Queue number
			startTime = Time.time;																									//Store when Camera Movement was triggered
			nextStage = true;																										//Trigger camera movement
		}

		//No Captions or SFX

//******************************************************************************************************************************	
		//Start the Sixteenth Queue
		//Camera Moves from Whole Mast Left to Home Position
		if (queue == 15 && nextStage == false && Time.time > startTime + delayTimes[2] && captionText.text == "") 
		{
			queue = 16;																												//Update Queue number
			startTime = Time.time;																									//Store when Camera Movement was triggered
			nextStage = true;																										//Trigger camera movement
		}

		//No Captions or SFX


	}// Update End

//******************************************************************************************************************************
//Function Section
	//Auto Update or reset play time
	void UpdatePlayTime()
	{
		if (playAudio.isPlaying)
		{
			playTime = playAudio.time;
		} 
		else 
		{
			playTime = 0;
		}
	}

}//Monobehavious End
