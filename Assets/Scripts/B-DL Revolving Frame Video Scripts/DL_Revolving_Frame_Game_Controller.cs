﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class DL_Revolving_Frame_Game_Controller : MonoBehaviour 
{
	public int queue = 0;																										//The index of text queues
	public bool nextStage = false;																								//a marker used to synch the camera movement with the audio, text display and SFX. 
	//This script ONLY sets it to TRUE, the Camera controller will set it to False
	public AudioClip[] audio;																									//Audio Player
	Bucyrus_DL_SFX DL_SFX;																										//The script that controls all hiding and highlighting
	public float startTime;
	public float delayTime = 2;
	public float playTime;
	public Text captionText;
	private float[] delayTimes = new float[] {3,5,5,3};																			//Corresponds to the Hold + Blend times of the camera moves
	private AudioSource playAudio;
	private bool audioTriggered = false;																						//Used to flag if the Audio Clip has been played
	private string[] captions = new string[] {

		"The revolving frame is the backbone of the dragline.",																	//Cap [0]

		"It supports all of the mechanical and secondary systems.",																//Cap [1]

		"These include the operator cab, the electrical room and the machine room.",											//Cap [2]

		"The revolving frame rests on the roller circle, which is a set of machined flat upper rails.",							//Cap [3]

		"The revolving frame is secured to the tub via a set of lifting hooks.",												//Cap [4]

		"The revolving frame rotates around the center pin."																	//Cap [5]
	};
	// Use this for initialization
	void Start () 
	{
		DL_SFX = GameObject.FindWithTag ("GameController").GetComponent<Bucyrus_DL_SFX> ();											//Get reference to the SFX script
		playAudio = GetComponent<AudioSource> ();																					//Get Reference to the Audio Source
		playAudio.clip = audio [0]; 																								//Load the first Audio Clip
		captionText.text = "";																										//Clear the Captions
		DL_SFX.showHide.aFrameLegs = false;																							//Hide A-Frame Legs
		DL_SFX.showHide.aFrameLugs = false;																							//Hide A-Frame Lugs
		DL_SFX.showHide.revFrameFloor = false;																						//Hide Revolving Frame Floor

	}//Start End
	
	// Update is called once per frame
	void Update () 
	{

		UpdatePlayTime ();

		//******************************************************************************************************************************
		//Start the first Queue 
		//Camera moves from Home to Revolving Frame Left
		if (queue == 0 && nextStage == false && Time.time > delayTime)																//START Queue 1 
		{ 																
			nextStage = true;																										//Trigger camera movement
			queue = 1;																												//Update the queue number
			startTime = Time.time;																									//Store when the Camera Movement was triggered
		}

		//Caption & SFX Manager during the first Queue
		//"The revolving frame is the backbone of the dragline." 															Cap [0]
		//"It supports all of the mechanical and secondary systems.",														Cap [1]
		//"These include the operator cab, the electrical room and the machine room." 										Cap [2]
		//Highlight Revolving Frame Till end
		if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes[0]) 
		{
			DL_SFX.fadeManager.hoistRopes.enabled = true;																			//Fade Hoist Ropes
			DL_SFX.fadeManager.houseCab.enabled = true;																				//Fade House Cab
			DL_SFX.fadeManager.houseLights.enabled = true;																			//Fade House Lights
			DL_SFX.fadeManager.housePlatform.enabled = true;																		//Fade House Platform
			DL_SFX.fadeManager.houseBody.enabled = true;																			//Fade House Body
		}

		if (queue == 1 && nextStage == false && Time.time > startTime + delayTimes [0] + 3) 										//Start Playing Audio after camera has stopped moving
		{										 																					//And the Fade is Completed	
			if (audioTriggered == false) 
			{
				playAudio.Play ();																									//Play loaded clip
				audioTriggered = true;																								//Flag audio clip has been triggered
			}
			if (playAudio.isPlaying == true && playTime > 0.1f) {DL_SFX.highlighting.revolvingFrame.status = Status.Flash;}			//Highl;ight Revolving Frame
			if (playAudio.isPlaying == true && playTime < 5.3) {captionText.text = captions [0];}									//If audio clip is playing, show captions
			if (playAudio.isPlaying == true && playTime > 5.3 && playTime < 10.9) {captionText.text = captions [1];}				
			if (playAudio.isPlaying == true && playTime > 10.9) {captionText.text = captions [2];}									
			if (!playAudio.isPlaying && Time.time > startTime + delayTimes[0] + 3 + playAudio.clip.length + delayTime + 1) 
			{
				captionText.text = "";																								//Don't clear caption till 2 Seconds after Audio ends
				DL_SFX.highlighting.revolvingFrame.status = Status.Off;																//Un-highlight Revolving Frame
			}			
		}

		//******************************************************************************************************************************
		// Start the Second Queue 
		//Revolving Frame Left to Roller Circle Closeup
		if (queue == 1 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 3
		{
			nextStage = true;
			playAudio.clip = audio [1];																								//change clip
			queue = 2;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the third Queue
		//"The revolving frame rests on the roller circle, which is a set of machined flat upper rails." Cap [3]
		//Highlight Upper roller rail till end
		if (queue == 2 && nextStage == false && Time.time > startTime + delayTimes[1]) 
		{
			DL_SFX.fadeManager.revFrame.enabled = true;																				//Fade Rev Frame
		}

		if (queue == 2 && nextStage == false && Time.time > startTime + delayTimes [1] + 3) 										//Start when camera stops moving
		{																															//And fading is complete
			if (audioTriggered == false) 
			{
				playAudio.Play ();
				audioTriggered = true;
			}

			if (playAudio.isPlaying == true ) 
			{
				captionText.text = captions [3];																					//If audio clip is playing, show captions
//				DL_SFX.showHide.revFrame = false;																					//Hide Rev Frame
			}																									
			if (playAudio.isPlaying == true && playTime > 1.7) 
			{
				DL_SFX.highlighting.rollerRailUpper.status = Status.Flash;															//Flash Upper Roller Rail
			}  				
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes [1] + 1 + 3) 
			{
				captionText.text = "";																								//Don't clear caption till 2 Seconds after Audio ends
				DL_SFX.highlighting.rollerRailUpper.status = Status.Off;															//Un-highlight Upper Roller Rail
			}			 

		}

		//******************************************************************************************************************************
		// Start the Third Queue 
		//No Camera Movement
		if (queue == 2 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + delayTimes [1] + 1 && captionText.text == "") //Start Queue 4
		{
			playAudio.clip = audio [2];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 3;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the Third Queue
		//"The revolving frame is secured to the tub via a set of lifting hooks." Cap [4]
		//Highlight Lifting Hooks till end
		if (queue == 3) 
		{
			if (playAudio.isPlaying == true && playTime > 3.0) {DL_SFX.highlighting.liftingHooks.status = Status.Flash;}			//Highlight Lifting Hooks
			if (playAudio.isPlaying == true ) {captionText.text = captions [4];}													//If audio clip is playing, show captions												
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime) 
			{
				captionText.text = "";																								//Don't clear caption till 2 Seconds after Audio ends
				DL_SFX.highlighting.liftingHooks.status = Status.Off;																//Un-highlight Lifting Hooks
			}			 

		}

		//******************************************************************************************************************************
		// Start the Fourth Queue 
		//No Camera Movement
		if (queue == 3 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 2 && captionText.text == "") //Start Queue 4
		{
			playAudio.clip = audio [3];																								//change clip
			playAudio.Play ();																										//play clip
			queue = 4;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the fourth Queue
		//"The revolving frame rotates aroud the center pin." Cap [5]
		//Highlight Center Pin Till end
		//Fade House cab, house body, house lights & house platform till end
		if (queue == 4) 
		{
			if (playAudio.isPlaying == true && playTime > 2.1) 
			{
				DL_SFX.highlighting.centerPin.status = Status.Flash;																//Highlight Center Pin

			}						
			if (playAudio.isPlaying == true ) {captionText.text = captions [5];}													//If audio clip is playing, show captions												
			if (!playAudio.isPlaying && Time.time > startTime + playAudio.clip.length + delayTime + 2) 
			{
				captionText.text = "";																								//Don't clear caption till 2 Seconds after Audio ends
				DL_SFX.highlighting.centerPin.status = Status.Off;																	//Un-highlight Center Pin
			}			 

		}

		//******************************************************************************************************************************
		// Start the Fifth Queue 
		//Camera moves from Roller circle closeup to Revolving frame left.
		if (queue == 4 && nextStage == false && Time.time > startTime + playAudio.clip.length + delayTime + 1 && captionText.text == "") //Start Queue 5
		{
			nextStage = true;
			queue = 5;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;

		}

		//Caption  & SFX Manager during the fifth Queue
		//No SFX
		if (queue == 5 && nextStage == false && Time.time > startTime + delayTimes [2]) 
		{
			if (playAudio.isPlaying == false ) {captionText.text="";}																//Blank Captions.
			DL_SFX.fadeManager.hoistRopes.enabled = false;																			//Un-fade Hoist Ropes
			DL_SFX.fadeManager.houseCab.enabled = false;																			//Un-fade House Cab
			DL_SFX.fadeManager.houseLights.enabled = false;																			//Un-fade House Lights
			DL_SFX.fadeManager.housePlatform.enabled = false;																		//Un-fade House Platform
			DL_SFX.fadeManager.houseBody.enabled = false;																			//Un-fade House Body
			DL_SFX.fadeManager.revFrame.enabled = false;																			//Un-fade Rev Frame
		}

		//******************************************************************************************************************************
		// Start the Sixth Queue 
		//Camera moves from Revolving frame left to Home.
		if (queue == 5 && nextStage == false && Time.time > startTime + delayTimes [2]  + delayTime + 3 && captionText.text == "") 	//Start Queue 6
		{
			nextStage = true;
			queue = 6;																												//Update the queue number
			startTime = Time.time;																									//Store when the clip started playing
			audioTriggered = false;
		}

		//Caption  & SFX Manager during the sixth Queue
		//No SFX
		if (queue == 6) 
		{
			if (playAudio.isPlaying == true ) {captionText.text="";}																//Blank Captions.
		}


	}//Update End

//******************************************************************************************************************************
	//Function Section
	//Auto Update or reset play time
	void UpdatePlayTime()
	{
		if (playAudio.isPlaying)
		{
			playTime = playAudio.time;
		} 
		else 
		{
			playTime = 0;
		}
	}



}//MonoBehavious End
