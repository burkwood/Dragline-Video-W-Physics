﻿using UnityEngine;
using System.Collections;

public class lightPulse : MonoBehaviour {
	public Light pulseLight;
	public float min, max, speed;

	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

	void FixedUpdate(){
		pulseLight.range = ((max-min)/2f) * Mathf.Sin (speed*Time.realtimeSinceStartup) + (min + max)/2f;
	}
}
