using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Threading;

public class splineRendererArray : MonoBehaviour {
	public bool active;
	public int numberOfSides;
	public float diameter;
	public Material mat;
	public int textureRep;
	public int pointsNumb;
	public bool createdMesh = false;
	public int startInd;
	public Vector3[] points;
	public bool colliderOn;

	private Vector3[] vertices;
	private Mesh mymesh;

	public float compTime;
	public float waitTime;
	
	void Start () {
		initaliseMesh ();
		pointsNumb = points.Length;
		if (pointsNumb > 3) {
			createMesh ();
		}
	}
	
	void Update () {
		if (pointsNumb < 3) {
			pointsNumb = points.Length;
		}
		if ((pointsNumb > 3) && (!createdMesh)){
			pointsNumb = points.Length;
			createMesh();
			createdMesh = true;
		}
	}

	Vector3[] createVertices(){
		Vector3[] vertices = new Vector3[points.Length * numberOfSides + 2];

		// General factors
		float Dtheta = (2f * Mathf.PI) / ((float)numberOfSides);
		vertices[0] = points [0];
		vertices[vertices.GetLength(0)-1] = points [points.Length-1];
		Quaternion rot;

		// Create start cap
		Vector3 norm = Vector3.Normalize (points [1] - points [0]);
		Vector3 p = Vector3.Normalize (Vector3.Cross (Vector3.up, norm));
		Vector3 pOld = p;
		Vector3 P0 = points [0] +  p * (diameter / 2f);
		for (int i = 0; i < numberOfSides; i++){
			rot = Quaternion.AngleAxis (((float)i) * Dtheta * Mathf.Rad2Deg, norm);
			vertices[i+1] = points [0] +  rot*p * (diameter / 2f);
		}
		Vector3 normOld = norm;

		// Middle Loops
		Vector3 norm0, norm1;
		for (int j = 0; j < points.Length-2; j++) {
			norm0 = Vector3.Normalize (points [j + 1] - points [j]);
			norm1 = Vector3.Normalize (points [j + 2] - points [j + 1]);
			norm = (norm0 + norm1)/2f;
			p = Vector3.Normalize (Vector3.Cross (Vector3.Cross (normOld, pOld), norm));

			P0 = points [j+1] +  p * (diameter / 2f);
			for (int i = 0; i < numberOfSides; i++){
				rot = Quaternion.AngleAxis (((float)i) * Dtheta * Mathf.Rad2Deg, norm);
				vertices[1 + (j+1) * numberOfSides + i] = points [j+1] +  rot*p * (diameter / 2f);
			}
			pOld = p;
			normOld = norm;
		}

		// Create end cap
		norm = Vector3.Normalize (points [points.Length-1] - points [points.Length-2]);
		p = Vector3.Normalize (Vector3.Cross (Vector3.Cross (normOld, pOld), norm));
		P0 = points [points.Length-1] +  p * (diameter / 2f);
		for (int i = 0; i < numberOfSides; i++){
			rot = Quaternion.AngleAxis (((float)i) * Dtheta * Mathf.Rad2Deg, norm);
			vertices[1 + (points.Length-1) * numberOfSides + i] = points [points.Length-1] +  rot*p * (diameter / 2f);
		}
		return vertices;
	}
		

	int[] createTriangles(){
		int[] triangles = new int[(numberOfSides * (points.Length-1) * 2 + 2 * numberOfSides) * 3];
		int ind = 0;

		// create start cap
		for (int i = 0; i < numberOfSides - 1; i++) {
			triangles [ind] = 0;
			triangles [ind + 1] = 2 + i;
			triangles [ind + 2] = 1 + i;
			ind += 3;
		}
		triangles [ind] = 0;
		triangles [ind + 1] = 1;
		triangles [ind + 2] = 2 + numberOfSides - 2;
		ind += 3;

		// Middle Loops
		for (int j = 0; j < points.Length-1; j++) {
			for (int i = 0; i < numberOfSides - 1; i++) {
				triangles [ind] = j * numberOfSides + i + 1;
				triangles [ind + 1] = (j+1) * numberOfSides + i + 2;
				triangles [ind + 2] = (j+1) * numberOfSides + i + 1;
				triangles [ind + 3] = j * numberOfSides + i + 1;
				triangles [ind + 4] = j * numberOfSides + i + 2;
				triangles [ind + 5] = (j+1) * numberOfSides + i + 2;
				ind += 6;
			}
			triangles [ind] = (j+2) * numberOfSides;
			triangles [ind + 1] = (j+1) * numberOfSides;
			triangles [ind + 2] = (j+1) * numberOfSides + 1;
			triangles [ind + 3] = (j+1) * numberOfSides + 1;
			triangles [ind + 4] = (j+1) * numberOfSides;
			triangles [ind + 5] = j * numberOfSides + 1;
			ind += 6;
		}

		// create end cap
		for (int i = 0; i < numberOfSides - 1; i++) {
			triangles [ind] = points.Length * numberOfSides + 1;
			triangles [ind + 1] =  points.Length * numberOfSides - 1 - i;
			triangles [ind + 2] = points.Length * numberOfSides - i;
			ind += 3;
		}
		triangles [ind] = points.Length * numberOfSides + 1;
		triangles [ind + 1] = points.Length * numberOfSides;
		triangles [ind + 2] = points.Length * numberOfSides - numberOfSides + 1;
		ind += 3;

		return triangles;
	}

	Vector2[] createUVs(){
		Vector2[] uvs = new Vector2[points.Length * numberOfSides + 2];

		// General params
		float Dtheta = (2f * Mathf.PI) / ((float)numberOfSides);

		// create start cap
		uvs [0] = new Vector2 (0.5f, 0.5f);

		// create side uvs
		int ind = 1;
		float dSide = 2f / ((float)numberOfSides-1f);
		for (int i = 0; i < points.Length; i++){
			for (int j = 0; j < numberOfSides; j++){
				uvs [ind] = new Vector2((((float) j)*dSide),((float)(i))/((float)textureRep));
				ind++;
			}
		}
		return uvs;
	}

	void initaliseMesh(){
		mymesh = new Mesh ();
		GameObject go = new GameObject();
		MeshFilter mf = new MeshFilter();
		go.transform.parent = this.transform;
		go.AddComponent(typeof(MeshFilter));
		go.AddComponent(typeof(MeshRenderer));
		go.GetComponent<MeshFilter>().mesh = mymesh;
		go.GetComponent<MeshRenderer>().material = mat;
		if (colliderOn) {
			go.AddComponent<MeshCollider> ();
		}
		mymesh.Clear ();
	}

	public void createMesh(){
		if (active) {
			mymesh.Clear ();
			if (points.Length > 2) {
				vertices = createVertices ();
				mymesh.vertices = vertices;
				mymesh.uv = createUVs ();
				mymesh.triangles = createTriangles ();
				
				mymesh.RecalculateBounds ();
				mymesh.RecalculateNormals ();
			}
		}
	}

	public void updateMesh(){
		if (active) {
			mymesh.vertices = createVertices ();
			mymesh.RecalculateBounds ();
			mymesh.RecalculateNormals ();
		}
	}

	public void resetChildren(){
		foreach (Transform child in transform){
			child.localPosition = Vector3.zero;
			child.localEulerAngles = Vector3.zero;
		}
	}
}
